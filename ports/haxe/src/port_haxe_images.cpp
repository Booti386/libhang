/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <hx/CFFI.h>

#include <libhang/core.h>

#include "kinds.h"

DEFINE_KIND(hx_lh_image_t_ptr)

extern "C" {
	static value hx_lhLoadImagePNG(value path) {
		lh_image_t *ret = lhLoadImagePNG(val_get_string(path));

		if(ret)
			return alloc_abstract(hx_lh_image_t_ptr, ret);

		return alloc_null();
	}

	static value hx_lhLoadImage(value path) {
		lh_image_t *ret = lhLoadImage(val_get_string(path));

		if(ret)
			return alloc_abstract(hx_lh_image_t_ptr, ret);

		return alloc_null();
	}
	
	static value hx_lhReleaseImage(value image) {
		lhReleaseImage((lh_image_t *)val_get_handle(image, hx_lh_image_t_ptr));

		return alloc_null();
	}
}

DEFINE_PRIM(hx_lhLoadImagePNG, 1)
DEFINE_PRIM(hx_lhLoadImage, 1)
DEFINE_PRIM(hx_lhReleaseImage, 1)

