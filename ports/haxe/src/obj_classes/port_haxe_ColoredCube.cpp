/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <hx/CFFI.h>

#include <libhang/core.h>
#include <libhang/obj_classes/ColoredCube.h>

#include "../kinds.h"

extern "C" {
	static value hx_lhRegColoredCube(value window) {
		return alloc_int(lhRegColoredCube((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr)));
	}

	static value hx_lhUnregColoredCube(value window) {
		lhUnregColoredCube((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhNewColoredCube(value *args, int count) {
		lh_vec3_t pos, angle;
		lh_rgba_t color;

		if(count != 6)
			return alloc_null();

		pos.x = val_field_numeric(args[2], val_id("x"));
		pos.y = val_field_numeric(args[2], val_id("y"));
		pos.z = val_field_numeric(args[2], val_id("z"));

		angle.x = val_field_numeric(args[3], val_id("x"));
		angle.y = val_field_numeric(args[3], val_id("y"));
		angle.z = val_field_numeric(args[3], val_id("z"));

		color.r = val_field_numeric(args[4], val_id("r"));
		color.g = val_field_numeric(args[4], val_id("g"));
		color.b = val_field_numeric(args[4], val_id("b"));
		color.a = val_field_numeric(args[4], val_id("a"));

		lh_obj_t *ret = lhNewColoredCube((lh_window_t *)val_get_handle(args[0], hx_lh_window_t_ptr), val_get_double(args[1]), &pos, &angle, &color, val_get_bool(args[5]) ? LH_TRUE : LH_FALSE);

		if(ret)
			return alloc_abstract(hx_lh_obj_t_ptr, ret);

		return alloc_null();
	}

	static value hx_lhDeleteColoredCube(value window, value surface) {
		lhDeleteColoredCube((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), (lh_obj_t *)val_get_handle(surface, hx_lh_obj_t_ptr));

		return alloc_null();
	}
}

DEFINE_PRIM(hx_lhRegColoredCube, 1)
DEFINE_PRIM(hx_lhUnregColoredCube, 1)
DEFINE_PRIM_MULT(hx_lhNewColoredCube)
DEFINE_PRIM(hx_lhDeleteColoredCube, 2)

