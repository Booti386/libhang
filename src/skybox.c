/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <libhang.h>

#include "core.h"
#include "skybox.h"

struct lh_skybox_ctx {
	struct lh_obj *obj;
	LHbool is_visible;
};

static struct lh_obj_class SkyBox_obj_class={
	.name="SkyBox",

	.is_visible = LH_TRUE,
	.priv_flags = LH_OBJ_CLASS_INTERNAL_FLAG,
	.uses = LH_BUF_POS_FLAG
			| LH_BUF_TEX_COORD_FLAG
			| LH_BUF_INDEX_FLAG
			| LH_TEX_FLAG,
	.overrides = 0,

	.shader_program = {
		.vertex_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"in vec3 a_VertexPosition; \n"
				"in vec2 a_TextureCoord; \n"
				"out vec2 v_TextureCoord; \n"
				" \n"
				"uniform mat4 u_ViewMatrix; \n"
				"uniform mat4 u_PMatrix; \n"
				" \n"
				"void main(void) { \n"
				"	gl_Position = u_PMatrix * mat4(mat3(u_ViewMatrix)) * vec4(a_VertexPosition, 1.0); \n"
				"	v_TextureCoord = a_TextureCoord; \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.fragment_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"out vec4 outColor; \n"
				"in vec2 v_TextureCoord; \n"
				"uniform sampler2D u_Sampler; \n"
				" \n"
				"void main(void) { \n"
				"	outColor = texture2D(u_Sampler, vec2(v_TextureCoord.s, v_TextureCoord.t)); \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.drawing_type = LH_DRAW_TRIANGLES,

		.nb_custom_uniforms = 1,
		.custom_uniforms = (struct lh_custom_uniform []){
			{ .name = "u_Sampler", .type = LH_TYPE_INT },
		},
	},
	.buffers = {
		[LH_BUF_POS] = {
			.nb_items = 24,
			.data = (float [3*24]){
				/* Front face */
				-1., -1., 1.,
				 1., -1., 1.,
				 1.,  1., 1.,
				-1.,  1., 1.,

				/* Right face */
				1., -1., -1.,
				1.,  1., -1.,
				1.,  1.,  1.,
				1., -1.,  1.,

				/* Back face */
				-1., -1., -1.,
				-1.,  1., -1.,
				 1.,  1., -1.,
				 1., -1., -1.,

				/* Left face */
				-1., -1., -1.,
				-1., -1.,  1.,
				-1.,  1.,  1.,
				-1.,  1., -1.,

				/* Top face */
				-1., 1., -1.,
				-1., 1.,  1.,
				 1., 1.,  1.,
				 1., 1., -1.,

				/* Bottom face */
				-1., -1., -1.,
				 1., -1., -1.,
				 1., -1.,  1.,
				-1., -1.,  1.,
			},
		},

		[LH_BUF_TEX_COORD] = {
			.nb_items = 24,
			.data = (float [2*24]){
				/* Front face */
				.0f,  .666666f,
				.25f, .666666f,
				.25f, .333333f,
				.0f,  .333333f,

				/* Right face */
				.5f,  .666666f,
				.5f,  .333333f,
				.25f, .333333f,
				.25f, .666666f,

				/* Back face */
				.75f, .666666f,
				.75f, .333333f,
				.5f,  .333333f,
				.5f,  .666666f,

				/* Left face */
				.75f, .666666f,
				1.f,  .666666f,
				1.f,  .333333f,
				.75f, .333333f,

				/* Top face */
				.0f,  .0f,
				.0f,  .333333f,
				.25f, .333333f,
				.25f, .0f,

				/* Bottom face */
				.25f, .666666f,
				.0f,  .666666f,
				.0f,  1.f,
				.25f, 1.f,
			},
		},

		[LH_BUF_INDEX] = {
			.nb_items = 36,
			.data = (int [1*36]){
				0, 1, 2,      0, 2, 3,    /* Front face  */
				4, 5, 6,      4, 6, 7,    /* Back face   */
				8, 9, 10,     8, 10, 11,  /* Top face    */
				12, 13, 14,   12, 14, 15, /* Bottom face */
				16, 17, 18,   16, 18, 19, /* Right face  */
				20, 21, 22,   20, 22, 23, /* Left face   */
			},
		},
	},

	.nb_textures = 1,
	.textures = (LHuint [1]){ LH_TEX_INVAL }
};

int lhSkyBoxInit(struct lh_window *window)
{
	struct lh_core_ctx *core = window->core;
	int ret = -1;

	core->skybox = malloc(sizeof(*core->skybox));
	if(!core->skybox)
		goto err_alloc;

	ret = lhObjClassReg(window, &SkyBox_obj_class);
	if(ret < 0)
		goto err_reg;
	core->skybox->obj = NULL;
	core->skybox->is_visible = LH_FALSE;
	return 0;

err_reg:
	free(core->skybox);
	core->skybox = NULL;
err_alloc:
	return ret;
}

int lhSkyBoxDeinit(struct lh_window *window)
{
	struct lh_core_ctx *core = window->core;

	if(core->skybox->obj)
		lhSkyBoxDestroy(window);
	lhObjClassUnreg(window, &SkyBox_obj_class);
	free(core->skybox);
	core->skybox = NULL;
	return 0;
}

struct lh_obj_class *lhSkyBoxGetObjClass(struct lh_window *window)
{
	lh_mark_used(window);

	return &SkyBox_obj_class;
}

int lhSkyBoxCreate(struct lh_window *window, LHuint texture, LHbool is_visible)
{
	struct lh_core_ctx *core = window->core;

	if(core->skybox->obj)
		return 0;
	core->skybox->obj = malloc(sizeof(*core->skybox->obj));
	if(!core->skybox->obj)
		return -1;

	core->skybox->obj->custom_uniforms_data = malloc(sizeof(*core->skybox->obj->custom_uniforms_data) * SkyBox_obj_class.shader_program.nb_custom_uniforms);
	if(!core->skybox->obj->custom_uniforms_data) {
		free(core->skybox->obj);
		core->skybox->obj = NULL;
		return -1;
	}

	lhGetCameraPos(window, &core->skybox->obj->pos);
	lh_quat_identity(&core->skybox->obj->angle);
	core->skybox->obj->is_visible = is_visible;
	core->skybox->obj->culling_enabled = LH_FALSE;
	core->skybox->obj->reversed_culling = LH_FALSE;
	core->skybox->obj->depth_test_enabled = LH_FALSE;

	core->skybox->obj->custom_uniforms_data[0].as_int = 0;

	lhSkyBoxSetTexture(window, texture);

	lhObjReg(window, &SkyBox_obj_class, core->skybox->obj);

	return 0;
}

void lhSkyBoxDestroy(struct lh_window *window)
{
	struct lh_core_ctx *core = window->core;

	if(!core->skybox->obj)
		return;
	lhObjUnreg(window, core->skybox->obj);

	free(core->skybox->obj->custom_uniforms_data);
	free(core->skybox->obj);
	core->skybox->obj = NULL;
}

int lhSkyBoxSetVisible(struct lh_window *window, LHbool is_visible)
{
	struct lh_core_ctx *core = window->core;

	core->skybox->obj->is_visible = is_visible;
	return 0;
}

void lhSkyBoxSetTexture(struct lh_window *window, LHuint texture)
{
	lh_mark_used(window);

	SkyBox_obj_class.textures[0] = texture;
}
