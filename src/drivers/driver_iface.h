/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_DRIVER_IFACE_H
#define LIBHANG_INTERNAL_DRIVER_IFACE_H 1

#include <libhang.h>

/* For Windows */
#undef CreateWindow

typedef struct {
	const char  *Name;

	int        (*Init)(void);
	int        (*PreDeinit)(void);
	int        (*Deinit)(void);
} ILHDriver;

typedef struct {
	ILHDriver                 base;

	enum lh_driver_os_caps  (*GetCaps)(void);

	void                   *(*MallocAligned)(size_t, size_t);
	void                    (*Free)(void *);

	void                    (*Delay)(LHuint);
	void                    (*DelayUSec)(LHuint);

	long                    (*AtomicCounterGet)(struct lh_atomic_counter *);
	long                    (*AtomicCounterSet)(struct lh_atomic_counter *, long);
	long                    (*AtomicCounterInc)(struct lh_atomic_counter *);
	long                    (*AtomicCounterDec)(struct lh_atomic_counter *);

	void                   *(*AtomicPointerGet)(struct lh_atomic_pointer *);
	void                   *(*AtomicPointerSet)(struct lh_atomic_pointer *, void *);

	struct lh_thread       *(*ThreadCreate)(int (*)(void *), void *);
	int                     (*ThreadWait)(struct lh_thread *, int *status);
	LHbool                  (*ThreadIsRunning)(struct lh_thread *);
	int                     (*ThreadRelease)(struct lh_thread *);
	int                     (*ThreadDestroy)(struct lh_thread *);

	struct lh_spinlock     *(*SpinCreate)(void);
	int                     (*SpinLock)(struct lh_spinlock *);
	int                     (*SpinUnlock)(struct lh_spinlock *);
	int                     (*SpinDestroy)(struct lh_spinlock *);

	struct lh_mutex        *(*MutexCreate)(void);
	int                     (*MutexLock)(struct lh_mutex *);
	int                     (*MutexUnlock)(struct lh_mutex *);
	int                     (*MutexDestroy)(struct lh_mutex *);

	struct lh_cond         *(*CondCreate)(void);
	int                     (*CondWait)(struct lh_cond *);
	int                     (*CondWakeSingle)(struct lh_cond *);
	int                     (*CondWakeAll)(struct lh_cond *);
	int                     (*CondDestroy)(struct lh_cond *);
} ILHOSDriver;

typedef struct {
	ILHDriver             base;

	void                (*AcquireGraphicCtx)(struct lh_window *);
	void                (*ReleaseGraphicCtx)(struct lh_window *);
	struct lh_window   *(*CreateWindow)(const char *, int, int, LHuint, LHuint, int, LHbool);
	int                 (*StartDrawing)(struct lh_window *);
	int                 (*StopDrawing)(struct lh_window *);
	int                 (*SetListeningEvents)(struct lh_window *, LHbool);
	int                 (*EnterEventLoop)(void);
	int                 (*LeaveEventLoop)(void);
	int                 (*DestroyWindow)(struct lh_window *);
	int                 (*AddEventHandler)(struct lh_window *, lh_event_handler_func_t, void *, LHbool);
	int                 (*RemoveEventHandler)(struct lh_window *, int);
	int                 (*GrabMouse)(struct lh_window *, LHbool);
	int                 (*GrabKeyboard)(struct lh_window *, LHbool);
	int                 (*LockCursor)(struct lh_window *, LHbool);
	int                 (*ShowCursor)(struct lh_window *, LHbool);

	lh_void_proc_t      (*GetGLProc)(const char *);
} ILHGLDriver;

#endif /* LIBHANG_INTERNAL_DRIVER_IFACE_H */
