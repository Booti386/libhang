/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_GL_GLX_XCB_PRIVATE_H
#define LIBHANG_INTERNAL_GL_GLX_XCB_PRIVATE_H 1

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xlib-xcb.h>
#include <GL/glx.h> /* MUST be included BEFORE libhang.h and libhang/GL/gl*.h */
#include "libhang/GL/glxext.h"

#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>

#include <libhang.h>

/* Atoms */
#define NB_ATOMS 3
enum {
	ATOM_WM_PROTOCOLS                 = 0,
	ATOM__NET_WM_SYNC_REQUEST_COUNTER = 1,
	ATOM_LH_CLASS                     = 2
};

/* WM protocol atoms */
#define NB_WM_PROTOCOL_ATOMS 2
enum {
	ATOM_WM_DELETE_WINDOW     = 0,
	ATOM__NET_WM_SYNC_REQUEST = 1
};

/* LH atoms */
#define NB_LH_ATOMS 1
enum {
	ATOM_LH_STOP_EVENT_THREAD = 0
};

#define LH_ATOM_DECL(name) { &TO_STR(name)[sizeof("ATOM_") - 1], (sizeof(TO_STR(name)) - 1) - (sizeof("ATOM_") - 1), XCB_ATOM_NONE }

struct lh_atom {
	const char *name;
	uint16_t name_len;
	xcb_atom_t atom;
};

struct lh_sys {
	struct LinkedList         windows;

	Display           *xlib_disp;
	xcb_connection_t  *connection;
	int                screen_num;
	xcb_screen_t      *screen;
	xcb_key_symbols_t *keysyms;
	xcb_window_t       dummy_window;
	struct lh_atom     atoms[NB_ATOMS];
	struct lh_atom     wm_atoms[NB_WM_PROTOCOL_ATOMS];
	struct lh_atom     lh_atoms[NB_LH_ATOMS];

	LHuint             evt_listening_win_count;
	LHbool             is_processing_evt;
	struct lh_cond    *leave_evt_loop_cond;
	struct lh_thread  *event_thread;
};

struct lh_window_blob {
	struct lh_window base;

	int64_t           id;

	GLXContext        glx_context;
	xcb_colormap_t    colormap;
	xcb_window_t      window;
	GLXWindow         glx_window;

	struct lh_thread  *draw_thread;
	LHbool            is_drawing;
	LHbool            is_listening_evt;

	struct LinkedList event_handlers;

	LHbool            is_mouse_grabbed;
	LHbool            is_keyboard_grabbed;

	LHbool            is_cursor_locked;
	int               mouse_real_x;
	int               mouse_real_y;

	LHbool            is_cursor_visible;
};

struct lh_event_handler {
	lh_event_handler_func_t handler;
	void *params;
	LHbool is_internal;
};

#endif /* LIBHANG_INTERNAL_GL_GLX_XCB_PRIVATE_H */
