/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_GL_WGL_WIN_KEYS_H
#define LIBHANG_INTERNAL_GL_WGL_WIN_KEYS_H 1

#include <windows.h>

#include <libhang.h>

static int lh_wgl_win_keysyms[] = {
	[VK_BACK]        = LHK_back_space,
	[VK_TAB]         = LHK_tab,
	[VK_RETURN]      = LHK_enter,
	[VK_ESCAPE]      = LHK_escape,
	[VK_DELETE]      = LHK_del,

	[VK_SPACE]       = LHK_space,

	[VK_MULTIPLY]    = LHK_asterisk,
	[VK_ADD]         = LHK_plus,
	[VK_SUBTRACT]    = LHK_minus,
	[VK_DIVIDE]      = LHK_slash,

	['0']            = LHK_0,
	['1']            = LHK_1,
	['2']            = LHK_2,
	['3']            = LHK_3,
	['4']            = LHK_4,
	['5']            = LHK_5,
	['6']            = LHK_6,
	['7']            = LHK_7,
	['8']            = LHK_8,
	['9']            = LHK_9,

	['A']            = LHK_a,
	['B']            = LHK_b,
	['C']            = LHK_c,
	['D']            = LHK_d,
	['E']            = LHK_e,
	['F']            = LHK_f,
	['G']            = LHK_g,
	['H']            = LHK_h,
	['I']            = LHK_i,
	['J']            = LHK_j,
	['K']            = LHK_k,
	['L']            = LHK_l,
	['M']            = LHK_m,
	['N']            = LHK_n,
	['O']            = LHK_o,
	['P']            = LHK_p,
	['Q']            = LHK_q,
	['R']            = LHK_r,
	['S']            = LHK_s,
	['T']            = LHK_t,
	['U']            = LHK_u,
	['V']            = LHK_v,
	['W']            = LHK_w,
	['X']            = LHK_x,
	['Y']            = LHK_y,
	['Z']            = LHK_z,

	[VK_SHIFT]       = LHK_shift,
	[VK_CONTROL]     = LHK_ctrl,
	[VK_MENU]        = LHK_alt,
	[VK_PAUSE]       = LHK_pause,
	[VK_CAPITAL]     = LHK_caps_lock,
	[VK_SCROLL]      = LHK_scroll_lock,
	[VK_SNAPSHOT]    = LHK_sys_req,

	[VK_F1]          = LHK_f1,
	[VK_F2]          = LHK_f2,
	[VK_F3]          = LHK_f3,
	[VK_F4]          = LHK_f4,
	[VK_F5]          = LHK_f5,
	[VK_F6]          = LHK_f6,
	[VK_F7]          = LHK_f7,
	[VK_F8]          = LHK_f8,
	[VK_F9]          = LHK_f9,
	[VK_F10]         = LHK_f10,
	[VK_F11]         = LHK_f11,
	[VK_F12]         = LHK_f12,
	[VK_LEFT]        = LHK_left,
	[VK_RIGHT]       = LHK_right,
	[VK_UP]          = LHK_up,
	[VK_DOWN]        = LHK_down,

	[VK_NUMPAD0]     = LHK_keypad_0,
	[VK_NUMPAD1]     = LHK_keypad_1,
	[VK_NUMPAD2]     = LHK_keypad_2,
	[VK_NUMPAD3]     = LHK_keypad_3,
	[VK_NUMPAD4]     = LHK_keypad_4,
	[VK_NUMPAD5]     = LHK_keypad_5,
	[VK_NUMPAD6]     = LHK_keypad_6,
	[VK_NUMPAD7]     = LHK_keypad_7,
	[VK_NUMPAD8]     = LHK_keypad_8,
	[VK_NUMPAD9]     = LHK_keypad_9
};

#endif /* LIBHANG_INTERNAL_GL_WGL_WIN_KEYS_H */
