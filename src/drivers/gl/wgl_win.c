/*
 * Copyright (C) 2014-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define _WIN32_WINNT 0x0501 /* _WIN32_WINNT_WINXP */

#include <stdio.h>
#include <stdlib.h>

#include <windows.h>
#include <windowsx.h>

#include <libhang.h>
#include "driver_iface.h"
#include "wgl_win-keys.h"
#include "wgl_win-private.h"
#include "libhang/GL/wglext.h"

#define LH_CUSTOM_EVENT_MSG (WM_USER + 0)

enum custom_rq {
	RQ_CREATE_WINDOW = 0,
	RQ_DESTROY_WINDOW,
	RQ_STOP_EVENT_THREAD
};

struct rq_create_window_params {
	HANDLE evt;
	struct lh_window *result;
	const char *title;
	int x;
	int y;
	LHuint width;
	LHuint height;
	int depth;
	LHbool dbl_buf;
};

struct rq_destroy_window_params {
	HANDLE evt;
	int result;
	struct lh_window *lh_window;
};

static struct lh_sys sys;

static LRESULT CALLBACK window_proc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
static DWORD event_thread_func(void *arg);

static int ILHGLDriver_Init(void)
{
	HANDLE event_thread_ready_evt;
	list_init(&sys.windows);
	sys.module = GetModuleHandle(NULL);

	memset(&sys.wc, '\0', sizeof(sys.wc));
	sys.wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	sys.wc.lpfnWndProc   = (WNDPROC)window_proc;
	sys.wc.cbClsExtra    = 0;
	sys.wc.cbWndExtra    = 0;
	sys.wc.hInstance     = sys.module;
	sys.wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
	sys.wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	sys.wc.hbrBackground = NULL;
	sys.wc.lpszMenuName  = NULL;
	sys.wc.lpszClassName = "lh_winclass_52b1a15ed8a2";

	/* Register window class */
	sys.wc_atom = RegisterClassA(&sys.wc);
	if (!sys.wc_atom) {
		fprintf(stderr, "RegisterClassA failed, aborting.\n");
		return -1;
	}

	sys.evt_listening_win_count = 0;
	sys.is_processing_evt = LH_FALSE;
	sys.leave_evt_loop_cond = lhCondCreate();
	sys.event_thread_id = 0;
	event_thread_ready_evt = CreateEventA(NULL, TRUE, FALSE, NULL);
	sys.event_thread = CreateThread(NULL, 0, event_thread_func, event_thread_ready_evt, 0, &sys.event_thread_id);
	WaitForSingleObject(event_thread_ready_evt, INFINITE);
	CloseHandle(event_thread_ready_evt);
	return 0;
}

static int ILHGLDriver_PreDeinit(void)
{
	PostThreadMessageA(sys.event_thread_id, LH_CUSTOM_EVENT_MSG, RQ_STOP_EVENT_THREAD, 0L);
	WaitForSingleObject(sys.event_thread, INFINITE);
	return 0;
}

static int ILHGLDriver_Deinit(void)
{
	lhCondDestroy(sys.leave_evt_loop_cond);
	UnregisterClassA(LongToPtr(sys.wc_atom), sys.module);
	return 0;
}

static void ILHGLDriver_AcquireGraphicCtx(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	wglMakeCurrent(window->dev, window->wgl_context);
}

static void ILHGLDriver_ReleaseGraphicCtx(struct lh_window *lh_window)
{
	lh_mark_used(lh_window);

	wglMakeCurrent(NULL, NULL);
}

static void ILHGLDriver_SwapBuffers(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;
	struct lh_gl_funcs *gl = &lh_window->gl;

	gl->Flush();
	gl->Finish();

	SwapBuffers(window->dev);
}

static int draw_loop(void *arg)
{
	struct lh_window *lh_window = arg;
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	ILHGLDriver_AcquireGraphicCtx(lh_window);

	while(window->is_drawing) {
		if(!lhIsDirtyObjBuffer(lh_window)) {
			lhDelay(1);
			continue;
		}

		lhDraw(lh_window);
		if(!lh_window->core->is_win_resized)
			ILHGLDriver_SwapBuffers(lh_window);
	}
	return 0;
}

static struct lh_window *ILHGLDriver_CreateWindow(const char *title, int x, int y, LHuint width, LHuint height, int depth, LHbool dbl_buf);
static int ILHGLDriver_DestroyWindow(struct lh_window *lh_window);

static DWORD event_thread_func(void *arg)
{
	BOOL result;
	MSG msg;
	HANDLE event_thread_ready_evt = arg;
	LHbool stop_event_thread = LH_FALSE;

	lh_mark_used(arg);

	/* Initialize the event queue. */
	PeekMessageA(&msg, NULL, 0, 0, PM_NOREMOVE);
	/* The event queue is ready. */
	SetEvent(event_thread_ready_evt);

	while(!stop_event_thread) {
		result = GetMessageA(&msg, NULL, 0, 0);
		if(result) {
			if(msg.message == LH_CUSTOM_EVENT_MSG) {
				switch(msg.wParam) {
					case RQ_CREATE_WINDOW: {
						struct rq_create_window_params *params = LongToPtr(msg.lParam);

						params->result = ILHGLDriver_CreateWindow(
							params->title,
							params->x, params->y,
							params->width, params->height,
							params->depth,
							params->dbl_buf
						);
						SetEvent(params->evt);
						break;
					}
					case RQ_DESTROY_WINDOW: {
						struct rq_destroy_window_params *params = LongToPtr(msg.lParam);

						params->result = ILHGLDriver_DestroyWindow(
							params->lh_window
						);
						SetEvent(params->evt);
						break;
					}
					case RQ_STOP_EVENT_THREAD:
						stop_event_thread = LH_TRUE;
						break;
				}
			}
			TranslateMessage(&msg);
			DispatchMessageA(&msg);
		}
	}
	return 0;
}

static void get_client_screen_rect(HWND hwnd, RECT *rect)
{
	GetClientRect(hwnd, rect);
	ClientToScreen(hwnd, &((POINT *)rect)[0]);
	ClientToScreen(hwnd, &((POINT *)rect)[1]);
}

static void cursor_lock_helper(struct lh_window *lh_window)
{
	struct lh_window_blob *window = LH_CONTAINER_OF(lh_window, struct lh_window_blob, base);
	RECT rect;

	if(window->is_cursor_locked) {
		get_client_screen_rect(window->handle, &rect);
		ClipCursor(&rect);
	}
}

static struct lh_window_blob *window_from_hwnd(HWND hwnd)
{
	struct LinkedListIterator it;

	list_it_init(&it, &sys.windows);
	for(struct lh_window_blob *window = list_it_get_cur(&it); window; window = list_it_get_next(&it))
		if(window->handle == hwnd)
			return window;

	return NULL;
}

static LRESULT CALLBACK window_proc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	struct lh_window_blob *window = window_from_hwnd(hwnd);
	struct lh_window *lh_window = &window->base;

	LRESULT result = 0;
	struct lh_event *lh_event = NULL;
	LHbool is_handled = LH_TRUE;
	struct LinkedListIterator callbacks_iterator;

	LHbool key_pressed = LH_FALSE;
	LHbool mouse_pressed = LH_FALSE;
	int mouse_button_id = LH_INVALID_MOUSE_BUTTON;

	if(!window || !lh_window->is_complete)
		return DefWindowProc(hwnd, msg, wParam, lParam);

	switch(msg)
	{
		case WM_ACTIVATE: {
			struct lh_event_focus_acquired *lh_local_event;

			lh_event = malloc(sizeof(*lh_local_event));
			if(!lh_event)
				break;
			lh_local_event = (struct lh_event_focus_acquired *)lh_event;

			lh_local_event->generic.type = (LOWORD(wParam) != WA_INACTIVE) ? LH_EVENT_FOCUS_ACQUIRED : LH_EVENT_FOCUS_LOST;
			lh_local_event->is_acquired = (lh_local_event->generic.type == LH_EVENT_FOCUS_ACQUIRED) ? LH_TRUE : LH_FALSE;
			break;
		}
		case WM_CLOSE: {
			struct lh_event_close *lh_local_event;

			lh_event = malloc(sizeof(*lh_local_event));
			if(!lh_event)
				break;
			lh_local_event = (struct lh_event_close *)lh_event;
			lh_local_event->generic.type = LH_EVENT_CLOSE;
			break;
		}
		case WM_PAINT: {
			lhDirtyObjBuffer(lh_window);
			break;
		}
		case WM_MOVE:
		case WM_MOVING:
			cursor_lock_helper(lh_window);
			break;
		case WM_SIZE: {
			struct lh_event_resized *lh_local_event;
			RECT bounds;
			LHuint width, height;

			GetClientRect(window->handle, &bounds);
			width = bounds.right - bounds.left;
			height = bounds.bottom - bounds.top;
			if(width == lh_window->width
					&& height == lh_window->height)
				break;
			cursor_lock_helper(lh_window);

			lh_event = malloc(sizeof(*lh_local_event));
			if(!lh_event)
				break;
			lh_local_event = (struct lh_event_resized *)lh_event;

			lh_local_event->generic.type = LH_EVENT_RESIZED;
			lh_local_event->old_w = lh_window->width;
			lh_local_event->old_h = lh_window->height;
			lh_window->width = width;
			lh_window->height = height;
			break;
		}
		case WM_KEYDOWN:
			key_pressed = LH_TRUE;
		case WM_KEYUP: {
			struct lh_event_key *lh_local_event;

			/* Discard auto-repeated keys */
			if(key_pressed && (HIWORD(lParam) & KF_REPEAT))
				break;
			if(LOWORD(lParam) > 1) /* Repeat count */
				break;

			lh_event = malloc(sizeof(*lh_local_event));
			if(!lh_event)
				break;
			lh_local_event = (struct lh_event_key *)lh_event;

			lh_local_event->generic.type = key_pressed ? LH_EVENT_KEY_PRESSED : LH_EVENT_KEY_RELEASED;
			lh_local_event->is_pressed = key_pressed;
			lh_local_event->key_id = -1;

			if (wParam < LH_ARRAY_SIZE(lh_wgl_win_keysyms))
				lh_local_event->key_id = lh_wgl_win_keysyms[wParam];
			break;
		}
		case WM_LBUTTONDOWN:
			mouse_button_id = LH_MOUSE_BUTTON_LEFT;
		case WM_MBUTTONDOWN:
			if(mouse_button_id == LH_INVALID_MOUSE_BUTTON)
				mouse_button_id = LH_MOUSE_BUTTON_MIDDLE;
		case WM_RBUTTONDOWN:
			mouse_pressed = LH_TRUE;
			if(mouse_button_id == LH_INVALID_MOUSE_BUTTON)
				mouse_button_id = LH_MOUSE_BUTTON_RIGHT;
		case WM_LBUTTONUP:
			if(mouse_button_id == LH_INVALID_MOUSE_BUTTON)
				mouse_button_id = LH_MOUSE_BUTTON_LEFT;
		case WM_MBUTTONUP:
			if(mouse_button_id == LH_INVALID_MOUSE_BUTTON)
				mouse_button_id = LH_MOUSE_BUTTON_MIDDLE;
		case WM_RBUTTONUP: {
			struct lh_event_mouse_pressed *lh_local_event;

			lh_event = malloc(sizeof(*lh_local_event));
			if(!lh_event)
				break;
			lh_local_event = (struct lh_event_mouse_pressed *)lh_event;

			if(mouse_button_id == LH_INVALID_MOUSE_BUTTON)
				mouse_button_id = LH_MOUSE_BUTTON_RIGHT;

			lh_local_event->generic.type = mouse_pressed ? LH_EVENT_MOUSE_PRESSED : LH_EVENT_MOUSE_RELEASED;
			lh_local_event->is_pressed = mouse_pressed;
			lh_local_event->x = GET_X_LPARAM(lParam);
			lh_local_event->y = GET_Y_LPARAM(lParam);
			lh_local_event->x_norm = (float)lh_local_event->x / lh_window->width;
			lh_local_event->y_norm = (float)lh_local_event->y / lh_window->height;
			lh_local_event->button_id = mouse_button_id;
			break;
		}
		case WM_MOUSEWHEEL: {
			struct lh_event_mouse_wheel *lh_local_event;

			window->mouse_wheel_cumul += GET_WHEEL_DELTA_WPARAM(wParam);
			if(window->mouse_wheel_cumul <= -WHEEL_DELTA
					|| window->mouse_wheel_cumul >= WHEEL_DELTA) {
				lh_event = malloc(sizeof(*lh_local_event));
				if(!lh_event)
					break;
				lh_local_event = (struct lh_event_mouse_wheel *)lh_event;

				lh_local_event->generic.type = LH_EVENT_MOUSE_WHEEL;
				lh_local_event->delta = -window->mouse_wheel_cumul / WHEEL_DELTA;
				window->mouse_wheel_cumul -= -lh_local_event->delta * WHEEL_DELTA;
			}
			break;
		}
		case WM_INPUT: {
			RAWINPUT *raw_input;
			RAWMOUSE *raw_mouse;
			UINT size = 0;

			is_handled = LH_FALSE;
			GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &size, sizeof(raw_input->header));

			if(!size)
				break;

			raw_input = malloc(size);
			if(!raw_input)
				break;

			if(GetRawInputData((HRAWINPUT)lParam, RID_INPUT, raw_input, &size, sizeof(raw_input->header)) != size) {
				free(raw_input);
				break;
			}

			switch(raw_input->header.dwType) {
				case RIM_TYPEMOUSE: {
					struct lh_event_mouse_moved *lh_local_event;
					RECT rect;
					POINT cursor_pos;

					is_handled = LH_TRUE;

					get_client_screen_rect(window->handle, &rect);
					GetCursorPos(&cursor_pos);
					if(!PtInRect(&rect, cursor_pos)) {
						window->is_cursor_inside = LH_FALSE;
						break;
					}

					raw_mouse = &raw_input->data.mouse;

					if((raw_mouse->usFlags & 1) == MOUSE_MOVE_ABSOLUTE) {
						window->mouse_real_x = raw_mouse->lLastX;
						window->mouse_real_y = raw_mouse->lLastY;
					} else {
						window->mouse_real_x += raw_mouse->lLastX;
						window->mouse_real_y += raw_mouse->lLastY;
					}
					if(window->mouse_real_x == window->mouse_old_x && window->mouse_real_y == window->mouse_old_y)
						break;

					if(!window->is_cursor_inside) {
						ScreenToClient(window->handle, &cursor_pos);
						window->mouse_real_x = cursor_pos.x;
						window->mouse_real_y = cursor_pos.y;
						window->is_cursor_inside = LH_TRUE;
					}

					cursor_pos.x = window->mouse_real_x;
					cursor_pos.y = window->mouse_real_y;
					ClientToScreen(window->handle, &cursor_pos);
					SetCursorPos(cursor_pos.x, cursor_pos.y);

					lh_event = malloc(sizeof(*lh_local_event));
					if(!lh_event)
						break;
					lh_local_event = (struct lh_event_mouse_moved *)lh_event;

					lh_local_event->generic.type = LH_EVENT_MOUSE_MOVED;
					lh_local_event->old_x = window->mouse_old_x;
					lh_local_event->old_y = window->mouse_old_y;
					window->mouse_old_x = lh_local_event->x = window->mouse_real_x;
					window->mouse_old_y = lh_local_event->y = window->mouse_real_y;
					lh_local_event->old_x_norm = window->mouse_old_x_norm;
					lh_local_event->old_y_norm = window->mouse_old_y_norm;
					window->mouse_old_x_norm = lh_local_event->x_norm = (float)lh_local_event->x / lh_window->width;
					window->mouse_old_y_norm = lh_local_event->y_norm = (float)lh_local_event->y / lh_window->height;
					break;
				}
			}
			if(!is_handled) {
				result = DefRawInputProc(&raw_input, 1, sizeof(raw_input->header));
				is_handled = LH_TRUE;
			}
			free(raw_input);
			break;
		}
		default:
			is_handled = LH_FALSE;
	}

	if(!is_handled) {
		result = DefWindowProc(hwnd, msg, wParam, lParam);
	} else if(lh_event) {
		LHuint ret = LH_EVENT_RET_UNHANDLED;
		list_it_init(&callbacks_iterator, &window->event_handlers);
		for(struct lh_event_handler *ev_handler = (struct lh_event_handler *)list_it_get_cur(&callbacks_iterator); ev_handler; ev_handler = list_it_get_next(&callbacks_iterator))
			if(((sys.is_processing_evt && window->is_listening_evt) || ev_handler->is_internal) && ev_handler->handler(lh_event, ev_handler->params) == LH_EVENT_RET_HANDLED)
				ret = LH_EVENT_RET_HANDLED;
		if(ret == LH_EVENT_RET_UNHANDLED) {
			switch(lh_event->type) {
				case LH_EVENT_CLOSE:
					if(window->is_listening_evt) {
						sys.evt_listening_win_count--;
						if(sys.evt_listening_win_count == 0)
							lhLeaveEventLoop();
						window->is_listening_evt = LH_FALSE;
					}
					lhDestroyWindow(lh_window);
					break;
			}
		}
		free(lh_event);
	}

	return result;
}

static struct lh_window *ILHGLDriver_CreateWindow(const char *title, int x, int y, LHuint width, LHuint height, int depth, LHbool dbl_buf)
{
	static const int ctx_attribs[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 2,
		WGL_CONTEXT_FLAGS_ARB, 0,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
		0, 0
	};
	static const DWORD exstyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	static const DWORD style = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1, /* Version 1 */
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL,
		PFD_TYPE_RGBA,
		0, /* DepthSize */
		0, 0, 0, 0, 0, 0,  /* Colors ignored */
		8,  /* AlphaSize 8 bits */
		0,  /* AlphaShift ignored */
		0,  /* AccumBit unused */
		0, 0, 0, 0, /* AccumSize ignored */
		16, /* zBufSize  16 bits */
		0,  /* StencSize unused */
		0,  /* AuxBuf unused */
		PFD_MAIN_PLANE, /* LayerType */
		0, /* Reserved */
		0, 0, 0 /* Underlaypane unused */
	};
	int result;
	int pf;
	HGLRC compat_wgl_ctx;
	struct lh_window_blob *window;
	struct lh_window *lh_window;
	RECT rect;

	switch(depth) {
		case LH_DEPTH_LOW:
			pfd.cDepthBits = 16; /* DepthSize 16 bits */
			break;
		case LH_DEPTH_MEDIUM:
			pfd.cDepthBits = 24; /* DepthSize 24 bits */
			break;
		case LH_DEPTH_HIGH:
			pfd.cDepthBits = 32; /* DepthSize 32 bits */
			break;
		default:
			fprintf(stderr, "Invalid depth specified.\n");
			return NULL;
	}

	pfd.dwFlags = dbl_buf ? pfd.dwFlags | PFD_DOUBLEBUFFER : pfd.dwFlags & (~PFD_DOUBLEBUFFER);

	window = malloc(sizeof(*window));
	if(!window)
		return NULL;
	lh_window = &window->base;

	lh_window->is_complete = LH_FALSE;
	lh_window->x = x;
	lh_window->y = y;
	lh_window->width = width;
	lh_window->height = height;

	window->draw_thread = NULL;
	window->is_drawing = LH_FALSE;
	window->is_listening_evt = LH_FALSE;

	list_init(&window->event_handlers);

	window->is_cursor_inside = LH_FALSE;
	window->is_mouse_grabbed = LH_FALSE;
	window->is_keyboard_grabbed = LH_FALSE;

	window->is_cursor_locked = LH_FALSE;
	window->mouse_real_x = 0;
	window->mouse_real_y = 0;

	window->is_cursor_visible = LH_TRUE;

	window->mouse_old_x = 0;
	window->mouse_old_y = 0;
	window->mouse_old_x_norm = 0.f;
	window->mouse_old_y_norm = 0.f;
	window->mouse_wheel_cumul = 0;

	/* Define the window real size from those of its content. */
	rect.left = x;
	rect.top = y;
	rect.right = x + width;
	rect.bottom = y + height;
	AdjustWindowRectEx(&rect, style, FALSE, exstyle);

	/* Create the window */
	window->handle = CreateWindowExA(
		exstyle,
		LongToPtr(sys.wc_atom),
		title,
		style,
		x, y, rect.right - rect.left, rect.bottom - rect.top,
		NULL, NULL, sys.module,	NULL
	);
	if(!window->handle) {
		fprintf(stderr, "libhang: CreateWindowExA() failed, aborting.\n");
		goto failed_create_window;
	}

	window->dev = GetDC(window->handle);
	if(!window->dev) {
		fprintf(stderr, "libhang: GetDC() failed, aborting.\n");
		goto failed_getdc;
	}

	pf = ChoosePixelFormat(window->dev, &pfd);
	if(!window->dev) {
		fprintf(stderr, "libhang: ChoosePixelFormat() failed, aborting.\n");
		goto failed_pf;
	}

	result = SetPixelFormat(window->dev, pf, &pfd);
	if(!result) {
		fprintf(stderr, "libhang: SetPixelFormat() failed, aborting.\n");
		goto failed_pf;
	}

	/* Create the WGL context */
	compat_wgl_ctx = wglCreateContext(window->dev);
	if(!compat_wgl_ctx) {
		fprintf(stderr, "libhang: wglCreateContext() failed, aborting.\n");
		goto failed_wgl_context;
	}
	wglMakeCurrent(window->dev, compat_wgl_ctx);
	PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
	window->wgl_context = NULL;
	if(wglCreateContextAttribsARB)
		window->wgl_context = wglCreateContextAttribsARB(window->dev, NULL, ctx_attribs);
	wglMakeCurrent(NULL, NULL);
	if(window->wgl_context) {
		wglDeleteContext(compat_wgl_ctx);
	} else {
		fprintf(stderr, "libhang: Warning: Using compatibility context.\n");
		window->wgl_context = compat_wgl_ctx;
	}

	result = wglMakeCurrent(window->dev, window->wgl_context);
	if(!result) {
		fprintf(stderr, "libhang: wglMakeCurrent() failed, aborting.\n");
		goto failed_make_current;
	}

	ILHGLDriver_ReleaseGraphicCtx(&window->base);

	window->raw_mouse.usUsagePage = 0x1;
	window->raw_mouse.usUsage = 0x2;
	window->raw_mouse.dwFlags = RIDEV_INPUTSINK;
	window->raw_mouse.hwndTarget = window->handle;
	RegisterRawInputDevices(&window->raw_mouse, 1, sizeof(window->raw_mouse));

	ShowWindow(window->handle, SW_SHOW);
	SetForegroundWindow(window->handle);

	list_push(&sys.windows, window);
	return lh_window;

failed_make_current:
	wglDeleteContext(window->wgl_context);

failed_wgl_context:
failed_pf:
	ReleaseDC(window->handle, window->dev);

failed_getdc:
	DestroyWindow(window->handle);

failed_create_window:
	free(window);
	return NULL;
}

static struct lh_window *ILHGLDriver_CreateWindow_wrapper(const char *title, int x, int y, LHuint width, LHuint height, int depth, LHbool dbl_buf)
{
	DWORD tid = GetCurrentThreadId();

	if(tid != sys.event_thread_id) {
		struct rq_create_window_params params = {
			.evt = CreateEventA(NULL, TRUE, FALSE, NULL),
			.result = NULL,
			.title = title,
			.x = x, .y = y,
			.width = width, .height = height,
			.depth = depth,
			.dbl_buf = dbl_buf
		};

		PostThreadMessageA(sys.event_thread_id, LH_CUSTOM_EVENT_MSG, RQ_CREATE_WINDOW, PtrToLong(&params));
		WaitForSingleObject(params.evt, INFINITE);
		CloseHandle(params.evt);
		return params.result;
	}
	return ILHGLDriver_CreateWindow(title, x, y, width, height, depth, dbl_buf);
}

static int ILHGLDriver_StartDrawing(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if(window->is_drawing)
		return -1;

	window->is_drawing = LH_TRUE;

	window->draw_thread = lhThreadCreate(draw_loop, lh_window);
	if(window->draw_thread)
		return -1;

	return 0;
}

static int ILHGLDriver_StopDrawing(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;
	int ret;

	if(!window->is_drawing)
		return 0;

	window->is_drawing = LH_FALSE;

	ret = lhThreadWait(window->draw_thread, NULL);
	if(ret < 0)
		return ret;

	ret = lhThreadRelease(window->draw_thread);
	if(ret < 0)
		return ret;

	return 0;
}

static int ILHGLDriver_SetListeningEvents(struct lh_window *lh_window, LHbool is_listening)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if(!window->is_listening_evt && is_listening)
		sys.evt_listening_win_count++;
	else if(window->is_listening_evt && !is_listening)
		sys.evt_listening_win_count--;
	window->is_listening_evt = is_listening;
	return 0;
}

static int ILHGLDriver_EnterEventLoop(void)
{
	sys.is_processing_evt = LH_TRUE;
	lhCondWait(sys.leave_evt_loop_cond);
	return 0;
}

static int ILHGLDriver_LeaveEventLoop(void)
{
	sys.is_processing_evt = LH_FALSE;
	lhCondWakeAll(sys.leave_evt_loop_cond);
	return 0;
}

static int ILHGLDriver_DestroyWindow(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	ILHGLDriver_SetListeningEvents(lh_window, LH_FALSE);

	list_clear(&window->event_handlers);

	window->raw_mouse.dwFlags |= RIDEV_REMOVE;
	RegisterRawInputDevices(&window->raw_mouse, 1, sizeof(window->raw_mouse));

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(window->wgl_context);
	ReleaseDC(window->handle, window->dev);
	DestroyWindow(window->handle);

	free(window);
	return 0;
}

static int ILHGLDriver_DestroyWindow_wrapper(struct lh_window *lh_window)
{
	DWORD tid = GetCurrentThreadId();

	if(tid != sys.event_thread_id) {
		struct rq_destroy_window_params params = {
			.evt = CreateEventA(NULL, TRUE, FALSE, NULL),
			.result = 0,
			.lh_window = lh_window
		};

		PostThreadMessageA(sys.event_thread_id, LH_CUSTOM_EVENT_MSG, RQ_DESTROY_WINDOW, PtrToLong(&params));
		WaitForSingleObject(params.evt, INFINITE);
		CloseHandle(params.evt);
		return params.result;
	}
	return ILHGLDriver_DestroyWindow(lh_window);
}

static int ILHGLDriver_AddEventHandler(struct lh_window *lh_window, lh_event_handler_func_t handler, void *params, LHbool is_internal)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;
	struct lh_event_handler ev_handler;

	ev_handler.handler = handler;
	ev_handler.params = params;
	ev_handler.is_internal = is_internal;

	return list_push_dup(&window->event_handlers, &ev_handler, sizeof(ev_handler));
}

static int ILHGLDriver_RemoveEventHandler(struct lh_window *lh_window, int id)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	list_remove(&window->event_handlers, id);
	return 0;
}

static int ILHGLDriver_GrabMouse(struct lh_window *lh_window, LHbool grabbed)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if((grabbed && window->is_mouse_grabbed)
			|| (!grabbed && !window->is_mouse_grabbed))
		return 0;
	if(grabbed) {
	} else {
	}
	window->is_mouse_grabbed = grabbed;
	return 0;
}

static int ILHGLDriver_GrabKeyboard(struct lh_window *lh_window, LHbool grabbed)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if((grabbed && window->is_keyboard_grabbed)
			|| (!grabbed && !window->is_keyboard_grabbed))
		return 0;
	if(grabbed) {
	} else {
	}
	window->is_keyboard_grabbed = grabbed;
	return 0;
}

static int ILHGLDriver_LockCursor(struct lh_window *lh_window, LHbool locked)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if((locked && window->is_cursor_locked)
			|| (!locked && !window->is_cursor_locked))
		return 0;
	window->is_cursor_locked = locked;
	if(locked) {
		cursor_lock_helper(lh_window);
	} else {
		POINT pos;

		ClipCursor(NULL);
		window->mouse_real_x %= lh_window->width;
		window->mouse_real_y %= lh_window->height;
		pos.x = window->mouse_real_x;
		pos.y = window->mouse_real_y;
		ClientToScreen(window->handle, &pos);
		SetCursorPos(pos.x, pos.y);
	}
	return 0;
}

static int ILHGLDriver_ShowCursor(struct lh_window *lh_window, LHbool visible)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if((visible && window->is_cursor_visible)
			|| (!visible && !window->is_cursor_visible))
		return 0;
	if(visible)
		ShowCursor(TRUE);
	else
		ShowCursor(FALSE);
	window->is_cursor_visible = visible;
	return 0;
}


static lh_void_proc_t ILHGLDriver_GetGLProc(const char *name)
{
	return (lh_void_proc_t)wglGetProcAddress(name);
}

ILHGLDriver __lhGLDriver_wgl_win = {
	.base = {
		.Name      = "wgl_win",
		.Init      = ILHGLDriver_Init,
		.PreDeinit = ILHGLDriver_PreDeinit,
		.Deinit    = ILHGLDriver_Deinit
	},
	.AcquireGraphicCtx  = ILHGLDriver_AcquireGraphicCtx,
	.ReleaseGraphicCtx  = ILHGLDriver_ReleaseGraphicCtx,
	.CreateWindow       = ILHGLDriver_CreateWindow_wrapper,
	.StartDrawing       = ILHGLDriver_StartDrawing,
	.StopDrawing        = ILHGLDriver_StopDrawing,
	.SetListeningEvents = ILHGLDriver_SetListeningEvents,
	.EnterEventLoop     = ILHGLDriver_EnterEventLoop,
	.LeaveEventLoop     = ILHGLDriver_LeaveEventLoop,
	.DestroyWindow      = ILHGLDriver_DestroyWindow_wrapper,
	.AddEventHandler    = ILHGLDriver_AddEventHandler,
	.RemoveEventHandler = ILHGLDriver_RemoveEventHandler,
	.GrabMouse          = ILHGLDriver_GrabMouse,
	.GrabKeyboard       = ILHGLDriver_GrabKeyboard,
	.LockCursor         = ILHGLDriver_LockCursor,
	.ShowCursor         = ILHGLDriver_ShowCursor,

	.GetGLProc          = ILHGLDriver_GetGLProc
};
