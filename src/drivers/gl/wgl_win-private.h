/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_GL_WGL_WIN_PRIVATE_H
#define LIBHANG_INTERNAL_GL_WGL_WIN_PRIVATE_H 1

#include <windows.h>

#include <libhang.h>

struct lh_sys {
	struct LinkedList   windows;
	HINSTANCE    module;

	WNDCLASS     wc;
	ATOM         wc_atom;

	LHuint       evt_listening_win_count;
	LHbool       is_processing_evt;
	struct lh_cond   *leave_evt_loop_cond;
	DWORD event_thread_id;
	HANDLE event_thread;
};

struct lh_window_blob {
	struct lh_window   base;

	HWND          handle;
	HDC           dev;
	HGLRC         wgl_context;

	struct lh_thread  *draw_thread;
	LHbool        is_drawing;
	LHbool        is_listening_evt;

	struct LinkedList    event_handlers;

	LHbool        is_cursor_inside;
	LHbool        is_mouse_grabbed;
	LHbool        is_keyboard_grabbed;

	LHbool        is_cursor_locked;
	int           mouse_real_x;
	int           mouse_real_y;

	LHbool        is_cursor_visible;

	int           mouse_old_x;
	int           mouse_old_y;
	float         mouse_old_x_norm;
	float         mouse_old_y_norm;
	int           mouse_wheel_cumul;

	RAWINPUTDEVICE raw_mouse;
};

struct lh_event_handler {
	lh_event_handler_func_t handler;
	void *params;
	LHbool is_internal;
};

struct lh_thread_blob {
	HANDLE handle;
	int (*func)(void *);
	void *params;
	LHbool is_running;
	LHbool is_launched;
};

#endif /* LIBHANG_INTERNAL_GL_WGL_WIN_PRIVATE_H */
