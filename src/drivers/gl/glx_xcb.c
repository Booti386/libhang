/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/Xlib.h>
#include <X11/Xlib-xcb.h>
#include <X11/Xlibint.h>

#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xcb_event.h>
#include <xcb/xcb_image.h>
#include <xcb/sync.h>

#include <libhang.h>
#include "driver_iface.h"
#include "glx_xcb-keys.h"
#include "glx_xcb-private.h"

static struct lh_sys sys = {
	.atoms = {
		/* Atoms */
		LH_ATOM_DECL(ATOM_WM_PROTOCOLS),
		LH_ATOM_DECL(ATOM__NET_WM_SYNC_REQUEST_COUNTER),
		LH_ATOM_DECL(ATOM_LH_CLASS)
	},
	.wm_atoms = {
		/* WM protocol atoms */
		LH_ATOM_DECL(ATOM_WM_DELETE_WINDOW),
		LH_ATOM_DECL(ATOM__NET_WM_SYNC_REQUEST)
	},
	.lh_atoms = {
		/* LH atoms */
		LH_ATOM_DECL(ATOM_LH_STOP_EVENT_THREAD)
	},
};

void init_atoms(void)
{
	xcb_intern_atom_reply_t *atom_reply;

	for(int i = 0; i < NB_ATOMS; i++) {
		atom_reply = xcb_intern_atom_reply(sys.connection, xcb_intern_atom(sys.connection, 0, sys.atoms[i].name_len, sys.atoms[i].name), NULL);
		sys.atoms[i].atom = atom_reply->atom;
		free(atom_reply);
	}
	for(int i = 0; i < NB_WM_PROTOCOL_ATOMS; i++) {
		atom_reply = xcb_intern_atom_reply(sys.connection, xcb_intern_atom(sys.connection, 0, sys.wm_atoms[i].name_len, sys.wm_atoms[i].name), NULL);
		sys.wm_atoms[i].atom = atom_reply->atom;
		free(atom_reply);
	}
	for(int i = 0; i < NB_LH_ATOMS; i++) {
		atom_reply = xcb_intern_atom_reply(sys.connection, xcb_intern_atom(sys.connection, 0, sys.lh_atoms[i].name_len, sys.lh_atoms[i].name), NULL);
		sys.lh_atoms[i].atom = atom_reply->atom;
		free(atom_reply);
	}
}

static int x_error_handler(Display *dpy, XErrorEvent *ev)
{
	lh_mark_used(dpy);
	lh_mark_used(ev);
	return 0;
}

static void send_stop_event_thread(void)
{
	xcb_client_message_event_t ev;

	ev.response_type = XCB_CLIENT_MESSAGE;
	ev.format = sizeof(ev.data.data32[0]) * 8;
	ev.window = sys.dummy_window;
	ev.type = sys.atoms[ATOM_LH_CLASS].atom;
	ev.data.data32[0] = sys.lh_atoms[ATOM_LH_STOP_EVENT_THREAD].atom;
	ev.data.data32[1] = 0;
	ev.data.data32[2] = 0;
	ev.data.data32[3] = 0;
	ev.data.data32[4] = 0;
	xcb_send_event(sys.connection, 0, sys.dummy_window, XCB_EVENT_MASK_NO_EVENT, (char *)&ev);
	xcb_flush(sys.connection);
}

static int ILHGLDriver_Init(void)
{
	xcb_sync_initialize_reply_t *sync_init_reply;
	xcb_query_extension_reply_t *query_ext_reply;

	XInitThreads();

	list_init(&sys.windows);

	sys.xlib_disp  = XOpenDisplay(NULL);
	if(!sys.xlib_disp)
		return -1;

	/* Do not abort on X server failure */
	XSetErrorHandler(x_error_handler);

	sys.connection = XGetXCBConnection(sys.xlib_disp);
	if(!sys.connection)
		return -1;

	XSetEventQueueOwner(sys.xlib_disp, XCBOwnsEventQueue);

	sys.screen_num = DefaultScreen(sys.xlib_disp);

	xcb_screen_iterator_t scr_itr = xcb_setup_roots_iterator(xcb_get_setup(sys.connection));
	for(int i = 0; i != sys.screen_num && scr_itr.rem; i++)
		xcb_screen_next(&scr_itr);
	sys.screen = scr_itr.data;
	if(!sys.screen)
		return -1;

	/* Retrieve keysyms - used in event loop */
	sys.keysyms = xcb_key_symbols_alloc(sys.connection);

	/* Enable extensions */
	sync_init_reply = xcb_sync_initialize_reply(sys.connection, xcb_sync_initialize(sys.connection, XCB_SYNC_MAJOR_VERSION, XCB_SYNC_MINOR_VERSION), NULL);
	free(sync_init_reply);
	query_ext_reply = xcb_query_extension_reply(sys.connection, xcb_query_extension(sys.connection, sizeof("xfixes") - 1, "xfixes"), NULL);
	free(query_ext_reply);

	/* Create a dummy window to be able to send events to the event thread */
	sys.dummy_window = xcb_generate_id(sys.connection);
	xcb_create_window(sys.connection, XCB_COPY_FROM_PARENT, sys.dummy_window, sys.screen->root, 0, 0, 1, 1, 0, XCB_WINDOW_CLASS_INPUT_ONLY, XCB_COPY_FROM_PARENT, 0, NULL);

	init_atoms();

	xcb_flush(sys.connection);

	sys.evt_listening_win_count = 0;
	sys.is_processing_evt = LH_FALSE;
	sys.leave_evt_loop_cond = lhCondCreate();
	sys.event_thread = NULL;
	return 0;
}

static int ILHGLDriver_PreDeinit(void)
{
	send_stop_event_thread();
	if(sys.event_thread) {
		lhThreadWait(sys.event_thread, NULL);
		lhThreadRelease(sys.event_thread);
		sys.event_thread = NULL;
	}
	xcb_destroy_window(sys.connection, sys.dummy_window);
	return 0;
}

static int ILHGLDriver_Deinit(void)
{
	lhCondDestroy(sys.leave_evt_loop_cond);
	xcb_key_symbols_free(sys.keysyms);
	XCloseDisplay(sys.xlib_disp);
	return 0;
}

static void ILHGLDriver_AcquireGraphicCtx(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	glXMakeCurrent(sys.xlib_disp, window->glx_window, window->glx_context);
	xcb_flush(sys.connection);
}

static void ILHGLDriver_ReleaseGraphicCtx(struct lh_window *lh_window)
{
	lh_mark_used(lh_window);

	glXMakeCurrent(sys.xlib_disp, 0, NULL);
	xcb_flush(sys.connection);
}

static void ILHGLDriver_SwapBuffers(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;
	struct lh_gl_funcs *gl = &lh_window->gl;

	gl->Flush();
	gl->Finish();

	glXSwapBuffers(sys.xlib_disp, window->glx_window);
	xcb_flush(sys.connection);
}

static int draw_loop(void *arg)
{
	struct lh_window *lh_window = arg;
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	ILHGLDriver_AcquireGraphicCtx(lh_window);

	while(window->is_drawing) {
		if(!lhIsDirtyObjBuffer(lh_window)) {
			lhDelay(1);
			continue;
		}

		lhDraw(lh_window);
		if(!lh_window->core->is_win_resized)
			ILHGLDriver_SwapBuffers(lh_window);
	}
	return 0;
}

static struct lh_window_blob *window_from_id(xcb_window_t id)
{
	struct LinkedListIterator it;

	list_it_init(&it, &sys.windows);
	for(struct lh_window_blob *window = list_it_get_cur(&it); window; window = list_it_get_next(&it))
		if(window->window == id)
			return window;

	return NULL;
}

static int event_thread_func(void *arg)
{
	struct lh_window_blob *window;
	struct lh_window *lh_window;
	LHbool stop_event_thread = LH_FALSE;
	LHbool key_pressed, mouse_pressed, focus_acquired;
	xcb_generic_event_t *xcb_event = NULL, *xcb_next_event = NULL;
	int mouse_old_x = 0, mouse_old_y = 0;
	float mouse_old_x_norm = 0.f, mouse_old_y_norm = 0.f;

	lh_mark_used(arg);

	xcb_event = NULL;
	while(!stop_event_thread) {
		struct lh_event *lh_event = NULL;
		struct LinkedListIterator callbacks_iterator;

		if(xcb_event)
			free(xcb_event);
		xcb_event = xcb_next_event;
		while(!xcb_event)
			xcb_event = xcb_wait_for_event(sys.connection);
		xcb_next_event = xcb_poll_for_event(sys.connection);

		focus_acquired = LH_FALSE;
		key_pressed = LH_FALSE;
		mouse_pressed = LH_FALSE;
		window = NULL;
		lh_window = NULL;

		switch(XCB_EVENT_RESPONSE_TYPE(xcb_event))
		{
			/* Error */
			case 0:
			{
				xcb_generic_error_t *error = (xcb_generic_error_t *)xcb_event;
				const char *err_label = xcb_event_get_error_label(error->error_code);
				const char *major_label= xcb_event_get_request_label(error->major_code);
				const char *loc_name = "unknown";
				uint32_t loc = 0;

				switch (error->error_code)
				{
					case XCB_REQUEST:
					case XCB_MATCH:
					case XCB_ACCESS:
					case XCB_ALLOC:
					case XCB_NAME:
					case XCB_LENGTH:
					case XCB_IMPLEMENTATION:
					{
						xcb_request_error_t *request_error = (xcb_request_error_t *)error;
						loc_name = "bad_value";
						loc = request_error->bad_value;
						break;
					}

					case XCB_VALUE:
					case XCB_WINDOW:
					case XCB_PIXMAP:
					case XCB_ATOM:
					case XCB_CURSOR:
					case XCB_FONT:
					case XCB_DRAWABLE:
					case XCB_COLORMAP:
					case XCB_G_CONTEXT:
					case XCB_ID_CHOICE:
					{
						xcb_value_error_t *value_error = (xcb_value_error_t *)error;
						loc_name = "bad_value";
						loc = value_error->bad_value;
						break;
					}
				}

				if(!err_label)
					err_label = "unknown";

				if(!major_label)
					major_label = "unknown";

				fprintf(stderr, "libhang: X Error: %u(%s), major=%u(%s), minor=%u, %s=%u, sequence=%u\n", error->error_code, err_label, error->major_code, major_label, error->minor_code, loc_name, loc, error->sequence);
				break;
			}

			case XCB_FOCUS_IN:
				focus_acquired = LH_TRUE;
				/* Fall through. */

			case XCB_FOCUS_OUT:
			{
				struct lh_event_focus *lh_local_event;
				xcb_focus_in_event_t *focus_event = (xcb_focus_in_event_t *)xcb_event;

				window = window_from_id(focus_event->event);
				if(!window)
					break;
				lh_window = &window->base;
				lh_event = malloc(sizeof(*lh_local_event));
				if(!lh_event)
					break;
				lh_local_event = (struct lh_event_focus *)lh_event;

				lh_local_event->generic.type = focus_acquired ? LH_EVENT_FOCUS_ACQUIRED : LH_EVENT_FOCUS_LOST;
				lh_local_event->is_acquired = focus_acquired;
				break;
			}

			case XCB_CONFIGURE_NOTIFY:
			{
				struct lh_event_resized *lh_local_event;
				xcb_configure_notify_event_t *configure_notify_event = (xcb_configure_notify_event_t *)xcb_event;

				window = window_from_id(configure_notify_event->window);
				if(!window)
					break;
				lh_window = &window->base;
				if(configure_notify_event->width == lh_window->width
						&& configure_notify_event->height == lh_window->height)
					break;

				lh_event = malloc(sizeof(*lh_local_event));
				if(!lh_event)
					break;
				lh_local_event = (struct lh_event_resized *)lh_event;

				lh_local_event->generic.type = LH_EVENT_RESIZED;
				lh_local_event->old_w = lh_window->width;
				lh_local_event->old_h = lh_window->height;
				lh_window->width = configure_notify_event->width;
				lh_window->height = configure_notify_event->height;
				break;
			}

			case XCB_EXPOSE:
			{
				struct lh_event_redraw *lh_local_event;
				xcb_expose_event_t *expose_event = (xcb_expose_event_t *)xcb_event;

				window = window_from_id(expose_event->window);
				if(!window)
					break;
				lh_window = &window->base;
				lh_event = malloc(sizeof(*lh_local_event));
				if(!lh_event)
					break;
				lh_local_event = (struct lh_event_redraw *)lh_event;

				lh_local_event->generic.type = LH_EVENT_REDRAW;
				break;
			}

			case XCB_KEY_PRESS:
				key_pressed = LH_TRUE;
				/* Fall through. */

			case XCB_KEY_RELEASE:
			{
				struct lh_event_key *lh_local_event;
				xcb_key_press_event_t *key_event = (xcb_key_press_event_t *)xcb_event, *key_next_event = NULL;
				xcb_keysym_t key;

				window = window_from_id(key_event->event);
				if(!window)
					break;
				lh_window = &window->base;
				lh_event = malloc(sizeof(*lh_local_event));
				if(!lh_event)
					break;
				lh_local_event = (struct lh_event_key *)lh_event;

				if(key_pressed) {
					lh_local_event->generic.type = LH_EVENT_KEY_PRESSED;
					key = xcb_key_press_lookup_keysym(sys.keysyms, key_event, 0);
				} else {
					key_next_event = (xcb_key_press_event_t *)xcb_next_event;

					/* Check for/discard auto-repeat events: Discard current release & next press. */
					/* /!\ key_event->event is a xcb_window_t /!\ */
					if (key_next_event && XCB_EVENT_RESPONSE_TYPE(key_next_event) == XCB_KEY_PRESS && key_next_event->time == key_event->time && key_next_event->detail == key_event->detail && key_next_event->event == key_event->event) {
						free(lh_event);
						lh_event = NULL;
						free(xcb_next_event);
						xcb_next_event = NULL;
						break;
					}

					lh_local_event->generic.type = LH_EVENT_KEY_RELEASED;
					key = xcb_key_release_lookup_keysym(sys.keysyms, key_event, 0);
				}

				lh_local_event->is_pressed = key_pressed;
				lh_local_event->key_id = -1;

				if (key < LH_ARRAY_SIZE(lh_glx_xcb_keysyms))
					lh_local_event->key_id = lh_glx_xcb_keysyms[key];
				break;
			}

			case XCB_BUTTON_PRESS:
				mouse_pressed = LH_TRUE;
				/* Fall through. */

			case XCB_BUTTON_RELEASE:
			{
				struct lh_event_mouse_button *lh_local_event_button;
				struct lh_event_mouse_wheel *lh_local_event_wheel;
				xcb_button_press_event_t *button_event = (xcb_button_press_event_t *)xcb_event;
				LHuint button_id = LH_INVALID_MOUSE_BUTTON;
				LHbool is_wheel = LH_FALSE;
				LHbool wheel_rot_up = LH_FALSE;

				window = window_from_id(button_event->event);
				if(!window)
					break;
				lh_window = &window->base;

				switch (button_event->detail)
				{
					case XCB_BUTTON_INDEX_1:
						button_id = LH_MOUSE_BUTTON_LEFT;
						break;

					case XCB_BUTTON_INDEX_2:
						button_id = LH_MOUSE_BUTTON_MIDDLE;
						break;

					case XCB_BUTTON_INDEX_3:
						button_id = LH_MOUSE_BUTTON_RIGHT;
						break;

					case XCB_BUTTON_INDEX_4:
						wheel_rot_up = LH_TRUE;
						/* Fall through. */

					case XCB_BUTTON_INDEX_5:
						is_wheel = LH_TRUE;
						break;
				}

				if(!is_wheel) {
					if(button_id == LH_INVALID_MOUSE_BUTTON)
						break;

					lh_event = malloc(sizeof(*lh_local_event_button));
					if(!lh_event)
						break;
					lh_local_event_button = (struct lh_event_mouse_button *)lh_event;

					lh_local_event_button->generic.type = mouse_pressed ? LH_EVENT_MOUSE_PRESSED : LH_EVENT_MOUSE_RELEASED;
					lh_local_event_button->is_pressed = mouse_pressed;
					lh_local_event_button->x = button_event->event_x;
					lh_local_event_button->y = button_event->event_y;
					lh_local_event_button->x_norm = (float)lh_local_event_button->x / lh_window->width;
					lh_local_event_button->y_norm = (float)lh_local_event_button->y / lh_window->height;
					lh_local_event_button->button_id = button_id;
				} else {
					if(!mouse_pressed)
						break;

					lh_event = malloc(sizeof(*lh_local_event_wheel));
					if(!lh_event)
						break;
					lh_local_event_wheel = (struct lh_event_mouse_wheel *)lh_event;

					lh_local_event_wheel->generic.type = LH_EVENT_MOUSE_WHEEL;
					lh_local_event_wheel->delta = wheel_rot_up ? -1 : 1;
				}
				break;
			}

			case XCB_MOTION_NOTIFY:
			{
				struct lh_event_mouse_moved *lh_local_event;
				xcb_motion_notify_event_t *motion_notify_event = (xcb_motion_notify_event_t *)xcb_event, *next_motion_notify_event = (xcb_motion_notify_event_t *)xcb_next_event;

				window = window_from_id(motion_notify_event->event);
				if(!window)
					break;
				lh_window = &window->base;
				/* Discard current motion notify events if next exists (makes mouse moves a lot smoother) */
				if(xcb_next_event && XCB_EVENT_RESPONSE_TYPE(xcb_next_event) == XCB_MOTION_NOTIFY && motion_notify_event->event == next_motion_notify_event->event)
					break;

				window->mouse_real_x = motion_notify_event->event_x;
				window->mouse_real_y = motion_notify_event->event_y;
				if(window->mouse_real_x == mouse_old_x && window->mouse_real_y == mouse_old_y)
					break;
				if(window->is_cursor_locked) {
					xcb_warp_pointer(sys.connection, window->window, window->window, 0, 0, 0, 0, lh_window->width / 2, lh_window->height / 2);
					xcb_flush(sys.connection);
					window->mouse_real_x = window->mouse_real_x - lh_window->width / 2 + mouse_old_x;
					window->mouse_real_y = window->mouse_real_y - lh_window->height / 2 + mouse_old_y;
				}

				lh_event = malloc(sizeof(*lh_local_event));
				if(!lh_event)
					break;
				lh_local_event = (struct lh_event_mouse_moved *)lh_event;

				lh_local_event->generic.type = LH_EVENT_MOUSE_MOVED;
				lh_local_event->old_x = mouse_old_x;
				lh_local_event->old_y = mouse_old_y;
				mouse_old_x = lh_local_event->x = window->mouse_real_x;
				mouse_old_y = lh_local_event->y = window->mouse_real_y;
				lh_local_event->old_x_norm = mouse_old_x_norm;
				lh_local_event->old_y_norm = mouse_old_y_norm;
				mouse_old_x_norm = lh_local_event->x_norm = (float)lh_local_event->x / lh_window->width;
				mouse_old_y_norm = lh_local_event->y_norm = (float)lh_local_event->y / lh_window->height;
				break;
			}

			case XCB_CLIENT_MESSAGE:
			{
				xcb_client_message_event_t *client_message_event = (xcb_client_message_event_t *)xcb_event;

				if(client_message_event->type == sys.atoms[ATOM_WM_PROTOCOLS].atom) {
					window = window_from_id(client_message_event->window);
					if(!window)
						break;
					lh_window = &window->base;

					/* Closing window */
					if(client_message_event->data.data32[0] == sys.wm_atoms[ATOM_WM_DELETE_WINDOW].atom) {
						struct lh_event_close *lh_local_event;

						lh_event = malloc(sizeof(*lh_local_event));
						if(!lh_event)
							break;
						lh_local_event = (struct lh_event_close *)lh_event;
						lh_local_event->generic.type = LH_EVENT_CLOSE;
					} else if(client_message_event->data.data32[0] == sys.wm_atoms[ATOM__NET_WM_SYNC_REQUEST].atom) {
						/* printf("a\n"); */ /* Don't know how to implement that for now. */
					}
				} else if(client_message_event->type == sys.atoms[ATOM_LH_CLASS].atom) {
					if(client_message_event->data.data32[0] == sys.lh_atoms[ATOM_LH_STOP_EVENT_THREAD].atom) {
						stop_event_thread = LH_TRUE;
						break;
					}
				}
				break;
			}

			default:
			{
				Bool (*wire_to_event)(Display *, XEvent *, xEvent *);
				Status (*event_to_wire)(Display *, XEvent *, xEvent *);
				XEvent xlib_event;
				xEvent new_event;

				/* If the event was not intercepted by XCB, and if a translation procedure was registered to Xlib, send the event to the procedure. */
				/* Mostly useful for OpenGL libs that intercepts DRI2 events this way. */
				XLockDisplay(sys.xlib_disp);

				/* Get the current conversion procedure for this event. */
				wire_to_event = XESetWireToEvent(sys.xlib_disp, xcb_event->response_type, NULL);
				if(wire_to_event) {

					/* Restore the original procedure. */
					XESetWireToEvent(sys.xlib_disp, xcb_event->response_type, wire_to_event);
					xcb_event->sequence = LastKnownRequestProcessed(sys.xlib_disp);

					/* Convert the event from the wire (native) format to the Xlib format, and propagate if necessary. */
					/* OpenGL never asks to propagate the event but others can, so this case must be handled anyway. */
					memset(&xlib_event, 0, sizeof(xlib_event));
					if(wire_to_event(sys.xlib_disp, &xlib_event, (xEvent *)xcb_event)) {

						/* The wire was translated to a new Xlib event that must be converted back to a wire and propagated. */
						event_to_wire = XESetEventToWire(sys.xlib_disp, xlib_event.type, NULL);
						if(event_to_wire) {

							/* Restore the original procedure. */
							XESetEventToWire(sys.xlib_disp, xlib_event.type, event_to_wire);

							/* Translate the Xlib event to a wire event that can be used by XCB. */
							memset(&new_event, 0, sizeof(new_event));
							if(event_to_wire(sys.xlib_disp, &xlib_event, &new_event))
								xcb_send_event(sys.connection, 0, xlib_event.xany.window, XCB_EVENT_MASK_NO_EVENT, (char *)&new_event);
						}
					}
				}
				XUnlockDisplay(sys.xlib_disp);
			}
		}

		if(lh_event) {
			if(window) {
				LHuint ret = LH_EVENT_RET_UNHANDLED;
				list_it_init(&callbacks_iterator, &window->event_handlers);
				for(struct lh_event_handler *ev_handler = list_it_get_cur(&callbacks_iterator); ev_handler; ev_handler = list_it_get_next(&callbacks_iterator)) {
					if((sys.is_processing_evt && window->is_listening_evt) || ev_handler->is_internal) {
						LHuint handler_ret;

						handler_ret = ev_handler->handler(lh_event, ev_handler->params);
						if(handler_ret == LH_EVENT_RET_HANDLED)
							ret = LH_EVENT_RET_HANDLED;
					}
				}
				if(ret == LH_EVENT_RET_UNHANDLED) {
					switch(lh_event->type) {
						case LH_EVENT_CLOSE:
							if(window->is_listening_evt) {
								sys.evt_listening_win_count--;
								if(sys.evt_listening_win_count == 0)
									lhLeaveEventLoop();
								window->is_listening_evt = LH_FALSE;
							}
							lhDestroyWindow(lh_window);
							break;
					}
				}
			}
			free(lh_event);
			lh_event = NULL;
		}
	}
	if(xcb_event)
		free(xcb_event);
	if(xcb_next_event)
		free(xcb_next_event);
	return 0;
}

static struct lh_window *ILHGLDriver_CreateWindow(const char *title, int x, int y, LHuint width, LHuint height, int depth, LHbool dbl_buf)
{
	LH32s glx_attribs[] = {
#define LH_GLX_ATTRIB_DBLBUF_IDX 1
		GLX_DOUBLEBUFFER, 0,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_STENCIL_SIZE, 8,
#define LH_GLX_ATTRIB_DEPTH_IDX 13
		GLX_DEPTH_SIZE, 0,
		GLX_BUFFER_SIZE, 24,
		GLX_RENDER_TYPE, GLX_RGBA_BIT,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
		GLX_X_RENDERABLE, True,
		None, None
	};
	static LH32s ctx_attribs[] = {
		GLX_RENDER_TYPE, GLX_RGBA_TYPE,
		GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
		GLX_CONTEXT_MINOR_VERSION_ARB, 2,
		GLX_CONTEXT_FLAGS_ARB, 0,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None, None
	};
	LH32u win_value_mask;
	LH32u win_value_list[3];
	GLXContext compat_glx_ctx;
	xcb_atom_t wm_atom_list[NB_WM_PROTOCOL_ATOMS];
	GLXFBConfig *fbconfigs;
	int nb_fbconfigs;
	GLXFBConfig fbconfig;
	int visual_id;
	struct lh_window_blob *window;
	struct lh_window *lh_window;

	switch(depth) {
		case LH_DEPTH_LOW:
			glx_attribs[LH_GLX_ATTRIB_DEPTH_IDX] = 16;
			break;
		case LH_DEPTH_MEDIUM:
			glx_attribs[LH_GLX_ATTRIB_DEPTH_IDX] = 24;
			break;
		case LH_DEPTH_HIGH:
			glx_attribs[LH_GLX_ATTRIB_DEPTH_IDX] = 32;
			break;
		default:
			fprintf(stderr, "libhang: Invalid depth (%u) specified.\n", depth);
			return NULL;
	}

	glx_attribs[LH_GLX_ATTRIB_DBLBUF_IDX] = dbl_buf ? True : False;

#undef LH_GLX_ATTRIB_DEPTH_IDX
#undef LH_GLX_ATTRIB_DBLBUF_IDX

	window = malloc(sizeof(*window));
	if(!window)
		return NULL;
	lh_window = &window->base;

	lh_window->x = x;
	lh_window->y = y;
	lh_window->width = width;
	lh_window->height = height;

	window->colormap = xcb_generate_id(sys.connection);
	window->window   = xcb_generate_id(sys.connection);

	window->draw_thread = NULL;
	window->is_drawing = LH_FALSE;
	window->is_listening_evt = LH_FALSE;

	list_init(&window->event_handlers);

	window->is_mouse_grabbed = LH_FALSE;
	window->is_keyboard_grabbed = LH_FALSE;

	window->is_cursor_locked = LH_FALSE;
	window->mouse_real_x = 0;
	window->mouse_real_y = 0;

	window->is_cursor_visible = LH_TRUE;

	/* Retrieve the best FBConfig and its Visual */
	fbconfigs = glXChooseFBConfig(sys.xlib_disp, sys.screen_num, glx_attribs, &nb_fbconfigs);
	if(!fbconfigs) {
		fprintf(stderr, "libhang: glxChooseFBConfig() failed, aborting.\n");
		goto failed_choose_fbconfig;
	}
	memcpy(&fbconfig, &fbconfigs[0], sizeof(fbconfigs[0]));
	XFree(fbconfigs);
	glXGetFBConfigAttrib(sys.xlib_disp, fbconfig, GLX_VISUAL_ID , &visual_id);

	/* Create a Colormap for the window */
	xcb_create_colormap(sys.connection , XCB_COLORMAP_ALLOC_NONE, window->colormap, sys.screen->root, visual_id);

	/* Set window mask/attributes */
	win_value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK | XCB_CW_COLORMAP;
	win_value_list[0] = sys.screen->white_pixel;
	win_value_list[1] = XCB_EVENT_MASK_FOCUS_CHANGE
			| XCB_EVENT_MASK_STRUCTURE_NOTIFY
			| XCB_EVENT_MASK_EXPOSURE
			| XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE
			| XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE
			| XCB_EVENT_MASK_POINTER_MOTION;
	win_value_list[2] = window->colormap;

	/* Create the window */
	xcb_create_window(sys.connection, XCB_COPY_FROM_PARENT, window->window,
		sys.screen->root, lh_window->x, lh_window->y, lh_window->width, lh_window->height, 0,
		XCB_WINDOW_CLASS_INPUT_OUTPUT,
		visual_id,
		win_value_mask,
		win_value_list
	);

	/* Map the window, needed by glXMakeContextCurrent */
	xcb_map_window(sys.connection, window->window);

	/* Attach a GLX window to our window */
	window->glx_window = glXCreateWindow(sys.xlib_disp, fbconfig, window->window, NULL);

	/* Create the GLX context */
	compat_glx_ctx = glXCreateNewContext(sys.xlib_disp, fbconfig, GLX_RGBA_TYPE, 0, True);
	if(!compat_glx_ctx) {
		fprintf(stderr, "libhang: glXCreateNewContext() failed, aborting.\n");
		goto failed_create_ctx;
	}
	glXMakeContextCurrent(sys.xlib_disp, window->glx_window, window->glx_window, compat_glx_ctx);
	PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB = (PFNGLXCREATECONTEXTATTRIBSARBPROC)glXGetProcAddress((GLubyte *)"glXCreateContextAttribsARB");
	window->glx_context = NULL;
	if(glXCreateContextAttribsARB)
		window->glx_context = glXCreateContextAttribsARB(sys.xlib_disp, fbconfig, 0, True, ctx_attribs);
	if(window->glx_context) {
		glXDestroyContext(sys.xlib_disp, compat_glx_ctx);
	} else {
		fprintf(stderr, "libhang: Warning: Using compatibility context.\n");
		window->glx_context = compat_glx_ctx;
	}

	/* Make our context current */
	if(!glXMakeContextCurrent(sys.xlib_disp, window->glx_window, window->glx_window, window->glx_context)) {
		fprintf(stderr, "libhang: glxMakeContextCurrent() failed, aborting.\n");
		goto failed_make_current;
	}

	for(int i = 0; i < NB_WM_PROTOCOL_ATOMS; i++)
		wm_atom_list[i] = sys.wm_atoms[i].atom;
	xcb_icccm_set_wm_protocols(sys.connection, window->window, sys.atoms[ATOM_WM_PROTOCOLS].atom, NB_WM_PROTOCOL_ATOMS, wm_atom_list);

	uint32_t id = xcb_generate_id(sys.connection);
	xcb_sync_int64_t ival = {0, 0};
	xcb_sync_create_counter(sys.connection, id, ival);
	xcb_change_property(
		sys.connection,
		XCB_PROP_MODE_REPLACE,
		window->window,
		sys.atoms[ATOM__NET_WM_SYNC_REQUEST_COUNTER].atom,
		XCB_ATOM_CARDINAL, sizeof(id) * 8, 1, &id
	);

	/* Set the window title */
	xcb_change_property(
		sys.connection,
		XCB_PROP_MODE_REPLACE,
		window->window,
		XCB_ATOM_WM_NAME,
		XCB_ATOM_STRING, sizeof(title[0]) * 8, strlen(title), title
	);

	ILHGLDriver_ReleaseGraphicCtx(lh_window);

	/* Delayed event thread launch */
	if(!sys.event_thread)
		sys.event_thread = lhThreadCreate(event_thread_func, NULL);

	window->id = list_push(&sys.windows, window);
	return lh_window;

failed_make_current:
	glXDestroyContext(sys.xlib_disp, window->glx_context);

failed_create_ctx:
	xcb_destroy_window(sys.connection, window->window);

failed_choose_fbconfig:
	free(window);
	return NULL;
}

static int ILHGLDriver_StartDrawing(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if(window->is_drawing)
		return -1;

	window->is_drawing = LH_TRUE;

	window->draw_thread = lhThreadCreate(draw_loop, lh_window);
	if(window->draw_thread)
		return -1;

	return 0;
}

static int ILHGLDriver_StopDrawing(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;
	int ret;

	if(!window->is_drawing)
		return 0;
	window->is_drawing = LH_FALSE;

	ret = lhThreadWait(window->draw_thread, NULL);
	if(ret < 0)
		return ret;

	ret = lhThreadRelease(window->draw_thread);
	if(ret < 0)
		return ret;
	return 0;
}

static int ILHGLDriver_SetListeningEvents(struct lh_window *lh_window, LHbool is_listening)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if(!window->is_listening_evt && is_listening)
		sys.evt_listening_win_count++;
	else if(window->is_listening_evt && !is_listening)
		sys.evt_listening_win_count--;
	window->is_listening_evt = is_listening;
	return 0;
}

static int ILHGLDriver_EnterEventLoop(void)
{
	sys.is_processing_evt = LH_TRUE;
	lhCondWait(sys.leave_evt_loop_cond);
	return 0;
}

static int ILHGLDriver_LeaveEventLoop(void)
{
	sys.is_processing_evt = LH_FALSE;
	lhCondWakeAll(sys.leave_evt_loop_cond);
	return 0;
}

static int ILHGLDriver_DestroyWindow(struct lh_window *lh_window)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	list_remove(&sys.windows, window->id);

	ILHGLDriver_StopDrawing(lh_window);
	ILHGLDriver_SetListeningEvents(lh_window, LH_FALSE);

	list_clear(&window->event_handlers);

	glXMakeContextCurrent(sys.xlib_disp, window->glx_window, 0, NULL);
	glXDestroyContext(sys.xlib_disp, window->glx_context);
	glXDestroyWindow(sys.xlib_disp, window->glx_window);
	xcb_destroy_window(sys.connection, window->window);

	xcb_flush(sys.connection);
	free(window);
	return 0;
}

static int ILHGLDriver_AddEventHandler(struct lh_window *lh_window, lh_event_handler_func_t handler, void *params, LHbool is_internal)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;
	struct lh_event_handler ev_handler;

	ev_handler.handler = handler;
	ev_handler.params = params;
	ev_handler.is_internal = is_internal;

	return list_push_dup(&window->event_handlers, &ev_handler, sizeof(ev_handler));
}

static int ILHGLDriver_RemoveEventHandler(struct lh_window *lh_window, int id)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	list_remove(&window->event_handlers, id);
	return 0;
}

static int ILHGLDriver_GrabMouse(struct lh_window *lh_window, LHbool grabbed)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if((grabbed && window->is_mouse_grabbed)
			|| (!grabbed && !window->is_mouse_grabbed))
		return 0;
	if(grabbed) {
		xcb_grab_pointer_reply_t *reply;
		reply = xcb_grab_pointer_reply(
			sys.connection,
			xcb_grab_pointer(
				sys.connection,
				1,
				window->window,
				XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION,
				XCB_GRAB_MODE_ASYNC,
				XCB_GRAB_MODE_ASYNC,
				window->window,
				XCB_NONE,
				XCB_CURRENT_TIME),
			NULL);
		free(reply);
	} else {
		xcb_ungrab_pointer(sys.connection, XCB_TIME_CURRENT_TIME);
	}
	xcb_flush(sys.connection);
	window->is_mouse_grabbed = grabbed;
	return 0;
}

static int ILHGLDriver_GrabKeyboard(struct lh_window *lh_window, LHbool grabbed)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;

	if((grabbed && window->is_keyboard_grabbed)
			|| (!grabbed && !window->is_keyboard_grabbed))
		return 0;
	if(grabbed) {
		xcb_grab_keyboard_reply_t *reply;
		reply = xcb_grab_keyboard_reply(
			sys.connection,
			xcb_grab_keyboard(
				sys.connection,
				1,
				window->window,
				XCB_CURRENT_TIME,
				XCB_GRAB_MODE_ASYNC,
				XCB_GRAB_MODE_ASYNC),
			NULL);
		free(reply);
	} else {
		xcb_ungrab_keyboard(sys.connection, XCB_TIME_CURRENT_TIME);
	}
	xcb_flush(sys.connection);
	window->is_keyboard_grabbed = grabbed;
	return 0;
}

static int ILHGLDriver_LockCursor(struct lh_window *lh_window, LHbool locked)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;
	int x, y;

	if((locked && window->is_cursor_locked)
			|| (!locked && !window->is_cursor_locked))
		return 0;
	if(locked) {
		x = lh_window->width / 2;
		y = lh_window->height / 2;
	} else {
		window->mouse_real_x %= lh_window->width;
		window->mouse_real_y %= lh_window->height;
		window->mouse_real_x = window->mouse_real_x < 0 ? (int)lh_window->width + window->mouse_real_x : window->mouse_real_x;
		window->mouse_real_y = window->mouse_real_y < 0 ? (int)lh_window->height + window->mouse_real_y : window->mouse_real_y;
		x = window->mouse_real_x;
		y = window->mouse_real_y;
	}
	xcb_warp_pointer(sys.connection, sys.screen->root, window->window, 0, 0, 0, 0, x, y);
	xcb_flush(sys.connection);
	window->is_cursor_locked = locked;
	return 0;
}

static int ILHGLDriver_ShowCursor(struct lh_window *lh_window, LHbool visible)
{
	struct lh_window_blob *window = (struct lh_window_blob *)lh_window;
	xcb_pixmap_t pixmap;
	xcb_cursor_t cursor;
	static const uint8_t blank_bits[] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};

	if((visible && window->is_cursor_visible)
			|| (!visible && !window->is_cursor_visible))
		return 0;
	if(!visible) {
		pixmap = xcb_create_pixmap_from_bitmap_data(sys.connection, sys.screen->root, (uint8_t *)blank_bits, 16, 16, 1, 0, 0, NULL);
		cursor = xcb_generate_id(sys.connection);
		xcb_create_cursor(sys.connection,
			cursor,
			pixmap,
			pixmap,
			0, 0, 0,
			0xFFFF, 0xFFFF, 0xFFFF,
			8, 8);
		xcb_free_pixmap(sys.connection, pixmap);
		xcb_change_window_attributes(sys.connection, window->window, XCB_CW_CURSOR, &cursor);
		xcb_free_cursor(sys.connection, cursor);
	} else {
		cursor = XCB_NONE;
		xcb_change_window_attributes(sys.connection, window->window, XCB_CW_CURSOR, &cursor);
	}
	xcb_flush(sys.connection);
	window->is_cursor_visible = visible;
	return 0;
}

static lh_void_proc_t ILHGLDriver_GetGLProc(const char *name)
{
	return (lh_void_proc_t)glXGetProcAddress((const GLubyte *)name);
}

ILHGLDriver __lhGLDriver_glx_xcb = {
	.base = {
		.Name      = "glx_xcb",
		.Init      = ILHGLDriver_Init,
		.PreDeinit = ILHGLDriver_PreDeinit,
		.Deinit    = ILHGLDriver_Deinit
	},
	.AcquireGraphicCtx  = ILHGLDriver_AcquireGraphicCtx,
	.ReleaseGraphicCtx  = ILHGLDriver_ReleaseGraphicCtx,
	.CreateWindow       = ILHGLDriver_CreateWindow,
	.StartDrawing       = ILHGLDriver_StartDrawing,
	.StopDrawing        = ILHGLDriver_StopDrawing,
	.SetListeningEvents = ILHGLDriver_SetListeningEvents,
	.EnterEventLoop     = ILHGLDriver_EnterEventLoop,
	.LeaveEventLoop     = ILHGLDriver_LeaveEventLoop,
	.DestroyWindow      = ILHGLDriver_DestroyWindow,
	.AddEventHandler    = ILHGLDriver_AddEventHandler,
	.RemoveEventHandler = ILHGLDriver_RemoveEventHandler,
	.GrabMouse          = ILHGLDriver_GrabMouse,
	.GrabKeyboard       = ILHGLDriver_GrabKeyboard,
	.LockCursor         = ILHGLDriver_LockCursor,
	.ShowCursor         = ILHGLDriver_ShowCursor,

	.GetGLProc          = ILHGLDriver_GetGLProc
};
