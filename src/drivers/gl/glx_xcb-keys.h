/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_GL_GLX_XCB_KEYS_H
#define LIBHANG_INTERNAL_GL_GLX_XCB_KEYS_H 1

#include <X11/keysym.h>

#include <libhang.h>

static int lh_glx_xcb_keysyms[] = {
	[XK_BackSpace]   = LHK_back_space,
	[XK_Tab]         = LHK_tab,
	[XK_Return]      = LHK_enter,
	[XK_Escape]      = LHK_escape,
	[XK_Delete]      = LHK_del,

	[XK_space]       = LHK_space,

	[XK_asterisk]    = LHK_asterisk,
	[XK_plus]        = LHK_plus,
	[XK_minus]       = LHK_minus,
	[XK_slash]       = LHK_slash,

	[XK_0]           = LHK_0,
	[XK_1]           = LHK_1,
	[XK_2]           = LHK_2,
	[XK_3]           = LHK_3,
	[XK_4]           = LHK_4,
	[XK_5]           = LHK_5,
	[XK_6]           = LHK_6,
	[XK_7]           = LHK_7,
	[XK_8]           = LHK_8,
	[XK_9]           = LHK_9,

	[XK_a]           = LHK_a,
	[XK_b]           = LHK_b,
	[XK_c]           = LHK_c,
	[XK_d]           = LHK_d,
	[XK_e]           = LHK_e,
	[XK_f]           = LHK_f,
	[XK_g]           = LHK_g,
	[XK_h]           = LHK_h,
	[XK_i]           = LHK_i,
	[XK_j]           = LHK_j,
	[XK_k]           = LHK_k,
	[XK_l]           = LHK_l,
	[XK_m]           = LHK_m,
	[XK_n]           = LHK_n,
	[XK_o]           = LHK_o,
	[XK_p]           = LHK_p,
	[XK_q]           = LHK_q,
	[XK_r]           = LHK_r,
	[XK_s]           = LHK_s,
	[XK_t]           = LHK_t,
	[XK_u]           = LHK_u,
	[XK_v]           = LHK_v,
	[XK_w]           = LHK_w,
	[XK_x]           = LHK_x,
	[XK_y]           = LHK_y,
	[XK_z]           = LHK_z,

	[XK_Shift_L]     = LHK_shift,
	[XK_Shift_R]     = LHK_shift,
	[XK_Control_L]   = LHK_ctrl,
	[XK_Control_R]   = LHK_ctrl,
	[XK_Alt_L]       = LHK_alt,
	[XK_Alt_R]       = LHK_alt,
	[XK_Pause]       = LHK_pause,
	[XK_Caps_Lock]   = LHK_caps_lock,
	[XK_Scroll_Lock] = LHK_scroll_lock,
	[XK_Sys_Req]     = LHK_sys_req,

	[XK_F1]          = LHK_f1,
	[XK_F2]          = LHK_f2,
	[XK_F3]          = LHK_f3,
	[XK_F4]          = LHK_f4,
	[XK_F5]          = LHK_f5,
	[XK_F6]          = LHK_f6,
	[XK_F7]          = LHK_f7,
	[XK_F8]          = LHK_f8,
	[XK_F9]          = LHK_f9,
	[XK_F10]         = LHK_f10,
	[XK_F11]         = LHK_f11,
	[XK_F12]         = LHK_f12,
	[XK_Left]        = LHK_left,
	[XK_Right]       = LHK_right,
	[XK_Up]          = LHK_up,
	[XK_Down]        = LHK_down,

	[XK_KP_Tab]      = LHK_keypad_tab,
	[XK_KP_Enter]    = LHK_keypad_enter,
	[XK_KP_Delete]   = LHK_keypad_del,

	[XK_KP_Space]    = LHK_keypad_space,
	[XK_KP_Multiply] = LHK_keypad_asterisk,
	[XK_KP_Add]      = LHK_keypad_plus,
	[XK_KP_Subtract] = LHK_keypad_minus,
	[XK_KP_Decimal]  = LHK_keypad_dot,
	[XK_KP_Divide]   = LHK_keypad_slash,

	[XK_KP_0]        = LHK_keypad_0,
	[XK_KP_1]        = LHK_keypad_1,
	[XK_KP_2]        = LHK_keypad_2,
	[XK_KP_3]        = LHK_keypad_3,
	[XK_KP_4]        = LHK_keypad_4,
	[XK_KP_5]        = LHK_keypad_5,
	[XK_KP_6]        = LHK_keypad_6,
	[XK_KP_7]        = LHK_keypad_7,
	[XK_KP_8]        = LHK_keypad_8,
	[XK_KP_9]        = LHK_keypad_9,

	[XK_KP_F1]       = LHK_keypad_f1,
	[XK_KP_F2]       = LHK_keypad_f2,
	[XK_KP_F3]       = LHK_keypad_f3,
	[XK_KP_F4]       = LHK_keypad_f4,
	[XK_KP_Left]     = LHK_keypad_left,
	[XK_KP_Right]    = LHK_keypad_right,
	[XK_KP_Up]       = LHK_keypad_up,
	[XK_KP_Down]     = LHK_keypad_down
};

#endif /* LIBHANG_INTERNAL_GL_GLX_XCB_KEYS_H */
