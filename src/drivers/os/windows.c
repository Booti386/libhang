/*
 * Copyright (C) 2014-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define _WIN32_WINNT 0x0501 /* _WIN32_WINNT_WINXP */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include <malloc.h> /* _aligned_malloc() */
#include <windows.h>
#include <psapi.h>

#include <libhang.h>
#include "driver_iface.h"
#include "windows-private.h"

#undef ARCH_AMD64
#if defined __amd64__ || defined __amd64 || defined __x86_64__ || defined __x86_64 || defined _M_X64 || defined _M_AMD64
#  define ARCH_AMD64 1
#endif

#undef ARCH_X86_ONLY
#if defined i386 || defined __i386 || defined __i386__ || defined __i486__ || defined __i586__ || defined __i686__ || defined __IA32__ || defined _M_I86 || defined _M_IX86 || defined __X86__ || defined _X86_ || defined __I86__ || defined __INTEL__ || defined __386  || defined __386__
#  define ARCH_X86_ONLY 1
#endif

#undef ARCH_X86
#if defined ARCH_AMD64 || defined ARCH_X86_ONLY
#  define ARCH_X86 1
#endif

#undef ARCH_IA64
#if defined __ia64__ || defined _IA64 || defined __IA64__ || defined __ia64 || defined _M_IA64 || defined __itanium__
#  define ARCH_IA64 1
#endif

#if !defined ARCH_IA64
#  ifndef InterlockedAddNoFence
#    define InterlockedAddNoFence(dst, val) (InterlockedAdd((dst), (val)))
#  endif

#  ifndef InterlockedAdd
#    define InterlockedAdd(dst, val) (InterlockedExchangeAdd((dst), (val)) + (val))
#  endif

#  ifndef InterlockedCompareExchangePointerNoFence
#    define InterlockedCompareExchangePointerNoFence(dst, val, cmp) (InterlockedCompareExchangePointer((dst), (val), (cmp)))
#  endif
#endif /* !ARCH_IA64 */

static struct lh_sys sys;

static LONG CALLBACK excp_handler(EXCEPTION_POINTERS *excp_info)
{
	EXCEPTION_RECORD *rec = excp_info->ExceptionRecord;
	CONTEXT *ctx = excp_info->ContextRecord;
	FILE *fp = NULL;
	HMODULE module;
	MODULEINFO module_info;
	char module_name[MAX_PATH + 1];
	MEMORY_BASIC_INFORMATION stack_info;
	uintptr_t sp = (uintptr_t)NULL;

	fp = fopen("crash.info", "w");
	if (!fp)
		goto leave;

	fprintf(fp, "Exception %#lx at address %#" PRIxPTR ":\n", rec->ExceptionCode, (uintptr_t)rec->ExceptionAddress);

	if (GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, rec->ExceptionAddress, &module))
	{
		DWORD end = GetModuleFileNameA(module, module_name, sizeof(module_name) - 1);

		if (!end)
		{
			strncpy(module_name, "<unknown>", sizeof(module_name));
			end = sizeof(module_name) - 1;
		}

		module_name[end] = '\0';
		GetModuleInformation(GetCurrentProcess(), module, &module_info, sizeof(module_info));
		fprintf(fp, "Module: %s @<%#" PRIxPTR ">\n\n", module_name, (uintptr_t)module_info.lpBaseOfDll);
		FreeLibrary(module);
	}

#ifdef ARCH_AMD64

	fprintf(fp, "rax = %#" PRIx64 "\n"
			"rbx = %#" PRIx64 "\n"
			"rcx = %#" PRIx64 "\n"
			"rdx = %#" PRIx64 "\n"
			"rdi = %#" PRIx64 "\n"
			"rsi = %#" PRIx64 "\n"
			"r8 = %#" PRIx64 "\n"
			"r9 = %#" PRIx64 "\n"
			"r10 = %#" PRIx64 "\n"
			"r11 = %#" PRIx64 "\n"
			"r12 = %#" PRIx64 "\n"
			"r13 = %#" PRIx64 "\n"
			"r14 = %#" PRIx64 "\n"
			"r15 = %#" PRIx64 "\n"
			"\n"
			"rsp = %#" PRIx64 "\n"
			"rbp = %#" PRIx64 "\n"
			"\n"
			"eflags = %#" PRIx32 "\n"
			"\n"
			"rip = %#" PRIx64 "\n"
			"\n",
		ctx->Rax, ctx->Rbx, ctx->Rcx, ctx->Rdx, ctx->Rdi, ctx->Rsi, ctx->R8,
		ctx->R9, ctx->R10, ctx->R11, ctx->R12, ctx->R13, ctx->R14, ctx->R15,
		ctx->Rsp, ctx->Rbp,
		(uint32_t)ctx->EFlags,
		ctx->Rip);

	sp = (uintptr_t)ctx->Rsp;

#elif defined ARCH_X86_ONLY

	fprintf(fp, "eax = %#" PRIx32 "\n"
			"ebx = %#" PRIx32 "\n"
			"ecx = %#" PRIx32 "\n"
			"edx = %#" PRIx32 "\n"
			"edi = %#" PRIx32 "\n"
			"esi = %#" PRIx32 "\n"
			"\n"
			"esp = %#" PRIx32 "\n"
			"ebp = %#" PRIx32 "\n"
			"\n"
			"eflags = %#" PRIx32 "\n"
			"\n"
			"eip = %#" PRIx32 "\n"
			"\n",
		(uint32_t)ctx->Eax, (uint32_t)ctx->Ebx, (uint32_t)ctx->Ecx,
		(uint32_t)ctx->Edx, (uint32_t)ctx->Edi, (uint32_t)ctx->Esi,
		(uint32_t)ctx->Esp, (uint32_t)ctx->Ebp,
		(uint32_t)ctx->EFlags,
		(uint32_t)ctx->Eip);

	sp = (uintptr_t)ctx->Esp;

#else

	fprintf(fp, "Unrecognized architecture, regs and stack dump are unavailable.\n");
	goto leave;

#endif /* ARCH_* */

	if (!VirtualQuery((void *)sp, &stack_info, sizeof(stack_info))
			|| stack_info.State != MEM_COMMIT)
	{
		fprintf(fp, "No stack available: sp is not within a mapped region.\n");
		goto leave;
	}

	if (stack_info.Protect & PAGE_GUARD
			|| ((stack_info.Protect & 0xFF) != PAGE_EXECUTE_READ
				&& (stack_info.Protect & 0xFF) != PAGE_EXECUTE_READWRITE
				&& (stack_info.Protect & 0xFF) != PAGE_READONLY
				&& (stack_info.Protect & 0xFF) != PAGE_READWRITE))
	{
		fprintf(fp, "No stack available: sp is not within a readable region.\n");
		goto leave;
	}

	fprintf(fp, "--- Stack dump ---\n");

	stack_info.RegionSize -= sp - (uintptr_t)stack_info.BaseAddress;
	if (stack_info.RegionSize > sizeof(uintptr_t) * 64)
		stack_info.RegionSize = sizeof(uintptr_t) * 64;

	stack_info.RegionSize /= sizeof(uintptr_t);

	for (LHuint i = 0; i < stack_info.RegionSize; i++)
		fprintf(fp, "sp[%u] = %#" PRIxPTR "\n", i, ((uintptr_t *)sp)[i]);

	fprintf(fp, "--- Stack dump end ---\n");

leave:
	if (fp)
		fclose(fp);

	return EXCEPTION_CONTINUE_SEARCH;
}

static int ILHOSDriver_Init(void)
{
	sys.excp_handler_handle = AddVectoredExceptionHandler(0, excp_handler);

	sys.perf_counter_freq.QuadPart = 0;
	QueryPerformanceFrequency(&sys.perf_counter_freq);
	if(!sys.perf_counter_freq.QuadPart)
		sys.perf_counter_freq.QuadPart = 1;
	return 0;
}

static int ILHOSDriver_PreDeinit(void)
{
	return 0;
}

static int ILHOSDriver_Deinit(void)
{
	RemoveVectoredExceptionHandler(sys.excp_handler_handle);
	return 0;
}

static enum lh_driver_os_caps ILHOSDriver_GetCaps(void)
{
	enum lh_driver_os_caps caps = LH_DRIVER_OS_CAP_ALIGNED_ALLOC
			| LH_DRIVER_OS_CAP_DELAY
			| LH_DRIVER_OS_CAP_ATOMIC
			| LH_DRIVER_OS_CAP_THREAD
			| LH_DRIVER_OS_CAP_SPINLOCK
			| LH_DRIVER_OS_CAP_MUTEX
			| LH_DRIVER_OS_CAP_COND;
	return caps;
}

static void *ILHOSDriver_MallocAligned(size_t alignment, size_t size)
{
	return _aligned_malloc(size, alignment);
}

static void ILHOSDriver_Free(void *ptr)
{
	_aligned_free(ptr);
}

static void ILHOSDriver_Delay(LHuint msec)
{
	Sleep(msec);
}

static void ILHOSDriver_DelayUSec(LHuint usec)
{
	LARGE_INTEGER init_pc, pc;

	if(!sys.perf_counter_freq.QuadPart)
		return;
	init_pc.QuadPart = 0;
	QueryPerformanceCounter(&init_pc);
	if(!init_pc.QuadPart)
		return;
	do {
		pc.QuadPart = 0;
		QueryPerformanceCounter(&pc);
	} while(pc.QuadPart && ((pc.QuadPart - init_pc.QuadPart) * 1000000) / sys.perf_counter_freq.QuadPart < usec);
}

static long ILHOSDriver_AtomicCounterGet(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);

	return InterlockedAddNoFence(&cnt->count, 0);
}

static long ILHOSDriver_AtomicCounterSet(struct lh_atomic_counter *lh_cnt, long val)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);

	InterlockedExchange(&cnt->count, val);
	return val;
}

static long ILHOSDriver_AtomicCounterInc(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);

	return InterlockedIncrement(&cnt->count);
}

static long ILHOSDriver_AtomicCounterDec(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);

	return InterlockedDecrement(&cnt->count);
}

static void *ILHOSDriver_AtomicPointerGet(struct lh_atomic_pointer *lh_pointer)
{
	union lh_atomic_pointer_blob *pointer = LH_CONTAINER_OF(lh_pointer, union lh_atomic_pointer_blob, base);

	return InterlockedCompareExchangePointerNoFence(&pointer->ptr, NULL, NULL);
}

static void *ILHOSDriver_AtomicPointerSet(struct lh_atomic_pointer *lh_pointer, void *val)
{
	union lh_atomic_pointer_blob *pointer = LH_CONTAINER_OF(lh_pointer, union lh_atomic_pointer_blob, base);

	InterlockedExchangePointer(&pointer->ptr, val);
	return val;
}

static DWORD WINAPI thread_wrap(LPVOID params)
{
	struct lh_thread_blob *thread = params;
	int ret = 0;

	ret = thread->func(thread->params);

	thread->is_running = LH_FALSE;
	return (DWORD)ret;
}

static struct lh_thread *ILHOSDriver_ThreadCreate(int (*func)(void *), void *params)
{
	struct lh_thread_blob *thread;

	thread = malloc(sizeof(*thread));
	if(!thread)
		return NULL;

	thread->func = func;
	thread->params = params;

	thread->is_launched = LH_TRUE;
	thread->is_running = LH_TRUE;

	thread->handle = CreateThread(NULL, 0, thread_wrap, thread, 0, NULL);
	if(!thread->handle) {
		free(thread);
		return NULL;
	}
	return &thread->base;
}

static int ILHOSDriver_ThreadWait(struct lh_thread *lh_thread, int *status)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	DWORD thread_status;
	int ret;

	if(!thread->is_launched)
		return 0;

	ret = WaitForSingleObject(thread->handle, INFINITE);
	if(ret != WAIT_OBJECT_0)
		return -1;
	ret = GetExitCodeThread(thread->handle, &thread_status);
	if(!ret && status)
		return -1;
	if(status)
		*status = (int)thread_status;

	thread->is_launched = LH_FALSE;
	return 0;
}

static LHbool ILHOSDriver_ThreadIsRunning(struct lh_thread *lh_thread)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	return thread->is_running;
}

static int ILHOSDriver_ThreadRelease(struct lh_thread *lh_thread)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	int ret;

	if(ILHOSDriver_ThreadIsRunning(lh_thread))
		return -1;

	ret = ILHOSDriver_ThreadWait(lh_thread, NULL);
	if(ret < 0)
		return ret;

	free(thread);
	return 0;
}

static int ILHOSDriver_ThreadDestroy(struct lh_thread *lh_thread)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	int ret;

	ret = TerminateThread(thread->handle, 0);
	if(!ret)
		return -1;

	free(lh_thread);
	return 0;
}

static struct lh_spinlock *ILHOSDriver_SpinCreate(void)
{
	struct lh_spinlock_blob *spinlock;

	spinlock = malloc(sizeof(*spinlock));
	if(!spinlock)
		return NULL;

	spinlock->lock = ILHOSDriver_MallocAligned(lh_next_pot_u32(sizeof(*spinlock->lock)), sizeof(*spinlock->lock));
	if(!spinlock->lock) {
		free(spinlock);
		return NULL;
	}
	*spinlock->lock = 0;

	return &spinlock->base;
}

static int ILHOSDriver_SpinLock(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);

	while(InterlockedCompareExchange(spinlock->lock, -1, 0) != 0)
		;
	return 0;
}

static int ILHOSDriver_SpinUnlock(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);

	InterlockedExchange(spinlock->lock, 0);
	return 0;
}

static int ILHOSDriver_SpinDestroy(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);

	ILHOSDriver_Free(spinlock->lock);
	free(spinlock);
	return 0;
}

static struct lh_mutex *ILHOSDriver_MutexCreate(void)
{
	struct lh_mutex_blob *mutex;

	mutex = malloc(sizeof(*mutex));
	if(!mutex)
		return NULL;

	/* Auto-reset, initially signaled */
	mutex->lock = CreateEventA(NULL, FALSE, TRUE, NULL);
	mutex->thread_id = (DWORD)-1;
	mutex->count = 0;

	return &mutex->base;
}

static int ILHOSDriver_MutexLock(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;
	DWORD cur_thread_id;

	cur_thread_id = GetCurrentThreadId();
	if(mutex->thread_id != cur_thread_id) {
		WaitForSingleObject(mutex->lock, INFINITE);
		mutex->thread_id = cur_thread_id;
	}
	mutex->count++;

	return 0;
}

static int ILHOSDriver_MutexUnlock(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;

	if(mutex->thread_id != GetCurrentThreadId()
			|| !mutex->count)
		return -1;
	if(mutex->count-- == 1) {
		mutex->thread_id = (DWORD)-1;
		SetEvent(mutex->lock);
	}

	return 0;
}

static int ILHOSDriver_MutexDestroy(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;

	CloseHandle(mutex->lock);
	free(mutex);

	return 0;
}

static struct lh_cond *ILHOSDriver_CondCreate(void)
{
	struct lh_cond_blob *cond;

	cond = malloc(sizeof(*cond));
	if(!cond)
		return NULL;

	/* Auto-reset, initially signaled */
	cond->count_lock = CreateEventA(NULL, FALSE, TRUE, NULL);
	if(!cond->count_lock)
		goto failed_count_lock;

	/* Auto-reset, initially non signaled */
	cond->awoken_evt = CreateEventA(NULL, FALSE, FALSE, NULL);
	if(!cond->awoken_evt)
		goto failed_awoken_evt;

	/* Auto-reset, initially non signaled */
	cond->evt = CreateEventA(NULL, FALSE, FALSE, NULL);
	if(!cond->evt)
		goto failed_evt;

	cond->count = 0;
	return &cond->base;

failed_evt:
	CloseHandle(cond->awoken_evt);
failed_awoken_evt:
	CloseHandle(cond->count_lock);
failed_count_lock:
	free(cond);
	return NULL;
}

static int ILHOSDriver_CondWait(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	DWORD ret;

	ret = WaitForSingleObject(cond->count_lock, INFINITE);
	if(ret != WAIT_OBJECT_0)
		return -1;
	cond->count++;
	SignalObjectAndWait(cond->count_lock, cond->evt, INFINITE, FALSE);
	if(ret != WAIT_OBJECT_0) {
		cond->count--;
		SetEvent(cond->count_lock);
		return -1;
	}
	SetEvent(cond->awoken_evt);
	return 0;
}

static int ILHOSDriver_CondWakeSingle(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	DWORD ret;

	ret = WaitForSingleObject(cond->count_lock, INFINITE);
	if(ret != WAIT_OBJECT_0)
		return -1;
	if(!cond->count)
		return 0;
	SetEvent(cond->evt);
	cond->count--;
	ret = WaitForSingleObject(cond->awoken_evt, INFINITE);
	if(ret != WAIT_OBJECT_0) {
		SetEvent(cond->count_lock);
		return -1;
	}
	SetEvent(cond->count_lock);
	return 0;
}

static int ILHOSDriver_CondWakeAll(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	DWORD ret;

	ret = WaitForSingleObject(cond->count_lock, INFINITE);
	if(ret != WAIT_OBJECT_0)
		return -1;
	while(cond->count > 0) {
		SetEvent(cond->evt);
		cond->count--;
		ret = WaitForSingleObject(cond->awoken_evt, INFINITE);
		if(ret != WAIT_OBJECT_0)
			break;
	}
	SetEvent(cond->count_lock);
	return 0;
}

static int ILHOSDriver_CondDestroy(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;

	CloseHandle(cond->count_lock);
	CloseHandle(cond->awoken_evt);
	CloseHandle(cond->evt);
	free(cond);
	return 0;
}

ILHOSDriver __lhOSDriver_windows = {
	.base = {
		.Name      = "windows",
		.Init      = ILHOSDriver_Init,
		.PreDeinit = ILHOSDriver_PreDeinit,
		.Deinit    = ILHOSDriver_Deinit
	},
	.GetCaps          = ILHOSDriver_GetCaps,
	.MallocAligned    = ILHOSDriver_MallocAligned,
	.Free             = ILHOSDriver_Free,
	.Delay            = ILHOSDriver_Delay,
	.DelayUSec        = ILHOSDriver_DelayUSec,
	.AtomicCounterGet = ILHOSDriver_AtomicCounterGet,
	.AtomicCounterSet = ILHOSDriver_AtomicCounterSet,
	.AtomicCounterInc = ILHOSDriver_AtomicCounterInc,
	.AtomicCounterDec = ILHOSDriver_AtomicCounterDec,
	.AtomicPointerGet = ILHOSDriver_AtomicPointerGet,
	.AtomicPointerSet = ILHOSDriver_AtomicPointerSet,
	.ThreadCreate     = ILHOSDriver_ThreadCreate,
	.ThreadWait       = ILHOSDriver_ThreadWait,
	.ThreadIsRunning  = ILHOSDriver_ThreadIsRunning,
	.ThreadRelease    = ILHOSDriver_ThreadRelease,
	.ThreadDestroy    = ILHOSDriver_ThreadDestroy,
	.SpinCreate       = ILHOSDriver_SpinCreate,
	.SpinLock         = ILHOSDriver_SpinLock,
	.SpinUnlock       = ILHOSDriver_SpinUnlock,
	.SpinDestroy      = ILHOSDriver_SpinDestroy,
	.MutexCreate      = ILHOSDriver_MutexCreate,
	.MutexLock        = ILHOSDriver_MutexLock,
	.MutexUnlock      = ILHOSDriver_MutexUnlock,
	.MutexDestroy     = ILHOSDriver_MutexDestroy,
	.CondCreate       = ILHOSDriver_CondCreate,
	.CondWait         = ILHOSDriver_CondWait,
	.CondWakeSingle   = ILHOSDriver_CondWakeSingle,
	.CondWakeAll      = ILHOSDriver_CondWakeAll,
	.CondDestroy      = ILHOSDriver_CondDestroy
};
