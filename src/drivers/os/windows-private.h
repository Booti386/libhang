/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_OS_WINDOWS_PRIVATE_H
#define LIBHANG_INTERNAL_OS_WINDOWS_PRIVATE_H 1

#include <windows.h>

#include <libhang.h>

struct lh_sys {
	void *excp_handler_handle;
	LARGE_INTEGER perf_counter_freq;
};

union lh_atomic_counter_blob {
	struct lh_atomic_counter base;
	long count;
};

union lh_atomic_pointer_blob {
	struct lh_atomic_pointer base;
	void *ptr;
};

struct lh_thread_blob {
	struct lh_thread base;

	HANDLE handle;
	int (*func)(void *);
	void *params;
	LHbool is_running;
	LHbool is_launched;
};

struct lh_spinlock_blob {
	struct lh_spinlock base;

	LONG *lock;
};

struct lh_mutex_blob {
	struct lh_mutex base;

	HANDLE lock;
	DWORD thread_id;
	LHuint count;
};

struct lh_cond_blob {
	struct lh_cond base;

	HANDLE count_lock;
	HANDLE awoken_evt;
	HANDLE evt;
	LHuint count;
};

#endif /* LIBHANG_INTERNAL_OS_WINDOWS_PRIVATE_H */
