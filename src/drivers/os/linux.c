/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#if defined(__STDC__) && __STDC__
#  if __STDC_VERSION__ >= 201112L
#    ifndef __STDC_NO_ATOMICS__
#      define ISOC_ATOMICS 1
#    endif
#  endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sched.h> /* sched_yield */
#include <time.h>
#include <errno.h>

#ifdef ISOC_ATOMICS
#  include <stdatomic.h>
#endif

#include <libhang.h>
#include "driver_iface.h"
#include "linux-private.h"

static int ILHOSDriver_Init(void)
{
	return 0;
}

static int ILHOSDriver_PreDeinit(void)
{
	return 0;
}

static int ILHOSDriver_Deinit(void)
{
	return 0;
}

static enum lh_driver_os_caps ILHOSDriver_GetCaps(void)
{
	enum lh_driver_os_caps caps = LH_DRIVER_OS_CAP_ALIGNED_ALLOC
			| LH_DRIVER_OS_CAP_DELAY
			| LH_DRIVER_OS_CAP_THREAD
			| LH_DRIVER_OS_CAP_SPINLOCK
			| LH_DRIVER_OS_CAP_MUTEX
			| LH_DRIVER_OS_CAP_COND;

#ifdef ISOC_ATOMICS
	caps |= LH_DRIVER_OS_CAP_ATOMIC;
#endif /* ISOC_ATOMICS */

	return caps;
}

static void *ILHOSDriver_MallocAligned(size_t alignment, size_t size)
{
	int ret;
	void *ptr = NULL;

	if(alignment < sizeof(void *))
		alignment = sizeof(void *);

	ret = posix_memalign(&ptr, alignment, size);
	if(ret == 0)
		return ptr;
	return NULL;
}

static void ILHOSDriver_Free(void *ptr)
{
	free(ptr);
}

static void delay_helper(LHuint sec, LHuint nsec) {
	struct timespec req, remain;

	remain.tv_sec  = sec;
	remain.tv_nsec = nsec;

	do {
		req.tv_sec  = remain.tv_sec;
		req.tv_nsec = remain.tv_nsec;
		remain.tv_sec  = 0;
		remain.tv_nsec = 0;
		errno = 0;
	} while(nanosleep(&req, &remain) == -1 && errno == EINTR);
}

static void ILHOSDriver_Delay(LHuint msec)
{
	delay_helper(msec / 1000, msec % 1000 * 1000000);
}

static void ILHOSDriver_DelayUSec(LHuint usec)
{
	delay_helper(usec / 1000000, usec % 1000000 * 1000);
}

#ifdef ISOC_ATOMICS

static long ILHOSDriver_AtomicCounterGet(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);

	return atomic_load(&cnt->cnt);
}

static long ILHOSDriver_AtomicCounterSet(struct lh_atomic_counter *lh_cnt, long val)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);

	atomic_store(&cnt->cnt, val);
	return val;
}

static long ILHOSDriver_AtomicCounterInc(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);

	return atomic_fetch_add(&cnt->cnt, 1) + 1;
}

static long ILHOSDriver_AtomicCounterDec(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);

	return atomic_fetch_sub(&cnt->cnt, 1) - 1;
}

static void *ILHOSDriver_AtomicPointerGet(struct lh_atomic_pointer *lh_ptr)
{
	union lh_atomic_pointer_blob *ptr = LH_CONTAINER_OF(lh_ptr, union lh_atomic_pointer_blob, base);

	return atomic_load(&ptr->ptr);
}

static void *ILHOSDriver_AtomicPointerSet(struct lh_atomic_pointer *lh_ptr, void *val)
{
	union lh_atomic_pointer_blob *pointer = LH_CONTAINER_OF(lh_ptr, union lh_atomic_pointer_blob, base);

	atomic_store(&pointer->ptr, val);
	return val;
}

#endif /* ISOC_ATOMICS */

static void *thread_wrap(void *params)
{
	struct lh_thread_blob *thread = params;
	int unused;

	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &unused);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &unused);
	thread->ret = thread->func(thread->params);

	thread->is_running = LH_FALSE;
	return NULL;
}

static struct lh_thread *ILHOSDriver_ThreadCreate(int (*func)(void *), void *params)
{
	struct lh_thread_blob *thread;
	int ret;

	thread = malloc(sizeof(*thread));
	if(!thread)
		return NULL;
	thread->func = func;
	thread->params = params;
	thread->ret = 0;
	thread->is_launched = LH_TRUE;
	thread->is_running = LH_TRUE;
	ret = pthread_create(&thread->id, NULL, thread_wrap, thread);
	if(ret) {
		free(thread);
		return NULL;
	}
	return &thread->base;
}

static int ILHOSDriver_ThreadWait(struct lh_thread *lh_thread, int *status)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	int ret;

	if(!thread->is_launched)
		return 0;
	ret = pthread_join(thread->id, NULL);
	if(ret)
		return -1;
	if(status)
		*status = thread->ret;
	thread->is_launched = LH_FALSE;
	return 0;
}

static LHbool ILHOSDriver_ThreadIsRunning(struct lh_thread *lh_thread)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;

	return thread->is_running;
}

static int ILHOSDriver_ThreadRelease(struct lh_thread *lh_thread)
{
	int ret;

	if(ILHOSDriver_ThreadIsRunning(lh_thread))
		return -1;
	ret = ILHOSDriver_ThreadWait(lh_thread, NULL);
	if(ret < 0)
		return ret;
	free(lh_thread);
	return 0;
}

static int ILHOSDriver_ThreadDestroy(struct lh_thread *lh_thread)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	int ret;

	ret = pthread_cancel(thread->id);
	if(ret)
		return -1;
	free(thread);
	return 0;
}

static struct lh_spinlock *ILHOSDriver_SpinCreate(void)
{
	struct lh_spinlock_blob *spinlock;
	int ret;

	spinlock = malloc(sizeof(*spinlock));
	if(!spinlock)
		return NULL;

	ret = pthread_spin_init(&spinlock->lock, PTHREAD_PROCESS_PRIVATE);
	if(ret) {
		free(spinlock);
		return NULL;
	}
	return &spinlock->base;
}

static int ILHOSDriver_SpinLock(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);
	int ret;

	ret = pthread_spin_lock(&spinlock->lock);
	if(ret)
		return -1;
	return 0;
}

static int ILHOSDriver_SpinUnlock(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);
	int ret;

	ret = pthread_spin_unlock(&spinlock->lock);
	if(ret)
		return -1;
	return 0;
}

static int ILHOSDriver_SpinDestroy(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);
	int ret;

	ret = pthread_spin_destroy(&spinlock->lock);
	if(ret)
		return -1;
	free(spinlock);
	return 0;
}

static struct lh_mutex *ILHOSDriver_MutexCreate(void)
{
	struct lh_mutex_blob *mutex;
	int ret;
	pthread_mutexattr_t attr;

	ret = pthread_mutexattr_init(&attr);
	if(ret)
		return NULL;
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	if(ret)
		return NULL;
	mutex = malloc(sizeof(*mutex));
	if(!mutex)
		return NULL;
	ret = pthread_mutex_init(&mutex->lock, &attr);
	if(ret) {
		free(mutex);
		return NULL;
	}
	pthread_mutexattr_destroy(&attr);
	mutex->count = 0;
	return &mutex->base;
}

static int ILHOSDriver_MutexLock(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;
	int ret;

	ret = pthread_mutex_lock(&mutex->lock);
	if(ret)
		return -1;
	mutex->count++;
	return 0;
}

static int ILHOSDriver_MutexUnlock(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;
	int ret;
	volatile unsigned int count;

	if(mutex->count)
		mutex->count--;
	count = mutex->count;
	ret = pthread_mutex_unlock(&mutex->lock);
	if(ret)
		return -1;
	if(count)
		sched_yield();
	return 0;
}

static int ILHOSDriver_MutexDestroy(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;
	int ret;

	ret = pthread_mutex_destroy(&mutex->lock);
	if(ret)
		return -1;
	free(mutex);
	return 0;
}

static struct lh_cond *ILHOSDriver_CondCreate(void)
{
	struct lh_cond_blob *cond;

	cond = malloc(sizeof(*cond));
	if(!cond)
		return NULL;
	if(pthread_mutex_init(&cond->wait_lock, NULL))
		goto failed_wait_lock;
	if(pthread_mutex_init(&cond->wake_lock, NULL))
		goto failed_wake_lock;
	if(pthread_cond_init(&cond->cond, NULL))
		goto failed_cond;
	if(pthread_cond_init(&cond->awoken_cond, NULL))
		goto failed_awoken_cond;
	cond->wake_type = LH_COND_WAKE_MISSED;
	cond->count = 0;
	return &cond->base;

failed_awoken_cond:
	pthread_cond_destroy(&cond->cond);
failed_cond:
	pthread_mutex_destroy(&cond->wake_lock);
failed_wake_lock:
	pthread_mutex_destroy(&cond->wait_lock);
failed_wait_lock:
	free(cond);
	return NULL;
}

static int ILHOSDriver_CondWait(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	int ret;
	int leave = 0;

	if(pthread_mutex_lock(&cond->wake_lock))
		return -1;
	cond->count++;
	ret = pthread_mutex_lock(&cond->wait_lock);
	pthread_mutex_unlock(&cond->wake_lock);
	if(ret)
		return -1;
	while(!leave) {
		ret = pthread_cond_wait(&cond->cond, &cond->wait_lock);
		if(ret)
			break;

		switch(cond->wake_type)
		{
			case LH_COND_WAKE_MISSED:
				break;

			case LH_COND_WAKE_SINGLE:
				cond->wake_type = LH_COND_WAKE_MISSED;
				/* Fall through. */

			case LH_COND_WAKE_ALL:
				cond->count--;
				if(!cond->count || cond->wake_type == LH_COND_WAKE_MISSED)
					pthread_cond_signal(&cond->awoken_cond);
				leave = 1;
				break;
		}
	}
	pthread_mutex_unlock(&cond->wait_lock);

	if(ret)
		return -1;
	return 0;
}

static int ILHOSDriver_CondWakeSingle(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	int ret = 0;

	if(pthread_mutex_lock(&cond->wake_lock))
		return -1;
	if(cond->count) {
		ret = pthread_mutex_lock(&cond->wait_lock);
		if(ret == 0) {
			cond->wake_type = LH_COND_WAKE_SINGLE;
			ret = pthread_cond_signal(&cond->cond);
			if(ret == 0)
				pthread_cond_wait(&cond->awoken_cond, &cond->wait_lock);
			pthread_mutex_unlock(&cond->wait_lock);
		}
	}
	pthread_mutex_unlock(&cond->wake_lock);

	if(ret)
		return -1;
	return 0;
}

static int ILHOSDriver_CondWakeAll(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	int ret = 0;

	if(pthread_mutex_lock(&cond->wake_lock))
		return -1;
	if(cond->count) {
		ret = pthread_mutex_lock(&cond->wait_lock);
		if(ret == 0) {
			cond->wake_type = LH_COND_WAKE_ALL;
			ret = pthread_cond_broadcast(&cond->cond);
			if(ret == 0)
				pthread_cond_wait(&cond->awoken_cond, &cond->wait_lock);
			pthread_mutex_unlock(&cond->wait_lock);
		}
	}
	pthread_mutex_unlock(&cond->wake_lock);

	if(ret)
		return -1;
	return 0;
}

static int ILHOSDriver_CondDestroy(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;

	pthread_mutex_destroy(&cond->wait_lock);
	pthread_mutex_destroy(&cond->wake_lock);
	pthread_cond_destroy(&cond->cond);
	pthread_cond_destroy(&cond->awoken_cond);
	free(cond);
	return 0;
}

ILHOSDriver __lhOSDriver_linux = {
	.base = {
		.Name      = "linux",
		.Init      = ILHOSDriver_Init,
		.PreDeinit = ILHOSDriver_PreDeinit,
		.Deinit    = ILHOSDriver_Deinit
	},
	.GetCaps          = ILHOSDriver_GetCaps,
	.MallocAligned    = ILHOSDriver_MallocAligned,
	.Free             = ILHOSDriver_Free,
	.Delay            = ILHOSDriver_Delay,
	.DelayUSec        = ILHOSDriver_DelayUSec,
#ifdef ISOC_ATOMICS
	.AtomicCounterGet = ILHOSDriver_AtomicCounterGet,
	.AtomicCounterSet = ILHOSDriver_AtomicCounterSet,
	.AtomicCounterInc = ILHOSDriver_AtomicCounterInc,
	.AtomicCounterDec = ILHOSDriver_AtomicCounterDec,
	.AtomicPointerGet = ILHOSDriver_AtomicPointerGet,
	.AtomicPointerSet = ILHOSDriver_AtomicPointerSet,
#endif /* ISOC_ATOMICS */
	.ThreadCreate     = ILHOSDriver_ThreadCreate,
	.ThreadWait       = ILHOSDriver_ThreadWait,
	.ThreadIsRunning  = ILHOSDriver_ThreadIsRunning,
	.ThreadRelease    = ILHOSDriver_ThreadRelease,
	.ThreadDestroy    = ILHOSDriver_ThreadDestroy,
	.SpinCreate       = ILHOSDriver_SpinCreate,
	.SpinLock         = ILHOSDriver_SpinLock,
	.SpinUnlock       = ILHOSDriver_SpinUnlock,
	.SpinDestroy      = ILHOSDriver_SpinDestroy,
	.MutexCreate      = ILHOSDriver_MutexCreate,
	.MutexLock        = ILHOSDriver_MutexLock,
	.MutexUnlock      = ILHOSDriver_MutexUnlock,
	.MutexDestroy     = ILHOSDriver_MutexDestroy,
	.CondCreate       = ILHOSDriver_CondCreate,
	.CondWait         = ILHOSDriver_CondWait,
	.CondWakeSingle   = ILHOSDriver_CondWakeSingle,
	.CondWakeAll      = ILHOSDriver_CondWakeAll,
	.CondDestroy      = ILHOSDriver_CondDestroy
};
