/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_OS_LINUX_PRIVATE_H
#define LIBHANG_INTERNAL_OS_LINUX_PRIVATE_H 1

#include <pthread.h>

#include <libhang.h>

enum lh_cond_wake_type {
	LH_COND_WAKE_MISSED = 0,
	LH_COND_WAKE_SINGLE,
	LH_COND_WAKE_ALL
};

union lh_atomic_counter_blob {
	struct lh_atomic_counter base;
#ifdef ISOC_ATOMICS
	atomic_long cnt;
#else
	long cnt;
#endif
};

union lh_atomic_pointer_blob {
	struct lh_atomic_counter base;
#ifdef ISOC_ATOMICS
	_Atomic void *ptr;
#else
	void *ptr;
#endif
};

struct lh_thread_blob {
	struct lh_thread base;

	pthread_t id;
	int (*func)(void *);
	void *params;
	int ret;
	LHbool is_running;
	LHbool is_launched;
};

struct lh_spinlock_blob {
	struct lh_spinlock base;

	pthread_spinlock_t lock;
};

struct lh_mutex_blob {
	struct lh_mutex base;

	pthread_mutex_t lock;
	unsigned int count;
};

struct lh_cond_blob {
	struct lh_cond base;

	pthread_mutex_t wait_lock;
	pthread_mutex_t wake_lock;
	pthread_cond_t cond;
	pthread_cond_t awoken_cond;
	enum lh_cond_wake_type wake_type;
	LHuint count;
};

#endif /* LIBHANG_INTERNAL_OS_LINUX_PRIVATE_H */
