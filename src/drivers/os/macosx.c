/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define __lhOSDriver_linux static __lhOSDriver_linux

#include "linux.c"

ILHOSDriver __lhOSDriver_macosx = {
	.base = {
		.Name      = "macosx",
		.Init      = ILHOSDriver_Init,
		.PreDeinit = ILHOSDriver_PreDeinit,
		.Deinit    = ILHOSDriver_Deinit
	},
	.GetCaps          = ILHOSDriver_GetCaps,
	.MallocAligned    = ILHOSDriver_MallocAligned,
	.Free             = ILHOSDriver_Free,
	.Delay            = ILHOSDriver_Delay,
	.DelayUSec        = ILHOSDriver_DelayUSec,
	.AtomicCounterGet = ILHOSDriver_AtomicCounterGet,
	.AtomicCounterSet = ILHOSDriver_AtomicCounterSet,
	.AtomicCounterInc = ILHOSDriver_AtomicCounterInc,
	.AtomicCounterDec = ILHOSDriver_AtomicCounterDec,
	.AtomicPointerGet = ILHOSDriver_AtomicPointerGet,
	.AtomicPointerSet = ILHOSDriver_AtomicPointerSet,
	.ThreadCreate     = ILHOSDriver_ThreadCreate,
	.ThreadWait       = ILHOSDriver_ThreadWait,
	.ThreadIsRunning  = ILHOSDriver_ThreadIsRunning,
	.ThreadRelease    = ILHOSDriver_ThreadRelease,
	.ThreadDestroy    = ILHOSDriver_ThreadDestroy,
	.SpinCreate       = ILHOSDriver_SpinCreate,
	.SpinLock         = ILHOSDriver_SpinLock,
	.SpinUnlock       = ILHOSDriver_SpinUnlock,
	.SpinDestroy      = ILHOSDriver_SpinDestroy,
	.MutexCreate      = ILHOSDriver_MutexCreate,
	.MutexLock        = ILHOSDriver_MutexLock,
	.MutexUnlock      = ILHOSDriver_MutexUnlock,
	.MutexDestroy     = ILHOSDriver_MutexDestroy,
	.CondCreate       = ILHOSDriver_CondCreate,
	.CondWait         = ILHOSDriver_CondWait,
	.CondWakeSingle   = ILHOSDriver_CondWakeSingle,
	.CondWakeAll      = ILHOSDriver_CondWakeAll,
	.CondDestroy      = ILHOSDriver_CondDestroy
};
