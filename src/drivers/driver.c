/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>

#include <libhang.h>
#include "driver_iface.h"
#include "driver.h"

#include "core.h"

#ifdef CONFIG_OS_LINUX
#  include "os/linux.h"
#endif
#ifdef CONFIG_OS_WINDOWS
#  include "os/windows.h"
#endif
#ifdef CONFIG_OS_MACOSX
#  include "os/macosx.h"
#endif

#ifdef CONFIG_GL_GLX_XCB
#  include "gl/glx_xcb.h"
#endif
#ifdef CONFIG_GL_WGL_WIN
#  include "gl/wgl_win.h"
#endif

static ILHOSDriver *os_drivers[] = {
#ifdef CONFIG_OS_LINUX
	&__lhOSDriver_linux,
#endif
#ifdef CONFIG_OS_WINDOWS
	&__lhOSDriver_windows,
#endif
#ifdef CONFIG_OS_MACOSX
	&__lhOSDriver_macosx,
#endif
	NULL
};

static ILHGLDriver *gl_drivers[] = {
#ifdef CONFIG_GL_GLX_XCB
	&__lhGLDriver_glx_xcb,
#endif
#ifdef CONFIG_GL_WGL_WIN
	&__lhGLDriver_wgl_win,
#endif
	NULL
};

static struct {
	struct LinkedList threads;
	struct LinkedList spins;
	struct LinkedList mutexes;
	struct LinkedList conds;
	struct LinkedList windows;
	struct lh_mutex *threads_lock;
	struct lh_mutex *spins_lock;
	struct lh_mutex *mutexes_lock;
	struct lh_mutex *conds_lock;
	struct lh_mutex *windows_lock;
} sys;

static ILHOSDriver *os_driver_impl = NULL;
static ILHGLDriver *gl_driver_impl = NULL;
static LHbool init_done = LH_FALSE;

static int probe_drivers(ILHDriver **drivers, ILHDriver **dst)
{
	int ret = -1;

	*dst = NULL;

	for(int i = 0; drivers[i]; i++) {
		ILHDriver *cur_driver = drivers[i];
		ret = cur_driver->Init();

		if(ret == 0) {
			*dst = cur_driver;
			break;
		}
	}

	return ret;
}

int lhInit(void)
{
	int ret;

	if(init_done)
		return 0;
	init_done = LH_TRUE;

	os_driver_impl = NULL;
	gl_driver_impl = NULL;
	list_init(&sys.threads);
	list_init(&sys.spins);
	list_init(&sys.mutexes);
	list_init(&sys.conds);
	list_init(&sys.windows);
	sys.threads_lock = NULL;
	sys.spins_lock = NULL;
	sys.mutexes_lock = NULL;
	sys.conds_lock = NULL;
	sys.windows_lock = NULL;

	ret = probe_drivers((ILHDriver **)os_drivers, (ILHDriver **)&os_driver_impl);
	if(ret < 0) {
		fprintf(stderr, "libhang: No suitable OS driver found, aborting.\n");
		goto err;
	}
	fprintf(stdout, "libhang: Using OS driver \"%s\".\n", os_driver_impl->base.Name);

	ret = -1;
	sys.threads_lock = os_driver_impl->MutexCreate();
	if(!sys.threads_lock) {
		fprintf(stderr, "libhang: Unable to create threads mutex.\n");
		goto err;
	}
	sys.spins_lock = os_driver_impl->MutexCreate();
	if(!sys.spins_lock) {
		fprintf(stderr, "libhang: Unable to create spins mutex.\n");
		goto err;
	}
	sys.mutexes_lock = os_driver_impl->MutexCreate();
	if(!sys.mutexes_lock) {
		fprintf(stderr, "libhang: Unable to create mutexes mutex.\n");
		goto err;
	}
	sys.conds_lock = os_driver_impl->MutexCreate();
	if(!sys.conds_lock) {
		fprintf(stderr, "libhang: Unable to create conds mutex.\n");
		goto err;
	}

	ret = probe_drivers((ILHDriver **)gl_drivers, (ILHDriver **)&gl_driver_impl);
	if(ret < 0)
		fprintf(stderr, "libhang: No suitable GL driver found, 3D API will not be available.\n");
	else
		fprintf(stdout, "libhang: Using GL driver \"%s\".\n", gl_driver_impl->base.Name);

	ret = -1;
	sys.windows_lock = os_driver_impl->MutexCreate();
	if(!sys.windows_lock) {
		fprintf(stderr, "libhang: Unable to create windows mutex.\n");
		goto err;
	}
	return 0;

err:
	lhDeinit();
	return ret;
}

typedef int (*purge_sys_list_func)(void *);
static void purge_sys_list(struct LinkedList *list, struct lh_mutex *m, purge_sys_list_func destroy_func) {
	struct LinkedListIterator it;

	if(!m)
		return;

	os_driver_impl->MutexLock(m);
	list_it_init(&it, list);
	for(struct lh_window *elem = list_it_get_cur(&it); elem; elem = list_it_get_next(&it)) {
		list_remove(list, list_it_get_cur_id(&it));
		destroy_func(elem);
	}
	os_driver_impl->MutexUnlock(m);
	os_driver_impl->MutexDestroy(m);
}

void lhDeinit(void)
{
	if(!init_done)
		return;
	init_done = LH_FALSE;

	if(gl_driver_impl) {
		gl_driver_impl->base.PreDeinit();
		purge_sys_list(&sys.windows, sys.windows_lock, (purge_sys_list_func)gl_driver_impl->DestroyWindow);
		gl_driver_impl->base.Deinit();
	}

	if(os_driver_impl) {
		os_driver_impl->base.PreDeinit();
		purge_sys_list(&sys.threads, sys.threads_lock, (purge_sys_list_func)os_driver_impl->ThreadDestroy);
		purge_sys_list(&sys.spins, sys.spins_lock, (purge_sys_list_func)os_driver_impl->SpinDestroy);
		purge_sys_list(&sys.mutexes, sys.mutexes_lock, (purge_sys_list_func)os_driver_impl->MutexDestroy);
		purge_sys_list(&sys.conds, sys.conds_lock, (purge_sys_list_func)os_driver_impl->CondDestroy);
		os_driver_impl->base.Deinit();
	}
}

enum lh_driver_os_caps lhGetOSCaps(void)
{
	return os_driver_impl->GetCaps();
}

void *lhMallocAligned(size_t alignment, size_t size)
{
	void *real_ptr;
	void **real_ptr_ptr;
	void *ptr;
	uintptr_t mask;

	/* Check that size is > 0 */
	if(size <= 0)
		return NULL;

	/* Check that alignment is a power of two */
	if((alignment & (alignment - 1)) != 0)
		return NULL;

	/* Because we are kind */
	if(!alignment)
		alignment = 1;

	/* Call the OS driver if supported */
	if(os_driver_impl->MallocAligned)
		return os_driver_impl->MallocAligned(alignment, size);

	real_ptr = malloc(sizeof(*real_ptr_ptr) + (alignment - 1) + size);
	mask = ~(uintptr_t)(alignment - 1);
	ptr = (void *)((((uintptr_t)real_ptr) + sizeof(*real_ptr_ptr) + alignment - 1) & mask);
	real_ptr_ptr = ptr;
	real_ptr_ptr[-1] = real_ptr;
	return ptr;
}

void lhFree(void *ptr)
{
	void *real_ptr;
	void **real_ptr_ptr;

	if(!ptr)
		return;

	if(os_driver_impl->Free) {
		os_driver_impl->Free(ptr);
		return;
	}

	real_ptr_ptr = ptr;
	real_ptr = real_ptr_ptr[-1];
	free(real_ptr);
}

void lhDelay(LHuint msec)
{
	os_driver_impl->Delay(msec);
}

void lhDelayUSec(LHuint usec)
{
	os_driver_impl->DelayUSec(usec);
}

long lhAtomicCounterGet(struct lh_atomic_counter *cnt)
{
	return os_driver_impl->AtomicCounterGet(cnt);
}

long lhAtomicCounterSet(struct lh_atomic_counter *cnt, long val)
{
	return os_driver_impl->AtomicCounterSet(cnt, val);
}

long lhAtomicCounterInc(struct lh_atomic_counter *cnt)
{
	return os_driver_impl->AtomicCounterInc(cnt);
}

long lhAtomicCounterDec(struct lh_atomic_counter *cnt)
{
	return os_driver_impl->AtomicCounterDec(cnt);
}

void *lhAtomicPointerGet(struct lh_atomic_pointer *pointer)
{
	return os_driver_impl->AtomicPointerGet(pointer);
}

void *lhAtomicPointerSet(struct lh_atomic_pointer *pointer, void *val)
{
	return os_driver_impl->AtomicPointerSet(pointer, val);
}

struct lh_thread *lhThreadCreate(int (*func)(void *), void *params)
{
	struct lh_thread *thread = os_driver_impl->ThreadCreate(func, params);
	if(!thread)
		return NULL;

	os_driver_impl->MutexLock(sys.threads_lock);
	thread->id = list_push(&sys.threads, thread);
	os_driver_impl->MutexUnlock(sys.threads_lock);
	return thread;
}

int lhThreadWait(struct lh_thread *thread, int *status)
{
	return os_driver_impl->ThreadWait(thread, status);
}

LHbool lhThreadIsRunning(struct lh_thread *thread)
{
	return os_driver_impl->ThreadIsRunning(thread);
}

int lhThreadRelease(struct lh_thread *thread)
{
	LHuint id = thread->id;
	int ret;

	ret = os_driver_impl->ThreadRelease(thread);
	if(ret < 0)
		return ret;

	os_driver_impl->MutexLock(sys.threads_lock);
	list_remove(&sys.threads, id);
	os_driver_impl->MutexUnlock(sys.threads_lock);
	return 0;
}

int lhThreadDestroy(struct lh_thread *thread)
{
	LHuint id = thread->id;
	int ret;

	ret = os_driver_impl->ThreadDestroy(thread);
	if(ret < 0)
		return ret;

	os_driver_impl->MutexLock(sys.threads_lock);
	list_remove(&sys.threads, id);
	os_driver_impl->MutexUnlock(sys.threads_lock);
	return 0;
}

struct lh_spinlock *lhSpinCreate(void)
{
	struct lh_spinlock *spin = os_driver_impl->SpinCreate();

	os_driver_impl->MutexLock(sys.spins_lock);
	spin->id = list_push(&sys.spins, spin);
	os_driver_impl->MutexUnlock(sys.spins_lock);
	return spin;
}

int lhSpinLock(struct lh_spinlock *spin)
{
	return os_driver_impl->SpinLock(spin);
}

int lhSpinUnlock(struct lh_spinlock *spin)
{
	return os_driver_impl->SpinUnlock(spin);
}

int lhSpinDestroy(struct lh_spinlock *spin)
{
	LHuint id = spin->id;
	int ret;

	ret = os_driver_impl->SpinDestroy(spin);
	if(ret < 0)
		return ret;

	os_driver_impl->MutexLock(sys.spins_lock);
	list_remove(&sys.spins, id);
	os_driver_impl->MutexUnlock(sys.spins_lock);
	return 0;
}

struct lh_mutex *lhMutexCreate(void)
{
	struct lh_mutex *mutex = os_driver_impl->MutexCreate();

	os_driver_impl->MutexLock(sys.mutexes_lock);
	mutex->id = list_push(&sys.mutexes, mutex);
	os_driver_impl->MutexUnlock(sys.mutexes_lock);
	return mutex;
}

int lhMutexLock(struct lh_mutex *mutex)
{
	return os_driver_impl->MutexLock(mutex);
}

int lhMutexUnlock(struct lh_mutex *mutex)
{
	return os_driver_impl->MutexUnlock(mutex);
}

int lhMutexDestroy(struct lh_mutex *mutex)
{
	LHuint id = mutex->id;
	int ret;

	ret = os_driver_impl->MutexDestroy(mutex);
	if(ret < 0)
		return ret;

	os_driver_impl->MutexLock(sys.mutexes_lock);
	list_remove(&sys.mutexes, id);
	os_driver_impl->MutexUnlock(sys.mutexes_lock);
	return 0;
}

struct lh_cond *lhCondCreate(void)
{
	struct lh_cond *cond = os_driver_impl->CondCreate();

	os_driver_impl->MutexLock(sys.conds_lock);
	cond->id = list_push(&sys.conds, cond);
	os_driver_impl->MutexUnlock(sys.conds_lock);
	return cond;
}

int lhCondWait(struct lh_cond *cond)
{
	return os_driver_impl->CondWait(cond);
}

int lhCondWakeSingle(struct lh_cond *cond)
{
	return os_driver_impl->CondWakeSingle(cond);
}

int lhCondWakeAll(struct lh_cond *cond)
{
	return os_driver_impl->CondWakeAll(cond);
}

int lhCondDestroy(struct lh_cond *cond)
{
	LHuint id = cond->id;
	int ret;

	ret = os_driver_impl->CondDestroy(cond);
	if(ret < 0)
		return ret;

	os_driver_impl->MutexLock(sys.conds_lock);
	list_remove(&sys.conds, id);
	os_driver_impl->MutexUnlock(sys.conds_lock);
	return 0;
}

#define LH_GL_SET_CORE_PTR(fn, type) \
	{ \
		type proc = (type)gl_driver_impl->GetGLProc("gl" #fn); \
		if(!proc) \
			proc = gl ## fn; \
		gl->fn = proc; \
	}

struct lh_window *lhCreateWindow(const char *title, int x, int y, LHuint width, LHuint height, int depth, LHbool dbl_buf)
{
	struct lh_window *window;
	struct lh_gl_funcs *gl;

	window = gl_driver_impl->CreateWindow(title, x, y, width, height, depth, dbl_buf);
	if(!window)
		return NULL;
	window->core = NULL;
	gl = &window->gl;

	/* Initialize mutex for per-thread context sync. */
	window->graphic_ctx_mutex = lhMutexCreate();

	gl_driver_impl->AcquireGraphicCtx(window);

	/* Retrieve GL function pointers */
	LH_GL_SET_CORE_PTR(GetString, PFNGLGETSTRINGPROC);
	LH_GL_SET_CORE_PTR(CullFace, PFNGLCULLFACEPROC);
	LH_GL_SET_CORE_PTR(FrontFace, PFNGLFRONTFACEPROC);
	LH_GL_SET_CORE_PTR(BindTexture, PFNGLBINDTEXTUREPROC);
	LH_GL_SET_CORE_PTR(BlendFunc, PFNGLBLENDFUNCPROC);
	LH_GL_SET_CORE_PTR(Clear, PFNGLCLEARPROC);
	LH_GL_SET_CORE_PTR(ClearColor, PFNGLCLEARCOLORPROC);
	LH_GL_SET_CORE_PTR(DepthFunc, PFNGLDEPTHFUNCPROC);
	LH_GL_SET_CORE_PTR(Disable, PFNGLDISABLEPROC);
	LH_GL_SET_CORE_PTR(DrawBuffer, PFNGLDRAWBUFFERPROC);
	LH_GL_SET_CORE_PTR(ReadBuffer, PFNGLREADBUFFERPROC);
	LH_GL_SET_CORE_PTR(ReadPixels, PFNGLREADPIXELSPROC);
	LH_GL_SET_CORE_PTR(DrawElements, PFNGLDRAWELEMENTSPROC);
	LH_GL_SET_CORE_PTR(Enable, PFNGLENABLEPROC);
	LH_GL_SET_CORE_PTR(GenTextures, PFNGLGENTEXTURESPROC);
	LH_GL_SET_CORE_PTR(PixelStorei, PFNGLPIXELSTOREIPROC);
	LH_GL_SET_CORE_PTR(TexImage2D, PFNGLTEXIMAGE2DPROC);
	LH_GL_SET_CORE_PTR(TexParameteri, PFNGLTEXPARAMETERIPROC);
	LH_GL_SET_CORE_PTR(DeleteTextures, PFNGLDELETETEXTURESPROC);

	gl->GenBuffers = (PFNGLGENBUFFERSPROC)gl_driver_impl->GetGLProc("glGenBuffers");
	gl->BufferData = (PFNGLBUFFERDATAPROC)gl_driver_impl->GetGLProc("glBufferData");
	gl->BufferSubData = (PFNGLBUFFERSUBDATAPROC)gl_driver_impl->GetGLProc("glBufferSubData");
	gl->MapBufferRange = (PFNGLMAPBUFFERRANGEPROC)gl_driver_impl->GetGLProc("glMapBufferRange");
	gl->UnmapBuffer = (PFNGLUNMAPBUFFERPROC)gl_driver_impl->GetGLProc("glUnmapBuffer");
	gl->BindBuffer = (PFNGLBINDBUFFERPROC)gl_driver_impl->GetGLProc("glBindBuffer");
	gl->DeleteBuffers = (PFNGLDELETEBUFFERSPROC)gl_driver_impl->GetGLProc("glDeleteBuffers");

	gl->ActiveTexture = (PFNGLACTIVETEXTUREPROC)gl_driver_impl->GetGLProc("glActiveTexture");
	gl->GenerateMipmap = (PFNGLGENERATEMIPMAPPROC)gl_driver_impl->GetGLProc("glGenerateMipmap");
	gl->TexImage2DMultisample = (PFNGLTEXIMAGE2DMULTISAMPLEPROC)gl_driver_impl->GetGLProc("glTexImage2DMultisample");

	gl->GenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC)gl_driver_impl->GetGLProc("glGenFramebuffers");
	gl->BindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC)gl_driver_impl->GetGLProc("glBindFramebuffer");
	gl->FramebufferTexture = (PFNGLFRAMEBUFFERTEXTUREPROC)gl_driver_impl->GetGLProc("glFramebufferTexture");
	gl->FramebufferTexture1D = (PFNGLFRAMEBUFFERTEXTURE1DPROC)gl_driver_impl->GetGLProc("glFramebufferTexture1D");
	gl->FramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC)gl_driver_impl->GetGLProc("glFramebufferTexture2D");
	gl->FramebufferTexture3D = (PFNGLFRAMEBUFFERTEXTURE3DPROC)gl_driver_impl->GetGLProc("glFramebufferTexture3D");
	gl->FramebufferTextureLayer = (PFNGLFRAMEBUFFERTEXTURELAYERPROC)gl_driver_impl->GetGLProc("glFramebufferTextureLayer");
	gl->CheckFramebufferStatus = (PFNGLCHECKFRAMEBUFFERSTATUSPROC)gl_driver_impl->GetGLProc("glCheckFramebufferStatus");
	gl->BlitFramebuffer = (PFNGLBLITFRAMEBUFFERPROC)gl_driver_impl->GetGLProc("glBlitFramebuffer");
	gl->DeleteFramebuffers = (PFNGLDELETEFRAMEBUFFERSPROC)gl_driver_impl->GetGLProc("glDeleteFramebuffers");

	gl->GenRenderbuffers = (PFNGLGENRENDERBUFFERSPROC)gl_driver_impl->GetGLProc("glGenRenderbuffers");
	gl->BindRenderbuffer = (PFNGLBINDRENDERBUFFERPROC)gl_driver_impl->GetGLProc("glBindRenderbuffer");
	gl->RenderbufferStorage = (PFNGLRENDERBUFFERSTORAGEPROC)gl_driver_impl->GetGLProc("glRenderbufferStorage");
	gl->RenderbufferStorageMultisample = (PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC)gl_driver_impl->GetGLProc("glRenderbufferStorageMultisample");
	gl->FramebufferRenderbuffer = (PFNGLFRAMEBUFFERRENDERBUFFERPROC)gl_driver_impl->GetGLProc("glFramebufferRenderbuffer");
	gl->DeleteRenderbuffers = (PFNGLDELETERENDERBUFFERSPROC)gl_driver_impl->GetGLProc("glDeleteRenderbuffers");

	gl->GenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)gl_driver_impl->GetGLProc("glGenVertexArrays");
	gl->BindVertexArray = (PFNGLBINDVERTEXARRAYPROC)gl_driver_impl->GetGLProc("glBindVertexArray");

	gl->CreateShader = (PFNGLCREATESHADERPROC)gl_driver_impl->GetGLProc("glCreateShader");
	gl->ShaderSource = (PFNGLSHADERSOURCEPROC)gl_driver_impl->GetGLProc("glShaderSource");
	gl->CompileShader = (PFNGLCOMPILESHADERPROC)gl_driver_impl->GetGLProc("glCompileShader");
	gl->GetShaderiv = (PFNGLGETSHADERIVPROC)gl_driver_impl->GetGLProc("glGetShaderiv");
	gl->GetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)gl_driver_impl->GetGLProc("glGetShaderInfoLog");
	gl->DeleteShader = (PFNGLDELETESHADERPROC)gl_driver_impl->GetGLProc("glDeleteShader");

	gl->CreateProgram = (PFNGLCREATEPROGRAMPROC)gl_driver_impl->GetGLProc("glCreateProgram");
	gl->UseProgram = (PFNGLUSEPROGRAMPROC)gl_driver_impl->GetGLProc("glUseProgram");
	gl->AttachShader = (PFNGLATTACHSHADERPROC)gl_driver_impl->GetGLProc("glAttachShader");
	gl->LinkProgram = (PFNGLLINKPROGRAMPROC)gl_driver_impl->GetGLProc("glLinkProgram");
	gl->DeleteProgram = (PFNGLDELETEPROGRAMPROC)gl_driver_impl->GetGLProc("glDeleteProgram");

	gl->BindAttribLocation = (PFNGLBINDATTRIBLOCATIONPROC)gl_driver_impl->GetGLProc("glBindAttribLocation");
	gl->BindFragDataLocation = (PFNGLBINDFRAGDATALOCATIONPROC)gl_driver_impl->GetGLProc("glBindFragDataLocation");
	gl->GetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC)gl_driver_impl->GetGLProc("glGetAttribLocation");
	gl->GetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)gl_driver_impl->GetGLProc("glGetUniformLocation");

	gl->VertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC)gl_driver_impl->GetGLProc("glVertexAttribPointer");
	gl->EnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC)gl_driver_impl->GetGLProc("glEnableVertexAttribArray");
	gl->DisableVertexAttribArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)gl_driver_impl->GetGLProc("glDisableVertexAttribArray");
	gl->VertexAttribDivisor = (PFNGLVERTEXATTRIBDIVISORPROC)gl_driver_impl->GetGLProc("glVertexAttribDivisor");

	gl->Uniform1f = (PFNGLUNIFORM1FPROC)gl_driver_impl->GetGLProc("glUniform1f");
	gl->Uniform1i = (PFNGLUNIFORM1IPROC)gl_driver_impl->GetGLProc("glUniform1i");
	gl->Uniform1ui = (PFNGLUNIFORM1UIPROC)gl_driver_impl->GetGLProc("glUniform1ui");
	gl->Uniform2fv = (PFNGLUNIFORM2FVPROC)gl_driver_impl->GetGLProc("glUniform2fv");
	gl->Uniform3fv = (PFNGLUNIFORM3FVPROC)gl_driver_impl->GetGLProc("glUniform3fv");
	gl->Uniform4fv = (PFNGLUNIFORM4FVPROC)gl_driver_impl->GetGLProc("glUniform4fv");
	gl->Uniform2iv = (PFNGLUNIFORM2IVPROC)gl_driver_impl->GetGLProc("glUniform2iv");
	gl->Uniform3iv = (PFNGLUNIFORM3IVPROC)gl_driver_impl->GetGLProc("glUniform3iv");
	gl->Uniform4iv = (PFNGLUNIFORM4IVPROC)gl_driver_impl->GetGLProc("glUniform4iv");
	gl->Uniform2uiv = (PFNGLUNIFORM2UIVPROC)gl_driver_impl->GetGLProc("glUniform2uiv");
	gl->Uniform3uiv = (PFNGLUNIFORM3UIVPROC)gl_driver_impl->GetGLProc("glUniform3uiv");
	gl->Uniform4uiv = (PFNGLUNIFORM4UIVPROC)gl_driver_impl->GetGLProc("glUniform4uiv");
	gl->UniformMatrix2fv = (PFNGLUNIFORMMATRIX2FVPROC)gl_driver_impl->GetGLProc("glUniformMatrix2fv");
	gl->UniformMatrix3fv = (PFNGLUNIFORMMATRIX3FVPROC)gl_driver_impl->GetGLProc("glUniformMatrix3fv");
	gl->UniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC)gl_driver_impl->GetGLProc("glUniformMatrix4fv");

	LH_GL_SET_CORE_PTR(Finish, PFNGLFINISHPROC);
	LH_GL_SET_CORE_PTR(Flush, PFNGLFLUSHPROC);
	LH_GL_SET_CORE_PTR(Viewport, PFNGLVIEWPORTPROC);

	gl->DrawElementsInstanced = (PFNGLDRAWELEMENTSINSTANCEDPROC)gl_driver_impl->GetGLProc("glDrawElementsInstanced");
	gl->DrawElementsInstancedBaseVertex = (PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC)gl_driver_impl->GetGLProc("glDrawElementsInstancedBaseVertex");
	gl->DrawArraysInstanced = (PFNGLDRAWARRAYSINSTANCEDPROC)gl_driver_impl->GetGLProc("glDrawArraysInstanced");

	gl_driver_impl->ReleaseGraphicCtx(window);

	lhCoreInit(window);

	os_driver_impl->MutexLock(sys.windows_lock);
	window->id = list_push(&sys.windows, window);
	os_driver_impl->MutexUnlock(sys.windows_lock);

	window->is_complete = LH_TRUE;
	return window;
}

void lhAcquireGraphicLock(struct lh_window *window)
{
	lhMutexLock(window->graphic_ctx_mutex);
}

void lhReleaseGraphicLock(struct lh_window *window)
{
	lhMutexUnlock(window->graphic_ctx_mutex);
}

int lhStartDrawing(struct lh_window *window)
{
	return gl_driver_impl->StartDrawing(window);
}

int lhStopDrawing(struct lh_window *window)
{
	return gl_driver_impl->StopDrawing(window);
}

int lhSetListeningEvents(struct lh_window *window, LHbool is_listening)
{
	return gl_driver_impl->SetListeningEvents(window, is_listening);
}

int lhEnterEventLoop(void)
{
	return gl_driver_impl->EnterEventLoop();
}

int lhLeaveEventLoop(void)
{
	return gl_driver_impl->LeaveEventLoop();
}

int lhDestroyWindow(struct lh_window *window)
{
	window->is_complete = LH_FALSE;

	lhStopDrawing(window);
	lhCoreDeinit(window);

	os_driver_impl->MutexLock(sys.windows_lock);
	list_remove(&sys.windows, window->id);
	os_driver_impl->MutexUnlock(sys.windows_lock);

	lhMutexDestroy(window->graphic_ctx_mutex);
	return gl_driver_impl->DestroyWindow(window);
}

int lhAddEventHandler(struct lh_window *window, lh_event_handler_func_t handler, void *params)
{
	return gl_driver_impl->AddEventHandler(window, handler, params, LH_FALSE);
}

int lhAddInternalEventHandler(struct lh_window *window, lh_event_handler_func_t handler, void *params)
{
	return gl_driver_impl->AddEventHandler(window, handler, params, LH_TRUE);
}

int lhRemoveEventHandler(struct lh_window *window, int id)
{
	return gl_driver_impl->RemoveEventHandler(window, id);
}

int lhGrabMouse(struct lh_window *window, LHbool grabbed)
{
	return gl_driver_impl->GrabMouse(window, grabbed);
}

int lhGrabKeyboard(struct lh_window *window, LHbool grabbed)
{
	return gl_driver_impl->GrabKeyboard(window, grabbed);
}

int lhLockCursor(struct lh_window *window, LHbool locked)
{
	return gl_driver_impl->LockCursor(window, locked);
}

int lhShowCursor(struct lh_window *window, LHbool visible)
{
	return gl_driver_impl->ShowCursor(window, visible);
}
