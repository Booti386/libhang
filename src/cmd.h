/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_CMD_H
#define LIBHANG_INTERNAL_CMD_H 1

#include <libhang.h>

#define CMD_PARAM(type, data_type, data) { (type), (lh_dynamic_type_t){ .as_ ## data_type = (data) } }
#define CMD_PARAM_IMM(data_type, data)        CMD_PARAM(CMD_PARAM_TYPE_IMM, data_type, data)
#define CMD_PARAM_IMM_CLONED(data_type, data) CMD_PARAM(CMD_PARAM_TYPE_IMM_CLONED, data_type, data)
#define CMD_PARAM_PTR(data_type, data)        CMD_PARAM(CMD_PARAM_TYPE_PTR, data_type, data)
#define CMD_PARAM_STOR_IMM(data_type, data)   CMD_PARAM(CMD_PARAM_TYPE_STOR_IMM, data_type, data)
#define CMD_PARAM_STOR_PTR(data_type, data)   CMD_PARAM(CMD_PARAM_TYPE_STOR_PTR, data_type, data)
#define CMD_RET_NONE           CMD_PARAM(CMD_RET_TYPE_NONE, uint, 0)
#define CMD_RET_PTR(data_type, data)      CMD_PARAM(CMD_RET_TYPE_PTR, data_type, data)
#define CMD_RET_STOR_IMM(data_type, data) CMD_PARAM(CMD_RET_TYPE_STOR_IMM, data_type, data)
#define CMD_RET_STOR_PTR(data_type, data) CMD_PARAM(CMD_RET_TYPE_STOR_PTR, data_type, data)
#define CMD_DATA(n, ...) (n), ((struct lh_graphic_cmd_param [n]){ __VA_ARGS__ })
#define CMD_DATA0() (0), ((struct lh_graphic_cmd_param [1]){ CMD_PARAM_IMM(int, 0) })
#define CMD_STOR_IDX(n) (n)

enum {
	CMD_BEGIN = 0,
	CMD_END,

	CMD_COPY_UINT,
	CMD_COPY_MAT4,
	CMD_MEMCPY,

	CMD_DEC_UINT,
	CMD_ADD_UINT,

	CMD_CORE_SHOW_VERSIONS,

	CMD_GROUP_BUFFER_GROW,
	CMD_GROUP_BUFFER_GROW_FIXED,
	CMD_GROUP_BUFFER_SUB_DATA,
	CMD_GROUP_BUFFER_SUB_DATA_FIXED,

	CMD_GRAPH_ACTIVE_TEXTURE,
	CMD_GRAPH_ATTACH_SHADER,
	CMD_GRAPH_BIND_ATTRIB_LOCATION,
	CMD_GRAPH_BIND_BUFFER,
	CMD_GRAPH_BIND_FRAG_DATA_LOCATION,
	CMD_GRAPH_BIND_FRAMEBUFFER,
	CMD_GRAPH_BIND_RENDERBUFFER,
	CMD_GRAPH_BIND_TEXTURE,
	CMD_GRAPH_BIND_VERTEX_ARRAY,
	CMD_GRAPH_BLEND_FUNC,
	CMD_GRAPH_BUFFER_DATA,
	CMD_GRAPH_BUFFER_SUB_DATA,
	CMD_GRAPH_CLEAR_COLOR,
	CMD_GRAPH_COMPILE_SHADER,
	CMD_GRAPH_CREATE_PROGRAM,
	CMD_GRAPH_CREATE_SHADER,
	CMD_GRAPH_CULL_FACE,
	CMD_GRAPH_DELETE_BUFFER,
	CMD_GRAPH_DELETE_FRAMEBUFFER,
	CMD_GRAPH_DELETE_PROGRAM,
	CMD_GRAPH_DELETE_RENDERBUFFER,
	CMD_GRAPH_DELETE_SHADER,
	CMD_GRAPH_DELETE_TEXTURE,
	CMD_GRAPH_DEPTH_FUNC,
	CMD_GRAPH_DISABLE,
	CMD_GRAPH_DRAW_BUFFER,
	CMD_GRAPH_ENABLE,
	CMD_GRAPH_FRAMEBUFFER_RENDERBUFFER,
	CMD_GRAPH_FRONT_FACE,
	CMD_GRAPH_GEN_BUFFER,
	CMD_GRAPH_GEN_FRAMEBUFFER,
	CMD_GRAPH_GEN_RENDERBUFFER,
	CMD_GRAPH_GEN_TEXTURE,
	CMD_GRAPH_GEN_VERTEX_ARRAY,
	CMD_GRAPH_GENERATE_MIPMAP,
	CMD_GRAPH_GET_UNIFORM_LOCATION,
	CMD_GRAPH_LINK_PROGRAM,
	CMD_GRAPH_MAP_BUFFER_RANGE,
	CMD_GRAPH_PIXEL_STOREI,
	CMD_GRAPH_RENDERBUFFER_STORAGE_MULTISAMPLE,
	CMD_GRAPH_SHADER_SOURCE,
	CMD_GRAPH_TEX_IMAGE_2D,
	CMD_GRAPH_TEX_PARAMETERI,
	CMD_GRAPH_UNMAP_BUFFER,
	CMD_GRAPH_USE_PROGRAM,
	CMD_GRAPH_VIEWPORT
};

enum {
	CMD_PARAM_TYPE_IMM = 0,    /* Param is an immediate value */
	CMD_PARAM_TYPE_IMM_CLONED, /* Param is an immediate value duplicated on command creation (meaningful only for ptrs) */
	CMD_PARAM_TYPE_PTR,        /* Param is a value got from a pointer */
	CMD_PARAM_TYPE_STOR_IMM,   /* Param is an immediate value in a storage entry */
	CMD_PARAM_TYPE_STOR_PTR,   /* Param is a value got from a pointer in a storage entry */
};

#define CMD_RET_TYPE_NONE      CMD_PARAM_TYPE_IMM      /* Do not store the result */
#define CMD_RET_TYPE_PTR       CMD_PARAM_TYPE_PTR      /* Store the result into the memory referenced by a pointer */
#define CMD_RET_TYPE_STOR_IMM  CMD_PARAM_TYPE_STOR_IMM /* Store the result in a storage entry */
#define CMD_RET_TYPE_STOR_PTR  CMD_PARAM_TYPE_STOR_PTR /* Store the result into the memory pointed by a storage entry */

struct lh_graphic_cmd_param {
	LHuint type;
	lh_dynamic_type_t data;
};

struct lh_graphic_cmd {
	LHuint type;
	struct lh_graphic_cmd_param params[];
};

static int emit_sub_cmd(struct lh_window *window, LHuint type, LHuint nb_params, struct lh_graphic_cmd_param params[]);
static int emit_cmd(struct lh_window *window, LHuint type, LHuint nb_params, struct lh_graphic_cmd_param params[]);

static inline void *cmd_clone_param(struct lh_window *window, const void *param, LHuint len) {
	void *ret = NULL;

	lh_mark_used(window);

	if(param && len) {
		ret = malloc(len);
		if(ret)
			memcpy(ret, param, len);
	}
	return ret;
}

static inline lh_dynamic_type_t *cmd_get_param(struct lh_graphic_cmd *cmd, lh_dynamic_type_t stor[], LHuint i) {
	switch(cmd->params[i].type) {
		case CMD_PARAM_TYPE_IMM:
		case CMD_PARAM_TYPE_IMM_CLONED:
			return &cmd->params[i].data;
		case CMD_PARAM_TYPE_PTR:
			return cmd->params[i].data.as_ptr;
		case CMD_PARAM_TYPE_STOR_IMM:
			if(stor)
				return &stor[cmd->params[i].data.as_uint];
			return NULL;
		case CMD_PARAM_TYPE_STOR_PTR:
			if(stor)
				return stor[cmd->params[i].data.as_uint].as_ptr;
			return NULL;
	}
	return NULL;
}

static inline lh_dynamic_type_t *cmd_get_ret_ptr(struct lh_graphic_cmd *cmd, lh_dynamic_type_t stor[], LHuint i) {
	/* CMD_RET_TYPE_IMM_CLONED does not exists */
	if(cmd->params[i].type == CMD_PARAM_TYPE_IMM_CLONED)
		return NULL;
	/* CMD_RET_TYPE_NONE will cause a dummy write to command data. */
	return cmd_get_param(cmd, stor, i); /* RET_TYPEs are aliases to PARAM_TYPEs */
}

static inline void cmd_free_cloned_param(struct lh_graphic_cmd *cmd, LHuint i) {
	if(cmd->params[i].type == CMD_PARAM_TYPE_IMM_CLONED
			&& cmd->params[i].data.as_ptr) {
		free(cmd->params[i].data.as_ptr);
		cmd->params[i].data.as_ptr = NULL;
	}
}

#define EMIT_CMD_(proto, callback) \
	static inline int emit_cmd_ ## proto { \
		return emit_ ## callback; \
	} \
	static inline int emit_sub_cmd_ ## proto { \
		return emit_sub_ ## callback; \
	}

/* ALL the cmds of the same type MUST have the same number of params. */

EMIT_CMD_(copy_uint(struct lh_window *window, LHuint *dst, LHuint a),
		cmd(window, CMD_COPY_UINT, CMD_DATA(
			2,
			CMD_PARAM_IMM(ptr, dst),
			CMD_PARAM_IMM(uint, a)
		)))

EMIT_CMD_(copy_uint_from_stor(struct lh_window *window, LHuint *dst, LHuint src_stor_idx),
		cmd(window, CMD_COPY_UINT, CMD_DATA(
			2,
			CMD_PARAM_IMM(ptr, dst),
			CMD_PARAM_STOR_IMM(uint, src_stor_idx)
		)))

EMIT_CMD_(copy_mat4_to_stor(struct lh_window *window, LHuint dst_stor_idx, lh_mat4_t *src),
		cmd(window, CMD_COPY_MAT4, CMD_DATA(
			2,
			CMD_PARAM_STOR_IMM(uint, dst_stor_idx),
			CMD_PARAM_IMM(ptr, src)
		)))
EMIT_CMD_(copy_mat4_to_stor_from_clone(struct lh_window *window, LHuint dst_stor_idx, lh_mat4_t *src),
		cmd(window, CMD_COPY_MAT4, CMD_DATA(
			2,
			CMD_PARAM_STOR_IMM(uint, dst_stor_idx),
			CMD_PARAM_IMM_CLONED(ptr, cmd_clone_param(window, src, sizeof(*src)))
		)))

EMIT_CMD_(memcpy_from_stor(struct lh_window *window, void *dst, LHuint src_stor_idx, LHuint len),
		cmd(window, CMD_MEMCPY, CMD_DATA(
			3,
			CMD_PARAM_IMM(ptr, dst),
			CMD_PARAM_STOR_IMM(uint, src_stor_idx),
			CMD_PARAM_IMM(uint, len)
		)))
EMIT_CMD_(memcpy_to_stor(struct lh_window *window, LHuint dst_stor_idx, void *src, LHuint len),
		cmd(window, CMD_MEMCPY, CMD_DATA(
			3,
			CMD_PARAM_STOR_IMM(uint, dst_stor_idx),
			CMD_PARAM_IMM(ptr, src),
			CMD_PARAM_IMM(uint, len)
		)))
EMIT_CMD_(memcpy_to_stor_from_clone(struct lh_window *window, LHuint dst_stor_idx, void *src, LHuint len),
		cmd(window, CMD_MEMCPY, CMD_DATA(
			3,
			CMD_PARAM_STOR_IMM(uint, dst_stor_idx),
			CMD_PARAM_IMM_CLONED(ptr, cmd_clone_param(window, src, len)),
			CMD_PARAM_IMM(uint, len)
		)))
EMIT_CMD_(memcpy_stor(struct lh_window *window, LHuint dst_stor_idx, LHuint src_stor_idx, LHuint len),
		cmd(window, CMD_MEMCPY, CMD_DATA(
			3,
			CMD_PARAM_STOR_IMM(uint, dst_stor_idx),
			CMD_PARAM_STOR_IMM(uint, src_stor_idx),
			CMD_PARAM_IMM(uint, len)
		)))

EMIT_CMD_(dec_uint(struct lh_window *window, LHuint *dst),
		cmd(window, CMD_DEC_UINT, CMD_DATA(
			1,
			CMD_PARAM_IMM(ptr, dst)
		)))
EMIT_CMD_(add_uint(struct lh_window *window, LHuint *dst, LHuint a),
		cmd(window, CMD_ADD_UINT, CMD_DATA(
			2,
			CMD_PARAM_IMM(ptr, dst),
			CMD_PARAM_IMM(uint, a)
		)))

EMIT_CMD_(core_show_versions(struct lh_window *window),
		cmd(window, CMD_CORE_SHOW_VERSIONS, CMD_DATA0()))

EMIT_CMD_(group_buffer_grow_idptr_dataptr(struct lh_window *window, LHuint tgt, LHuint *id_ptr, LHuint len, void **data, LHuint usage),
		cmd(window, CMD_GROUP_BUFFER_GROW, CMD_DATA(
			5,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_PTR(ptr, id_ptr),
			CMD_PARAM_IMM(uint, len),
			CMD_PARAM_PTR(ptr, data),
			CMD_PARAM_IMM(uint, usage)
		)))

EMIT_CMD_(group_buffer_sub_data_idptr_dataptr(struct lh_window *window, LHuint tgt, LHuint *id_ptr, LHuint off, LHuint len, void **data),
		cmd(window, CMD_GROUP_BUFFER_SUB_DATA, CMD_DATA(
			5,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_PTR(ptr, id_ptr),
			CMD_PARAM_IMM(uint, off),
			CMD_PARAM_IMM(uint, len),
			CMD_PARAM_PTR(ptr, data)
		)))

EMIT_CMD_(graph_enable(struct lh_window *window, LHuint cap),
		cmd(window, CMD_GRAPH_ENABLE, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, cap)
		)))

EMIT_CMD_(graph_disable(struct lh_window *window, LHuint cap),
		cmd(window, CMD_GRAPH_DISABLE, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, cap)
		)))

EMIT_CMD_(graph_active_texture(struct lh_window *window, LHuint unit),
		cmd(window, CMD_GRAPH_ACTIVE_TEXTURE, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, unit)
		)))

EMIT_CMD_(graph_depth_func(struct lh_window *window, LHuint func),
		cmd(window, CMD_GRAPH_DEPTH_FUNC, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, func)
		)))

EMIT_CMD_(graph_blend_func(struct lh_window *window, LHuint src_func, LHuint dst_func),
		cmd(window, CMD_GRAPH_BLEND_FUNC, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, src_func),
			CMD_PARAM_IMM(uint, dst_func)
		)))

EMIT_CMD_(graph_front_face(struct lh_window *window, LHuint mode),
		cmd(window, CMD_GRAPH_FRONT_FACE, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, mode)
		)))

EMIT_CMD_(graph_cull_face(struct lh_window *window, LHuint mode),
		cmd(window, CMD_GRAPH_CULL_FACE, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, mode)
		)))

EMIT_CMD_(graph_viewport(struct lh_window *window, int x, int y, int w, int h),
		cmd(window, CMD_GRAPH_VIEWPORT, CMD_DATA(
			4,
			CMD_PARAM_IMM(int, x),
			CMD_PARAM_IMM(int, y),
			CMD_PARAM_IMM(int, w),
			CMD_PARAM_IMM(int, h)
		)))

EMIT_CMD_(graph_gen_vertex_array(struct lh_window *window, LHuint *dst_id),
		cmd(window, CMD_GRAPH_GEN_VERTEX_ARRAY, CMD_DATA(
			1,
			CMD_PARAM_IMM(ptr, dst_id)
		)))

EMIT_CMD_(graph_bind_vertex_array(struct lh_window *window, LHuint id),
		cmd(window, CMD_GRAPH_BIND_VERTEX_ARRAY, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, id)
		)))
EMIT_CMD_(graph_bind_vertex_array_ptr(struct lh_window *window, LHuint *id_ptr),
		cmd(window, CMD_GRAPH_BIND_VERTEX_ARRAY, CMD_DATA(
			1,
			CMD_PARAM_PTR(ptr, id_ptr)
		)))

EMIT_CMD_(graph_draw_buffer(struct lh_window *window, LHuint buf),
		cmd(window, CMD_GRAPH_DRAW_BUFFER, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, buf)
		)))

EMIT_CMD_(graph_clear_color(struct lh_window *window, lh_rgba_t *color),
		cmd(window, CMD_GRAPH_CLEAR_COLOR, CMD_DATA(
			1,
			CMD_PARAM_IMM(rgba, *color)
		)))

EMIT_CMD_(graph_gen_texture(struct lh_window *window, LHuint *dst_id),
		cmd(window, CMD_GRAPH_GEN_TEXTURE, CMD_DATA(
			1,
			CMD_PARAM_IMM(ptr, dst_id)
		)))

EMIT_CMD_(graph_bind_texture(struct lh_window *window, LHuint tgt, LHuint id),
		cmd(window, CMD_GRAPH_BIND_TEXTURE, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, id)
		)))
EMIT_CMD_(graph_bind_texture_ptr(struct lh_window *window, LHuint tgt, LHuint *id_ptr),
		cmd(window, CMD_GRAPH_BIND_TEXTURE, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_PTR(ptr, id_ptr)
		)))

EMIT_CMD_(graph_tex_parameteri(struct lh_window *window, LHuint tgt, LHuint param, LHuint value),
		cmd(window, CMD_GRAPH_TEX_PARAMETERI, CMD_DATA(
			3,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, param),
			CMD_PARAM_IMM(uint, value)
		)))

EMIT_CMD_(graph_pixel_storei(struct lh_window *window, LHuint param, LHuint value),
		cmd(window, CMD_GRAPH_PIXEL_STOREI, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, param),
			CMD_PARAM_IMM(uint, value)
		)))

EMIT_CMD_(graph_tex_image_2d(struct lh_window *window, LHuint tgt, int mipmap, int dst_fmt, LHuint w, LHuint h, int border, LHuint fmt, LHuint type, void *data),
		cmd(window, CMD_GRAPH_TEX_IMAGE_2D, CMD_DATA(
			9,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(int, mipmap),
			CMD_PARAM_IMM(int, dst_fmt),
			CMD_PARAM_IMM(uint, w),
			CMD_PARAM_IMM(uint, h),
			CMD_PARAM_IMM(int, border),
			CMD_PARAM_IMM(uint, fmt),
			CMD_PARAM_IMM(uint, type),
			CMD_PARAM_IMM(ptr, data)
		)))
EMIT_CMD_(graph_tex_image_2d_clone(struct lh_window *window, LHuint tgt, int mipmap, int dst_fmt, LHuint w, LHuint h, int border, LHuint fmt, LHuint type, void *data, LHuint data_len),
		cmd(window, CMD_GRAPH_TEX_IMAGE_2D, CMD_DATA(
			9,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(int, mipmap),
			CMD_PARAM_IMM(int, dst_fmt),
			CMD_PARAM_IMM(uint, w),
			CMD_PARAM_IMM(uint, h),
			CMD_PARAM_IMM(int, border),
			CMD_PARAM_IMM(uint, fmt),
			CMD_PARAM_IMM(uint, type),
			CMD_PARAM_IMM_CLONED(ptr, cmd_clone_param(window, data, data_len))
		)))

EMIT_CMD_(graph_generate_mipmap(struct lh_window *window, LHuint tgt),
		cmd(window, CMD_GRAPH_GENERATE_MIPMAP, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, tgt)
		)))

EMIT_CMD_(graph_delete_texture(struct lh_window *window, LHuint id),
		cmd(window, CMD_GRAPH_DELETE_TEXTURE, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, id)
		)))

EMIT_CMD_(graph_gen_buffer(struct lh_window *window, LHuint *dst_id),
		cmd(window, CMD_GRAPH_GEN_BUFFER, CMD_DATA(
			1,
			CMD_PARAM_IMM(ptr, dst_id)
		)))

EMIT_CMD_(graph_bind_buffer(struct lh_window *window, LHuint tgt, LHuint id),
		cmd(window, CMD_GRAPH_BIND_BUFFER, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, id)
		)))
EMIT_CMD_(graph_bind_buffer_ptr(struct lh_window *window, LHuint tgt, LHuint *id_ptr),
		cmd(window, CMD_GRAPH_BIND_BUFFER, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_PTR(ptr, id_ptr)
		)))

EMIT_CMD_(graph_buffer_data_clone(struct lh_window *window, LHuint tgt, LHuint len, void *data, LHuint usage),
		cmd(window, CMD_GRAPH_BUFFER_DATA, CMD_DATA(
			4,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, len),
			CMD_PARAM_IMM_CLONED(ptr, cmd_clone_param(window, data, len)),
			CMD_PARAM_IMM(uint, usage)
		)))

EMIT_CMD_(graph_buffer_sub_data_clone(struct lh_window *window, LHuint tgt, LHuint off, LHuint len, void *data),
		cmd(window, CMD_GRAPH_BUFFER_SUB_DATA, CMD_DATA(
			4,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, off),
			CMD_PARAM_IMM(uint, len),
			CMD_PARAM_IMM_CLONED(ptr, cmd_clone_param(window, data, len)),
		)))

EMIT_CMD_(graph_map_buffer_range_ret_stor(struct lh_window *window, LHuint tgt, LHuint off, LHuint len, LHuint access, LHuint ret_stor_idx),
		cmd(window, CMD_GRAPH_MAP_BUFFER_RANGE, CMD_DATA(
			5,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, off),
			CMD_PARAM_IMM(uint, len),
			CMD_PARAM_IMM(uint, access),
			CMD_RET_STOR_IMM(uint, ret_stor_idx)
		)))

EMIT_CMD_(graph_unmap_buffer(struct lh_window *window, LHuint tgt),
		cmd(window, CMD_GRAPH_UNMAP_BUFFER, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, tgt)
		)))

EMIT_CMD_(graph_delete_buffer_ptr(struct lh_window *window, LHuint *id),
		cmd(window, CMD_GRAPH_DELETE_BUFFER, CMD_DATA(
			1,
			CMD_PARAM_PTR(ptr, id)
		)))

EMIT_CMD_(graph_create_shader_ret_stor(struct lh_window *window, LHuint type, LHuint ret_stor_idx),
		cmd(window, CMD_GRAPH_CREATE_SHADER, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, type),
			CMD_RET_STOR_IMM(uint, ret_stor_idx)
		)))

EMIT_CMD_(graph_create_program_ret_stor(struct lh_window *window, LHuint ret_stor_idx),
		cmd(window, CMD_GRAPH_CREATE_PROGRAM, CMD_DATA(
			1,
			CMD_RET_STOR_IMM(uint, ret_stor_idx)
		)))

EMIT_CMD_(graph_shader_source_stor(struct lh_window *window, LHuint shader_stor_idx, LHuint nlines, char *const *src, const int *lengths),
		cmd(window, CMD_GRAPH_SHADER_SOURCE, CMD_DATA(
			4,
			CMD_PARAM_STOR_IMM(uint, shader_stor_idx),
			CMD_PARAM_IMM(uint, nlines),
			CMD_PARAM_IMM(ptr, (void *)src),
			CMD_PARAM_IMM(ptr, (void *)lengths)
		)))

EMIT_CMD_(graph_compile_shader_stor(struct lh_window *window, LHuint shader_stor_idx),
		cmd(window, CMD_GRAPH_COMPILE_SHADER, CMD_DATA(
			1,
			CMD_PARAM_STOR_IMM(uint, shader_stor_idx)
		)))

EMIT_CMD_(graph_attach_shader_stor_stor(struct lh_window *window, LHuint prog_stor_idx, LHuint shader_stor_idx),
		cmd(window, CMD_GRAPH_ATTACH_SHADER, CMD_DATA(
			2,
			CMD_PARAM_STOR_IMM(uint, prog_stor_idx),
			CMD_PARAM_STOR_IMM(uint, shader_stor_idx)
		)))

EMIT_CMD_(graph_bind_attrib_location_stor_clone(struct lh_window *window, LHuint prog_stor_idx, LHuint idx, const char *name),
		cmd(window, CMD_GRAPH_BIND_ATTRIB_LOCATION, CMD_DATA(
			3,
			CMD_PARAM_STOR_IMM(uint, prog_stor_idx),
			CMD_PARAM_IMM(uint, idx),
			CMD_PARAM_IMM_CLONED(ptr, cmd_clone_param(window, name, strlen(name) + 1))
		)))

EMIT_CMD_(graph_bind_frag_data_location_stor_clone(struct lh_window *window, LHuint prog_stor_idx, LHuint idx, const char *name),
		cmd(window, CMD_GRAPH_BIND_FRAG_DATA_LOCATION, CMD_DATA(
			3,
			CMD_PARAM_STOR_IMM(uint, prog_stor_idx),
			CMD_PARAM_IMM(uint, idx),
			CMD_PARAM_IMM_CLONED(ptr, cmd_clone_param(window, name, strlen(name) + 1))
		)))

EMIT_CMD_(graph_get_uniform_location_stor_ret_ptr_clone(struct lh_window *window, LHuint prog_stor_idx, const char *name, int *ret_ptr),
		cmd(window, CMD_GRAPH_GET_UNIFORM_LOCATION, CMD_DATA(
			3,
			CMD_PARAM_STOR_IMM(uint, prog_stor_idx),
			CMD_PARAM_IMM_CLONED(ptr, cmd_clone_param(window, name, strlen(name) + 1)),
			CMD_RET_PTR(ptr, ret_ptr)
		)))

EMIT_CMD_(graph_link_program_stor(struct lh_window *window, LHuint prog_stor_idx),
		cmd(window, CMD_GRAPH_LINK_PROGRAM, CMD_DATA(
			1,
			CMD_PARAM_STOR_IMM(uint, prog_stor_idx),
		)))

EMIT_CMD_(graph_use_program_stor(struct lh_window *window, LHuint prog_stor_idx),
		cmd(window, CMD_GRAPH_USE_PROGRAM, CMD_DATA(
			1,
			CMD_PARAM_STOR_IMM(uint, prog_stor_idx),
		)))

EMIT_CMD_(graph_delete_shader_stor(struct lh_window *window, LHuint shader_stor_idx),
		cmd(window, CMD_GRAPH_DELETE_SHADER, CMD_DATA(
			1,
			CMD_PARAM_STOR_IMM(uint, shader_stor_idx)
		)))

EMIT_CMD_(graph_delete_program(struct lh_window *window, LHuint program),
		cmd(window, CMD_GRAPH_DELETE_PROGRAM, CMD_DATA(
			1,
			CMD_PARAM_IMM(uint, program)
		)))

EMIT_CMD_(graph_gen_framebuffer(struct lh_window *window, LHuint *buf),
		cmd(window, CMD_GRAPH_GEN_FRAMEBUFFER, CMD_DATA(
			1,
			CMD_PARAM_IMM(ptr, buf)
		)))

EMIT_CMD_(graph_gen_renderbuffer(struct lh_window *window, LHuint *buf),
		cmd(window, CMD_GRAPH_GEN_RENDERBUFFER, CMD_DATA(
			1,
			CMD_PARAM_IMM(ptr, buf)
		)))

EMIT_CMD_(graph_bind_framebuffer(struct lh_window *window, LHuint tgt, LHuint buf),
		cmd(window, CMD_GRAPH_BIND_FRAMEBUFFER, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, buf)
		)))
EMIT_CMD_(graph_bind_framebuffer_ptr(struct lh_window *window, LHuint tgt, LHuint *buf),
		cmd(window, CMD_GRAPH_BIND_FRAMEBUFFER, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_PTR(ptr, buf)
		)))

EMIT_CMD_(graph_bind_renderbuffer(struct lh_window *window, LHuint tgt, LHuint buf),
		cmd(window, CMD_GRAPH_BIND_RENDERBUFFER, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, buf)
		)))
EMIT_CMD_(graph_bind_renderbuffer_ptr(struct lh_window *window, LHuint tgt, LHuint *buf),
		cmd(window, CMD_GRAPH_BIND_RENDERBUFFER, CMD_DATA(
			2,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_PTR(ptr, buf)
		)))

EMIT_CMD_(graph_framebuffer_renderbuffer(struct lh_window *window, LHuint tgt, LHuint attachment, LHuint renderbuf_tgt, LHuint renderbuf),
		cmd(window, CMD_GRAPH_FRAMEBUFFER_RENDERBUFFER, CMD_DATA(
			4,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, attachment),
			CMD_PARAM_IMM(uint, renderbuf_tgt),
			CMD_PARAM_IMM(uint, renderbuf)
		)))
EMIT_CMD_(graph_framebuffer_renderbuffer_ptr(struct lh_window *window, LHuint tgt, LHuint attachment, LHuint renderbuf_tgt, LHuint *renderbuf),
		cmd(window, CMD_GRAPH_FRAMEBUFFER_RENDERBUFFER, CMD_DATA(
			4,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(uint, attachment),
			CMD_PARAM_IMM(uint, renderbuf_tgt),
			CMD_PARAM_PTR(ptr, renderbuf)
		)))

EMIT_CMD_(graph_renderbuffer_storage_multisample(struct lh_window *window, LHuint tgt, int nsamples, LHuint fmt, int w, int h),
		cmd(window, CMD_GRAPH_RENDERBUFFER_STORAGE_MULTISAMPLE, CMD_DATA(
			5,
			CMD_PARAM_IMM(uint, tgt),
			CMD_PARAM_IMM(int, nsamples),
			CMD_PARAM_IMM(uint, fmt),
			CMD_PARAM_IMM(int, w),
			CMD_PARAM_IMM(int, h)
		)))

EMIT_CMD_(graph_delete_framebuffer_ptr(struct lh_window *window, LHuint *buf),
		cmd(window, CMD_GRAPH_DELETE_FRAMEBUFFER, CMD_DATA(
			1,
			CMD_PARAM_PTR(ptr, buf)
		)))

EMIT_CMD_(graph_delete_renderbuffer_ptr(struct lh_window *window, LHuint *buf),
		cmd(window, CMD_GRAPH_DELETE_RENDERBUFFER, CMD_DATA(
			1,
			CMD_PARAM_PTR(ptr, buf)
		)))

#endif /* LIBHANG_INTERNAL_CMD_H */
