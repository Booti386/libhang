/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>

#include <libhang.h>

#define READ_ONCE 256


static struct lh_obj_class Sample_obj_class = {
	.name="Sample",

	.is_visible = LH_TRUE,
	.priv_flags = 0,
	.uses = LH_BUF_POS_FLAG
			| LH_BUF_INDEX_FLAG,
	.overrides = 0,

	.shader_program = {
		.vertex_src = {
			.src = (char *[47]){
				"#version 150 \n"
				" \n"
				"in vec3 a_VertexPosition; \n"
				"in mat4 a_ModelMatrix; \n"
				" \n"
				"uniform bool u_IsInstanced; \n"
				"uniform mat4 u_ModelMatrix; \n"
				"uniform mat4 u_ViewMatrix; \n"
				"uniform mat4 u_PMatrix; \n"
				" \n"
				"uniform vec3 u_AmbientLight; \n"
				" \n"
				"uniform bool u_FogEnabled; \n"
				"uniform float u_FogMinDistance; \n"
				"uniform float u_FogMaxDistance; \n"
				" \n"
				"uniform float u_SideLength; \n"
				" \n"
				"out vec3 v_LightWeighting; \n"
				"out float v_FogWeighting; \n"
				" \n"
				"void main(void) { \n"
				"	mat4 modelMatrix = u_IsInstanced ? a_ModelMatrix : u_ModelMatrix; \n"
				"	mat4 mvMatrix = u_ViewMatrix * modelMatrix; \n"
				"	vec3 objPosition = modelMatrix[3].xyz; \n"
				"	vec3 cameraPosition = inverse(u_ViewMatrix)[3].xyz; \n"
				" \n"
				"	gl_Position = u_PMatrix * mvMatrix * vec4(a_VertexPosition, 1.0 / u_SideLength); \n"
				" \n"
				"	float cameraDistance = length(objPosition + mat3(modelMatrix) * a_VertexPosition * u_SideLength - cameraPosition); \n"
				"	v_LightWeighting = u_AmbientLight; \n"
				" \n"
				"	float fogDistance = cameraDistance; \n"
				"	v_FogWeighting = 0.0; \n"
				"	if(u_FogEnabled) { \n"
				"		// float fogWeighting = (fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance); \n"
				"		// float fogWeighting = sin(((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) * asin(1.0)); \n"
				"		float fogWeighting = (exp((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) - 1.0) / (exp(1.0) - 1.0); \n"
				"		// float fogWeighting = log((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance) + 1.0) / log(2.0); \n"
				"		v_FogWeighting = fogWeighting; \n"
				"	} \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.fragment_src = {
			.src = (char *[18]){
				"#version 150 \n"
				" \n"
				"out vec4 outColor; \n"
				" \n"
				"in vec3 v_LightWeighting; \n"
				"in float v_FogWeighting; \n"
				" \n"
				"uniform vec4 u_Color; \n"
				" \n"
				"uniform bool u_FogEnabled; \n"
				"uniform vec4 u_FogColor; \n"
				" \n"
				"void main(void) { \n"
				"	vec4 color = vec4(u_Color.rgb * v_LightWeighting, u_Color.a); \n"
				"	if(color.a <= 0.001) \n"
				"		discard; \n"
				"	if(u_FogEnabled) { \n"
				"		float fogWeighting = clamp(v_FogWeighting, 0.0, 1.0); \n"
				"		color = (1.0 - fogWeighting) * color + fogWeighting * u_FogColor; \n"
				"	} \n"
				"	outColor = color; \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.drawing_type = LH_DRAW_TRIANGLES,

#define NB_CUSTOM_UNIFORMS 2
		.nb_custom_uniforms = 2,
		.custom_uniforms = (struct lh_custom_uniform []){
#define UNIFORM_SIDE_LENGTH 0
			{ .name = "u_SideLength", .type = LH_TYPE_FLOAT },
#define UNIFORM_COLOR 1
			{ .name = "u_Color",      .type = LH_TYPE_VEC4  },
		},
	},
	.buffers = {
		[LH_BUF_POS] = { .nb_items = 0, .data = NULL },
		[LH_BUF_INDEX] = { .nb_items = 0, .data = NULL },
	},
};

static char *read_text_file(const char *path)
{
	FILE *fp;
	char *data = NULL;
	int data_len = 0;
	int data_idx = 0;
	void *ret;

	fp = fopen(path, "r");
	if(!fp)
		return NULL;

	while(!feof(fp) && !ferror(fp))
	{
		data_len += READ_ONCE - 1;
		char *tmp_ptr = realloc(data, data_len);
		if(data && !tmp_ptr) {
			free(data);
			fclose(fp);
			return NULL;
		}
		data = tmp_ptr;
		ret = fgets(&data[data_idx], READ_ONCE, fp);
		if(!ret)
			break;
		data_idx += strlen(&data[data_idx]);
	}
	if(ferror(fp)) {
		fclose(fp);
		return NULL;
	}

	fclose(fp);
	return data;
}

static void setup_obj_class(struct lh_obj_class *obj_class, lh_vec3_t *vertices, int vertices_idx, lh_vec2_t *tex_coords, int tex_coords_idx, LHuint *indices, int indices_idx)
{

	if(!obj_class->name)
		obj_class->name = Sample_obj_class.name;
	obj_class->is_visible = Sample_obj_class.is_visible;
	obj_class->priv_flags = Sample_obj_class.priv_flags;
	obj_class->uses = Sample_obj_class.uses;
	obj_class->overrides = Sample_obj_class.overrides;
	obj_class->shader_program.vertex_src.src = Sample_obj_class.shader_program.vertex_src.src;
	obj_class->shader_program.vertex_src.nb_lines = Sample_obj_class.shader_program.vertex_src.nb_lines;
	obj_class->shader_program.fragment_src.src = Sample_obj_class.shader_program.fragment_src.src;
	obj_class->shader_program.fragment_src.nb_lines = Sample_obj_class.shader_program.fragment_src.nb_lines;
	obj_class->shader_program.drawing_type = Sample_obj_class.shader_program.drawing_type;
	obj_class->shader_program.nb_custom_uniforms = Sample_obj_class.shader_program.nb_custom_uniforms;
	obj_class->shader_program.custom_uniforms = Sample_obj_class.shader_program.custom_uniforms;
	obj_class->buffers[LH_BUF_POS].data = vertices;
	obj_class->buffers[LH_BUF_POS].nb_items = vertices_idx;
	obj_class->buffers[LH_BUF_NORMAL].data = NULL;
	obj_class->buffers[LH_BUF_NORMAL].nb_items = 0;
	obj_class->buffers[LH_BUF_TEX_COORD].data = tex_coords;
	obj_class->buffers[LH_BUF_TEX_COORD].nb_items = tex_coords_idx;
	obj_class->buffers[LH_BUF_MODELMATRIX].data = NULL;
	obj_class->buffers[LH_BUF_MODELMATRIX].nb_items = 0;
	obj_class->buffers[LH_BUF_INDEX].data = indices;
	obj_class->buffers[LH_BUF_INDEX].nb_items = indices_idx;
}

struct lh_obj_class **lhCreateObjClassesFromOBJ(const char *data)
{
	int obj_classes_idx = 0;
	struct lh_obj_class **obj_classes = NULL;
	uintptr_t idx = 0;
	int cur_line = 1;
	lh_vec3_t *vertices = NULL;
	int vertices_idx = 0;
	lh_vec2_t *tex_coords = NULL;
	int tex_coords_idx = 0;
	LHuint *indices = NULL;
	int indices_idx = 0;
	int vertices_start = 0;

	if(!data)
		return NULL;

	obj_classes = malloc(sizeof(*obj_classes));
	if(!obj_classes)
		return NULL;
	obj_classes[obj_classes_idx] = NULL;

	while(data[idx])
	{
		switch(data[idx++]) {
			case '#': {
				/* Comment, skip the whole line */
				while(isprint(data[idx]) && data[idx] != '\n')
					idx++;
				break;
			}

			case 'm': {
				if(strncmp(&data[idx], "mtllib", 6)) {
					fprintf(stderr, "libhang: Unsupported directive \"mtllib\".\n");
					while(isprint(data[idx]) && data[idx] != '\n') /* Unsupported, ignore */
						idx++;
					break;
				}
				break;
			}

			case 'o': {
				const char *str;
				uintptr_t str_len;

				while(isspace(data[idx]) && data[idx] != '\n')
					idx++;
				if(data[idx] == '\n' || data[idx] == '\0') {
					fprintf(stderr, "libhang: Unterminated directive.\n");
					break;
				}
				str = &data[idx];
				while(isgraph(data[idx]))
					idx++;
				str_len = &data[idx] - str;

				if (obj_classes_idx > 0)
				{
					setup_obj_class(obj_classes[obj_classes_idx - 1], vertices, vertices_idx, tex_coords, tex_coords_idx, indices, indices_idx);
					vertices_start += vertices_idx;
					vertices = NULL;
					vertices_idx = 0;
					tex_coords = NULL;
					tex_coords_idx = 0;
					indices = NULL;
					indices_idx = 0;
				}

				obj_classes[obj_classes_idx] = malloc(sizeof(**obj_classes));
				obj_classes[obj_classes_idx]->name = malloc(str_len + 1);
				strncpy(obj_classes[obj_classes_idx]->name, str, str_len);
				obj_classes[obj_classes_idx]->name[str_len] = '\0';

				fprintf(stdout, "libhang: New obj_class : %s.\n", obj_classes[obj_classes_idx]->name);

				obj_classes_idx++;
				obj_classes = realloc(obj_classes, sizeof(*obj_classes) * (obj_classes_idx + 1));
				obj_classes[obj_classes_idx] = NULL;
				break;
			}

			case 'f':
			{
				const char *str;
				long idc[256];
				int nidc;

				for(nidc = 0; nidc < 256; nidc++) {
					char *str_end;

					while(data[idx] != '\n' && isspace(data[idx]))
						idx++;
					if(data[idx] == '\n' || data[idx] == '\0')
					{
						if (nidc == 0)
							fprintf(stderr, "libhang: Unterminated directive.\n");
						break;
					}

					str = &data[idx];
					while(isdigit(data[idx]))
						idx++;

					idc[nidc] = strtol(str, &str_end, 10) - 1 - vertices_start;
					if(str_end != &data[idx])
					{
						fprintf(stderr, "libhang: Unexpected symbol in directive f.\n");
						break;
					}

					if (data[idx] == '/')
						while(!isspace(data[idx]))
							idx++;

					str = &data[idx];
				}

				while(isspace(data[idx]) && data[idx] != '\n')
					idx++;
				if(data[idx] != '\n' && data[idx] != '\0')
					break;

				for (int i = nidc - 1; i >= 3; i--)
				{
					int id = (1 + (i - 3)) * 3;

					/* Reverse order needed to avoid overwrite */
					idc[id + 2] = idc[i - 1];
					idc[id + 1] = idc[i];
					idc[id] = idc[0];
				}

				nidc = (1 + (nidc - 3)) * 3;

				indices = realloc(indices, sizeof(*indices) * (indices_idx + nidc));

				for(int i = 0; i < nidc; i++)
					indices[indices_idx + i] = idc[i];

				indices_idx += nidc;
				break;
			}

			case 'v': {
				const char *str;
				float v[3];

				if (data[idx] == 't')
				{
					fprintf(stderr, "libhang: Unsupported directive \"vt\".\n");
					while(isprint(data[idx]) && data[idx] != '\n') /* Unsupported, ignore */
						idx++;
					break;
				}

				if (data[idx] == 'n')
				{
					fprintf(stderr, "libhang: Unsupported directive \"vn\".\n");
					while(isprint(data[idx]) && data[idx] != '\n') /* Unsupported, ignore */
						idx++;
					break;
				}

				for(int i=0;  i<3; i++) {
					char *str_end;

					while(isspace(data[idx]) && data[idx] != '\n')
						idx++;
					if(data[idx] == '\n' || data[idx] == '\0') {
						fprintf(stderr, "libhang: Unterminated directive.\n");
						break;
					}

					str = &data[idx];
					while(data[idx] == '-' || data[idx] == '.' || isdigit(data[idx]))
						idx++;

					v[i] = strtof(str, &str_end);
					if(str_end != &data[idx]) {
						fprintf(stderr, "libhang: Unexpected symbol in directive v.\n");
						break;
					}

					str = &data[idx];
				}

				while(isspace(data[idx]) && data[idx] != '\n')
					idx++;
				if(data[idx] != '\n' && data[idx] != '\0')
					break;

				vertices = realloc(vertices, sizeof(*vertices) * (vertices_idx + 1));

				vertices[vertices_idx].a[0] = v[0];
				vertices[vertices_idx].a[1] = v[1];
				vertices[vertices_idx].a[2] = v[2];

				vertices_idx++;
				break;
			}

			case 's': {
				fprintf(stderr, "libhang: Unsupported directive \"s\".\n");
				while(isprint(data[idx]) && data[idx] != '\n') /* Unsupported, ignore */
					idx++;
				break;
			}

			case 'u': {
				if(strncmp(&data[idx], "usemtl", 6)) {
					fprintf(stderr, "libhang: Unsupported directive \"usemtl\".\n");
					while(isprint(data[idx]) && data[idx] != '\n') /* Unsupported, ignore */
						idx++;
					break;
				}
				break;
			}
			
			default:
				fprintf(stderr, "libhang: (%d) Unsupported unknown directive.\n", cur_line);
		}

		/* Skip all the trailing spaces */
		while(isspace(data[idx]) && data[idx] != '\n')
			idx++;

		if(data[idx] != '\0' && data[idx] != '\n') {
			fprintf(stderr, "libhang: Error at line %d.\n", cur_line);
			return NULL;
		}
		idx++;

		cur_line++;
	}

	if(!obj_classes[0])
		return NULL;

	setup_obj_class(obj_classes[obj_classes_idx - 1], vertices, vertices_idx, tex_coords, tex_coords_idx, indices, indices_idx);

	return obj_classes;
}

struct lh_obj_class **lhCreateObjClassesFromOBJFile(const char *path)
{
	struct lh_obj_class **obj_classes;
	char *data;

	data = read_text_file(path);
	if(!data)
		return NULL;

	obj_classes = lhCreateObjClassesFromOBJ(data);

	free(data);
	return obj_classes;
}

struct lh_obj_class **lhCreateObjClassesFromFile(const char *path)
{
	struct lh_obj_class **obj_classes;

	if((obj_classes = lhCreateObjClassesFromOBJFile(path)))
		return obj_classes;
	else
		return NULL;
}

struct lh_obj *lhObjFileObjCreate(struct lh_window *window, struct lh_obj_class *obj_class, float side, lh_vec3_t *pos, lh_quat_t *angle, lh_rgba_t *color, LHbool is_visible)
{
	struct lh_obj *obj;

	obj = malloc(sizeof(*obj));
	if(!obj)
		return NULL;

	obj->custom_uniforms_data = malloc(sizeof(*obj->custom_uniforms_data) * obj_class->shader_program.nb_custom_uniforms);
	if(!obj->custom_uniforms_data)
		return NULL;

	obj->delete_proc = NULL;

	lh_vec3_dup(&obj->pos, pos);
	lh_quat_dup(&obj->angle, angle);
	obj->is_visible = is_visible;
	obj->culling_enabled = LH_FALSE;
	obj->reversed_culling = LH_FALSE;
	obj->depth_test_enabled = LH_TRUE;
	obj->is_illuminable = LH_FALSE;

	obj->custom_uniforms_data[UNIFORM_SIDE_LENGTH].as_float = side;
	lh_rgba_dup(&obj->custom_uniforms_data[UNIFORM_COLOR].as_rgba, color);

	lhObjReg(window, obj_class, obj);

	return obj;
}

void lhObjFileObjSetSideLength(struct lh_window *window, struct lh_obj *obj, float side_len)
{
	lh_mark_used(window);

	obj->custom_uniforms_data[UNIFORM_SIDE_LENGTH].as_float = side_len;
}

void lhObjFileObjSetColor(struct lh_window *window, struct lh_obj *obj, lh_rgba_t *color)
{
	lh_mark_used(window);

	lh_rgba_dup(&obj->custom_uniforms_data[UNIFORM_COLOR].as_rgba, color);
}
