/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>

#include <libhang.h>
#include <libhang/obj_classes/TexturedSurface.h>

struct lh_textured_surface_private_t
{
	LHuint nsquares_x;
	LHuint nsquares_y;
};
typedef struct lh_textured_surface_private_t lh_textured_surface_private_t;

static struct lh_obj_class TexturedSurface_obj_class = {
	.name = "TexturedSurface",

	.is_visible = LH_TRUE,
	.priv_flags = 0,
	.uses = LH_BUF_POS_FLAG
			| LH_BUF_NORMAL_FLAG
			| LH_BUF_TEX_COORD_FLAG
			| LH_BUF_INDEX_FLAG
			| LH_TEX_FLAG,
	.overrides = LH_BUF_POS_FLAG
			| LH_BUF_NORMAL_FLAG
			| LH_BUF_TEX_COORD_FLAG
			| LH_BUF_INDEX_FLAG
			| LH_TEX_FLAG,

	.shader_program = {
		.vertex_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"in vec3 a_VertexPosition; \n"
				"in vec3 a_VertexNormal; \n"
				"in vec2 a_TextureCoord; \n"
				"in mat4 a_ModelMatrix; \n"
				" \n"
				"uniform bool u_IsInstanced; \n"
				"uniform mat4 u_ModelMatrix; \n"
				"uniform mat4 u_ViewMatrix; \n"
				"uniform mat4 u_PMatrix; \n"
				" \n"
				"uniform vec3 u_AmbientLight; \n"
				" \n"
				"uniform bool u_IsIlluminable; \n"
				"uniform vec3 u_LightColor; \n"
				"uniform vec3 u_LightDirection; \n"
				" \n"
				"uniform bool u_FogEnabled; \n"
				"uniform float u_FogMinDistance; \n"
				"uniform float u_FogMaxDistance; \n"
				" \n"
				"uniform float u_SideLength; \n"
				" \n"
				"out vec3 v_LightWeighting; \n"
				"out vec2 v_TextureCoord; \n"
				"out float v_FogWeighting; \n"
				" \n"
				"void main(void) { \n"
				"	mat4 modelMatrix = u_IsInstanced ? a_ModelMatrix : u_ModelMatrix; \n"
				"	mat4 mvMatrix = u_ViewMatrix * modelMatrix; \n"
				"	vec3 objPosition = modelMatrix[3].xyz; \n"
				"	vec3 cameraPosition = inverse(u_ViewMatrix)[3].xyz; \n"
				" \n"
				"	gl_Position = u_PMatrix * mvMatrix * vec4(a_VertexPosition, 1.0 / u_SideLength); \n"
				"	v_TextureCoord = a_TextureCoord; \n"
				" \n"
				"	float cameraDistance = length(objPosition + mat3(modelMatrix) * a_VertexPosition * u_SideLength - cameraPosition); \n"
				"	v_LightWeighting = u_AmbientLight; \n"
				"	if(u_IsIlluminable) { \n"
				"		mat3 nMatrix = mat3(modelMatrix); \n"
				"		v_LightWeighting = min(u_AmbientLight + u_LightColor * max(dot(nMatrix * a_VertexNormal, -normalize(u_LightDirection)), 0.0), vec3(1.0, 1.0, 1.0)); \n"
				"	} \n"
				" \n"
				"	float fogDistance = cameraDistance; \n"
				"	v_FogWeighting = 0.0; \n"
				"	if(u_FogEnabled) { \n"
				"		// float fogWeighting = (fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance); \n"
				"		// float fogWeighting = sin(((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) * asin(1.0)); \n"
				"		float fogWeighting = (exp((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) - 1.0) / (exp(1.0) - 1.0); \n"
				"		// float fogWeighting = log((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance) + 1.0) / log(2.0); \n"
				"		v_FogWeighting = fogWeighting; \n"
				"	} \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.fragment_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"out vec4 outColor; \n"
				" \n"
				"in vec2 v_TextureCoord; \n"
				" \n"
				"in vec3 v_LightWeighting; \n"
				"in float v_FogWeighting; \n"
				" \n"
				"uniform sampler2D u_Sampler; \n"
				" \n"
				"uniform bool u_FogEnabled; \n"
				"uniform vec4 u_FogColor; \n"
				" \n"
				"void main(void) { \n"
				"	vec4 texColor = texture2D(u_Sampler, vec2(v_TextureCoord.s, v_TextureCoord.t)); \n"
				"	vec4 color = vec4(texColor.rgb * v_LightWeighting, texColor.a); \n"
				"	if(color.a <= 0.001) \n"
				"		discard; \n"
				"	if(u_FogEnabled) { \n"
				"		float fogWeighting = clamp(v_FogWeighting, 0.0, 1.0); \n"
				"		color = (1.0 - fogWeighting) * color + fogWeighting * u_FogColor; \n"
				"	} \n"
				"	outColor = color; \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.drawing_type = LH_DRAW_TRIANGLES,

		.nb_custom_uniforms = 2,
		.custom_uniforms = (struct lh_custom_uniform []){
			{ .name = "u_SideLength", .type = LH_TYPE_FLOAT },
			{ .name = "u_Sampler",    .type = LH_TYPE_INT   },
		},
	},

	.nb_textures = 1
};

int lhRegTexturedSurface(struct lh_window *window)
{
	return lhObjClassReg(window, &TexturedSurface_obj_class);
}

void lhUnregTexturedSurface(struct lh_window *window)
{
	lhObjClassUnreg(window, &TexturedSurface_obj_class);
}

struct lh_obj *lhNewTexturedSurface(struct lh_window *window, float factor, LHuint nsquares_x, LHuint nsquares_y, float *surface_vertices, lh_vec3_t *pos, lh_quat_t *angle, LHuint texture, LHbool is_visible)
{
	struct lh_obj *surface;
	lh_textured_surface_private_t *private;
	float *vertices, *vertex_normals, *tex_coords;
	LHuint *vertex_indices;
	LHuint nvertices;
	LHuint nindices;

	surface = malloc(sizeof(*surface));
	if(!surface)
		return NULL;

	surface->private_data = malloc(sizeof(lh_textured_surface_private_t));
	if(!surface->private_data)
		return NULL;
	private = (lh_textured_surface_private_t *)surface->private_data;

	surface->custom_uniforms_data = malloc(sizeof(*surface->custom_uniforms_data) * TexturedSurface_obj_class.shader_program.nb_custom_uniforms);
	if(!surface->custom_uniforms_data)
		return NULL;

	surface->custom_textures = malloc(sizeof(*surface->custom_textures) * TexturedSurface_obj_class.nb_textures);
	if(!surface->custom_textures)
		return NULL;

	lh_vec3_dup(&surface->pos, pos);
	lh_quat_dup(&surface->angle, angle);
	surface->is_visible = is_visible;
	surface->culling_enabled = LH_TRUE;
	surface->reversed_culling = LH_FALSE;
	surface->depth_test_enabled = LH_TRUE;
	surface->is_illuminable = LH_TRUE;

	private->nsquares_x = nsquares_x;
	private->nsquares_y = nsquares_y;

	surface->custom_uniforms_data[0].as_float = factor;
	surface->custom_uniforms_data[1].as_int = 0;

	surface->custom_textures[0] = texture;

	lhObjReg(window, &TexturedSurface_obj_class, surface);

	surface->custom_buffers[LH_BUF_POS].nb_items = (nsquares_x + 1) * (nsquares_y + 1);

	nvertices = surface->custom_buffers[LH_BUF_POS].nb_items * LH_BUF_ITEM_SIZE(LH_BUF_POS);

	vertices = lhAllocBufferData(LH_BUF_POS, nvertices);
	if(!vertices)
		return NULL;

	surface->custom_buffers[LH_BUF_POS].data = vertices;

	lhSetupCustomBuffer(window, surface, LH_BUF_POS);


	surface->custom_buffers[LH_BUF_NORMAL].nb_items = (nsquares_x + 1) * (nsquares_y + 1);

	vertex_normals = lhAllocBufferData(LH_BUF_NORMAL, nvertices);
	if(!vertex_normals)
		return NULL;

	surface->custom_buffers[LH_BUF_NORMAL].data = vertex_normals;

	lhSetupCustomBuffer(window, surface, LH_BUF_NORMAL);


	surface->custom_buffers[LH_BUF_TEX_COORD].nb_items = (nsquares_x + 1) * (nsquares_y + 1);

	tex_coords = lhAllocBufferData(LH_BUF_TEX_COORD, nvertices);
	if(!tex_coords)
		return NULL;

	for(LHuint i=0; i<=nsquares_y; i++) {
		for(LHuint j=0; j<=nsquares_x; j++) {
			LHuint idx = 2 * ((nsquares_x + 1) * i + j);

			tex_coords[idx] = (double)i / (double)nsquares_y;
			tex_coords[idx + 1] = (double)j / (double)nsquares_x;
		}
	}

	surface->custom_buffers[LH_BUF_TEX_COORD].data = tex_coords;

	lhSetupCustomBuffer(window, surface, LH_BUF_TEX_COORD);


	surface->custom_buffers[LH_BUF_INDEX].nb_items = 6 * nsquares_x * nsquares_y;

	nindices = surface->custom_buffers[LH_BUF_INDEX].nb_items * LH_BUF_ITEM_SIZE(LH_BUF_INDEX);

	vertex_indices = lhAllocBufferData(LH_BUF_INDEX, nindices);
	if(!vertex_indices)
		return NULL;

	for(LHuint i=0; i<nsquares_y; i++) {
		for(LHuint j=0; j<nsquares_x; j++) {
			LHuint idx = 6 * (nsquares_x * i + j);

			vertex_indices[idx]   = nsquares_x * i + i + j + 1;
			vertex_indices[idx+1] = nsquares_x * (i + 1) + i + j + 1;
			vertex_indices[idx+2] = nsquares_x * i + i + j;

			vertex_indices[idx+3] = nsquares_x * i + i + j + 1;
			vertex_indices[idx+4] = nsquares_x * (i + 1) + i + j + 2;
			vertex_indices[idx+5] = nsquares_x * (i + 1) + i + j + 1;
		}
	}

	surface->custom_buffers[LH_BUF_INDEX].data = vertex_indices;

	lhSetupCustomBuffer(window, surface, LH_BUF_INDEX);

	lhTexturedSurfaceUploadVertices(window, surface, surface_vertices);

	return surface;
}

void lhDeleteTexturedSurface(struct lh_window *window, struct lh_obj *surface)
{
	lhObjUnreg(window, surface);

	free(surface->custom_buffers[LH_BUF_POS].data);
	free(surface->custom_buffers[LH_BUF_NORMAL].data);
	free(surface->custom_buffers[LH_BUF_TEX_COORD].data);
	free(surface->custom_buffers[LH_BUF_INDEX].data);

	lhDeleteCustomBuffer(window, surface, LH_BUF_POS);
	lhDeleteCustomBuffer(window, surface, LH_BUF_NORMAL);
	lhDeleteCustomBuffer(window, surface, LH_BUF_TEX_COORD);
	lhDeleteCustomBuffer(window, surface, LH_BUF_INDEX);

	free(surface->custom_textures);
	free(surface->custom_uniforms_data);
	free(surface->private_data);
	free(surface);
}

void lhTexturedSurfaceUploadVertices(struct lh_window *window, struct lh_obj *surface, float *new_vertices)
{
	lh_textured_surface_private_t *private = (lh_textured_surface_private_t *)surface->private_data;
	lh_vec3_t *vertices, *vertex_normals;

	vertices = surface->custom_buffers[LH_BUF_POS].data;
	vertex_normals = surface->custom_buffers[LH_BUF_NORMAL].data;

	memcpy(vertices, new_vertices, lhGetBufferDataLen(LH_BUF_INDEX, surface->custom_buffers[LH_BUF_NORMAL].nb_items) * LH_BUF_ITEM_SIZE(LH_BUF_NORMAL));

	for(LHuint i = 0; i <= private->nsquares_y; i++) {
		for(LHuint j = 0; j < private->nsquares_x; j++) {
			LHuint idx = (private->nsquares_x + 1) * i + j;

			lh_vec3_t n, a, b, c;

			if(i == private->nsquares_y) {
				lh_vec3_dup(&vertex_normals[idx], &vertex_normals[idx - (private->nsquares_x + 1)]);
				lh_vec3_dup(&vertex_normals[idx + 1], &vertex_normals[idx - private->nsquares_x]);
			} else {
				lh_vec3_sub(&a, &vertices[idx], &vertices[idx + 3]);
				lh_vec3_sub(&b, &vertices[idx], &vertices[idx + (private->nsquares_x + 1)]);
				lh_vec3_mul_cross(&c, &a, &b);
				lh_vec3_norm(&n, &c);

				lh_vec3_dup(&vertex_normals[idx], &n);
				lh_vec3_dup(&vertex_normals[idx + 1], &n);
			}
		}
	}

	lhUpdateCustomBuffer(window, surface, LH_BUF_POS);
	lhUpdateCustomBuffer(window, surface, LH_BUF_NORMAL);
}
