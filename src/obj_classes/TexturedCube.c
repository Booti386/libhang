/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>

#include <libhang.h>
#include <libhang/obj_classes/TexturedCube.h>

static struct lh_obj_class TexturedCube_obj_class = {
	.name = "TexturedCube",

	.is_visible = LH_TRUE,
	.priv_flags = 0,
	.uses = LH_BUF_POS_FLAG
			| LH_BUF_NORMAL_FLAG
			| LH_BUF_TEX_COORD_FLAG
			| LH_BUF_INDEX_FLAG
			| LH_TEX_FLAG,
	.overrides = LH_TEX_FLAG,

	.shader_program = {
		.vertex_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"in vec3 a_VertexPosition; \n"
				"in vec3 a_VertexNormal; \n"
				"in vec2 a_TextureCoord; \n"
				"in mat4 a_ModelMatrix; \n"
				" \n"
				"uniform bool u_IsInstanced; \n"
				"uniform mat4 u_ModelMatrix; \n"
				"uniform mat4 u_ViewMatrix; \n"
				"uniform mat4 u_PMatrix; \n"
				" \n"
				"uniform vec3 u_AmbientLight; \n"
				" \n"
				"uniform bool u_IsIlluminable; \n"
				"uniform vec3 u_LightColor; \n"
				"uniform vec3 u_LightDirection; \n"
				" \n"
				"uniform bool u_FogEnabled; \n"
				"uniform float u_FogMinDistance; \n"
				"uniform float u_FogMaxDistance; \n"
				" \n"
				"uniform float u_SideLength; \n"
				" \n"
				"out vec3 v_LightWeighting; \n"
				"out vec2 v_TextureCoord; \n"
				"out float v_FogWeighting; \n"
				" \n"
				"void main(void) { \n"
				"	mat4 modelMatrix = u_IsInstanced ? a_ModelMatrix : u_ModelMatrix; \n"
				"	mat4 mvMatrix = u_ViewMatrix * modelMatrix; \n"
				"	vec3 objPosition = modelMatrix[3].xyz; \n"
				"	vec3 cameraPosition = inverse(u_ViewMatrix)[3].xyz; \n"
				" \n"
				"	gl_Position = u_PMatrix * mvMatrix * vec4(a_VertexPosition, 1.0 / u_SideLength); \n"
				"	v_TextureCoord = a_TextureCoord; \n"
				" \n"
				"	float cameraDistance = length(objPosition + mat3(modelMatrix) * a_VertexPosition * u_SideLength - cameraPosition); \n"
				"	v_LightWeighting = u_AmbientLight; \n"
				"	if(u_IsIlluminable) { \n"
				"		mat3 nMatrix = mat3(modelMatrix); \n"
				"		v_LightWeighting = min(u_AmbientLight + u_LightColor * max(dot(nMatrix * a_VertexNormal, -normalize(u_LightDirection)), 0.0), vec3(1.0, 1.0, 1.0)); \n"
				"	} \n"
				" \n"
				"	float fogDistance = cameraDistance; \n"
				"	v_FogWeighting = 0.0; \n"
				"	if(u_FogEnabled) { \n"
				"		// float fogWeighting = (fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance); \n"
				"		// float fogWeighting = sin(((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) * asin(1.0)); \n"
				"		float fogWeighting = (exp((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) - 1.0) / (exp(1.0) - 1.0); \n"
				"		// float fogWeighting = log((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance) + 1.0) / log(2.0); \n"
				"		v_FogWeighting = fogWeighting; \n"
				"	} \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.fragment_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"out vec4 outColor; \n"
				" \n"
				"in vec2 v_TextureCoord; \n"
				" \n"
				"in vec3 v_LightWeighting; \n"
				"in float v_FogWeighting; \n"
				" \n"
				"uniform sampler2D u_Sampler; \n"
				" \n"
				"uniform bool u_FogEnabled; \n"
				"uniform vec4 u_FogColor; \n"
				" \n"
				"void main(void) { \n"
				"	vec4 texColor = texture2D(u_Sampler, vec2(v_TextureCoord.s, v_TextureCoord.t)); \n"
				"	vec4 color = vec4(texColor.rgb * v_LightWeighting, texColor.a); \n"
				"	if(color.a <= 0.001) \n"
				"		discard; \n"
				"	if(u_FogEnabled) { \n"
				"		float fogWeighting = clamp(v_FogWeighting, 0.0, 1.0); \n"
				"		color = (1.0 - fogWeighting) * color + fogWeighting * u_FogColor; \n"
				"	} \n"
				"	outColor = color; \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.drawing_type = LH_DRAW_TRIANGLES,

		.nb_custom_uniforms = 2,
		.custom_uniforms = (struct lh_custom_uniform []){
			{ .name = "u_SideLength", .type = LH_TYPE_FLOAT },
			{ .name = "u_Sampler",    .type = LH_TYPE_INT   },
		},
	},
	.buffers = {
		[LH_BUF_POS] = {
			.nb_items = 24,
			.data = (float [3*24]){
				/* Front face */
				-.5, -.5, .5,
				 .5, -.5, .5,
				 .5,  .5, .5,
				-.5,  .5, .5,

				/* Back face */
				-.5, -.5, -.5,
				-.5,  .5, -.5,
				 .5,  .5, -.5,
				 .5, -.5, -.5,

				/* Top face */
				-.5,  .5, -.5,
				-.5,  .5,  .5,
				 .5,  .5,  .5,
				 .5,  .5, -.5,

				/* Bottom face */
				-.5, -.5, -.5,
				 .5, -.5, -.5,
				 .5, -.5,  .5,
				-.5, -.5,  .5,

				/* Right face */
				 .5, -.5, -.5,
				 .5,  .5, -.5,
				 .5,  .5,  .5,
				 .5, -.5,  .5,

				/* Left face */
				-.5, -.5, -.5,
				-.5, -.5,  .5,
				-.5,  .5,  .5,
				-.5,  .5, -.5,
			},
		},

		[LH_BUF_NORMAL] = {
			.nb_items = 24,
			.data = (float [3*24]){
				/* Front face */
				0.,  0.,  1.,
				0.,  0.,  1.,
				0.,  0.,  1.,
				0.,  0.,  1.,

				/* Back face */
				0.,  0., -1.,
				0.,  0., -1.,
				0.,  0., -1.,
				0.,  0., -1.,

				/* Top face */
				0.,  1.,  0.,
				0.,  1.,  0.,
				0.,  1.,  0.,
				0.,  1.,  0.,

				/* Bottom face */
				0., -1.,  0.,
				0., -1.,  0.,
				0., -1.,  0.,
				0., -1.,  0.,

				/* Right face */
				1.,  0.,  0.,
				1.,  0.,  0.,
				1.,  0.,  0.,
				1.,  0.,  0.,

				/* Left face */
				-1., 0.,  0.,
				-1., 0.,  0.,
				-1., 0.,  0.,
				-1., 0.,  0.,
			},
		},

		[LH_BUF_TEX_COORD] = {
			.nb_items = 24,
			.data = (float [2*24]){
				/* Front face */
				.0f,       .5f,
				.3333333f, .5f,
				.3333333f, .0f,
				.0f,       .0f,

				/* Back face */
				.6666666f, .5f,
				.6666666f, .0f,
				.3333333f, .0f,
				.3333333f, .5f,

				/* Top face */
				.6666666f, .0f,
				.6666666f, .5f,
				1.f,       .5f,
				1.f,       .0f,

				/* Bottom face */
				.3333333f, .5f,
				.0f,       .5f,
				.0f,       1.f,
				.3333333f, 1.f,

				/* Right face */
				.6666666f, 1.f,
				.6666666f, .5f,
				.3333333f, .5f,
				.3333333f, 1.f,

				/* Left face */
				.6666666f, 1.f,
				1.f,       1.f,
				1.f,       .5f,
				.6666666f, .5f,
			},
		},

		[LH_BUF_INDEX] = {
			.nb_items = 36,
			.data = (int [1*36]){
				0, 1, 2,      0, 2, 3,    /* Front face  */
				4, 5, 6,      4, 6, 7,    /* Back face   */
				8, 9, 10,     8, 10, 11,  /* Top face    */
				12, 13, 14,   12, 14, 15, /* Bottom face */
				16, 17, 18,   16, 18, 19, /* Right face  */
				20, 21, 22,   20, 22, 23, /* Left face   */
			},
		},
	},

	.nb_textures = 1
};

int lhRegTexturedCube(struct lh_window *window)
{
	return lhObjClassReg(window, &TexturedCube_obj_class);
}

void lhUnregTexturedCube(struct lh_window *window)
{
	lhObjClassUnreg(window, &TexturedCube_obj_class);
}

struct lh_obj *lhNewTexturedCube(struct lh_window *window, float side, lh_vec3_t *pos, lh_quat_t *angle, LHuint texture, LHbool is_visible)
{
	struct lh_obj *cube;

	cube = malloc(sizeof(*cube));
	if(!cube)
		return NULL;

	cube->custom_uniforms_data = malloc(sizeof(*cube->custom_uniforms_data) * TexturedCube_obj_class.shader_program.nb_custom_uniforms);
	if(!cube->custom_uniforms_data)
		return NULL;

	cube->custom_textures = malloc(sizeof(*cube->custom_textures) * TexturedCube_obj_class.nb_textures);
	if(!cube->custom_textures)
		return NULL;

	cube->delete_proc = lhDeleteTexturedCube;

	lh_vec3_dup(&cube->pos, pos);
	lh_quat_dup(&cube->angle, angle);
	cube->is_visible = is_visible;
	cube->culling_enabled = LH_TRUE;
	cube->reversed_culling = LH_FALSE;
	cube->depth_test_enabled = LH_TRUE;
	cube->is_illuminable = LH_TRUE;

	cube->custom_uniforms_data[0].as_float = side;
	cube->custom_uniforms_data[1].as_int = 0;

	cube->custom_textures[0] = texture;

	lhObjReg(window, &TexturedCube_obj_class, cube);

	return cube;
}

void lhTexturedCubeSetTexture(struct lh_obj *cube, LHuint texture)
{
	cube->custom_textures[0] = texture;
}

void lhDeleteTexturedCube(struct lh_window *window, struct lh_obj *cube)
{
	lhObjUnreg(window, cube);

	free(cube->custom_uniforms_data);
	free(cube);
}
