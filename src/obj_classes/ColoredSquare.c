/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>

#include <libhang.h>
#include <libhang/obj_classes/ColoredSquare.h>

static struct lh_obj_class ColoredSquare_obj_class = {
	.name = "ColoredSquare",

	.is_visible = LH_TRUE,
	.priv_flags = 0,
	.uses = LH_BUF_POS_FLAG
			| LH_BUF_NORMAL_FLAG
			| LH_BUF_INDEX_FLAG,
	.overrides = 0,

	.shader_program = {
		.vertex_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"in vec3 a_VertexPosition; \n"
				"in vec3 a_VertexNormal; \n"
				"in mat4 a_ModelMatrix; \n"
				" \n"
				"uniform bool u_IsInstanced; \n"
				"uniform mat4 u_ModelMatrix; \n"
				"uniform mat4 u_ViewMatrix; \n"
				"uniform mat4 u_PMatrix; \n"
				" \n"
				"uniform vec3 u_AmbientLight; \n"
				" \n"
				"uniform bool u_IsIlluminable; \n"
				"uniform vec3 u_LightColor; \n"
				"uniform vec3 u_LightDirection; \n"
				" \n"
				"uniform bool u_FogEnabled; \n"
				"uniform float u_FogMinDistance; \n"
				"uniform float u_FogMaxDistance; \n"
				" \n"
				"uniform float u_SideLength; \n"
				" \n"
				"out vec3 v_LightWeighting; \n"
				"out float v_FogWeighting; \n"
				" \n"
				"void main(void) { \n"
				"	mat4 modelMatrix = u_IsInstanced ? a_ModelMatrix : u_ModelMatrix; \n"
				"	mat4 mvMatrix = u_ViewMatrix * modelMatrix; \n"
				"	vec3 objPosition = modelMatrix[3].xyz; \n"
				"	vec3 cameraPosition = inverse(u_ViewMatrix)[3].xyz; \n"
				" \n"
				"	gl_Position = u_PMatrix * mvMatrix * vec4(a_VertexPosition, 1.0 / u_SideLength); \n"
				" \n"
				"	float cameraDistance = length(objPosition + mat3(modelMatrix) * a_VertexPosition * u_SideLength - cameraPosition); \n"
				"	v_LightWeighting = u_AmbientLight; \n"
				"	if(u_IsIlluminable) { \n"
				"		mat3 nMatrix = mat3(modelMatrix); \n"
				"		v_LightWeighting = min(u_AmbientLight + u_LightColor * max(dot(nMatrix * a_VertexNormal, -normalize(u_LightDirection)), 0.0), vec3(1.0, 1.0, 1.0)); \n"
				"	} \n"
				" \n"
				"	float fogDistance = cameraDistance; \n"
				"	v_FogWeighting = 0.0; \n"
				"	if(u_FogEnabled) { \n"
				"		// float fogWeighting = (fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance); \n"
				"		// float fogWeighting = sin(((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) * asin(1.0)); \n"
				"		float fogWeighting = (exp((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) - 1.0) / (exp(1.0) - 1.0); \n"
				"		// float fogWeighting = log((fogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance) + 1.0) / log(2.0); \n"
				"		v_FogWeighting = fogWeighting; \n"
				"	} \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.fragment_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"out vec4 outColor; \n"
				" \n"
				"in vec3 v_LightWeighting; \n"
				"in float v_FogWeighting; \n"
				" \n"
				"uniform vec4 u_Color; \n"
				" \n"
				"uniform bool u_FogEnabled; \n"
				"uniform vec4 u_FogColor; \n"
				" \n"
				"void main(void) { \n"
				"	vec4 color = vec4(u_Color.rgb * v_LightWeighting, u_Color.a); \n"
				"	if(color.a <= 0.001) \n"
				"		discard; \n"
				"	if(u_FogEnabled) { \n"
				"		float fogWeighting = clamp(v_FogWeighting, 0.0, 1.0); \n"
				"		color = (1.0 - fogWeighting) * color + fogWeighting * u_FogColor; \n"
				"	} \n"
				"	outColor = color; \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.drawing_type = LH_DRAW_TRIANGLES,

		.nb_custom_uniforms = 2,
		.custom_uniforms = (struct lh_custom_uniform []){
			{ .name = "u_SideLength", .type = LH_TYPE_FLOAT },
			{ .name = "u_Color",      .type = LH_TYPE_VEC4  },
		},
	},
	.buffers = {
		[LH_BUF_POS] = {
			.nb_items = 4,
			.data = (float [3*4]){
				-.5, 0., -.5,
				-.5, 0.,  .5,
				 .5, 0.,  .5,
				 .5, 0., -.5,
			},
		},

		[LH_BUF_NORMAL] = {
			.nb_items = 4,
			.data = (float [3*4]){
				0., 1., 0.,
				0., 1., 0.,
				0., 1., 0.,
				0., 1., 0.,
			},
		},

		[LH_BUF_INDEX] = {
			.nb_items = 6,
			.data = (int [1*6]){
				0, 1, 2,   0, 2, 3
			},
		},
	},
};

int lhRegColoredSquare(struct lh_window *window)
{
	return lhObjClassReg(window, &ColoredSquare_obj_class);
}

void lhUnregColoredSquare(struct lh_window *window)
{
	lhObjClassUnreg(window, &ColoredSquare_obj_class);
}

struct lh_obj *lhNewColoredSquare(struct lh_window *window, float side, lh_vec3_t *pos, lh_quat_t *angle, lh_rgba_t *color, LHbool is_visible)
{
	struct lh_obj *square;

	square = malloc(sizeof(*square));
	if(!square)
		return NULL;

	square->custom_uniforms_data = malloc(sizeof(*square->custom_uniforms_data) * ColoredSquare_obj_class.shader_program.nb_custom_uniforms);
	if(!square->custom_uniforms_data) {
		free(square);
		return NULL;
	}

	square->delete_proc = lhDeleteColoredSquare;

	lh_vec3_dup(&square->pos, pos);
	lh_quat_dup(&square->angle, angle);
	square->is_visible = is_visible;
	square->culling_enabled = LH_TRUE;
	square->reversed_culling = LH_FALSE;
	square->depth_test_enabled = LH_TRUE;
	square->is_illuminable = LH_TRUE;

	square->custom_uniforms_data[0].as_float = side;
	lh_rgba_dup(&square->custom_uniforms_data[1].as_rgba, color);

	lhObjReg(window, &ColoredSquare_obj_class, square);

	return square;
}

void lhColoredSquareSetColor(struct lh_window *window, struct lh_obj *square, lh_rgba_t *color)
{
	lh_mark_used(window);

	lh_rgba_dup(&square->custom_uniforms_data[1].as_rgba, color);
}

void lhDeleteColoredSquare(struct lh_window *window, struct lh_obj *square)
{
	lhObjUnreg(window, square);

	free(square->custom_uniforms_data);
	free(square);
}
