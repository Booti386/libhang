libhang := libhang$(DYN_LIB_EXT)

objs-y := \
	core.c.o \
	fb2d.c.o \
	images.c.o \
	opensimplexnoise.c.o \
	obj_file.c.o \
	perlinnoise.c.o \
	skybox.c.o \
	text.c.o \
	obj_classes/ColoredCube.c.o \
	obj_classes/TexturedCube.c.o \
	obj_classes/ColoredCuboid.c.o \
	obj_classes/ColoredSquare.c.o \
	obj_classes/TexturedSquare.c.o \
	obj_classes/ColoredSurface.c.o \
	obj_classes/TexturedSurface.c.o

os_drv_linux_objs   := drivers/os/linux.c.o
os_drv_windows_objs := drivers/os/windows.c.o
os_drv_macosx_objs  := drivers/os/macosx.c.o

gl_drv_glx_xcb_objs := drivers/gl/glx_xcb.c.o
gl_drv_wgl_win_objs := drivers/gl/wgl_win.c.o

CFLAGS-y += -I. -Idrivers

CFLAGS-$(CONFIG_OS_LINUX)   += -fPIC
CFLAGS-$(CONFIG_OS_WINDOWS) +=
CFLAGS-$(CONFIG_OS_MACOSX)  += -fPIC

CFLAGS-$(os_drv_linux_objs)-y   := -pthread -D_POSIX_C_SOURCE=201504L
CFLAGS-$(os_drv_windows_objs)-y := -fms-extensions
CFLAGS-$(os_drv_macosx_objs)-y  :=

drv_objs-y := drivers/driver.c.o
drv_objs-$(CONFIG_OS_LINUX)   += $(os_drv_linux_objs)
drv_objs-$(CONFIG_OS_WINDOWS) += $(os_drv_windows_objs)
drv_objs-$(CONFIG_OS_MACOSX)  += $(os_drv_macosx_objs)

drv_objs-$(CONFIG_GL_GLX_XCB) += $(gl_drv_glx_xcb_objs)
drv_objs-$(CONFIG_GL_WGL_WIN) += $(gl_drv_wgl_win_objs)

LDFLAGS-$(CONFIG_OS_LINUX)   += -pthread
LDFLAGS-$(CONFIG_OS_WINDOWS) += -lpsapi
LDFLAGS-$(CONFIG_OS_MACOSX)  +=

LDFLAGS-$(CONFIG_GL_GLX_XCB) += -lGL -lX11 -lX11-xcb -lxcb -lxcb-keysyms -lxcb-icccm -lxcb-image -lxcb-sync
LDFLAGS-$(CONFIG_GL_WGL_WIN) += -lopengl32 -lgdi32

objs-y += $(drv_objs-y)

LDFLAGS-$(libhang)-y += $(LDFLAGS_SHARED)
LDFLAGS-$(libhang)-y += -lpng

BINS-y := $(libhang)

OBJS-$(libhang)-y := $(objs-y)
