/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libhang.h>

#include "cmd.h"
#include "core.h"
#include "fb2d.h"
#include "skybox.h"
#include "drivers/driver.h"

/* For Windows */
#undef near
#undef far

/* For glDrawElements */
#define BUFFER_OFFSET(count, length) ((void *)(count * length))


static int emit_sub_cmd_helper(struct SyncLinkedList *cmds, LHuint type, LHuint nb_params, struct lh_graphic_cmd_param params[])
{
	struct lh_graphic_cmd *cmd;

	cmd = malloc(sizeof(*cmd) + nb_params * sizeof(params[0]));
	if(!cmd)
		return -1;
	cmd->type = type;
	memcpy(cmd->params, params, nb_params * sizeof(params[0]));

	list_push(sync_list_get_unsafe_list(cmds), cmd);
	return 0;
}

static int emit_sub_cmd(struct lh_window *window, LHuint type, LHuint nb_params, struct lh_graphic_cmd_param params[])
{
	return emit_sub_cmd_helper(lhAtomicPointerGet(&window->core->graphic_cmds), type, nb_params, params);
}

static int emit_cmd(struct lh_window *window, LHuint type, LHuint nb_params, struct lh_graphic_cmd_param params[])
{
	struct lh_core_ctx *core = window->core;
	struct SyncLinkedList *cmds;
	int ret;

	while(1) {
		cmds = lhAtomicPointerGet(&core->graphic_cmds);
		sync_list_lock(cmds);
		if(cmds == lhAtomicPointerGet(&core->graphic_cmds))
			break;
		sync_list_unlock(cmds);
	}
	ret = emit_sub_cmd_helper(cmds, type, nb_params, params);
	sync_list_unlock(cmds);
	return ret;
}

static void *begin_cmd(struct lh_window *window, LHuint nb_elems)
{
	struct lh_core_ctx *core = window->core;
	lh_dynamic_type_t *stor;
	struct SyncLinkedList *cmds;

	if(nb_elems) {
		stor = malloc(nb_elems * sizeof(stor[0]));
		if(!stor)
			return NULL;
	} else
		stor = NULL;

	while(1) {
		cmds = lhAtomicPointerGet(&core->graphic_cmds);
		sync_list_lock(cmds);
		if(cmds == lhAtomicPointerGet(&core->graphic_cmds))
			break;
		sync_list_unlock(cmds);
	}
	emit_sub_cmd(window, CMD_BEGIN, CMD_DATA(
		1,
		CMD_PARAM_IMM(ptr, stor)
	));

	return stor;
}

static void end_cmd(struct lh_window *window)
{
	emit_sub_cmd(window, CMD_END, CMD_DATA0());
	sync_list_unlock(lhAtomicPointerGet(&window->core->graphic_cmds));
}

static inline void compute_pMatrix(struct lh_window *window) {
	struct lh_core_ctx *core = window->core;

	lh_mat4_perspective(&core->pMatrix, 45, (float)window->width / (float)window->height, core->render_distances.near, core->render_distances.far);
}

static LHuint on_event(struct lh_event *ev, void *param) {
	struct lh_window *window = param;
	struct lh_core_ctx *core = window->core;

	switch(ev->type) {
		case LH_EVENT_RESIZED:
			core->is_win_resized = LH_TRUE;
			lhDirtyObjBuffer(window);
			break;

		case LH_EVENT_REDRAW:
			lhRedraw(window);
			break;
	}
	return LH_EVENT_RET_UNHANDLED;
}

int lhCoreInit(struct lh_window *window) {
	struct lh_core_ctx *core;

	core = malloc(sizeof(*core));
	if(!core)
		return -1;
	window->core = core;

	sync_list_init(&core->immediate_cmds);
	sync_list_init(&core->deferred_cmds);
	lhAtomicPointerSet(&core->graphic_cmds, &core->immediate_cmds);

	/* Display rendering subsystem versions */
	emit_cmd_core_show_versions(window);

	/* Enable primary texture unit */
	emit_cmd_graph_active_texture(window, GL_TEXTURE0);

	/* Enable depth test */
	emit_cmd_graph_enable(window, GL_DEPTH_TEST);
	emit_cmd_graph_depth_func(window, GL_LEQUAL);
	core->depth_test_enabled = LH_TRUE;

	/* Enable blending */
	emit_cmd_graph_enable(window, GL_BLEND);
	emit_cmd_graph_blend_func(window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* Don't show back faces */
	emit_cmd_graph_enable(window, GL_CULL_FACE);
	core->culling_enabled = LH_TRUE;
	emit_cmd_graph_front_face(window, GL_CCW);
	emit_cmd_graph_cull_face(window, GL_BACK);
	core->reversed_culling = LH_FALSE;

	emit_cmd_graph_viewport(window, 0, 0, window->width, window->height);

	/* Dummy VAO needed for drawing */
	emit_cmd_graph_gen_vertex_array(window, &core->draw_vao);
	emit_cmd_graph_bind_vertex_array_ptr(window, &core->draw_vao);

	emit_cmd_graph_draw_buffer(window, GL_BACK);

	core->is_obj_buffer_dirty = LH_FALSE;
	core->is_win_resized = LH_FALSE;
	list_init(&core->obj_classes);
	list_init(&core->textures);

	lh_distances_zero(&core->render_distances);
	lh_rgba_dup(&core->back_color, &LH_RGBA(0.f, 0.f, 0.f, 1.f));
	lh_rgb_dup(&core->ambient_light, &LH_RGB(1.f, 1.f, 1.f));

	for(LHuint i = 0; i < LH_MAX_LIGHTS; i++) {
		core->lights[i].type = LH_LIGHT_DISABLED;
		lh_rgb_dup(&core->lights[i].color, &LH_RGB(0.f, 0.f, 0.f));
		lh_vec3_zero(&core->lights[i].direction);
	}

	lh_vec3_zero(&core->camera.pos);
	lh_quat_identity(&core->camera.angle);

	core->fog_enabled = LH_FALSE;
	lh_distances_zero(&core->fog_distances);
	lh_rgba_dup(&core->fog_color, &LH_RGBA(0.f, 0.f, 0.f, 0.f));

	core->msaa.nb_samples = LH_MSAA_NONE;
	core->msaa.fbo = 0;
	core->msaa.color_renderbuf = 0;
	core->msaa.depth_renderbuf = 0;

	lhAtomicCounterSet(&core->counter_frames, 0);

	/* Internal inits. */
	lhSkyBoxInit(window);
	lhFB2DInit(window);

	core->internal_ev_handler_id = lhAddInternalEventHandler(window, on_event, window);

	return 0;
}

int lhCoreDeinit(struct lh_window *window) {
	struct lh_core_ctx *core = window->core;
	struct LinkedListIterator iterator;

	lhRemoveEventHandler(window, core->internal_ev_handler_id);

	/* Internal deinits. */
	lhFB2DDeinit(window);
	lhSkyBoxDeinit(window);

	/* Unregister all sub-classes */
	list_it_init(&iterator, &core->obj_classes);
	for(struct lh_obj_class *obj_class = list_it_get_cur(&iterator); obj_class; obj_class = list_it_get_next(&iterator))
		lhObjClassUnreg(window, obj_class);

	/* Unload all textures */
	list_it_init(&iterator, &core->textures);
	for(int *tex = list_it_get_cur(&iterator); tex; tex = list_it_get_next(&iterator))
		lhTexUnload(window, *tex);

	lhAtomicPointerSet(&core->graphic_cmds, NULL);
	sync_list_deinit(&core->deferred_cmds);
	sync_list_deinit(&core->immediate_cmds);

	free(window->core);
	window->core = NULL;

	return 0;
}

lh_distances_t *lhGetRenderDistances(struct lh_window *window, lh_distances_t *dst_distances) {
	struct lh_core_ctx *core = window->core;

	lh_distances_dup(dst_distances, &core->render_distances);
	return dst_distances;
}

void lhSetRenderDistances(struct lh_window *window, lh_distances_t *src_distances) {
	struct lh_core_ctx *core = window->core;

	if(src_distances->near <= 0
			|| src_distances->far <= src_distances->near)
		return;

	lh_distances_dup(&core->render_distances, src_distances);
	compute_pMatrix(window);
}

lh_rgba_t *lhGetBackColor(struct lh_window *window, lh_rgba_t *dst_color) {
	struct lh_core_ctx *core = window->core;

	lh_rgba_dup(dst_color, &core->back_color);
	return dst_color;
}

void lhSetBackColor(struct lh_window *window, lh_rgba_t *src_color) {
	struct lh_core_ctx *core = window->core;

	lh_rgba_dup(&core->back_color, src_color);
	emit_cmd_graph_clear_color(window, &core->back_color);
}

lh_rgb_t *lhGetAmbientLight(struct lh_window *window, lh_rgb_t *dst_color) {
	struct lh_core_ctx *core = window->core;

	lh_rgb_dup(dst_color, &core->ambient_light);
	return dst_color;
}

void lhSetAmbientLight(struct lh_window *window, lh_rgb_t *src_color) {
	struct lh_core_ctx *core = window->core;

	lh_rgb_dup(&core->ambient_light, src_color);
}

void lhEnableLight(struct lh_window *window, LHuint num, LHuint type) {
	struct lh_core_ctx *core = window->core;

	if(num >= LH_MAX_LIGHTS)
		return;

	core->lights[num].type = type;
}

void lhSetLightParameters(struct lh_window *window, LHuint num, lh_rgb_t *color, lh_vec3_t *direction) {
	struct lh_core_ctx *core = window->core;
	struct lh_light *light = &core->lights[num];

	if(num >= LH_MAX_LIGHTS)
		return;

	lh_rgb_dup(&light->color, color);
	lh_vec3_dup(&light->direction, direction);
}

lh_vec3_t *lhGetCameraPos(struct lh_window *window, lh_vec3_t *dst_pos) {
	struct lh_core_ctx *core = window->core;

	lh_vec3_dup(dst_pos, &core->camera.real_pos);
	return dst_pos;
}

void lhSetCameraPos(struct lh_window *window, lh_vec3_t *src_pos) {
	struct lh_core_ctx *core = window->core;

	lh_vec3_dup(&core->camera.real_pos, src_pos);
	/* The camera is inverted relative to the scene. */
	lh_vec3_neg(&core->camera.pos, src_pos);
}

lh_quat_t *lhGetCameraAngle(struct lh_window *window, lh_quat_t *dst_angle) {
	struct lh_core_ctx *core = window->core;

	/* The camera is inverted relative to the scene. */
	lh_quat_conjug(dst_angle, &core->camera.angle);
	return dst_angle;
}

void lhSetCameraAngle(struct lh_window *window, lh_quat_t *src_angle) {
	struct lh_core_ctx *core = window->core;

	/* The camera is inverted relative to the scene. */
	lh_quat_conjug(&core->camera.angle, src_angle);
}

void lhEnableFog(struct lh_window *window, LHbool enabled) {
	struct lh_core_ctx *core = window->core;

	core->fog_enabled = enabled;
}

void lhSetFogParameters(struct lh_window *window, lh_distances_t *fog_distances, lh_rgba_t *color) {
	struct lh_core_ctx *core = window->core;

	lh_distances_dup(&core->fog_distances, fog_distances);
	lh_rgba_dup(&core->fog_color, color);
}

int lhBeginCmdBuffer(struct lh_window *window)
{
	struct lh_core_ctx *core = window->core;

	/* Don't disturb a begin/end pair. */
	sync_list_lock(&core->immediate_cmds);
	lhAtomicPointerSet(&core->graphic_cmds, &core->deferred_cmds);
	sync_list_unlock(&core->immediate_cmds);
	return 0;
}

struct lh_cmd_buffer *lhEndCmdBuffer(struct lh_window *window)
{
	struct lh_core_ctx *core = window->core;
	struct lh_cmd_buffer *buffer;
	struct LinkedList *cmds;

	buffer = malloc(sizeof(*buffer));
	if(!buffer)
		return NULL;
	list_init(&buffer->cmds);

	/* Don't disturb a begin/end pair. */
	cmds = sync_list_lock(&core->deferred_cmds);
	list_append(&buffer->cmds, cmds);
	lhAtomicPointerSet(&core->graphic_cmds, &core->immediate_cmds);
	sync_list_unlock(&core->deferred_cmds);
	return buffer;
}

static inline void optimize_cmds(struct LinkedListIterator *it);

int lhOptimizeCmdBuffer(struct lh_cmd_buffer *buffer)
{
	struct LinkedListIterator it;
	struct LinkedList *cmds = &buffer->cmds;

	list_it_init(&it, cmds);
	optimize_cmds(&it);
	list_it_deinit(&it);
	return 0;
}

int lhExecuteCmdBuffer(struct lh_window *window, struct lh_cmd_buffer *buffer)
{
	sync_list_append(&window->core->immediate_cmds, &buffer->cmds);
	list_deinit(&buffer->cmds);
	free(buffer);
	return 0;
}

LHuint lhTexLoadFromImage(struct lh_window *window, struct lh_image *image)
{
	if(image == LH_IMG_INVAL)
		return -1;

	switch(image->color_type)
	{
		case LH_IMG_COLOR_RGB:
			return lhTexLoadRGB(window, image->data, image->width, image->height);
		case LH_IMG_COLOR_RGBA:
			return lhTexLoadRGBA(window, image->data, image->width, image->height);
		default:
			return LH_TEX_INVAL;
	}
}

static LHulong tex_load(struct lh_window *window, void *data, LHuint width, LHuint height, LHuint align, LHuint format, LHuint data_len)
{
	struct lh_core_ctx *core = window->core;
	LHuint gl_id = 0;
	void *gl_id_ptr;
	LHulong tex_id;

	/* Do not interfere with drawing */
	lhAcquireGraphicLock(window);

	tex_id = list_push_dup(&core->textures, &gl_id, sizeof(gl_id));
	if(tex_id == LL_INVAL_ID)
		return LH_TEX_INVAL;
	gl_id_ptr = list_get_last_elem(&core->textures);

	lhReleaseGraphicLock(window);

	begin_cmd(window, 0);

	emit_sub_cmd_graph_gen_texture(window, gl_id_ptr);
	emit_sub_cmd_graph_bind_texture_ptr(window, GL_TEXTURE_2D, gl_id_ptr);

	emit_sub_cmd_graph_pixel_storei(window, GL_UNPACK_ALIGNMENT, align);
	emit_sub_cmd_graph_tex_image_2d_clone(window, GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data, data_len);
	emit_sub_cmd_graph_generate_mipmap(window, GL_TEXTURE_2D);
	emit_sub_cmd_graph_tex_parameteri(window, GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	emit_sub_cmd_graph_tex_parameteri(window, GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	emit_sub_cmd_graph_tex_parameteri(window, GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	emit_sub_cmd_graph_tex_parameteri(window, GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	end_cmd(window);

	return tex_id;
}

/* TODO: Full-rewrite and fix it! */
LHuint lhTexLoadFromWindow(struct lh_window *window, struct lh_window *src_window, LHuint color_type)
{
	struct lh_gl_funcs *gl = &window->gl;
	GLenum format;
	int align;
	int row_len;
	LHuint len;
	uint64_t tex_id;
	char *data;
	int width, height;

	width = src_window->width;
	height = src_window->height;

	switch(color_type)
	{
		/* LH_TEX_COLOR_RGB */
		case LH_TEX_COLOR_RGB888:
			format = GL_RGB;
			align = 1;
			row_len = width * sizeof(lh_rgb888_t);
			break;
		/* LH_TEX_COLOR_RGBA */
		case LH_TEX_COLOR_RGBA8888:
			format = GL_RGBA;
			align = 4;
			row_len = width * sizeof(lh_rgba8888_t);
			break;
		default:
			return LH_TEX_INVAL;
	}

	len = row_len * height;
	if(!len)
		return LH_TEX_INVAL;

	data = malloc(len);
	if(!data)
		return LH_TEX_INVAL;

	gl->ReadBuffer(GL_BACK);
	gl->ReadPixels(0, 0, width, height, format, GL_UNSIGNED_BYTE, data);
	for(int i = 0; i < height / 2; i++) {
		char *cur_row = data + i * row_len;
		char *cur_row_last = data + len - (i + 1) * row_len;
		for(int j = 0; j < row_len; j++) {
			char tmp;
			tmp = cur_row[j];
			cur_row[j] = cur_row_last[j];
			cur_row_last[j] = tmp;
		}
	}
/*
	if(src_window != window) {
		lhReleaseGraphicCtx(src_window);
		lhAcquireGraphicCtx(window);
	}*/

	tex_id = tex_load(window, data, width, height, align, format, len);
	/*lhReleaseGraphicCtx(window);*/

	return tex_id;
}

LHuint lhTexLoadRGB(struct lh_window *window, lh_rgb888_t *data, LHuint width, LHuint height)
{
	/* Lines are 1-byte aligned (3-bytes alignment is not supported) */
	return tex_load(window, data, width, height, 1, GL_RGB, width * height * sizeof(data[0]));
}

LHuint lhTexLoadRGBA(struct lh_window *window, lh_rgba8888_t *data, LHuint width, LHuint height)
{
	return tex_load(window, data, width, height, 4, GL_RGBA, width * height * sizeof(data[0]));
}

void lhTexEnableSmoothing(struct lh_window *window, LHuint tex_id, LHuint mask)
{
	struct lh_core_ctx *core = window->core;
	LHuint *gl_tex_ptr;
	int min_filter = GL_NEAREST_MIPMAP_NEAREST, mag_filter = GL_NEAREST;

	if(tex_id == LH_TEX_INVAL)
		return;

	begin_cmd(window, 0);

	gl_tex_ptr = list_get_elem_by_id(&core->textures, tex_id);
	if(gl_tex_ptr) {
		if(mask & LH_TEX_SMOOTH_LINEAR_FLAG) {
			if(mask & LH_TEX_SMOOTH_MIPMAP_LINEAR_FLAG)
				min_filter = GL_LINEAR_MIPMAP_LINEAR;
			else
				min_filter = GL_LINEAR_MIPMAP_NEAREST;
			mag_filter = GL_LINEAR;
		} else if(mask & LH_TEX_SMOOTH_MIPMAP_LINEAR_FLAG)
			min_filter = GL_NEAREST_MIPMAP_LINEAR;
		emit_sub_cmd_graph_bind_texture_ptr(window, GL_TEXTURE_2D, gl_tex_ptr);
		emit_sub_cmd_graph_tex_parameteri(window, GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter);
		emit_sub_cmd_graph_tex_parameteri(window, GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter);
	}

	end_cmd(window);
}

void lhTexUnload(struct lh_window *window, LHuint tex_id)
{
	struct lh_core_ctx *core = window->core;
	LHuint *gl_tex_ptr;

	if(tex_id == LH_TEX_INVAL)
		return;

	begin_cmd(window, 0);

	gl_tex_ptr = list_get_elem_by_id(&core->textures, tex_id);
	if(gl_tex_ptr) {
		/* Make sure that the texture is currently not bound */
		emit_sub_cmd_graph_bind_texture(window, GL_TEXTURE_2D, 0);
		emit_sub_cmd_graph_delete_texture(window, *gl_tex_ptr);
		list_remove(&core->textures, tex_id);
	}

	end_cmd(window);
}


static int update_buffer(struct lh_window *window, struct lh_obj_class *obj_class, struct lh_obj *obj, int type, LHbool is_obj_class)
{
	struct lh_buffer *bufs;
	struct lh_buffer *buf;
	LHuint data_len;
	int buf_type;
	int mask;

	if(type >= LH_NB_BUFS)
		return -1;

	if(is_obj_class)
		bufs = obj_class->buffers;
	else
		bufs = obj->custom_buffers;

	mask = LH_TO_FLAG(type);
	buf = &bufs[type];

	if(!buf->id)
		return -1;

	if(!(obj_class->uses & mask)
			|| (is_obj_class && (obj_class->overrides & mask))
			|| (!is_obj_class && !(obj_class->overrides & mask)))
		return 0;

	buf_type = GL_ARRAY_BUFFER;
	data_len = sizeof(float);

	if(type == LH_BUF_INDEX) {
		buf_type = GL_ELEMENT_ARRAY_BUFFER;
		data_len = sizeof(int);
	}

	begin_cmd(window, 1);

	emit_sub_cmd_graph_bind_buffer_ptr(window, buf_type, &buf->id);
	emit_sub_cmd_graph_map_buffer_range_ret_stor(window, buf_type, 0, data_len * LH_BUF_ITEM_SIZE(type) * buf->nb_items, GL_MAP_WRITE_BIT, CMD_STOR_IDX(0));
	emit_sub_cmd_memcpy_to_stor_from_clone(window, CMD_STOR_IDX(0), buf->data, data_len * LH_BUF_ITEM_SIZE(type) * buf->nb_items);
	emit_sub_cmd_graph_unmap_buffer(window, buf_type);

	end_cmd(window);

	return 0;
}

static int delete_buffer(struct lh_window *window, struct lh_obj_class *obj_class, struct lh_obj *obj, int type, LHbool is_obj_class)
{
	struct lh_buffer *bufs;
	struct lh_buffer *buf;
	int buf_type;
	int mask;

	if(type >= LH_NB_BUFS)
		return -1;

	if(is_obj_class)
		bufs = obj_class->buffers;
	else
		bufs = obj->custom_buffers;

	mask = LH_TO_FLAG(type);
	buf = &bufs[type];

	if(!buf->id)
		return -1;

	if(!(obj_class->uses & mask)
			|| (is_obj_class && (obj_class->overrides & mask))
			|| (!is_obj_class && !(obj_class->overrides & mask)))
		return 0;

	buf_type = GL_ARRAY_BUFFER;
	if(type == LH_BUF_INDEX)
		buf_type = GL_ELEMENT_ARRAY_BUFFER;

	begin_cmd(window, 0);

	/* Make sure the buffer is not currently bound */
	emit_sub_cmd_graph_bind_buffer(window, buf_type, 0);
	emit_sub_cmd_graph_delete_buffer_ptr(window, &buf->id);

	end_cmd(window);

	return 0;
}

static int setup_buffer(struct lh_window *window, struct lh_obj_class *obj_class, struct lh_obj *obj, LHuint type, LHbool is_obj_class)
{
	struct lh_buffer *bufs;
	struct lh_buffer *buf;
	LHuint data_len;
	int buf_type;
	int mask;

	if(type >= LH_NB_BUFS)
		return -1;

	if(is_obj_class)
		bufs = obj_class->buffers;
	else
		bufs = obj->custom_buffers;

	mask = LH_TO_FLAG(type);
	buf = &bufs[type];

	if(!(obj_class->uses & mask)
			|| (is_obj_class && (obj_class->overrides & mask))
			|| (!is_obj_class && !(obj_class->overrides & mask)))
		return 0;

	if(type == LH_BUF_INDEX) {
		buf_type = GL_ELEMENT_ARRAY_BUFFER;
		data_len = sizeof(int);
	} else {
		buf_type = GL_ARRAY_BUFFER;
		data_len = sizeof(float);
	}

	begin_cmd(window, 0);

	if(buf->id) {
		emit_sub_cmd_graph_bind_buffer(window, buf_type, 0);
		emit_sub_cmd_graph_delete_buffer_ptr(window, &buf->id);
	}

	emit_sub_cmd_graph_gen_buffer(window, &buf->id);
	emit_sub_cmd_graph_bind_buffer_ptr(window, buf_type, &buf->id);
	emit_sub_cmd_graph_buffer_data_clone(window, buf_type, data_len * LH_BUF_ITEM_SIZE(type) * buf->nb_items, buf->data, GL_STATIC_DRAW);

	end_cmd(window);

	return 0;
}

struct lh_obj_clone *lhObjCloneCreate(struct lh_window *window, struct lh_obj *parent_obj, lh_vec3_t *pos, lh_quat_t *angle)
{
	struct lh_obj_clone *obj_clone;

	lh_mark_used(window);

	obj_clone = malloc(sizeof(*obj_clone));
	if(!obj_clone)
		return NULL;

	obj_clone->obj = parent_obj;
	obj_clone->id = LL_INVAL_ID;
	lh_vec3_dup(&obj_clone->pos, pos);
	lh_quat_dup(&obj_clone->angle, angle);
	obj_clone->instanced_idx = 0;
	obj_clone->is_visible = LH_FALSE;

	return obj_clone;
}

int lhObjClassReg(struct lh_window *window, struct lh_obj_class *obj_class)
{
	struct lh_core_ctx *core = window->core;

	begin_cmd(window, 3);

	/* Shaders and Program */
	emit_sub_cmd_graph_create_shader_ret_stor(window, GL_VERTEX_SHADER, CMD_STOR_IDX(0));
	emit_sub_cmd_graph_create_shader_ret_stor(window, GL_FRAGMENT_SHADER, CMD_STOR_IDX(1));
	emit_sub_cmd_graph_create_program_ret_stor(window, CMD_STOR_IDX(2));

	emit_sub_cmd_graph_shader_source_stor(window, CMD_STOR_IDX(0),  obj_class->shader_program.vertex_src.nb_lines, obj_class->shader_program.vertex_src.src, NULL);
	emit_sub_cmd_graph_shader_source_stor(window, CMD_STOR_IDX(1),  obj_class->shader_program.fragment_src.nb_lines, obj_class->shader_program.fragment_src.src, NULL);

	emit_sub_cmd_graph_compile_shader_stor(window, CMD_STOR_IDX(0));
	emit_sub_cmd_graph_compile_shader_stor(window, CMD_STOR_IDX(1));

	emit_sub_cmd_graph_attach_shader_stor_stor(window, CMD_STOR_IDX(2), CMD_STOR_IDX(0));
	emit_sub_cmd_graph_attach_shader_stor_stor(window, CMD_STOR_IDX(2), CMD_STOR_IDX(1));

	/* Bind attributes before linking */
	emit_sub_cmd_graph_bind_attrib_location_stor_clone(window, CMD_STOR_IDX(2), LH_ATTR_POS, "a_VertexPosition");
	emit_sub_cmd_graph_bind_attrib_location_stor_clone(window, CMD_STOR_IDX(2), LH_ATTR_NORMAL, "a_VertexNormal");
	emit_sub_cmd_graph_bind_attrib_location_stor_clone(window, CMD_STOR_IDX(2), LH_ATTR_TEX_COORD, "a_TextureCoord");
	emit_sub_cmd_graph_bind_attrib_location_stor_clone(window, CMD_STOR_IDX(2), LH_ATTR_MODELMATRIX, "a_ModelMatrix");

	emit_sub_cmd_graph_bind_frag_data_location_stor_clone(window, CMD_STOR_IDX(2), 0, "outColor");

	emit_sub_cmd_graph_link_program_stor(window, CMD_STOR_IDX(2));
	emit_sub_cmd_graph_use_program_stor(window, CMD_STOR_IDX(2));

	/* Not needed anymore */
	emit_sub_cmd_graph_delete_shader_stor(window, CMD_STOR_IDX(0));
	emit_sub_cmd_graph_delete_shader_stor(window, CMD_STOR_IDX(1));

	emit_sub_cmd_copy_uint_from_stor(window, &obj_class->shader_program.program, CMD_STOR_IDX(2));

	/* Uniforms */

	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_IsInstanced", &obj_class->shader_program.isInstancedUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_ModelMatrix", &obj_class->shader_program.modelMatrixUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_ViewMatrix", &obj_class->shader_program.viewMatrixUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_PMatrix", &obj_class->shader_program.pMatrixUniform);

	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_FogEnabled", &obj_class->shader_program.fogEnabledUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_FogMinDistance", &obj_class->shader_program.fogMinDistanceUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_FogMaxDistance", &obj_class->shader_program.fogMaxDistanceUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_FogColor", &obj_class->shader_program.fogColorUniform);

	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_AbsPosition", &obj_class->shader_program.absPositionUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_CameraPosition", &obj_class->shader_program.cameraPositionUniform);

	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_AmbientLight", &obj_class->shader_program.ambientLightUniform);

	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_IsIlluminable", &obj_class->shader_program.isIlluminableUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_LightColor", &obj_class->shader_program.lightColorUniform);
	emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), "u_LightDirection", &obj_class->shader_program.lightDirectionUniform);

	/* Custom Uniforms */

	for(LHuint i = 0; i < obj_class->shader_program.nb_custom_uniforms; i++)
		emit_sub_cmd_graph_get_uniform_location_stor_ret_ptr_clone(window, CMD_STOR_IDX(2), obj_class->shader_program.custom_uniforms[i].name, &obj_class->shader_program.custom_uniforms[i].id);

	end_cmd(window);

	/* Buffers */

	obj_class->uses |= LH_BUF_MODELMATRIX_FLAG; /* Force use */
	obj_class->overrides |= LH_BUF_MODELMATRIX_FLAG; /* Force override */
	setup_buffer(window, obj_class, NULL, LH_BUF_POS, LH_TRUE);
	setup_buffer(window, obj_class, NULL, LH_BUF_NORMAL, LH_TRUE);
	setup_buffer(window, obj_class, NULL, LH_BUF_TEX_COORD, LH_TRUE);
	setup_buffer(window, obj_class, NULL, LH_BUF_INDEX, LH_TRUE);

	/* Registration */

	lhAcquireGraphicLock(window);

	obj_class->id = list_push(&core->obj_classes, obj_class);
	list_init(&obj_class->objs);

	lhReleaseGraphicLock(window);

	return obj_class->id;
}

int lhObjReg(struct lh_window *window, struct lh_obj_class *obj_class, struct lh_obj *obj)
{
	/* Do not interfere with drawing */
	lhAcquireGraphicLock(window);

	obj->id = list_push(&obj_class->objs, obj);
	obj->obj_class = obj_class;

	obj->nb_clones = 0;
	list_init(&obj->clones);
	obj->nb_allocated_clones = 0;
	list_init(&obj->gc_clones);

	for(int i = 0; i < LH_NB_BUFS; i++) {
		obj->custom_buffers[i].id = 0;
		obj->custom_buffers[i].data = NULL;
		obj->custom_buffers[i].nb_items = 0;
	}
	setup_buffer(window, obj_class, obj, LH_BUF_MODELMATRIX, LH_FALSE);

	obj->clones_model_matrix_lock = lhSpinCreate();

	lhReleaseGraphicLock(window);

	return obj->id;
}

int lhObjCloneReg(struct lh_window *window, struct lh_obj_clone *obj_clone, LHbool is_visible)
{
	int ret = -1;
	struct lh_obj *obj = obj_clone->obj;

	if(obj_clone->id != LL_INVAL_ID)
		return -1;

	lhSpinLock(obj->clones_model_matrix_lock);

	if(list_get_last_elem_id(&obj->gc_clones) != LL_INVAL_ID) {
		LHuint *instanced_idx_ptr = list_get_last_elem(&obj->gc_clones);
		obj_clone->instanced_idx = *instanced_idx_ptr;
		list_pop(&obj->gc_clones);
	} else {
		obj_clone->instanced_idx = obj->nb_clones;
		obj->nb_clones++;
		if(obj->nb_clones > obj->nb_allocated_clones) {
			void *new_data;
			LHuint new_nb_alloc_clones;

			if(!obj->nb_allocated_clones)
				new_nb_alloc_clones = obj->nb_clones;
			else
				new_nb_alloc_clones = obj->nb_allocated_clones + 20;
			new_data = realloc(obj->custom_buffers[LH_BUF_MODELMATRIX].data, sizeof(lh_mat4_t) * new_nb_alloc_clones);
			if(!new_data)
				goto fail_unlock;
			obj->custom_buffers[LH_BUF_MODELMATRIX].data = new_data;
			for(LHuint i = obj->nb_allocated_clones; i < new_nb_alloc_clones; i++)
				lh_mat4_zero(&((lh_mat4_t *)obj->custom_buffers[LH_BUF_MODELMATRIX].data)[i]);
			obj->nb_allocated_clones = new_nb_alloc_clones;
			emit_cmd_group_buffer_grow_idptr_dataptr(
				window,
				GL_ARRAY_BUFFER,
				&obj->custom_buffers[LH_BUF_MODELMATRIX].id,
				obj->nb_allocated_clones * sizeof(lh_mat4_t),
				&obj->custom_buffers[LH_BUF_MODELMATRIX].data,
				GL_DYNAMIC_DRAW
			);
		}
		emit_cmd_add_uint(window, &obj->custom_buffers[LH_BUF_MODELMATRIX].nb_items, 4); /* matrix4 = 4 * vec4 */
	}

	lhSpinUnlock(obj->clones_model_matrix_lock);

	obj_clone->id = list_push(&obj->clones, obj_clone);
	if(obj_clone->id == LL_INVAL_ID)
		goto fail;

	obj_clone->is_visible = !is_visible; /* Hack */
	lhObjCloneSetVisible(window, obj_clone, is_visible);
	return 0;

fail_unlock:
	lhSpinUnlock(obj->clones_model_matrix_lock);
fail:
	return ret;
}

void lhObjSetPos(struct lh_obj *obj, lh_vec3_t *pos)
{
	lh_vec3_dup(&obj->pos, pos);
}

void lhObjSetAngle(struct lh_obj *obj, lh_quat_t *angle)
{
	lh_quat_dup(&obj->angle, angle);
}

void lhObjSetParams(struct lh_obj *obj, lh_vec3_t *pos, lh_quat_t *angle)
{
	lh_vec3_dup(&obj->pos, pos);
	lh_quat_dup(&obj->angle, angle);
}

void lhObjSetVisible(struct lh_obj *obj, LHbool is_visible)
{
	obj->is_visible = is_visible;
}

void lhObjEnableCulling(struct lh_obj *obj, LHbool is_enabled)
{
	obj->culling_enabled = is_enabled;
}

void lhObjSetReversedCulling(struct lh_obj *obj, LHbool is_reversed)
{
	obj->reversed_culling = is_reversed;
}

void lhObjEnableDepthTest(struct lh_obj *obj, LHbool is_enabled)
{
	obj->depth_test_enabled = is_enabled;
}

void lhObjSetIlluminable(struct lh_obj *obj, LHbool is_illuminable)
{
	obj->is_illuminable = is_illuminable;
}

void lhObjCloneSetParams(struct lh_window *window, struct lh_obj_clone *obj_clone, lh_vec3_t *pos, lh_quat_t *angle)
{
	lh_vec3_dup(&obj_clone->pos, pos);
	lh_quat_dup(&obj_clone->angle, angle);
	if(obj_clone->is_visible) {
		obj_clone->is_visible = LH_FALSE; /* Hack */
		lhObjCloneSetVisible(window, obj_clone, LH_TRUE);
	}
}

void lhObjCloneSetVisible(struct lh_window *window, struct lh_obj_clone *obj_clone, LHbool is_visible)
{
	struct lh_obj *obj = obj_clone->obj;
	struct lh_buffer *buf;
	lh_mat4_t *modelMatrices_ptr;

	if((is_visible && obj_clone->is_visible)
			|| (!is_visible && !obj_clone->is_visible))
		return;

	obj_clone->is_visible = is_visible;

	lhSpinLock(obj->clones_model_matrix_lock);
	buf = &obj->custom_buffers[LH_BUF_MODELMATRIX];

	modelMatrices_ptr = buf->data;
	if(!is_visible)
		lh_mat4_zero(&modelMatrices_ptr[obj_clone->instanced_idx]);
	else
		lh_mat4_model(&modelMatrices_ptr[obj_clone->instanced_idx], &obj_clone->pos, &obj_clone->angle);

	emit_cmd_group_buffer_sub_data_idptr_dataptr(
		window,
		GL_ARRAY_BUFFER,
		&obj->custom_buffers[LH_BUF_MODELMATRIX].id,
		obj_clone->instanced_idx * sizeof(modelMatrices_ptr[0]),
		sizeof(modelMatrices_ptr[0]),
		&buf->data
	);
	lhSpinUnlock(obj->clones_model_matrix_lock);
}

int lhObjClassUnreg(struct lh_window *window, struct lh_obj_class *obj_class)
{
	struct lh_core_ctx *core = window->core;
	struct LinkedListIterator objs_iterator;

	/* Do not interfere with drawing */
	lhAcquireGraphicLock(window);
	list_remove(&core->obj_classes, obj_class->id);
	lhReleaseGraphicLock(window);

	emit_cmd_graph_delete_program(window, obj_class->shader_program.program);

	delete_buffer(window, obj_class, NULL, LH_BUF_POS, LH_TRUE);
	delete_buffer(window, obj_class, NULL, LH_BUF_NORMAL, LH_TRUE);
	delete_buffer(window, obj_class, NULL, LH_BUF_TEX_COORD, LH_TRUE);
	delete_buffer(window, obj_class, NULL, LH_BUF_INDEX, LH_TRUE);

	/* Unregister all the sub-objects */
	list_it_init(&objs_iterator, &obj_class->objs);
	for(struct lh_obj *obj = list_it_get_cur(&objs_iterator); obj; obj = list_it_get_next(&objs_iterator))
		lhObjUnreg(window, obj);

	obj_class->id = -1;

	return 0;
}

int lhObjUnreg(struct lh_window *window, struct lh_obj *obj)
{
	/* Do not interfere with drawing */
	lhAcquireGraphicLock(window);
	list_remove(&obj->obj_class->objs, obj->id);
	lhReleaseGraphicLock(window);

	lhSpinDestroy(obj->clones_model_matrix_lock);

	delete_buffer(window, obj->obj_class, obj, LH_BUF_POS, LH_FALSE);
	delete_buffer(window, obj->obj_class, obj, LH_BUF_NORMAL, LH_FALSE);
	delete_buffer(window, obj->obj_class, obj, LH_BUF_TEX_COORD, LH_FALSE);
	delete_buffer(window, obj->obj_class, obj, LH_BUF_MODELMATRIX, LH_FALSE);
	delete_buffer(window, obj->obj_class, obj, LH_BUF_INDEX, LH_FALSE);

	obj->id = -1;
	obj->obj_class = NULL;

	return 0;
}

int lhObjCloneUnreg(struct lh_window *window, struct lh_obj_clone *obj_clone)
{
	int ret = -1;
	struct lh_obj *obj = obj_clone->obj;
	LHulong gc_id;

	if(obj_clone->id == LL_INVAL_ID)
		return -1;

	lhAcquireGraphicLock(window);

	lhObjCloneSetVisible(window, obj_clone, LH_FALSE);
	list_remove(&obj->clones, obj_clone->id);
	obj_clone->id = LL_INVAL_ID;

	gc_id = list_push_dup(&obj->gc_clones, &obj_clone->instanced_idx, sizeof(obj_clone->instanced_idx));
	if(gc_id == LL_INVAL_ID)
		goto fail;
	ret = 0;

	fail:
	lhReleaseGraphicLock(window);
	return ret;
}

int lhObjCloneDestroy(struct lh_window *window, struct lh_obj_clone *obj_clone)
{
	if(!obj_clone)
		return -1;

	if(obj_clone->id != LL_INVAL_ID)
		lhObjCloneUnreg(window, obj_clone);
	free(obj_clone);
	return 0;
}

LHuint lhGetBufferDataLen(int type, LHuint nb_elems)
{
	LHuint len;

	switch(type) {
		case LH_BUF_POS:
		case LH_BUF_NORMAL:
		case LH_BUF_TEX_COORD:
			len = sizeof(float) * nb_elems;
			break;

		case LH_BUF_INDEX:
			len = sizeof(LHuint) * nb_elems;
			break;

		default:
			return -1;
	}

	return len;
}

void *lhAllocBufferData(int type, LHuint nb_elems)
{
	LHuint len;

	if(!nb_elems)
		return NULL;

	switch(type) {
		case LH_BUF_POS:
		case LH_BUF_NORMAL:
		case LH_BUF_TEX_COORD:
			len = sizeof(float) * nb_elems;
			break;

		case LH_BUF_INDEX:
			len = sizeof(LHuint) * nb_elems;
			break;

		default:
			return NULL;
	}

	return malloc(len);
}

int lhSetupCustomBuffer(struct lh_window *window, struct lh_obj *obj, int type)
{
	return setup_buffer(window, obj->obj_class, obj, type, LH_FALSE);
}

int lhUpdateCustomBuffer(struct lh_window *window, struct lh_obj *obj, int type)
{
	return update_buffer(window, obj->obj_class, obj, type, LH_FALSE);
}

int lhDeleteCustomBuffer(struct lh_window *window, struct lh_obj *obj, int type)
{
	return delete_buffer(window, obj->obj_class, obj, type, LH_FALSE);
}

int lhEnableMSAA(struct lh_window *window, LHuint nb_samples)
{
	struct lh_core_ctx *core = window->core;
	int ret = -1;

	switch(nb_samples) {
		case LH_MSAA_NONE:
		case LH_MSAA_2x:
		case LH_MSAA_4x:
		case LH_MSAA_8x:
		case LH_MSAA_16x:
		case LH_MSAA_32x:
			if(nb_samples == core->msaa.nb_samples)
				return 0;
			break;

		default:
			return -1;
	}

	if(nb_samples == LH_MSAA_NONE) {
		begin_cmd(window, 0);

		emit_sub_cmd_graph_disable(window, GL_MULTISAMPLE);

		emit_sub_cmd_graph_bind_framebuffer_ptr(window, GL_FRAMEBUFFER, &core->msaa.fbo);
		emit_sub_cmd_graph_framebuffer_renderbuffer(window, GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, 0);
		emit_sub_cmd_graph_framebuffer_renderbuffer(window, GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
		emit_sub_cmd_graph_bind_framebuffer(window, GL_FRAMEBUFFER, 0);

		emit_sub_cmd_graph_bind_renderbuffer(window, GL_RENDERBUFFER, 0);

		emit_sub_cmd_graph_delete_renderbuffer_ptr(window, &core->msaa.color_renderbuf);
		emit_sub_cmd_graph_delete_renderbuffer_ptr(window, &core->msaa.depth_renderbuf);
		emit_sub_cmd_graph_delete_framebuffer_ptr(window, &core->msaa.fbo);

		end_cmd(window);

		ret = 0;
	} else {
		begin_cmd(window, 0);

		emit_sub_cmd_graph_enable(window, GL_MULTISAMPLE);

		if(core->msaa.nb_samples == LH_MSAA_NONE) {
			emit_sub_cmd_graph_gen_framebuffer(window, &core->msaa.fbo);
			emit_sub_cmd_graph_gen_renderbuffer(window, &core->msaa.color_renderbuf);
			emit_sub_cmd_graph_gen_renderbuffer(window, &core->msaa.depth_renderbuf);
		}

		emit_sub_cmd_graph_bind_framebuffer_ptr(window, GL_FRAMEBUFFER, &core->msaa.fbo);

		emit_sub_cmd_graph_bind_renderbuffer_ptr(window, GL_RENDERBUFFER, &core->msaa.color_renderbuf);
		emit_sub_cmd_graph_renderbuffer_storage_multisample(window, GL_RENDERBUFFER, nb_samples, GL_RGBA, window->width, window->height);
		emit_sub_cmd_graph_framebuffer_renderbuffer_ptr(window, GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, &core->msaa.color_renderbuf);

		emit_sub_cmd_graph_bind_renderbuffer_ptr(window, GL_RENDERBUFFER, &core->msaa.depth_renderbuf);
		emit_sub_cmd_graph_renderbuffer_storage_multisample(window, GL_RENDERBUFFER, nb_samples, GL_DEPTH_COMPONENT, window->width, window->height);
		emit_sub_cmd_graph_framebuffer_renderbuffer_ptr(window, GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, &core->msaa.depth_renderbuf);

		end_cmd(window);

		/*if(gl->CheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
			disable_msaa(window);
			ret = -1;
		} else {*/
			core->msaa.nb_samples = nb_samples;
			ret = 0;
		/*}*/
	}

	core->msaa.nb_samples = nb_samples;

	return ret;
}

long lhGetPerfCounter(struct lh_window *window, enum lh_perf_counter counter)
{
	switch(counter) {
		case LH_PERF_COUNTER_FRAMES:
			return lhAtomicCounterGet(&window->core->counter_frames);

		default:
			break;
	}
	return -1;
}

void lhResetPerfCounter(struct lh_window *window, enum lh_perf_counter counter)
{
	switch(counter) {
		case LH_PERF_COUNTER_FRAMES:
			lhAtomicCounterSet(&window->core->counter_frames, 0);
			break;

		default:
			break;
	}
}

static inline void optimize_cmds(struct LinkedListIterator *it) {
	struct LinkedList *cmds;
	struct LinkedListIterator backup_it;
	struct LinkedListIterator backup_prev_it;

	cmds = list_it_get_list(it);

	for(struct lh_graphic_cmd *cmd = list_it_get_cur(it); cmd; cmd = list_it_get_next(it)) {
		unsigned long cmd_id = list_it_get_cur_id(it);
		void *prev;

		list_it_copy(it, &backup_it);
		prev = list_it_get_prev(it);
		list_it_copy(it, &backup_prev_it);
		list_it_get_next(it);

		switch(cmd->type) {
			case CMD_GROUP_BUFFER_GROW:
			{
				const LHuint tgt = cmd_get_param(cmd, NULL, 0)->as_uint;
				const LHuint *id_ptr = &cmd_get_param(cmd, NULL, 1)->as_uint;
				const LHuint len = cmd_get_param(cmd, NULL, 2)->as_uint;
				void *const *data_ptr = &cmd_get_param(cmd, NULL, 3)->as_ptr;
				const LHuint usage = cmd_get_param(cmd, NULL, 4)->as_uint;
				LHuint real_len = len;
				struct lh_graphic_cmd *chk_cmd;

				while((chk_cmd = list_it_get_next(it))) {
					LHuint chk_tgt;
					const LHuint *chk_id_ptr;
					LHuint chk_len;
					void *const *chk_data_ptr;
					LHuint chk_usage;

					if(chk_cmd->type != CMD_GROUP_BUFFER_GROW)
						continue;

					chk_tgt = cmd_get_param(chk_cmd, NULL, 0)->as_uint;
					chk_id_ptr = &cmd_get_param(chk_cmd, NULL, 1)->as_uint;
					chk_len = cmd_get_param(chk_cmd, NULL, 2)->as_uint;
					chk_data_ptr = &cmd_get_param(chk_cmd, NULL, 3)->as_ptr;
					chk_usage = cmd_get_param(chk_cmd, NULL, 4)->as_uint;
					if(chk_tgt != tgt
							|| chk_id_ptr != id_ptr
							|| *chk_id_ptr != *id_ptr
							|| chk_data_ptr != data_ptr
							|| chk_usage != usage)
						continue;

					if(chk_len > real_len)
						real_len = chk_len;

					list_remove(cmds, list_it_get_cur_id(it));
				}

				cmd->type = CMD_GROUP_BUFFER_GROW_FIXED;
				cmd->params[2].data.as_uint = real_len;

				list_it_copy(&backup_it, it);
				list_it_refresh(it);
				break;
			}

			case CMD_GROUP_BUFFER_SUB_DATA:
			{
				const LHuint tgt = cmd_get_param(cmd, NULL, 0)->as_uint;
				const LHuint *id_ptr = &cmd_get_param(cmd, NULL, 1)->as_uint;
				const LHuint off = cmd_get_param(cmd, NULL, 2)->as_uint;
				const LHuint len = cmd_get_param(cmd, NULL, 3)->as_uint;
				void *const *data_ptr = &cmd_get_param(cmd, NULL, 4)->as_ptr;
				LHuint real_off = off;
				LHuint real_len = len;
				struct lh_graphic_cmd *chk_cmd;
				unsigned long last_good_cmd_id = LL_INVAL_ID;
				struct lh_graphic_cmd *last_good_cmd = NULL;

				while((chk_cmd = list_it_get_next(it))) {
					LHuint chk_tgt;
					const LHuint *chk_id_ptr;
					LHuint chk_off;
					LHuint chk_len;
					void *const *chk_data_ptr;

					if(chk_cmd->type != CMD_GROUP_BUFFER_SUB_DATA)
						continue;

					chk_tgt = cmd_get_param(chk_cmd, NULL, 0)->as_uint;
					chk_id_ptr = &cmd_get_param(chk_cmd, NULL, 1)->as_uint;
					chk_off = cmd_get_param(chk_cmd, NULL, 2)->as_uint;
					chk_len = cmd_get_param(chk_cmd, NULL, 3)->as_uint;
					chk_data_ptr = &cmd_get_param(chk_cmd, NULL, 4)->as_ptr;
					if(chk_tgt != tgt
							|| chk_id_ptr != id_ptr
							|| *chk_id_ptr != *id_ptr
							|| chk_data_ptr != data_ptr)
						continue;

					if(last_good_cmd_id != LL_INVAL_ID) {
						list_remove(cmds, last_good_cmd_id);
						free(last_good_cmd);
					}
					last_good_cmd_id = list_it_get_cur_id(it);
					last_good_cmd = chk_cmd;

					if(chk_off < real_off) {
						real_len += real_off - chk_off;
						real_off = chk_off;
					}

					if(chk_off + chk_len > real_off + real_len)
						real_len += (chk_off + chk_len) - (real_off + real_len);
				}

				if(!last_good_cmd)
					last_good_cmd = cmd;
				last_good_cmd->type = CMD_GROUP_BUFFER_SUB_DATA_FIXED;
				last_good_cmd->params[2].data.as_uint = real_off;
				last_good_cmd->params[3].data.as_uint = real_len;

				if(last_good_cmd != cmd) {
					list_remove(cmds, cmd_id);
					free(cmd);
					if(prev) {
						list_it_copy(&backup_prev_it, it);
						list_it_refresh(it);
					} else {
						list_it_seek_first(it);
						list_it_get_prev(it);
					}
				} else {
					list_it_copy(&backup_it, it);
					list_it_refresh(it);
				}
				break;
			}

			default:
				list_it_copy(&backup_it, it);
				break;
		}
	}
}

static inline void process_cmds(struct lh_window *window) {
	struct lh_core_ctx *core = window->core;
	struct lh_gl_funcs *gl = &window->gl;
	struct SyncLinkedListIterator sit;
	struct LinkedListIterator *it;
	struct LinkedList *graphic_cmds;
	lh_dynamic_type_t *stor = NULL;

	sync_list_it_init(&sit, &core->immediate_cmds);
	it = sync_list_it_lock(&sit, &graphic_cmds);

	optimize_cmds(it);
	list_it_seek_first(it);

	for(struct lh_graphic_cmd *cmd = list_it_get_cur(it); cmd; cmd = list_it_get_next(it)) {
		switch(cmd->type) {
			case CMD_BEGIN:
				stor = cmd_get_param(cmd, stor, 0)->as_ptr;
				break;
			case CMD_END:
				if(stor) {
					free(stor);
					stor = NULL;
				}
				break;
			case CMD_MEMCPY:
				memcpy(
					cmd_get_param(cmd, stor, 0)->as_ptr,
					cmd_get_param(cmd, stor, 1)->as_ptr,
					cmd_get_param(cmd, stor, 2)->as_uint
				);
				cmd_free_cloned_param(cmd, 1);
				break;
			case CMD_COPY_UINT:
				*(LHuint *)cmd_get_param(cmd, stor, 0)->as_ptr = cmd_get_param(cmd, stor, 1)->as_uint;
				break;
			case CMD_COPY_MAT4:
				lh_mat4_dup(cmd_get_param(cmd, stor, 0)->as_ptr, &cmd_get_param(cmd, stor, 1)->as_mat4);
				cmd_free_cloned_param(cmd, 1);
				break;

			case CMD_DEC_UINT:
				(*(LHuint *)cmd_get_param(cmd, stor, 0)->as_ptr)--;
				break;
			case CMD_ADD_UINT:
				*(LHuint *)cmd_get_param(cmd, stor, 0)->as_ptr += cmd_get_param(cmd, stor, 1)->as_uint;
				break;

			case CMD_CORE_SHOW_VERSIONS:
			{
				const char *gl_ver, *gl_ven, *gl_renderer, *glsl_ver;

				gl_ver = (const char *)gl->GetString(GL_VERSION);
				gl_ven = (const char *)gl->GetString(GL_VENDOR);
				gl_renderer = (const char *)gl->GetString(GL_RENDERER);
				glsl_ver = (const char *)gl->GetString(GL_SHADING_LANGUAGE_VERSION);
				printf("libhang: GL version: %s\n", gl_ver ? gl_ver : "(null)");
				printf("libhang: GL vendor: %s\n", gl_ven ? gl_ven : "(null)");
				printf("libhang: GL renderer: %s\n", gl_renderer ? gl_renderer : "(null)");
				printf("libhang: GLSL version: %s\n", glsl_ver ? glsl_ver : "(null)");
				break;
			}

			case CMD_GROUP_BUFFER_GROW_FIXED:
			{
				const LHuint tgt = cmd_get_param(cmd, stor, 0)->as_uint;
				const LHuint *id_ptr = &cmd_get_param(cmd, stor, 1)->as_uint;
				const LHuint len = cmd_get_param(cmd, stor, 2)->as_uint;
				void *const *data_ptr = &cmd_get_param(cmd, stor, 3)->as_ptr;
				const LHuint usage = cmd_get_param(cmd, stor, 4)->as_uint;

				gl->BindBuffer(tgt, *id_ptr);
				gl->BufferData(tgt, len, NULL, usage);
				gl->BufferSubData(tgt, 0, len, *data_ptr);
				break;
			}
			case CMD_GROUP_BUFFER_SUB_DATA_FIXED:
			{
				const LHuint tgt = cmd_get_param(cmd, stor, 0)->as_uint;
				const LHuint *id_ptr = &cmd_get_param(cmd, stor, 1)->as_uint;
				const LHuint off = cmd_get_param(cmd, stor, 2)->as_uint;
				const LHuint len = cmd_get_param(cmd, stor, 3)->as_uint;
				void *const *data_ptr = &cmd_get_param(cmd, stor, 4)->as_ptr;
				char *data = *data_ptr;

				data = &data[off];
				gl->BindBuffer(tgt, *id_ptr);
				gl->BufferSubData(tgt, off, len, data);
				break;
			}

			case CMD_GRAPH_ENABLE:
				gl->Enable(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_DISABLE:
				gl->Disable(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_ACTIVE_TEXTURE:
				gl->ActiveTexture(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_DEPTH_FUNC:
				gl->DepthFunc(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_BLEND_FUNC:
				gl->BlendFunc(cmd_get_param(cmd, stor, 0)->as_uint, cmd_get_param(cmd, stor, 1)->as_uint);
				break;
			case CMD_GRAPH_FRONT_FACE:
				gl->FrontFace(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_CULL_FACE:
				gl->CullFace(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_VIEWPORT:
				gl->Viewport(
					cmd_get_param(cmd, stor, 0)->as_int,
					cmd_get_param(cmd, stor, 1)->as_int,
					cmd_get_param(cmd, stor, 2)->as_int,
					cmd_get_param(cmd, stor, 3)->as_int
				);
				break;
			case CMD_GRAPH_GEN_VERTEX_ARRAY:
				gl->GenVertexArrays(1, cmd_get_param(cmd, stor, 0)->as_ptr);
				break;
			case CMD_GRAPH_BIND_VERTEX_ARRAY:
				gl->BindVertexArray(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_DRAW_BUFFER:
				gl->DrawBuffer(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_CLEAR_COLOR:
			{
				lh_rgba_t *color = &cmd_get_param(cmd, stor, 0)->as_rgba;
				gl->ClearColor(color->comp.r, color->comp.g, color->comp.b, color->comp.a);
				break;
			}
			case CMD_GRAPH_GEN_TEXTURE:
				gl->GenTextures(1, cmd_get_param(cmd, stor, 0)->as_ptr);
				break;
			case CMD_GRAPH_BIND_TEXTURE:
				gl->BindTexture(cmd_get_param(cmd, stor, 0)->as_uint, cmd_get_param(cmd, stor, 1)->as_uint);
				break;
			case CMD_GRAPH_TEX_PARAMETERI:
				gl->TexParameteri(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_uint,
					cmd_get_param(cmd, stor, 2)->as_uint
				);
				break;
			case CMD_GRAPH_PIXEL_STOREI:
				gl->PixelStorei(cmd_get_param(cmd, stor, 0)->as_uint, cmd_get_param(cmd, stor, 1)->as_uint);
				break;
			case CMD_GRAPH_TEX_IMAGE_2D:
				gl->TexImage2D(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_int,
					cmd_get_param(cmd, stor, 2)->as_int,
					cmd_get_param(cmd, stor, 3)->as_uint,
					cmd_get_param(cmd, stor, 4)->as_uint,
					cmd_get_param(cmd, stor, 5)->as_int,
					cmd_get_param(cmd, stor, 6)->as_uint,
					cmd_get_param(cmd, stor, 7)->as_uint,
					cmd_get_param(cmd, stor, 8)->as_ptr
				);
				cmd_free_cloned_param(cmd, 8);
				break;
			case CMD_GRAPH_GENERATE_MIPMAP:
				gl->GenerateMipmap(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_DELETE_TEXTURE:
				gl->DeleteTextures(1, &cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_GEN_BUFFER:
				gl->GenBuffers(1, cmd_get_param(cmd, stor, 0)->as_ptr);
				break;
			case CMD_GRAPH_BIND_BUFFER:
				gl->BindBuffer(cmd_get_param(cmd, stor, 0)->as_uint, cmd_get_param(cmd, stor, 1)->as_uint);
				break;
			case CMD_GRAPH_BUFFER_DATA:
				gl->BufferData(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_uint,
					cmd_get_param(cmd, stor, 2)->as_ptr,
					cmd_get_param(cmd, stor, 3)->as_uint
				);
				cmd_free_cloned_param(cmd, 2);
				break;
			case CMD_GRAPH_BUFFER_SUB_DATA:
				gl->BufferSubData(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_uint,
					cmd_get_param(cmd, stor, 2)->as_uint,
					cmd_get_param(cmd, stor, 3)->as_ptr
				);
				cmd_free_cloned_param(cmd, 3);
				break;
			case CMD_GRAPH_MAP_BUFFER_RANGE:
				cmd_get_ret_ptr(cmd, stor, 4)->as_ptr = gl->MapBufferRange(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_uint,
					cmd_get_param(cmd, stor, 2)->as_uint,
					cmd_get_param(cmd, stor, 3)->as_uint
				);
				break;
			case CMD_GRAPH_UNMAP_BUFFER:
				gl->UnmapBuffer(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_DELETE_BUFFER:
			{
				lh_dynamic_type_t *p0 = cmd_get_param(cmd, stor, 0);
				gl->DeleteBuffers(1, &p0->as_uint);
				p0->as_uint = 0;
				break;
			}
			case CMD_GRAPH_CREATE_SHADER:
				cmd_get_ret_ptr(cmd, stor, 1)->as_uint = gl->CreateShader(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_CREATE_PROGRAM:
				cmd_get_ret_ptr(cmd, stor, 0)->as_uint = gl->CreateProgram();
				break;
			case CMD_GRAPH_SHADER_SOURCE:
				gl->ShaderSource(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_uint,
					cmd_get_param(cmd, stor, 2)->as_ptr,
					cmd_get_param(cmd, stor, 3)->as_ptr
				);
				break;
			case CMD_GRAPH_COMPILE_SHADER:
			{
				LHuint shader = cmd_get_param(cmd, stor, 0)->as_uint;
				int status = 0;
				int logbuf_len;
				char *new_logbuf = NULL;
				char *logbuf = "(no more informations available)";

				gl->CompileShader(shader);
				gl->GetShaderiv(shader, GL_COMPILE_STATUS, &status);
				if(!status) {
					gl->GetShaderiv(shader, GL_INFO_LOG_LENGTH, &logbuf_len);
					if(logbuf_len > 0) {
						new_logbuf = malloc(logbuf_len);
						if(new_logbuf) {
							gl->GetShaderInfoLog(shader, logbuf_len, NULL, new_logbuf);
							logbuf = new_logbuf;
						}
					}
					fprintf(stderr, "Unable to compile shader %u:\n%s\n", shader, logbuf);
					if(new_logbuf)
						free(new_logbuf);
				}
				break;
			}
			case CMD_GRAPH_ATTACH_SHADER:
				gl->AttachShader(cmd_get_param(cmd, stor, 0)->as_uint, cmd_get_param(cmd, stor, 1)->as_uint);
				break;
			case CMD_GRAPH_BIND_ATTRIB_LOCATION:
				gl->BindAttribLocation(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_uint,
					cmd_get_param(cmd, stor, 2)->as_ptr
				);
				cmd_free_cloned_param(cmd, 2);
				break;
			case CMD_GRAPH_BIND_FRAG_DATA_LOCATION:
				gl->BindFragDataLocation(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_uint,
					cmd_get_param(cmd, stor, 2)->as_ptr
				);
				cmd_free_cloned_param(cmd, 2);
				break;
			case CMD_GRAPH_GET_UNIFORM_LOCATION:
				cmd_get_ret_ptr(cmd, stor, 2)->as_int = gl->GetUniformLocation(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_ptr
				);
				cmd_free_cloned_param(cmd, 1);
				break;
			case CMD_GRAPH_LINK_PROGRAM:
				gl->LinkProgram(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_USE_PROGRAM:
				gl->UseProgram(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_DELETE_SHADER:
				gl->DeleteShader(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_DELETE_PROGRAM:
				gl->DeleteProgram(cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_GEN_FRAMEBUFFER:
				gl->GenFramebuffers(1, cmd_get_param(cmd, stor, 0)->as_ptr);
				break;
			case CMD_GRAPH_GEN_RENDERBUFFER:
				gl->GenRenderbuffers(1, cmd_get_param(cmd, stor, 0)->as_ptr);
				break;
			case CMD_GRAPH_BIND_FRAMEBUFFER:
				gl->BindFramebuffer(cmd_get_param(cmd, stor, 0)->as_uint, cmd_get_param(cmd, stor, 1)->as_uint);
				break;
			case CMD_GRAPH_BIND_RENDERBUFFER:
				gl->BindRenderbuffer(cmd_get_param(cmd, stor, 0)->as_uint, cmd_get_param(cmd, stor, 1)->as_uint);
				break;
			case CMD_GRAPH_FRAMEBUFFER_RENDERBUFFER:
				gl->FramebufferRenderbuffer(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_uint,
					cmd_get_param(cmd, stor, 2)->as_uint,
					cmd_get_param(cmd, stor, 3)->as_uint
				);
				break;
			case CMD_GRAPH_RENDERBUFFER_STORAGE_MULTISAMPLE:
				gl->RenderbufferStorageMultisample(
					cmd_get_param(cmd, stor, 0)->as_uint,
					cmd_get_param(cmd, stor, 1)->as_int,
					cmd_get_param(cmd, stor, 2)->as_uint,
					cmd_get_param(cmd, stor, 3)->as_int,
					cmd_get_param(cmd, stor, 4)->as_int
				);
				break;
			case CMD_GRAPH_DELETE_FRAMEBUFFER:
				gl->DeleteFramebuffers(1, &cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			case CMD_GRAPH_DELETE_RENDERBUFFER:
				gl->DeleteRenderbuffers(1, &cmd_get_param(cmd, stor, 0)->as_uint);
				break;
			default:
				fprintf(stderr, "%s(): Unknown command 0x%x?!\n", __func__, cmd->type);
		}
		list_remove(graphic_cmds, list_it_get_cur_id(it));
		free(cmd);
	}
	sync_list_it_unlock(&sit);
	sync_list_it_deinit(&sit);
}

void lhDirtyObjBuffer(struct lh_window *window) {
	struct lh_core_ctx *core = window->core;

	core->is_obj_buffer_dirty = LH_TRUE;
}

/* TODO: Not really amazing... */
void lhDirtyObjBufferWaitRender(struct lh_window *window) {
	struct lh_core_ctx *core = window->core;

	core->is_obj_buffer_dirty = LH_TRUE;
	while(core->is_obj_buffer_dirty)
		lhDelay(1);
}

LHbool lhIsDirtyObjBuffer(struct lh_window *window) {
	struct lh_core_ctx *core = window->core;

	return core->is_obj_buffer_dirty;
}

static inline void enable_buffer(struct lh_window *window, struct lh_obj_class *obj_class, struct lh_obj *obj, LHuint type, LHbool is_obj_class)
{
	struct lh_gl_funcs *gl = &window->gl;
	struct lh_buffer *bufs;
	struct lh_buffer *buf;
	int mask;
	int binding_point;

	if(is_obj_class)
		bufs = obj_class->buffers;
	else
		bufs = obj->custom_buffers;

	if(type >= LH_NB_BUFS)
		return;

	mask = LH_TO_FLAG(type);
	buf = &bufs[type];

	if(type == LH_BUF_INDEX)
		binding_point = GL_ELEMENT_ARRAY_BUFFER;
	else
		binding_point = GL_ARRAY_BUFFER;

	if(!(obj_class->uses & mask)) {
		if(type == LH_BUF_INDEX)
			gl->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		else
			gl->DisableVertexAttribArray(LH_BUF_TO_ATTR(type));
		return;
	}
	if((is_obj_class && (obj_class->overrides & mask))
			|| (!is_obj_class && !(obj_class->overrides & mask)))
		return;

	gl->BindBuffer(binding_point, buf->id);
	if(buf->id && type == LH_BUF_MODELMATRIX && obj->nb_clones) {
		for(int i = 0; i < 4; i++) {
			gl->EnableVertexAttribArray(LH_BUF_TO_ATTR(type) + i);
			gl->VertexAttribPointer(LH_BUF_TO_ATTR(type) + i, LH_BUF_ITEM_SIZE(type), GL_FLOAT, GL_FALSE, sizeof(lh_mat4_t), BUFFER_OFFSET(i, sizeof(lh_vec4_t)));
			gl->VertexAttribDivisor(LH_BUF_TO_ATTR(type) + i, 1);
		}
	} else if(type == LH_BUF_MODELMATRIX && !obj->nb_clones) {
		for(int i = 0; i < 4; i++) {
			gl->DisableVertexAttribArray(LH_BUF_TO_ATTR(type) + i);
			gl->VertexAttribDivisor(LH_BUF_TO_ATTR(type) + i, 0);
		}
	} else if(buf->id && type != LH_BUF_INDEX) {
		gl->EnableVertexAttribArray(LH_BUF_TO_ATTR(type));
		gl->VertexAttribPointer(LH_BUF_TO_ATTR(type), LH_BUF_ITEM_SIZE(type), GL_FLOAT, GL_FALSE, 0, 0);
	}
}

static inline void enable_textures(struct lh_window *window, struct lh_obj_class *obj_class, struct lh_obj *obj, LHbool is_obj_class)
{
	struct lh_core_ctx *core = window->core;
	struct lh_gl_funcs *gl = &window->gl;
	LHuint *textures;

	if(!(obj_class->uses & LH_TEX_FLAG))
		return;

	if(is_obj_class)
		textures = obj_class->textures;
	else
		textures = obj->custom_textures;

	if(is_obj_class && !(obj_class->overrides & LH_TEX_FLAG))
		for(LHuint i = 0; i < obj_class->nb_textures; i++) {
			LHuint *tex_ptr = list_get_elem_by_id(&core->textures, textures[i]);
			if(!tex_ptr)
				continue;
			gl->ActiveTexture(GL_TEXTURE0 + i);
			gl->BindTexture(GL_TEXTURE_2D, *tex_ptr);
		}
	else if(!is_obj_class && (obj_class->overrides & LH_TEX_FLAG))
		for(LHuint i = 0; i < obj_class->nb_textures; i++) {
			LHuint *tex_ptr = list_get_elem_by_id(&core->textures, textures[i]);
			if(!tex_ptr)
				continue;
			gl->BindTexture(GL_TEXTURE_2D, *tex_ptr);
		}
	else if(is_obj_class)
		for(LHuint i = 0; i < obj_class->nb_textures; i++)
			gl->ActiveTexture(GL_TEXTURE0 + i);
}

static inline void upload_custom_uniforms(struct lh_window *window, struct lh_obj_class *obj_class, struct lh_obj *obj)
{
	struct lh_gl_funcs *gl = &window->gl;
	lh_dynamic_type_t *uniforms_data = obj ? obj->custom_uniforms_data : obj_class->gbl_uniforms_data;

	for(LHuint i = 0; i < obj_class->shader_program.nb_custom_uniforms; i++) {
		int id = obj_class->shader_program.custom_uniforms[i].id;

		if (!obj && !obj_class->shader_program.custom_uniforms[i].is_gbl)
			continue;

		if (obj && obj_class->shader_program.custom_uniforms[i].is_gbl)
			continue;

		switch(obj_class->shader_program.custom_uniforms[i].type) {
			case LH_TYPE_FLOAT:
				gl->Uniform1f(id, uniforms_data[i].as_float);
				break;
			case LH_TYPE_INT:
				gl->Uniform1i(id, uniforms_data[i].as_int);
				break;
			case LH_TYPE_UINT:
				gl->Uniform1ui(id, uniforms_data[i].as_uint);
				break;
			case LH_TYPE_VEC2:
				gl->Uniform2fv(id, 1, uniforms_data[i].as_vec2.a);
				break;
			case LH_TYPE_VEC3:
				gl->Uniform3fv(id, 1, uniforms_data[i].as_vec3.a);
				break;
			case LH_TYPE_VEC4:
				gl->Uniform4fv(id, 1, uniforms_data[i].as_vec4.a);
				break;
			case LH_TYPE_IVEC2:
				gl->Uniform2iv(id, 1, uniforms_data[i].as_ivec2.a);
				break;
			case LH_TYPE_IVEC3:
				gl->Uniform3iv(id, 1, uniforms_data[i].as_ivec3.a);
				break;
			case LH_TYPE_IVEC4:
				gl->Uniform4iv(id, 1, uniforms_data[i].as_ivec4.a);
				break;
			case LH_TYPE_UVEC2:
				gl->Uniform2uiv(id, 1, uniforms_data[i].as_uvec2.a);
				break;
			case LH_TYPE_UVEC3:
				gl->Uniform3uiv(id, 1, uniforms_data[i].as_uvec3.a);
				break;
			case LH_TYPE_UVEC4:
				gl->Uniform4uiv(id, 1, uniforms_data[i].as_uvec4.a);
				break;
			case LH_TYPE_MAT2:
				gl->UniformMatrix2fv(id, 1, GL_FALSE, uniforms_data[i].as_mat2.la);
				break;
			case LH_TYPE_MAT3:
				gl->UniformMatrix3fv(id, 1, GL_FALSE, uniforms_data[i].as_mat3.la);
				break;
			case LH_TYPE_MAT4:
				gl->UniformMatrix4fv(id, 1, GL_FALSE, uniforms_data[i].as_mat4.la);
				break;
		}
	}
}

static inline GLenum lh_drawing_type_to_gl(LHuint drawing_type) {
	switch(drawing_type) {
		case LH_DRAW_TRIANGLES:
			return GL_TRIANGLES;
		case LH_DRAW_LINES:
			return GL_LINES;
	}
	return GL_TRIANGLES;
}

static int draw_obj_class(struct lh_window *window, struct lh_obj_class *obj_class, lh_mat4_t *viewMatrix)
{
	struct lh_core_ctx *core = window->core;
	struct lh_gl_funcs *gl = &window->gl;

	LHuint nb_items = 0;
	struct lh_obj_class *fb2d_obj_class = lhFB2DGetObjClass(window);

	if(!obj_class->is_visible || list_is_empty(&obj_class->objs))
		return 0;

	gl->UseProgram(obj_class->shader_program.program);

	gl->Uniform3fv(obj_class->shader_program.ambientLightUniform, 1, core->ambient_light.a);

	gl->Uniform3fv(obj_class->shader_program.lightColorUniform, 1, core->lights[0].color.a);
	gl->Uniform3fv(obj_class->shader_program.lightDirectionUniform, 1, core->lights[0].direction.a);

	enable_buffer(window, obj_class, NULL, LH_BUF_POS, LH_TRUE);
	enable_buffer(window, obj_class, NULL, LH_BUF_NORMAL, LH_TRUE);
	enable_buffer(window, obj_class, NULL, LH_BUF_TEX_COORD, LH_TRUE);
	enable_buffer(window, obj_class, NULL, LH_BUF_MODELMATRIX, LH_TRUE);
	enable_buffer(window, obj_class, NULL, LH_BUF_INDEX, LH_TRUE);

	enable_textures(window, obj_class, NULL, LH_TRUE);

	gl->UniformMatrix4fv(obj_class->shader_program.viewMatrixUniform, 1, GL_FALSE, viewMatrix->la);
	gl->UniformMatrix4fv(obj_class->shader_program.pMatrixUniform, 1, GL_FALSE, core->pMatrix.la);

	gl->Uniform1i(obj_class->shader_program.fogEnabledUniform, core->fog_enabled);
	if(core->fog_enabled) {
		gl->Uniform1f(obj_class->shader_program.fogMinDistanceUniform, core->fog_distances.near);
		gl->Uniform1f(obj_class->shader_program.fogMaxDistanceUniform, core->fog_distances.far);
		gl->Uniform4fv(obj_class->shader_program.fogColorUniform, 1, core->fog_color.a);
	}

	if(obj_class->shader_program.nb_custom_uniforms
			&& obj_class->gbl_uniforms_data)
		upload_custom_uniforms(window, obj_class, NULL);

	if(!(obj_class->overrides & LH_BUF_INDEX_FLAG))
		nb_items = obj_class->buffers[LH_BUF_INDEX].nb_items;

	struct LinkedListIterator objs_iterator;
	list_it_init(&objs_iterator, &obj_class->objs);
	for(struct lh_obj *obj = (struct lh_obj *)list_it_get_cur(&objs_iterator); obj; obj = (struct lh_obj *)list_it_get_next(&objs_iterator)) {
		lh_vec3_t cam_pos, vec_cam_obj;
		LHuint nb_clones = 0;

		/*
		 * lhFB2DComputeCoords() sets obj->is_visible, so we need to call it
		 * before the test.
		 */
		if(fb2d_obj_class
				&& fb2d_obj_class->id == obj_class->id)
			lhFB2DComputeCoords(window, obj);

		if(!obj->is_visible && !obj->nb_clones)
			continue;

		if(obj->nb_clones) {
			nb_clones = mini(obj->nb_clones, obj->custom_buffers[LH_BUF_MODELMATRIX].nb_items / LH_BUF_ITEM_SIZE(LH_BUF_MODELMATRIX));
			if(!nb_clones)
				continue;
		}

		if(obj->culling_enabled && !core->culling_enabled) {
			gl->Enable(GL_CULL_FACE);
			core->culling_enabled = LH_TRUE;
		} else if(!obj->culling_enabled && core->culling_enabled) {
			gl->Disable(GL_CULL_FACE);
			core->culling_enabled = LH_FALSE;
		}
		if(obj->reversed_culling && !core->reversed_culling) {
			gl->CullFace(GL_FRONT);
			core->reversed_culling = LH_TRUE;
		} else if(!obj->reversed_culling && core->reversed_culling) {
			gl->CullFace(GL_BACK);
			core->reversed_culling = LH_FALSE;
		}
		if(obj->depth_test_enabled && !core->depth_test_enabled) {
			gl->Enable(GL_DEPTH_TEST);
			core->depth_test_enabled = LH_TRUE;
		} else if(!obj->depth_test_enabled && core->depth_test_enabled) {
			gl->Disable(GL_DEPTH_TEST);
			core->depth_test_enabled = LH_FALSE;
		}

		lhGetCameraPos(window, &cam_pos);
		lh_vec3_sub(&vec_cam_obj, &obj->pos, &cam_pos);

		enable_buffer(window, obj_class, obj, LH_BUF_POS, LH_FALSE);
		enable_buffer(window, obj_class, obj, LH_BUF_NORMAL, LH_FALSE);
		enable_buffer(window, obj_class, obj, LH_BUF_TEX_COORD, LH_FALSE);
		enable_buffer(window, obj_class, obj, LH_BUF_MODELMATRIX, LH_FALSE);
		enable_buffer(window, obj_class, obj, LH_BUF_INDEX, LH_FALSE);

		enable_textures(window, obj_class, obj, LH_FALSE);

		gl->Uniform1i(obj_class->shader_program.isIlluminableUniform, obj->is_illuminable);
		if(nb_clones) {
			gl->Uniform1i(obj_class->shader_program.isInstancedUniform, LH_TRUE);
		} else {
			lh_mat4_t tmp_matrix, modelMatrix;

			gl->Uniform1i(obj_class->shader_program.isInstancedUniform, LH_FALSE);
			lh_mat4_identity(&tmp_matrix);
			lh_mat4_translate_in_place(&tmp_matrix, &obj->pos);
			lh_mat4_rotate_quat(&modelMatrix, &tmp_matrix, &obj->angle);
			gl->UniformMatrix4fv(obj_class->shader_program.modelMatrixUniform, 1, GL_FALSE, modelMatrix.la);
		}

		if(obj_class->shader_program.nb_custom_uniforms)
			upload_custom_uniforms(window, obj_class, obj);

		if(obj_class->overrides & LH_BUF_INDEX_FLAG)
			nb_items = obj->custom_buffers[LH_BUF_INDEX].nb_items;

		if(nb_clones)
			gl->DrawElementsInstanced(lh_drawing_type_to_gl(obj_class->shader_program.drawing_type), nb_items, GL_UNSIGNED_INT, 0, nb_clones);
		else
			gl->DrawElements(lh_drawing_type_to_gl(obj_class->shader_program.drawing_type), nb_items, GL_UNSIGNED_INT, 0);
	}
	return 0;
}

void lhDraw(struct lh_window *window)
{
	struct lh_core_ctx *core = window->core;
	struct lh_gl_funcs *gl = &window->gl;

	lh_mat4_t tmp_matrix, viewMatrix;
	struct lh_obj_class *skybox_obj_class = lhSkyBoxGetObjClass(window);
	struct lh_obj_class *fb2d_obj_class = lhFB2DGetObjClass(window);

	if(!lhIsDirtyObjBuffer(window))
		return;

	lhAcquireGraphicLock(window);

	process_cmds(window);

	if(core->msaa.nb_samples != LH_MSAA_NONE)
		gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, core->msaa.fbo);

	if(core->is_win_resized) {
		gl->Viewport(0, 0, window->width, window->height);

		if(core->msaa.nb_samples != LH_MSAA_NONE) {
			gl->BindRenderbuffer(GL_RENDERBUFFER, core->msaa.color_renderbuf);
			gl->RenderbufferStorageMultisample(GL_RENDERBUFFER, core->msaa.nb_samples, GL_RGBA, window->width, window->height);
			gl->BindRenderbuffer(GL_RENDERBUFFER, core->msaa.depth_renderbuf);
			gl->RenderbufferStorageMultisample(GL_RENDERBUFFER, core->msaa.nb_samples, GL_DEPTH_COMPONENT24, window->width, window->height);
		}

		compute_pMatrix(window);
		core->is_win_resized = LH_FALSE;
	}

	gl->Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	lh_mat4_identity(&tmp_matrix);
	lh_mat4_rotate_quat(&viewMatrix, &tmp_matrix, &core->camera.angle);
	lh_mat4_translate_in_place(&viewMatrix, &core->camera.pos);

	if(skybox_obj_class
			&& skybox_obj_class->id != LL_INVAL_ID)
		draw_obj_class(window, skybox_obj_class, &viewMatrix);

	struct LinkedListIterator obj_classes_iterator;
	list_it_init(&obj_classes_iterator, &core->obj_classes);
	for(struct lh_obj_class *obj_class = (struct lh_obj_class *)list_it_get_cur(&obj_classes_iterator); obj_class; obj_class = (struct lh_obj_class *)list_it_get_next(&obj_classes_iterator)) {
		if(obj_class->priv_flags & LH_OBJ_CLASS_INTERNAL_FLAG)
			continue;
		draw_obj_class(window, obj_class, &viewMatrix);
	}

	if(fb2d_obj_class
			&& fb2d_obj_class->id != LL_INVAL_ID) {
		gl->Clear(GL_DEPTH_BUFFER_BIT);
		draw_obj_class(window, fb2d_obj_class, &viewMatrix);
	}

	if(core->msaa.nb_samples != LH_MSAA_NONE) {
		gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		gl->BindFramebuffer(GL_READ_FRAMEBUFFER, core->msaa.fbo);
		gl->DrawBuffer(GL_BACK);
		gl->BlitFramebuffer(0, 0, window->width, window->height, 0, 0, window->width, window->height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
		gl->BindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	}

	core->is_obj_buffer_dirty = LH_FALSE;

	lhAtomicCounterInc(&core->counter_frames);

	lhReleaseGraphicLock(window);
}

void lhRedraw(struct lh_window *window)
{
	/* TODO: Real implementation... */
	lhDirtyObjBuffer(window);
}
