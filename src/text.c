/*
 * Copyright (C) 2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <libhang.h>


uint32_t lhUTF8CharDecode(const char *cs, int *bytes_read)
{
	const LHuchar *c = (const LHuchar *)cs;
	uint32_t ret = 0;
	int nbytes = 0;
	int br;

	br = 1;

	if ((*c & 0x80) == 0)
	{
		nbytes = 1;
		ret = *c & 0x7F;
	}
	else if ((*c & 0xE0) == 0xC0)
	{
		nbytes = 2;
		ret = *c & 0x1F;
	}
	else if ((*c & 0xF0) == 0xE0)
	{
		nbytes = 3;
		ret = *c & 0x0F;
	}
	else if ((*c & 0xF8) == 0xF0)
	{
		nbytes = 4;
		ret = *c & 0x07;
	}
	else
	{
		ret = 0xFFFD;
		goto done;
	}

	c++;

	while (br < nbytes)
	{
		ret <<= 6;

		if ((*c & 0xC0) != 0x80)
		{
			ret = 0xFFFD;
			goto done;
		}

		ret |= *c & 0x3F;
		c++;
		br++;
	}

done:
	if (bytes_read)
		*bytes_read = br;
	return ret;
}
