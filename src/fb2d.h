/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_FB2D_H
#define LIBHANG_INTERNAL_FB2D_H 1

#include <libhang.h>

extern int lhFB2DInit(struct lh_window *window);
extern int lhFB2DDeinit(struct lh_window *window);
extern struct lh_obj_class *lhFB2DGetObjClass(struct lh_window *window);
extern void lhFB2DComputeCoords(struct lh_window *window, struct lh_obj *obj);

#endif /* LIBHANG_INTERNAL_FB2D_H */
