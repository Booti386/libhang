/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>
#include <memory.h>
#include <png.h>

#include <libhang.h>

struct lh_image *lhImageCreate(LHuint w, LHuint h, LHuint color_type, lh_dynamic_type_t *fill_color)
{
	struct lh_image *image;
	LHuint bpp;

	if (!w || !h)
		return LH_IMG_INVAL;

	switch (color_type)
	{
		case LH_IMG_COLOR_RGB:  bpp = sizeof(lh_rgb888_t); break;
		case LH_IMG_COLOR_RGBA: bpp = sizeof(lh_rgba8888_t); break;
		default:
			return LH_IMG_INVAL;
	}

	image = malloc(sizeof(*image));
	if (!image)
		return LH_IMG_INVAL;

	image->data = malloc(w * h * bpp);
	if (!image->data)
	{
		free(image);
		return LH_IMG_INVAL;
	}

	image->width = w;
	image->height = h;
	image->depth = 8; /* 8 bits per component */
	image->color_type = color_type;
	lhImageFill(image, fill_color);
	return image;
}

struct lh_image *lhImageDup(struct lh_image *img)
{
	struct lh_image *image;
	LHuint bpp;
	LHuint data_len;

	switch (img->color_type)
	{
		case LH_IMG_COLOR_RGB:  bpp = sizeof(lh_rgb888_t); break;
		case LH_IMG_COLOR_RGBA: bpp = sizeof(lh_rgba8888_t); break;
		default:
			return LH_IMG_INVAL;
	}

	image = malloc(sizeof(*image));
	if (!image)
		return LH_IMG_INVAL;

	data_len = img->width * img->height * bpp;
	image->data = malloc(data_len);
	if (!image->data)
	{
		free(image);
		return LH_IMG_INVAL;
	}

	image->width = img->width;
	image->height = img->height;
	image->depth = img->depth;
	image->color_type = img->color_type;
	memcpy(image->data, img->data, data_len);
	return image;
}

int lhImageFill(struct lh_image *image, lh_dynamic_type_t *fill_color)
{
	lh_dynamic_type_t *data = image->data;
	LHuint max_pix = image->width * image->height;

	if (image->color_type == LH_IMG_COLOR_RGB)
	{
		lh_rgb888_t *data_rgb888 = &data->as_rgb888;

		for (LHuint i = 0; i < max_pix; i++)
			lh_rgb888_dup(&data_rgb888[i], &fill_color->as_rgb888);
	}
	else if (image->color_type == LH_IMG_COLOR_RGBA)
	{
		lh_rgba8888_t *data_rgba8888 = &data->as_rgba8888;

		for (LHuint i = 0; i < max_pix; i++)
			lh_rgba8888_dup(&data_rgba8888[i], &fill_color->as_rgba8888);
	}
	else
		return -1;

	return 0;
}

int lhImageFillRect(struct lh_image *image, lh_dynamic_type_t *fill_color, lh_ivec4_t *rect)
{
	lh_dynamic_type_t *data = image->data;
	lh_uvec4_t fixed_rect;
	LHuint max_x, max_y;

	fixed_rect.c.x = maxi(rect->c.x, 0);
	fixed_rect.c.y = maxi(rect->c.y, 0);
	fixed_rect.c.z = (LHuint)(rect->c.x + rect->c.z) > image->width ? image->width - fixed_rect.c.x : rect->c.z - (fixed_rect.c.x - rect->c.x);
	fixed_rect.c.w = (LHuint)(rect->c.y + rect->c.w) > image->height ? image->height - fixed_rect.c.y : rect->c.w - (fixed_rect.c.y - rect->c.y);

	/* Nothing to draw, it's a short trip. */
	if (fixed_rect.c.z <= 0 || fixed_rect.c.w <= 0
			|| fixed_rect.c.x >= image->width || fixed_rect.c.y >= image->height
			|| fixed_rect.c.x + fixed_rect.c.z <= 0 || fixed_rect.c.y + fixed_rect.c.w <= 0)
		return 0;

	max_x = fixed_rect.c.x + fixed_rect.c.z;
	max_y = fixed_rect.c.y + fixed_rect.c.w;

	if (image->color_type == LH_IMG_COLOR_RGB)
	{
		lh_rgb888_t *data_rgb888 = &data->as_rgb888;

		for (LHuint y = fixed_rect.c.y; y < max_y; y++)
		{
			LHuint y_idx = y * image->width;

			for (LHuint x = fixed_rect.c.x; x < max_x; x++)
				lh_rgb888_dup(&data_rgb888[y_idx + x], &fill_color->as_rgb888);
		}
	}
	else if (image->color_type == LH_IMG_COLOR_RGBA)
	{
		lh_rgba8888_t *data_rgba8888 = &data->as_rgba8888;

		for (LHuint y = fixed_rect.c.y; y < max_y; y++)
		{
			LHuint y_idx = y * image->width;

			for (LHuint x = fixed_rect.c.x; x < max_x; x++)
				lh_rgba8888_dup(&data_rgba8888[y_idx + x], &fill_color->as_rgba8888);
		}
	}
	else
		return -1;

	return 0;
}

struct lh_image *lhImageLoadPNG(const char *path)
{
	FILE *volatile fp = NULL;
	LHuchar header[8];
	int length_read;
	struct lh_image *volatile image = NULL;
	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL;
	double gamma;
	char *volatile data = NULL;
	png_bytep *row_pointers = NULL;

	fp = fopen(path, "rb");
	if (!fp)
		goto failed;

	image = malloc(sizeof(*image));
	if (!image)
		goto failed;

	length_read = fread(header, 1, 8, fp);
	if (png_sig_cmp(header, 0, length_read) != 0)
		goto failed;

	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
		goto failed;

	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
		goto failed;

	if (setjmp(png_jmpbuf(png_ptr)))
		goto failed;

	png_init_io(png_ptr, fp);
	png_set_sig_bytes(png_ptr, 8);
	png_read_info(png_ptr, info_ptr);

	image->width = png_get_image_width(png_ptr, info_ptr);
	image->height = png_get_image_height(png_ptr, info_ptr);
	image->depth = png_get_bit_depth(png_ptr, info_ptr);
	if (image->depth == 16)
		png_set_strip_16(png_ptr);

	switch (png_get_color_type(png_ptr, info_ptr))
	{
		case PNG_COLOR_TYPE_RGB:
			image->color_type = LH_IMG_COLOR_RGB;
			break;

		case PNG_COLOR_TYPE_RGBA:
			image->color_type = LH_IMG_COLOR_RGBA;
			break;

		case PNG_COLOR_TYPE_PALETTE:
			png_set_palette_to_rgb(png_ptr); /* Convert palette to RGB. */
			image->color_type = LH_IMG_COLOR_RGB;
			break;

		case PNG_COLOR_TYPE_GRAY:
		case PNG_COLOR_TYPE_GA:
			if(image->depth < 8)
				png_set_expand_gray_1_2_4_to_8(png_ptr);
			png_set_gray_to_rgb(png_ptr); /* Convert gray to RGB. */
			image->color_type = LH_IMG_COLOR_RGB;
			break;

		default:
			goto failed;
	}

	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
	{
		png_set_tRNS_to_alpha(png_ptr);
		image->color_type = LH_IMG_COLOR_RGBA;
	}

	image->depth = 8;

	if (!png_get_gAMA(png_ptr, info_ptr, &gamma))
		gamma = 1.0;
	png_set_gamma(png_ptr, 1.0, gamma);

	png_set_interlace_handling(png_ptr);
	png_read_update_info(png_ptr, info_ptr);

	data = malloc(sizeof(png_byte) * png_get_rowbytes(png_ptr, info_ptr) * image->height);
	if (!data)
		goto failed;

	row_pointers = (png_bytep *)malloc(sizeof(png_bytep) * image->height);
	if (!row_pointers)
		goto failed;

	/* Make rows pointing directly into data offsets. */
	for (LHuint i = 0; i < image->height; i++)
		row_pointers[i] = (png_byte *)(data + i * sizeof(png_byte) * png_get_rowbytes(png_ptr, info_ptr));

	png_read_image(png_ptr, row_pointers);

	free(row_pointers);
	row_pointers = NULL;
	png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
	png_destroy_info_struct(png_ptr, &info_ptr);
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	fclose(fp);

	image->data = data;
	return image;

failed:
	if(row_pointers)
		free(row_pointers);

	if(data)
		free(data);

	if(image)
		free(image);

	if(fp)
		fclose(fp);

	return LH_IMG_INVAL;
}

struct lh_image *lhImageLoad(const char *path)
{
	struct lh_image *image;

	if ((image = lhImageLoadPNG(path)) != LH_IMG_INVAL)
		return image;
	else
		return LH_IMG_INVAL;
}

int lhImageWritePNG(struct lh_image *image, const char *path)
{
	volatile int ret = -1;
	FILE *volatile fp = NULL;
	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL;
	int color_type;
	LHuint bpp;

	fp = fopen(path, "wb");
	if (!fp)
		goto failed;

	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
		goto failed;

	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
		goto failed;

	if (setjmp(png_jmpbuf(png_ptr)))
		goto failed;

	png_init_io(png_ptr, fp);

	switch (image->color_type)
	{
		case LH_IMG_COLOR_RGB:
			color_type = PNG_COLOR_TYPE_RGB;
			bpp = 3;
			break;

		case LH_IMG_COLOR_RGBA:
			color_type = PNG_COLOR_TYPE_RGBA;
			bpp = 4;
			break;

		default:
			goto failed;
	}

	png_set_IHDR(png_ptr, info_ptr, image->width, image->height, image->depth, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	png_write_info(png_ptr, info_ptr);

	for (size_t i = 0; i < image->height; i++)
		png_write_row(png_ptr, &((LHubyte *)image->data)[i * bpp * image->width]);

	png_write_end(png_ptr, NULL);
	ret = 0;

failed:
	if (info_ptr)
	{
		png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
		png_destroy_info_struct(png_ptr, &info_ptr);
	}

	if (png_ptr)
		png_destroy_write_struct(&png_ptr, NULL);

	if (fp)
		fclose(fp);

	return ret;
}

int lhImageDestroy(struct lh_image *image)
{
	if (!image)
		return -1;

	free(image->data);
	free(image);
	return 0;
}
