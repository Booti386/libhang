/*
 * Copyright (C) 2014-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_LL_H
#define LIBHANG_LL_H 1

/**
 * @file
 * @brief Linked list
 */

#include <stdlib.h>
#include <string.h>

#define LL_EMPTY ((struct LinkedListEntry *)NULL)
#define LL_INVAL_ID ((unsigned long)-1)

struct LinkedListEntry
{
	struct {
		unsigned long id;
		void *data;
		struct LinkedListEntry *prev;
		struct LinkedListEntry *next;
	} h;
	char content[1]; /* content MUST be the last entry of the struct */
};

struct LinkedList
{
	struct LinkedListEntry *cur;
	struct LinkedListEntry *first;
	struct LinkedListEntry *last;
};

static inline struct LinkedListEntry *list_entry_alloc(unsigned long content_size)
{
	struct LinkedListEntry *dummy_list_entry;
	return malloc(sizeof(dummy_list_entry->h) + content_size);
}

static inline void list_entry_free(struct LinkedListEntry *list_entry)
{
	free(list_entry);
}

static inline void list_entry_set_data(struct LinkedListEntry *list_entry, void *data)
{
	list_entry->h.data = data;
}

static inline void list_entry_copy_data(struct LinkedListEntry *list_entry, void *data, unsigned long size)
{
	if(data && size) {
		memcpy(list_entry->content, data, size);
		list_entry->h.data = &list_entry->content;
	} else
		list_entry->h.data = NULL;
}

static inline struct LinkedList *list_init(struct LinkedList *list)
{
	list->cur = LL_EMPTY;
	list->first = LL_EMPTY;
	list->last = LL_EMPTY;
	return list;
}

static inline void list_seek_last(struct LinkedList *list)
{
	list->cur = list->last;
}

static inline void list_seek_first(struct LinkedList *list)
{
	list->cur = list->first;
}

static inline int list_is_empty(struct LinkedList *list)
{
	/* list->cur, first and last are always equal to LL_EMPTY at the same time. */
	return (list->cur == LL_EMPTY) ? 1 : 0;
}

static inline void list_append(struct LinkedList *list, struct LinkedList *other)
{
	struct LinkedListEntry *search_list;

	if(list_is_empty(list)) {
		list->cur = other->cur;
		list->first = other->first;
		list->last = other->last;
		goto purge_other;
	}

	if(list_is_empty(other))
		goto purge_other;

	list->last->h.next = other->first;
	other->first->h.prev = list->last;

	search_list = other->first;
	while(search_list != LL_EMPTY) {
		search_list->h.id = search_list->h.prev->h.id + 1;
		search_list = search_list->h.next;
	}

	list->last = other->last;

purge_other:
	other->cur = LL_EMPTY;
	other->first = LL_EMPTY;
	other->last = LL_EMPTY;
}

static inline unsigned long list_push_entry(struct LinkedList *list, struct LinkedListEntry *list_entry)
{
	if(list_is_empty(list)) {
		list_entry->h.id = 0;
		list->cur = list_entry;
		list->first = list_entry;
	} else {
		list_entry->h.id = list->last->h.id + 1;
		list->last->h.next = list_entry;
	}
	list_entry->h.prev = list->last;
	list_entry->h.next = LL_EMPTY;
	list->last = list_entry;
	return list_entry->h.id;
}

static inline unsigned long list_push(struct LinkedList *list, void *elem)
{
	struct LinkedListEntry *list_entry;

	list_entry = list_entry_alloc(0);
	if (!list_entry)
		return LL_INVAL_ID;
	list_entry_set_data(list_entry, elem);
	return list_push_entry(list, list_entry);
}

static inline unsigned long list_push_dup(struct LinkedList *list, void *elem, unsigned long size)
{
	struct LinkedListEntry *list_entry;

	list_entry = list_entry_alloc(size);
	if (!list_entry)
		return LL_INVAL_ID;
	list_entry_copy_data(list_entry, elem, size);
	return list_push_entry(list, list_entry);
}

static inline struct LinkedListEntry *list_pop_entry(struct LinkedList *list)
{
	struct LinkedListEntry *last_list_entry = list->last;
	struct LinkedListEntry *prev_list_entry;

	if(list_is_empty(list))
		return LL_EMPTY;

	prev_list_entry = last_list_entry->h.prev;
	if(!prev_list_entry)
		list->first = LL_EMPTY;
	else
		prev_list_entry->h.next = LL_EMPTY;
	if(list->cur == last_list_entry)
		list->cur = prev_list_entry;
	list->last = prev_list_entry;
	return last_list_entry;
}

static inline int list_pop(struct LinkedList *list)
{
	struct LinkedListEntry *last_list_entry;

	last_list_entry = list_pop_entry(list);
	if(last_list_entry == LL_EMPTY)
		return -1;
	list_entry_free(last_list_entry);
	return 0;
}

static inline void list_clear(struct LinkedList *list)
{
	while(list_pop(list) == 0);
}

static inline struct LinkedList *list_deinit(struct LinkedList *list)
{
	list_clear(list);
	list->cur = LL_EMPTY;
	list->first = LL_EMPTY;
	list->last = LL_EMPTY;
	return list;
}

static inline int list_remove(struct LinkedList *list, unsigned long id)
{
	struct LinkedListEntry *search_list = list->cur;
	int move_to_next;

	if(list_is_empty(list))
		return -1;

	if(id < search_list->h.id)
		move_to_next = 0;
	else
		move_to_next = 1;
	while(search_list) {
		if(search_list->h.id == id) {
			if(search_list->h.next != LL_EMPTY)
				search_list->h.next->h.prev = search_list->h.prev;
			else
				list->last = search_list->h.prev;
			if(search_list->h.prev != LL_EMPTY)
				search_list->h.prev->h.next = search_list->h.next;
			else
				list->first = search_list->h.next;
			if(search_list->h.next != LL_EMPTY)
				list->cur = search_list->h.next;
			else
				list->cur = search_list->h.prev;
			/* list->cur, first and last are always equal to LL_EMPTY at the same time. */
			if(list->cur == LL_EMPTY) {
				list->first = LL_EMPTY;
				list->last = LL_EMPTY;
			}
			free(search_list);
			return 0;
		}

		if(move_to_next)
			search_list = search_list->h.next;
		else
			search_list = search_list->h.prev;
		if(search_list)
			list->cur = search_list;
	}
	return -1;
}

static inline unsigned long list_get_first_elem_id(struct LinkedList *list)
{
	return (list->first == LL_EMPTY) ? LL_INVAL_ID : list->first->h.id;
}

static inline void *list_get_first_elem(struct LinkedList *list)
{
	return (list->first == LL_EMPTY) ? NULL : list->first->h.data;
}

static inline unsigned long list_get_last_elem_id(struct LinkedList *list)
{
	return (list->last == LL_EMPTY) ? LL_INVAL_ID : list->last->h.id;
}

static inline void *list_get_last_elem(struct LinkedList *list)
{
	return (list->last == LL_EMPTY) ? NULL : list->last->h.data;
}

static inline void *list_get_elem_by_id(struct LinkedList *list, unsigned long id)
{
	struct LinkedListEntry *search_list = list->cur;
	int move_to_next;

	if(list_is_empty(list))
		return NULL;

	if(id < search_list->h.id)
		move_to_next = 0;
	else
		move_to_next = 1;
	while(search_list) {
		if(search_list->h.id == id)
			return search_list->h.data;

		if(move_to_next)
			search_list = search_list->h.next;
		else
			search_list = search_list->h.prev;
		if(search_list)
			list->cur = search_list;
	}
	return NULL;
}

#endif /* LIBHANG_LL_H */
