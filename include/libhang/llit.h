/*
 * Copyright (C) 2014-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_LLIT_H
#define LIBHANG_LLIT_H 1

/**
 * @file
 * @brief Linked list iterator
 */

#include "types.h"
#include "ll.h"

struct LinkedListIterator
{
	struct LinkedList      *list;
	struct LinkedListEntry *cur;
	struct LinkedListEntry *prev_cached;
	struct LinkedListEntry *next_cached;
};


static inline struct LinkedList *list_it_get_list(struct LinkedListIterator *it)
{
	return it->list;
}

static inline void list_it_seek_first(struct LinkedListIterator *it)
{
	it->cur = it->list->first;
	it->prev_cached = LL_EMPTY;
	if(it->cur != LL_EMPTY)
		it->next_cached = it->cur->h.next;
	else
		it->next_cached = LL_EMPTY;
}

static inline void list_it_seek_last(struct LinkedListIterator *it)
{
	it->cur = it->list->last;
	if(it->cur != LL_EMPTY)
		it->prev_cached = it->cur->h.prev;
	else
		it->prev_cached = LL_EMPTY;
	it->next_cached = LL_EMPTY;
}

static inline struct LinkedListIterator *list_it_init(struct LinkedListIterator *it, struct LinkedList *list)
{
	it->list = list;
	list_it_seek_first(it);

	return it;
}

static inline struct LinkedListIterator *list_it_deinit(struct LinkedListIterator *it)
{
	it->list = NULL;

	return it;
}

static inline struct LinkedListIterator *list_it_refresh(struct LinkedListIterator *it)
{
	it->prev_cached = it->cur->h.prev;
	it->next_cached = it->cur->h.next;

	return it;
}

static inline struct LinkedListIterator *list_it_copy(struct LinkedListIterator *it, struct LinkedListIterator *dst_it)
{
	dst_it->list = it->list;
	dst_it->cur = it->cur;
	dst_it->prev_cached = it->prev_cached;
	dst_it->next_cached = it->next_cached;

	return it;
}

static inline unsigned long list_it_get_cur_id(struct LinkedListIterator *it)
{
	if(it->cur == LL_EMPTY)
		return -1;

	return it->cur->h.id;
}

static inline void *list_it_get_cur(struct LinkedListIterator *it)
{
	if(it->cur == LL_EMPTY)
		return NULL;

	return it->cur->h.data;
}

static inline void *list_it_get_prev(struct LinkedListIterator *it)
{
	if(it->cur != LL_EMPTY)
		it->next_cached = it->cur;
	it->cur = it->prev_cached;
	if(it->cur == LL_EMPTY)
		return NULL;

	it->prev_cached = it->cur->h.prev;

	return it->cur->h.data;
}

static inline void *list_it_get_next(struct LinkedListIterator *it)
{
	if(it->cur != LL_EMPTY)
		it->prev_cached = it->cur;
	it->cur = it->next_cached;
	if(it->cur == LL_EMPTY)
		return NULL;

	it->next_cached = it->cur->h.next;

	return it->cur->h.data;
}

#endif /* LIBHANG_LLIT_H */
