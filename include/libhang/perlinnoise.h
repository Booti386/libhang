/*
 * Copyright (C) 2014-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_PERLINNOISE_H
#define LIBHANG_PERLINNOISE_H 1

/**
 * @file
 * @brief Perlin noise
 */

#include "types.h"

#define PN_INVAL ((struct lh_perlin_noise *)NULL)

#ifdef __cplusplus
extern "C" {
#endif

	extern struct lh_perlin_noise *lh_pn_init(struct lh_perlin_noise **ppn, unsigned int rand_array_size, const int *rand_array);
	extern void lh_pn_end(struct lh_perlin_noise **pn);
	extern unsigned int lh_pn_get_rand_array(struct lh_perlin_noise *pn, float *rand_array);
	extern float lh_pn_noise1D(struct lh_perlin_noise *pn, float x);
	extern float lh_pn_noise2D(struct lh_perlin_noise *pn, float x, float y);
	extern float lh_pn_noise3D(struct lh_perlin_noise *pn, float x, float y, float z);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_PERLINNOISE_H */
