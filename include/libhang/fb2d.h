/*
 * Copyright (C) 2015-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_FB2D_H
#define LIBHANG_FB2D_H 1

/**
 * @file
 * @brief GPU-accelerated 2D engine
 */

#include "types.h"
#include "driver_gl.h"

/**
 * @defgroup fb2d FB2D
 * @{
 */

#define LH_FB2D_PCT_OFF_FLAG  LH_TO_FLAG(0)
#define LH_FB2D_PCT_POS_FLAG  LH_TO_FLAG(1)
#define LH_FB2D_PCT_SIZE_FLAG LH_TO_FLAG(2)

#define LH_FB2D_PCT_NONE          0
#define LH_FB2D_PCT_OFF_POS_FLAG  (LH_FB2D_PCT_OFF_FLAG | LH_FB2D_PCT_POS_FLAG)
#define LH_FB2D_PCT_OFF_SIZE_FLAG (LH_FB2D_PCT_OFF_FLAG | LH_FB2D_PCT_SIZE_FLAG)
#define LH_FB2D_PCT_POS_SIZE_FLAG (LH_FB2D_PCT_POS_FLAG | LH_FB2D_PCT_SIZE_FLAG)
#define LH_FB2D_PCT_ALL_FLAG      (LH_FB2D_PCT_OFF_FLAG | LH_FB2D_PCT_POS_FLAG | LH_FB2D_PCT_SIZE_FLAG)

#define LH_FB2D_CLIP(off_x, off_y, pos_x, pos_y, size_x, size_y) ((struct lh_fb2d_clip){ {{(float)(off_x), (float)(off_y)}}, {{(float)(pos_x), (float)(pos_y)}}, {{(float)(size_x), (float)(size_y)}} })

enum {
	LH_FB2D_KEEP_RATIO_NONE = 0,
	LH_FB2D_KEEP_RATIO_X,
	LH_FB2D_KEEP_RATIO_Y,
	LH_FB2D_KEEP_RATIO_MIN,
	LH_FB2D_KEEP_RATIO_MAX
};

#define LH_FB2D_KEEP_RATIO_OFF_SHIFT  0
#define LH_FB2D_KEEP_RATIO_POS_SHIFT  3
#define LH_FB2D_KEEP_RATIO_SIZE_SHIFT 6
#define LH_FB2D_KEEP_RATIO_OFF_MASK  (0x7 << LH_FB2D_KEEP_RATIO_OFF_SHIFT)
#define LH_FB2D_KEEP_RATIO_POS_MASK  (0x7 << LH_FB2D_KEEP_RATIO_POS_SHIFT)
#define LH_FB2D_KEEP_RATIO_SIZE_MASK (0x7 << LH_FB2D_KEEP_RATIO_SIZE_SHIFT)

#define LH_FB2D_KEEP_RATIO_OFF_NONE (LH_FB2D_KEEP_RATIO_NONE << LH_FB2D_KEEP_RATIO_OFF_SHIFT)
#define LH_FB2D_KEEP_RATIO_OFF_X    (LH_FB2D_KEEP_RATIO_X << LH_FB2D_KEEP_RATIO_OFF_SHIFT)
#define LH_FB2D_KEEP_RATIO_OFF_Y    (LH_FB2D_KEEP_RATIO_Y << LH_FB2D_KEEP_RATIO_OFF_SHIFT)
#define LH_FB2D_KEEP_RATIO_OFF_MIN  (LH_FB2D_KEEP_RATIO_MIN << LH_FB2D_KEEP_RATIO_OFF_SHIFT)
#define LH_FB2D_KEEP_RATIO_OFF_MAX  (LH_FB2D_KEEP_RATIO_MAX << LH_FB2D_KEEP_RATIO_OFF_SHIFT)
#define LH_FB2D_KEEP_RATIO_POS_NONE (LH_FB2D_KEEP_RATIO_NONE << LH_FB2D_KEEP_RATIO_POS_SHIFT)
#define LH_FB2D_KEEP_RATIO_POS_X    (LH_FB2D_KEEP_RATIO_X << LH_FB2D_KEEP_RATIO_POS_SHIFT)
#define LH_FB2D_KEEP_RATIO_POS_Y    (LH_FB2D_KEEP_RATIO_Y << LH_FB2D_KEEP_RATIO_POS_SHIFT)
#define LH_FB2D_KEEP_RATIO_POS_MIN  (LH_FB2D_KEEP_RATIO_MIN << LH_FB2D_KEEP_RATIO_POS_SHIFT)
#define LH_FB2D_KEEP_RATIO_POS_MAX  (LH_FB2D_KEEP_RATIO_MAX << LH_FB2D_KEEP_RATIO_POS_SHIFT)
#define LH_FB2D_KEEP_RATIO_SIZE_NONE (LH_FB2D_KEEP_RATIO_NONE << LH_FB2D_KEEP_RATIO_SIZE_SHIFT)
#define LH_FB2D_KEEP_RATIO_SIZE_X    (LH_FB2D_KEEP_RATIO_X << LH_FB2D_KEEP_RATIO_SIZE_SHIFT)
#define LH_FB2D_KEEP_RATIO_SIZE_Y    (LH_FB2D_KEEP_RATIO_Y << LH_FB2D_KEEP_RATIO_SIZE_SHIFT)
#define LH_FB2D_KEEP_RATIO_SIZE_MIN  (LH_FB2D_KEEP_RATIO_MIN << LH_FB2D_KEEP_RATIO_SIZE_SHIFT)
#define LH_FB2D_KEEP_RATIO_SIZE_MAX  (LH_FB2D_KEEP_RATIO_MAX << LH_FB2D_KEEP_RATIO_SIZE_SHIFT)

struct lh_fb2d_surface;

struct lh_fb2d_clip {
	lh_vec2_t off;
	lh_vec2_t pos;
	lh_vec2_t size;
};

#ifdef __cplusplus
extern "C" {
#endif

	extern LHbool lhFB2DGetVisible(void);
	extern void lhFB2DSetVisible(LHbool is_visible);
	extern int lhFB2DCameraSetViewPos(struct lh_window *window, lh_vec2_t *pos);
	extern int lhFB2DCameraSetPos(struct lh_window *window, lh_vec2_t *pos);
	extern int lhFB2DCameraMove(struct lh_window *window, lh_vec2_t *pos);
	extern int lhFB2DCameraSetRotation(struct lh_window *window, float angle);
	extern int lhFB2DCameraRotate(struct lh_window *window, float angle);
	extern int lhFB2DCameraSetZoom(struct lh_window *window, lh_vec2_t *scale);
	extern struct lh_fb2d_surface *lhFB2DBlitSurface(struct lh_window *window, struct lh_fb2d_surface *parent_surface, LHuint texture, LHuint tex_width, LHuint tex_height, float angle, LHulong keep_ratio_flags, LHulong dst_mask, struct lh_fb2d_clip *dst_clip, LHulong src_mask, struct lh_fb2d_clip *src_clip, float global_alpha, LHbool is_visible);
	extern struct lh_fb2d_surface *lhFB2DBlitSurfaceSimple(struct lh_window *window, struct lh_fb2d_surface *parent_surface, lh_uvec4_t dst_rect, float dst_rotation, lh_uvec4_t src_rect, LHuint src_texture, float global_alpha, LHbool is_visible);
	extern struct lh_fb2d_surface *lhFB2DSurfaceClone(struct lh_window *window, struct lh_fb2d_surface *surface, LHbool is_visible);
	extern int lhFB2DSurfaceDetach(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceAttach(struct lh_window *window, struct lh_fb2d_surface *surface, struct lh_fb2d_surface *parent_surface);
	extern float lhFB2DSurfaceGetRotation(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceSetRotation(struct lh_fb2d_surface *surface, float angle);
	extern int lhFB2DSurfaceRotate(struct lh_fb2d_surface *surface, float angle);
	extern LHulong lhFB2DSurfaceGetKeepRatioFlags(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceSetKeepRatioFlags(struct lh_fb2d_surface *surface, LHulong keep_ratio_flags);
	extern LHuint lhFB2DSurfaceGetTexture(struct lh_fb2d_surface *surface, LHuint *tex_width, LHuint *tex_height);
	extern int lhFB2DSurfaceSetTexture(struct lh_fb2d_surface *surface, LHuint texture, LHuint tex_width, LHuint tex_height);
	extern float lhFB2DSurfaceGetGlobalAlpha(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceSetGlobalAlpha(struct lh_fb2d_surface *surface, float global_alpha);
	extern LHbool lhFB2DSurfaceGetVisible(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceSetVisible(struct lh_fb2d_surface *surface, LHbool is_visible);
	extern LHuint lhFB2DSurfaceGetZIndex(struct lh_fb2d_surface *surface);
	extern float lhFB2DSurfaceGetEffectiveZIndex(struct lh_window *window, struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceMoveUp(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceMoveDown(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceMoveAbove(struct lh_fb2d_surface *surface, struct lh_fb2d_surface *tgt_surf);
	extern int lhFB2DSurfaceMoveBelow(struct lh_fb2d_surface *surface, struct lh_fb2d_surface *tgt_surf);
	extern int lhFB2DSurfaceMoveTop(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceMoveBottom(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceSwap(struct lh_fb2d_surface *surface, struct lh_fb2d_surface *other_surf);
	extern LHulong lhFB2DSurfaceGetDstClipPctMode(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceSetDstClipPctMode(struct lh_fb2d_surface *surface, LHulong mask);
	extern int lhFB2DSurfaceGetDstClip(struct lh_fb2d_surface *surface, struct lh_fb2d_clip *clip);
	extern int lhFB2DSurfaceSetDstClip(struct lh_fb2d_surface *surface, struct lh_fb2d_clip *clip);
	extern int lhFB2DSurfaceSetDstRectPctMode(struct lh_fb2d_surface *surface, LHulong mask);
	extern int lhFB2DSurfaceGetEffectiveDstRect(struct lh_window *window, struct lh_fb2d_surface *surface, lh_rect2d_t *rect);
	extern int lhFB2DSurfaceSetDstRect(struct lh_fb2d_surface *surface, lh_vec4_t *rect);
	extern int lhFB2DSurfaceSetDstOffPctMode(struct lh_fb2d_surface *surface, LHbool is_pct);
	extern int lhFB2DSurfaceSetDstOff(struct lh_fb2d_surface *surface, lh_vec2_t *off);
	extern int lhFB2DSurfaceSetDstPosPctMode(struct lh_fb2d_surface *surface, LHbool is_pct);
	extern int lhFB2DSurfaceSetDstPos(struct lh_fb2d_surface *surface, lh_vec2_t *pos);
	extern int lhFB2DSurfaceMove(struct lh_fb2d_surface *surface, lh_vec2_t *pos);
	extern int lhFB2DSurfaceSetDstSizePctMode(struct lh_fb2d_surface *surface, LHbool is_pct);
	extern int lhFB2DSurfaceSetDstSize(struct lh_fb2d_surface *surface, lh_vec2_t *size);
	extern LHulong lhFB2DSurfaceGetSrcClipPctMode(struct lh_fb2d_surface *surface);
	extern int lhFB2DSurfaceSetSrcClipPctMode(struct lh_fb2d_surface *surface, LHulong mask);
	extern int lhFB2DSurfaceGetSrcClip(struct lh_fb2d_surface *surface, struct lh_fb2d_clip *clip);
	extern int lhFB2DSurfaceSetSrcClip(struct lh_fb2d_surface *surface, struct lh_fb2d_clip *clip);
	extern int lhFB2DSurfaceSetSrcRectPctMode(struct lh_fb2d_surface *surface, LHulong mask);
	extern int lhFB2DSurfaceSetSrcRect(struct lh_fb2d_surface *surface, lh_vec4_t *rect);
	extern int lhFB2DSurfaceSetSrcOffPctMode(struct lh_fb2d_surface *surface, LHbool is_pct);
	extern int lhFB2DSurfaceSetSrcOff(struct lh_fb2d_surface *surface, lh_vec2_t *off);
	extern int lhFB2DSurfaceSetSrcPosPctMode(struct lh_fb2d_surface *surface, LHbool is_pct);
	extern int lhFB2DSurfaceSetSrcPos(struct lh_fb2d_surface *surface, lh_vec2_t *pos);
	extern int lhFB2DSurfaceSetSrcSizePctMode(struct lh_fb2d_surface *surface, LHbool is_pct);
	extern int lhFB2DSurfaceSetSrcSize(struct lh_fb2d_surface *surface, lh_vec2_t *size);
	extern struct lh_fb2d_surface *lhFB2DGetPointedSurface(struct lh_window *window);
	extern void lhFB2DRemoveSurface(struct lh_window *window, struct lh_fb2d_surface *surface);
	extern void lhFB2DClear(struct lh_window *window);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* LIBHANG_FB2D_H */
