/*
 * Copyright (C) 2016-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_DRIVER_GL_H
#define LIBHANG_DRIVER_GL_H 1

/**
 * @file
 * @brief Window/GL abstraction layer
 */

#ifndef GL_GLEXT_PROTOTYPES
#  define GL_GLEXT_PROTOTYPES 1
#endif
#include "GL/glcorearb.h"

#include "types.h"
#include "core.h"

/* Declarations of stuctures with cyclic dependencies go here. */
struct lh_core_ctx;

/* Depth precision */
#define LH_DEPTH_DEFAULT  LH_DEPTH_MEDIUM
#define LH_DEPTH_LOW      0
#define LH_DEPTH_MEDIUM   1
#define LH_DEPTH_HIGH     2

struct lh_gl_funcs {
	PFNGLGETSTRINGPROC                        GetString;
	PFNGLCULLFACEPROC                         CullFace;
	PFNGLFRONTFACEPROC                        FrontFace;
	PFNGLBINDTEXTUREPROC                      BindTexture;
	PFNGLBLENDFUNCPROC                        BlendFunc;
	PFNGLCLEARPROC                            Clear;
	PFNGLCLEARCOLORPROC                       ClearColor;
	PFNGLDEPTHFUNCPROC                        DepthFunc;
	PFNGLDISABLEPROC                          Disable;
	PFNGLDRAWBUFFERPROC                       DrawBuffer;
	PFNGLREADBUFFERPROC                       ReadBuffer;
	PFNGLREADPIXELSPROC                       ReadPixels;
	PFNGLDRAWELEMENTSPROC                     DrawElements;
	PFNGLENABLEPROC                           Enable;
	PFNGLGENTEXTURESPROC                      GenTextures;
	PFNGLPIXELSTOREIPROC                      PixelStorei;
	PFNGLTEXIMAGE2DPROC                       TexImage2D;
	PFNGLTEXPARAMETERIPROC                    TexParameteri;
	PFNGLDELETETEXTURESPROC                   DeleteTextures;

	PFNGLGENBUFFERSPROC                       GenBuffers;
	PFNGLBUFFERDATAPROC                       BufferData;
	PFNGLBUFFERSUBDATAPROC                    BufferSubData;
	PFNGLMAPBUFFERRANGEPROC                   MapBufferRange;
	PFNGLUNMAPBUFFERPROC                      UnmapBuffer;
	PFNGLBINDBUFFERPROC                       BindBuffer;
	PFNGLDELETEBUFFERSPROC                    DeleteBuffers;

	PFNGLACTIVETEXTUREPROC                    ActiveTexture;
	PFNGLGENERATEMIPMAPPROC                   GenerateMipmap;
	PFNGLTEXIMAGE2DMULTISAMPLEPROC            TexImage2DMultisample;

	PFNGLGENFRAMEBUFFERSPROC                  GenFramebuffers;
	PFNGLBINDFRAMEBUFFERPROC                  BindFramebuffer;
	PFNGLFRAMEBUFFERTEXTUREPROC               FramebufferTexture;
	PFNGLFRAMEBUFFERTEXTURE1DPROC             FramebufferTexture1D;
	PFNGLFRAMEBUFFERTEXTURE2DPROC             FramebufferTexture2D;
	PFNGLFRAMEBUFFERTEXTURE3DPROC             FramebufferTexture3D;
	PFNGLFRAMEBUFFERTEXTURELAYERPROC          FramebufferTextureLayer;
	PFNGLCHECKFRAMEBUFFERSTATUSPROC           CheckFramebufferStatus;
	PFNGLBLITFRAMEBUFFERPROC                  BlitFramebuffer;
	PFNGLDELETEFRAMEBUFFERSPROC               DeleteFramebuffers;

	PFNGLGENRENDERBUFFERSPROC                 GenRenderbuffers;
	PFNGLBINDRENDERBUFFERPROC                 BindRenderbuffer;
	PFNGLRENDERBUFFERSTORAGEPROC              RenderbufferStorage;
	PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC   RenderbufferStorageMultisample;
	PFNGLFRAMEBUFFERRENDERBUFFERPROC          FramebufferRenderbuffer;
	PFNGLDELETERENDERBUFFERSPROC              DeleteRenderbuffers;

	PFNGLGENVERTEXARRAYSPROC                  GenVertexArrays;
	PFNGLBINDVERTEXARRAYPROC                  BindVertexArray;

	PFNGLCREATESHADERPROC                     CreateShader;
	PFNGLSHADERSOURCEPROC                     ShaderSource;
	PFNGLCOMPILESHADERPROC                    CompileShader;
	PFNGLGETSHADERIVPROC                      GetShaderiv;
	PFNGLGETSHADERINFOLOGPROC                 GetShaderInfoLog;
	PFNGLDELETESHADERPROC                     DeleteShader;

	PFNGLCREATEPROGRAMPROC                    CreateProgram;
	PFNGLUSEPROGRAMPROC                       UseProgram;
	PFNGLATTACHSHADERPROC                     AttachShader;
	PFNGLLINKPROGRAMPROC                      LinkProgram;
	PFNGLDELETEPROGRAMPROC                    DeleteProgram;

	PFNGLBINDATTRIBLOCATIONPROC               BindAttribLocation;
	PFNGLBINDFRAGDATALOCATIONPROC             BindFragDataLocation;
	PFNGLGETATTRIBLOCATIONPROC                GetAttribLocation;
	PFNGLGETUNIFORMLOCATIONPROC               GetUniformLocation;

	PFNGLVERTEXATTRIBPOINTERPROC              VertexAttribPointer;
	PFNGLENABLEVERTEXATTRIBARRAYPROC          EnableVertexAttribArray;
	PFNGLDISABLEVERTEXATTRIBARRAYPROC         DisableVertexAttribArray;
	PFNGLVERTEXATTRIBDIVISORPROC              VertexAttribDivisor;

	PFNGLUNIFORM1FPROC                        Uniform1f;
	PFNGLUNIFORM1IPROC                        Uniform1i;
	PFNGLUNIFORM1UIPROC                       Uniform1ui;
	PFNGLUNIFORM2FVPROC                       Uniform2fv;
	PFNGLUNIFORM3FVPROC                       Uniform3fv;
	PFNGLUNIFORM4FVPROC                       Uniform4fv;
	PFNGLUNIFORM2IVPROC                       Uniform2iv;
	PFNGLUNIFORM3IVPROC                       Uniform3iv;
	PFNGLUNIFORM4IVPROC                       Uniform4iv;
	PFNGLUNIFORM2UIVPROC                      Uniform2uiv;
	PFNGLUNIFORM3UIVPROC                      Uniform3uiv;
	PFNGLUNIFORM4UIVPROC                      Uniform4uiv;
	PFNGLUNIFORMMATRIX2FVPROC                 UniformMatrix2fv;
	PFNGLUNIFORMMATRIX3FVPROC                 UniformMatrix3fv;
	PFNGLUNIFORMMATRIX4FVPROC                 UniformMatrix4fv;

	PFNGLFINISHPROC                           Finish;
	PFNGLFLUSHPROC                            Flush;
	PFNGLVIEWPORTPROC                         Viewport;

	PFNGLDRAWELEMENTSINSTANCEDPROC            DrawElementsInstanced;
	PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC  DrawElementsInstancedBaseVertex;
	PFNGLDRAWARRAYSINSTANCEDPROC              DrawArraysInstanced;
};

struct lh_window {
	int64_t id;
	LHbool is_complete; /* Needed for some drivers */
	int x;
	int y;
	LHuint width;
	LHuint height;

	struct lh_mutex *graphic_ctx_mutex;

	struct lh_core_ctx *core;
	struct lh_gl_funcs gl;
};

/* Event types */
enum {
	LH_INVALID_EVENT = 0,
	LH_EVENT_MOUSE_PRESSED,
	LH_EVENT_MOUSE_RELEASED,
	LH_EVENT_MOUSE_WHEEL,
	LH_EVENT_MOUSE_MOVED,
	LH_EVENT_KEY_PRESSED,
	LH_EVENT_KEY_RELEASED,
	LH_EVENT_FOCUS_ACQUIRED,
	LH_EVENT_FOCUS_LOST,
	LH_EVENT_RESIZED,
	LH_EVENT_CLOSE,
	LH_EVENT_REDRAW
};

/* Callback return values */
enum {
	LH_EVENT_RET_UNHANDLED = 0,
	LH_EVENT_RET_HANDLED
};

struct lh_event {
	/* LH_EVENT_* */
	LHuint type;
};

/* Mouse buttons IDs */
#define LH_INVALID_MOUSE_BUTTON  0
#define LH_MOUSE_BUTTON_LEFT     1
#define LH_MOUSE_BUTTON_MIDDLE   2
#define LH_MOUSE_BUTTON_RIGHT    3

struct lh_event_mouse_button {
	struct lh_event generic;

	LHbool is_pressed;
	int x, y;
	float x_norm, y_norm;
	int button_id;
};
#define lh_event_mouse_pressed lh_event_mouse_button
#define lh_event_mouse_released lh_event_mouse_button

struct lh_event_mouse_wheel {
	struct lh_event generic;

	int delta; /* Negative value = scroll up; positive = scroll down */
};

struct lh_event_mouse_moved {
	struct lh_event generic;

	int old_x, old_y;
	int x, y;
	float old_x_norm, old_y_norm;
	float x_norm, y_norm;
};

struct lh_event_key {
	struct lh_event generic;

	LHbool is_pressed;
	int key_id;
};
#define lh_event_key_pressed lh_event_key
#define lh_event_key_released lh_event_key

struct lh_event_focus {
	struct lh_event generic;

	LHbool is_acquired;
};
#define lh_event_focus_acquired lh_event_focus
#define lh_event_focus_lost lh_event_focus

struct lh_event_resized {
	struct lh_event generic;

	int old_w;
	int old_h;
	/* New width and height are stored in window */
};

struct lh_event_close {
	struct lh_event generic;
};

struct lh_event_redraw {
	struct lh_event generic;
};

typedef LHuint (*lh_event_handler_func_t)(struct lh_event *, void *);

#ifdef __cplusplus
extern "C" {
#endif

	extern struct lh_window *lhCreateWindow(const char *title, int x, int y, LHuint width, LHuint height, int depth, LHbool dbl_buf);
	extern void lhAcquireGraphicLock(struct lh_window *window);
	extern void lhReleaseGraphicLock(struct lh_window *window);
	extern int lhStartDrawing(struct lh_window *window);
	extern int lhStopDrawing(struct lh_window *window);
	extern int lhSetListeningEvents(struct lh_window *window, LHbool is_listening);
	extern int lhEnterEventLoop(void);
	extern int lhLeaveEventLoop(void);
	extern int lhDestroyWindow(struct lh_window *window);
	extern int lhAddEventHandler(struct lh_window *window, lh_event_handler_func_t handler, void *params);
	extern int lhRemoveEventHandler(struct lh_window *window, int id);
	extern int lhGrabMouse(struct lh_window *window, LHbool grabbed);
	extern int lhGrabKeyboard(struct lh_window *window, LHbool grabbed);
	extern int lhLockCursor(struct lh_window *window, LHbool locked);
	extern int lhShowCursor(struct lh_window *window, LHbool visible);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_DRIVER_GL_H */
