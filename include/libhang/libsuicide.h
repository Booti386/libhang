/*
 * Copyright (C) 2014-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_LIBSUICIDE_H
#define LIBHANG_LIBSUICIDE_H 1

/**
 * @file
 * @brief Maths
 */

#ifndef INSIDE_LIBHANG_TYPES_H
#  error "Never use <libhang/libsuicide.h> directly, please include <libhang/types.h> instead."
#endif

#include <float.h>
#include <math.h>

/* For Windows... */
#undef near
#undef far

#define LH_PI 3.1415926535897932384626433832795

#define LH_DISTANCES(n, f) ((lh_distances_t){ (n), (f) })

#define LH_VEC2(x, y) ((lh_vec2_t){ {(x), (y)} })
#define LH_VEC3(x, y, z) ((lh_vec3_t){{ (x), (y), (z)} })
#define LH_VEC4(x, y, z, w) ((lh_vec4_t){ {(x), (y), (z), (w)} })

#define LH_IVEC2(x, y) ((lh_ivec2_t){ {(x), (y)} })
#define LH_IVEC3(x, y, z) ((lh_ivec3_t){{ (x), (y), (z)} })
#define LH_IVEC4(x, y, z, w) ((lh_ivec4_t){ {(x), (y), (z), (w)} })

#define LH_UVEC2(x, y) ((lh_uvec2_t){ {(x), (y)} })
#define LH_UVEC3(x, y, z) ((lh_uvec3_t){{ (x), (y), (z)} })
#define LH_UVEC4(x, y, z, w) ((lh_uvec4_t){ {(x), (y), (z), (w)} })

#define LH_QUAT(x, y, z, w) ((lh_quat_t){ {(x), (y), (z), (w)} })

#define LH_MAT2(x0, y0, x1, y1) ((lh_mat2_t){ { (x0), (y0), (x1), (y1) } })
#define LH_MAT3(x0, y0, z0, x1, y1, z1, x2, y2, z2) ((lh_mat3_t){ { (x0), (y0), (z0), (x1), (y1), (z1), (x2), (y2), (z2) } })
#define LH_MAT4(x0, y0, z0, w0, x1, y1, z1, w1, x2, y2, z2, w2, x3, y3, z3, w3) ((lh_mat4_t){ { (x0), (y0), (z0), (w0), (x1), (y1), (z1), (w1), (x2), (y2), (z2), (w2), (x3), (y3), (z3), (w3) } })

#define LH_RGB(r, g, b) ((lh_rgb_t){ {(r), (g), (b)} })
#define LH_RGB888(r, g, b) ((lh_rgb888_t){ {(r), (g), (b)} })
#define LH_RGBA(r, g, b, a) ((lh_rgba_t){ {(r), (g), (b), (a)} })
#define LH_RGBA8888(r, g, b, a) ((lh_rgba8888_t){ {(r), (g), (b), (a)} })

#define LH_LINE(p, q) ((lh_line_t){ (p), (q) })
#define LH_PLANE(p, q) ((lh_plane_t){ (p), (q) })
#define LH_SPACE(p, q) ((lh_space_t){ (p), (q) })
#define LH_RECT2D(p, angle, s) ((lh_rect2d_t){ (p), (angle), (s) })
#define LH_RECT(p, q, s) ((lh_rect_t){ (p), (q), (s) })
#define LH_CUBOID(p, q, s) ((lh_cuboid_t){ (p), (q), (s) })

typedef struct {
	float near, far;
} lh_distances_t;

typedef union {
	float a[2];
	struct {
		float x, y;
	} c;
} lh_vec2_t;
typedef union {
	float a[3];
	struct {
		float x, y, z;
	} c;
	struct {
		lh_vec2_t xy;
	} v2;
} lh_vec3_t;
typedef union {
	float a[4];
	struct {
		float x, y, z, w;
	} c;
	struct {
		lh_vec2_t xy, zw;
	} v2;
	struct {
		lh_vec3_t xyz;
	} v3;
} lh_vec4_t;

typedef union {
	int a[2];
	struct {
		int x, y;
	} c;
} lh_ivec2_t;
typedef union {
	int a[3];
	struct {
		int x, y, z;
	} c;
	struct {
		lh_ivec2_t xy;
	} v2;
} lh_ivec3_t;
typedef union {
	int a[4];
	struct {
		int x, y, z, w;
	} c;
	struct {
		lh_ivec2_t xy, zw;
	} v2;
	struct {
		lh_ivec3_t xyz;
	} v3;
} lh_ivec4_t;

typedef union {
	LHuint a[2];
	struct {
		LHuint x, y;
	} c;
} lh_uvec2_t;
typedef union {
	LHuint a[3];
	struct {
		LHuint x, y, z;
	} c;
} lh_uvec3_t;
typedef union {
	LHuint a[4];
	struct {
		LHuint x, y, z, w;
	} c;
} lh_uvec4_t;

typedef lh_vec4_t lh_quat_t;

typedef union {
	float a[2][2];
	float la[4];
	struct {
		float x0, y0;
		float x1, y1;
	} c;
} lh_mat2_t;
typedef union {
	float a[3][3];
	float la[9];
	struct {
		float x0, y0, z0;
		float x1, y1, z1;
		float x2, y2, z2;
	} c;
} lh_mat3_t;
typedef union {
	float a[4][4];
	float la[16];
	struct {
		float x0, y0, z0, w0;
		float x1, y1, z1, w1;
		float x2, y2, z2, w2;
		float x3, y3, z3, w3;
	} c;
} lh_mat4_t;

typedef union {
	float a[3];
	struct {
		float r, g, b;
	} comp;
} lh_rgb_t;
typedef union {
	LHubyte a[3];
	struct {
		LHubyte r, g, b;
	} comp;
} lh_rgb888_t;
typedef union {
	float a[4];
	struct {
		float r, g, b, a;
	} comp;
} lh_rgba_t;
typedef union {
	LHubyte a[4];
	struct {
		LHubyte r, g, b, a;
	} comp;
} lh_rgba8888_t;

typedef struct {
	lh_vec3_t p;
	lh_quat_t q;
} lh_pq_t;
typedef lh_pq_t lh_line_t;
typedef lh_pq_t lh_plane_t;
typedef lh_pq_t lh_space_t;

typedef struct
{
	lh_vec2_t p;
	float angle;
	lh_vec2_t s;
} lh_rect2d_t;

typedef struct
{
	lh_vec3_t p;
	lh_quat_t q;
	lh_vec2_t s;
} lh_rect_t;

typedef struct
{
	lh_vec3_t p;
	lh_quat_t q;
	lh_vec3_t s;
} lh_cuboid_t;

static inline void lh_quat_conjug(lh_quat_t *r, const lh_quat_t *a);
static inline void lh_quat_hamilton(lh_quat_t *r, const lh_quat_t *a, const lh_quat_t *b);

static inline void lh_mat4_invert(lh_mat4_t *r, const lh_mat4_t *a);
static inline void lh_mat4_transpose(lh_mat4_t *r, const lh_mat4_t *a);

static inline float invsqrtf(const float a) {
	return 1.f / sqrtf(a);
}

static inline double invsqrtd(const double a) {
	return 1. / sqrt(a);
}

static inline float minf(const float a, const float b) {
	return a < b ? a : b;
}

static inline double mind(const double a, const double b) {
	return a < b ? a : b;
}

static inline int mini(const int a, const int b) {
	return a < b ? a : b;
}

static inline float maxf(const float a, const float b) {
	return a > b ? a : b;
}

static inline double maxd(const double a, const double b) {
	return a > b ? a : b;
}

static inline int maxi(const int a, const int b) {
	return a > b ? a : b;
}

static inline float clampf(const float d, const float min, const float max) {
	const float t = d < min ? min : d;
	return t > max ? max : t;
}

static inline double clampd(const double d, const double min, const double max) {
	const double t = d < min ? min : d;
	return t > max ? max : t;
}

static inline int clampi(const int d, const int min, const int max) {
	const int t = d < min ? min : d;
	return t > max ? max : t;
}

static inline LHbool betweenf(const float a, const float b, const float c) {
	if(b < c)
		return b <= a && a <= c;
	else
		return c <= a && a <= b;
}

static inline LHbool betweend(const double a, const double b, const double c) {
	if(b < c)
		return b <= a && a <= c;
	else
		return c <= a && a <= b;
}

static inline LHbool betweeni(const int a, const int b, const int c) {
	if(b < c)
		return b <= a && a <= c;
	else
		return c <= a && a <= b;
}

static inline void lh_distances_dup(lh_distances_t *r, const lh_distances_t *a) {
	r->near = a->near;
	r->far  = a->far;
}

static inline void lh_distances_zero(lh_distances_t *r) {
	r->near = 0.f;
	r->far = 0.f;
}

static inline void lh_vec2_dup(lh_vec2_t *r, const lh_vec2_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
}

static inline void lh_vec2_zero(lh_vec2_t *r) {
	float *R = r->a;

	R[0] = 0.f;
	R[1] = 0.f;
}

static inline void lh_vec2_neg(lh_vec2_t *r, const lh_vec2_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = -A[0];
	R[1] = -A[1];
}

static inline void lh_vec2_add(lh_vec2_t *r, const lh_vec2_t *a, const lh_vec2_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0] + B[0];
	R[1] = A[1] + B[1];
}

static inline void lh_vec2_sub(lh_vec2_t *r, const lh_vec2_t *a, const lh_vec2_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0] - B[0];
	R[1] = A[1] - B[1];
}

static inline void lh_vec2_scale(lh_vec2_t *r, const lh_vec2_t *a, const float s) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0] * s;
	R[1] = A[1] * s;
}

static inline float lh_vec2_mul_inner(const lh_vec2_t *a, const lh_vec2_t *b) {
	const float *A = a->a;
	const float *B = b->a;

	return A[0]*B[0] + A[1]*B[1];
}

static inline void lh_vec2_mul(lh_vec2_t *r, const lh_vec2_t *a, const lh_vec2_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0] * B[0];
	R[1] = A[1] * B[1];
}

static inline float lh_vec2_len(const lh_vec2_t *a) {
	return sqrt(lh_vec2_mul_inner(a, a));
}

static inline void lh_vec2_norm(lh_vec2_t *r, const lh_vec2_t *a) {
	const float s = 1.f / lh_vec2_len(a);
	lh_vec2_scale(r, a, s);
}

static inline void lh_vec2_rotate(lh_vec2_t *r, const lh_vec2_t *a, const float t) {
	float *R = r->a;
	const float *A = a->a;
	const float ct = cos(t);
	const float st = sin(t);

	const float x = A[0] * ct - A[1] * st;
	R[1] = A[0] * st + A[1] * ct;
	R[0] = x;
}

static inline void lh_vec3_dup(lh_vec3_t *r, const lh_vec3_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_vec3_zero(lh_vec3_t *r) {
	float *R = r->a;

	R[0] = 0.f;
	R[1] = 0.f;
	R[2] = 0.f;
}

static inline void lh_vec3_neg(lh_vec3_t *r, const lh_vec3_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = -A[0];
	R[1] = -A[1];
	R[2] = -A[2];
}

static inline void lh_vec3_add(lh_vec3_t *r, const lh_vec3_t *a, const lh_vec3_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0] + B[0];
	R[1] = A[1] + B[1];
	R[2] = A[2] + B[2];
}

static inline void lh_vec3_sub(lh_vec3_t *r, const lh_vec3_t *a, const lh_vec3_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0] - B[0];
	R[1] = A[1] - B[1];
	R[2] = A[2] - B[2];
}

static inline void lh_vec3_scale(lh_vec3_t *r, const lh_vec3_t *a, const float s) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0] * s;
	R[1] = A[1] * s;
	R[2] = A[2] * s;
}

static inline float lh_vec3_mul_inner(const lh_vec3_t *a, const lh_vec3_t *b) {
	const float *A = a->a;
	const float *B = b->a;

	return A[0]*B[0] + A[1]*B[1] + A[2]*B[2];
}

static inline void lh_vec3_mul_cross(lh_vec3_t *r, const lh_vec3_t *a, const lh_vec3_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[1]*B[2] - A[2]*B[1];
	R[1] = A[2]*B[0] - A[0]*B[2];
	R[2] = A[0]*B[1] - A[1]*B[0];
}

static inline void lh_vec3_mul(lh_vec3_t *r, const lh_vec3_t *a, const lh_vec3_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0] * B[0];
	R[1] = A[1] * B[1];
	R[2] = A[2] * B[2];
}

static inline float lh_vec3_len(const lh_vec3_t *a) {
	return sqrt(lh_vec3_mul_inner(a, a));
}

static inline void lh_vec3_norm(lh_vec3_t *r, const lh_vec3_t *a) {
	const float s = 1.f / lh_vec3_len(a);
	lh_vec3_scale(r, a, s);
}

static inline void lh_vec3_rotateX(lh_vec3_t *r, const lh_vec3_t *a, const float t) {
	float *R = r->a;
	const float *A = a->a;
	const float ct = cos(t);
	const float st = sin(t);

	const float y = A[1] * ct - A[2] * st;
	R[2] = A[1] * st + A[2] * ct;
	R[1] = y;
}

static inline void lh_vec3_rotateY(lh_vec3_t *r, const lh_vec3_t *a, const float t) {
	float *R = r->a;
	const float *A = a->a;
	const float ct = cos(t);
	const float st = sin(t);

	const float z = A[0] * st + A[2] * ct;
	R[0] = A[0] * ct - A[2] * st;
	R[2] = z;
}

static inline void lh_vec3_rotateZ(lh_vec3_t *r, const lh_vec3_t *a, const float t) {
	float *R = r->a;
	const float *A = a->a;
	const float ct = cos(t);
	const float st = sin(t);

	const float x = A[0] * ct - A[1] * st;
	R[1] = A[0] * st + A[1] * ct;
	R[0] = x;
}

static inline void lh_vec3_rotate_quat(lh_vec3_t *r, const lh_vec3_t *a, const lh_quat_t *q) {
	float *R = r->a;
	const float *A = a->a;
	lh_quat_t rq, q_;

	lh_quat_conjug(&q_, q);
	lh_quat_hamilton(&rq, q, &LH_QUAT(A[0], A[1], A[2], 0.f));
	lh_quat_hamilton(&rq, &rq, &q_);

	R[0] = rq.c.x;
	R[1] = rq.c.y;
	R[2] = rq.c.z;
}

static inline void lh_vec3_ux_rotate_quat(lh_vec3_t *r, const lh_quat_t *q) {
	lh_vec3_rotate_quat(r, &LH_VEC3(1.f, 0.f, 0.f), q);
}

static inline void lh_vec3_uy_rotate_quat(lh_vec3_t *r, const lh_quat_t *q) {
	lh_vec3_rotate_quat(r, &LH_VEC3(0.f, 1.f, 0.f), q);
}

static inline void lh_vec3_uz_rotate_quat(lh_vec3_t *r, const lh_quat_t *q) {
	lh_vec3_rotate_quat(r, &LH_VEC3(0.f, 0.f, 1.f), q);
}

static inline void lh_vec3_euler_from_quat(lh_vec3_t *r, const lh_quat_t *q) {
	float *R = r->a;
	const float *Q = q->a;
	const float q11 = Q[1] * Q[1];
	const float sat = Q[1] * Q[3] - Q[0] * Q[2];

	if(fabsf(sat - 0.5f) < FLT_EPSILON) {
		R[0] = 2.f*atan2f(Q[0], Q[3]);
		R[1] = LH_PI / 2.f;
		R[2] = 0.f;
	} else if(fabsf(sat + 0.5f) < FLT_EPSILON) {
		R[0] = 2.f*atan2f(Q[0], Q[3]);
		R[1] = -LH_PI / 2.f;
		R[2] = 0.f;
	} else {
		R[0] = atan2f(2.f * (Q[3] * Q[0] + Q[2] * Q[1]), 1.f - 2.f * (Q[0] * Q[0] + q11));
		R[1] = asinf(2.f * sat);
		R[2] = atan2f(2.f * (Q[3] * Q[2] + Q[0] * Q[1]), 1.f - 2.f * (Q[2] * Q[2] + q11));
	}
}

static inline void lh_vec4_dup(lh_vec4_t *r, const lh_vec4_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_vec4_zero(lh_vec4_t *r) {
	float *R = r->a;

	R[0] = 0.f;
	R[1] = 0.f;
	R[2] = 0.f;
	R[3] = 0.f;
}

static inline void lh_vec4_neg(lh_vec4_t *r, const lh_vec4_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = -A[0];
	R[1] = -A[1];
	R[2] = -A[2];
	R[3] = -A[3];
}

static inline void lh_vec4_add(lh_vec4_t *r, const lh_vec4_t *a, const lh_vec4_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0] + B[0];
	R[1] = A[1] + B[1];
	R[2] = A[2] + B[2];
	R[3] = A[3] + B[3];
}

static inline void lh_vec4_sub(lh_vec4_t *r, const lh_vec4_t *a, const lh_vec4_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0] - B[0];
	R[1] = A[1] - B[1];
	R[2] = A[2] - B[2];
	R[3] = A[3] - B[3];
}

static inline void lh_vec4_scale(lh_vec4_t *r, const lh_vec4_t *a, const float s) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0] * s;
	R[1] = A[1] * s;
	R[2] = A[2] * s;
	R[3] = A[3] * s;
}

static inline float lh_vec4_mul_inner(const lh_vec4_t *a, const lh_vec4_t *b) {
	const float *A = a->a;
	const float *B = b->a;

	return A[0]*B[0] + A[1]*B[1] + A[2]*B[2] + A[3]*B[3];
}

static inline float lh_vec4_len(const lh_vec4_t *a) {
	return sqrtf(lh_vec4_mul_inner(a, a));
}

static inline void lh_vec4_norm(lh_vec4_t *r, const lh_vec4_t *a) {
	const float s = 1.f / lh_vec4_len(a);
	lh_vec4_scale(r, a, s);
}

static inline void lh_ivec2_dup(lh_ivec2_t *r, const lh_ivec2_t *a) {
	int *R = r->a;
	const int *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
}

static inline void lh_ivec2_zero(lh_ivec2_t *r) {
	int *R = r->a;

	R[0] = 0;
	R[1] = 0;
}

static inline void lh_ivec2_add(lh_ivec2_t *r, const lh_ivec2_t *a, const lh_ivec2_t *b) {
	int *R = r->a;
	const int *A = a->a;
	const int *B = b->a;

	R[0] = A[0] + B[0];
	R[1] = A[1] + B[1];
}

static inline void lh_ivec3_dup(lh_ivec3_t *r, const lh_ivec3_t *a) {
	int *R = r->a;
	const int *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_ivec3_zero(lh_ivec3_t *r) {
	int *R = r->a;

	R[0] = 0;
	R[1] = 0;
	R[2] = 0;
}

static inline void lh_ivec4_dup(lh_ivec4_t *r, const lh_ivec4_t *a) {
	int *R = r->a;
	const int *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_ivec4_zero(lh_ivec4_t *r) {
	int *R = r->a;

	R[0] = 0;
	R[1] = 0;
	R[2] = 0;
	R[3] = 0;
}

static inline void lh_uvec2_dup(lh_uvec2_t *r, const lh_uvec2_t *a) {
	LHuint *R = r->a;
	const LHuint *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
}

static inline void lh_uvec2_zero(lh_uvec2_t *r) {
	LHuint *R = r->a;

	R[0] = 0;
	R[1] = 0;
}

static inline void lh_uvec3_dup(lh_uvec3_t *r, const lh_uvec3_t *a) {
	LHuint *R = r->a;
	const LHuint *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_uvec3_zero(lh_uvec3_t *r) {
	LHuint *R = r->a;

	R[0] = 0;
	R[1] = 0;
	R[2] = 0;
}

static inline void lh_uvec4_dup(lh_uvec4_t *r, const lh_uvec4_t *a) {
	LHuint *R = r->a;
	const LHuint *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_uvec4_zero(lh_uvec4_t *r) {
	LHuint *R = r->a;

	R[0] = 0;
	R[1] = 0;
	R[2] = 0;
	R[3] = 0;
}

static inline void lh_quat_dup(lh_quat_t *r, const lh_quat_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_quat_identity(lh_quat_t *r) {
	float *R = r->a;

	R[0] = 0.f;
	R[1] = 0.f;
	R[2] = 0.f;
	R[3] = 1.f;
}

static inline void lh_quat_from_vec3_euler(lh_quat_t *r, const lh_vec3_t *a) {
	float *R = r->a;
	const float *A = a->a;
	const float c0 = cosf(A[0] * .5f);
	const float c1 = cosf(A[1] * .5f);
	const float c2 = cosf(A[2] * .5f);
	const float s0 = sinf(A[0] * .5f);
	const float s1 = sinf(A[1] * .5f);
	const float s2 = sinf(A[2] * .5f);

	R[0] = s0 * c1 * c2 - c0 * s1 * s2;
	R[1] = c0 * s1 * c2 + s0 * c1 * s2;
	R[2] = c0 * c1 * s2 - s0 * s1 * c2;
	R[3] = c0 * c1 * c2 + s0 * s1 * s2;
}

static inline void lh_quat_from_two_vec3(lh_quat_t *r, const lh_vec3_t *a, const lh_vec3_t *b) {
	float *R = r->a;
	lh_vec3_t w;

	lh_vec3_mul_cross(&w, a, b);

	R[0] = w.c.x;
	R[1] = w.c.y;
	R[2] = w.c.z;
	R[3] = lh_vec3_mul_inner(a, b);

	R[3] += lh_vec4_len(r);

	lh_vec4_norm(r, r);
}

static inline void lh_quat_scale(lh_quat_t *r, const lh_quat_t *a, const float s) {
	lh_vec4_scale((lh_vec4_t *)r, (const lh_vec4_t *)a, s);
}

static inline void lh_quat_add(lh_quat_t *r, const lh_quat_t *a, const lh_quat_t *b) {
	lh_vec4_add((lh_vec4_t *)r, (const lh_vec4_t *)a, (const lh_vec4_t *)b);
}

static inline void lh_quat_mul(lh_quat_t *r, const lh_quat_t *a, const lh_quat_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;

	R[0] = A[0]*B[0] - A[1]*B[1] - A[2]*B[2] - A[3]*B[3];
	R[1] = A[0]*B[1] + A[1]*B[0] + A[2]*B[3] - A[3]*B[2];
	R[2] = A[0]*B[2] - A[1]*B[3] + A[2]*B[0] + A[3]*B[1];
	R[3] = A[0]*B[3] - A[1]*B[2] - A[2]*B[1] + A[3]*B[0];
}

static inline void lh_quat_norm(lh_quat_t *r, const lh_quat_t *a) {
	lh_vec4_norm((lh_vec4_t *)r, (const lh_vec4_t *)a);
}

static inline void lh_quat_conjug(lh_quat_t *r, const lh_quat_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = -A[0];
	R[1] = -A[1];
	R[2] = -A[2];
	R[3] = A[3];
}

static inline void lh_quat_hamilton(lh_quat_t *r, const lh_quat_t *a, const lh_quat_t *b) {
	float *R = r->a;
	const float *A = a->a;
	const float *B = b->a;
	const float as = A[3], bs = B[3];
	lh_vec3_t av = LH_VEC3(A[0], A[1], A[2]);
	lh_vec3_t bv = LH_VEC3(B[0], B[1], B[2]);
	lh_vec3_t ab_cross, r3;

	R[3] = as*bs - lh_vec3_mul_inner(&av, &bv);

	lh_vec3_mul_cross(&ab_cross, &av, &bv);
	lh_vec3_scale(&av, &av, bs);
	lh_vec3_scale(&bv, &bv, as);
	lh_vec3_add(&r3, &av, &bv);
	lh_vec3_add(&r3, &r3, &ab_cross);

	R[0] = r3.c.x;
	R[1] = r3.c.y;
	R[2] = r3.c.z;
}

static inline void lh_quat_lerp(lh_quat_t *r, const lh_quat_t *a, const lh_quat_t *b, const float t) {
	lh_quat_t a1, b1, c;

	lh_quat_scale(&a1, a, 1 - t);
	lh_quat_scale(&b1, b, t);
	lh_quat_add(&c, &a1, &b1);
	lh_quat_norm(r, &c);
}

static inline void lh_mat2_dup(lh_mat2_t *r, const lh_mat2_t *a) {
	float (*R)[2] = r->a;
	const float (*A)[2] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
}

static inline void lh_mat2_zero(lh_mat2_t *r) {
	float (*R)[2] = r->a;

	R[0][0] = 0.f;
	R[0][1] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 0.f;
}

static inline void lh_mat2_identity(lh_mat2_t *r) {
	float (*R)[2] = r->a;

	R[0][0] = 1.f;
	R[0][1] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 1.f;
}

static inline void lh_mat2_mul(lh_mat2_t *r, const lh_mat2_t *a, const lh_mat2_t *b) {
	float (*R)[2] = r->a;
	const float (*A)[2] = a->a;
	const float (*B)[2] = b->a;

	R[0][0] = B[0][0]*A[0][0] + B[0][1]*A[1][0];
	R[0][1] = B[0][0]*A[0][1] + B[0][1]*A[1][1];

	R[1][0] = B[1][0]*A[0][0] + B[1][1]*A[1][0];
	R[1][1] = B[1][0]*A[0][1] + B[1][1]*A[1][1];
}

static inline void lh_mat2_invert(lh_mat2_t *r, const lh_mat2_t *a) {
	float (*R)[2] = r->a;
	const float (*A)[2] = a->a;

	const float idet = 1.0f / (A[0][0] * A[1][1] - A[0][1] * A[1][0]);
	R[0][0] =  A[1][1] * idet;
	R[0][1] = -A[0][1] * idet;
	R[1][0] = -A[1][0] * idet;
	R[1][1] =  A[0][0] * idet;
}

static inline void lh_mat2_rot_angle(lh_mat2_t *r, const lh_mat2_t *a, const float angle) {
	lh_mat2_t m;
	float (*M)[2] = m.a;
	const float c = cos(angle);
	const float s = sin(angle);

	M[0][0] = c;
	M[0][1] = s;

	M[1][0] = s;
	M[1][1] = c;

	lh_mat2_mul(r, a, &m);
}

static inline void lh_mat3_dup(lh_mat3_t *r, const lh_mat3_t *a) {
	float (*R)[3] = r->a;
	const float (*A)[3] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];
	R[0][2] = A[0][2];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
	R[1][2] = A[1][2];

	R[2][0] = A[2][0];
	R[2][1] = A[2][1];
	R[2][2] = A[2][2];
}

static inline void lh_mat3_zero(lh_mat3_t *r) {
	float (*R)[3] = r->a;

	R[0][0] = 0.f;
	R[0][1] = 0.f;
	R[0][2] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 0.f;
	R[1][2] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = 0.f;
}

static inline void lh_mat3_from_mat4(lh_mat3_t *r, const lh_mat4_t *a) {
	float (*R)[3] = r->a;
	const float (*A)[4] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];
	R[0][2] = A[0][2];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
	R[1][2] = A[1][2];

	R[2][0] = A[2][0];
	R[2][1] = A[2][1];
	R[2][2] = A[2][2];
}

static inline void lh_mat3_normal_from_mat4(lh_mat3_t *r, const lh_mat4_t *a) {
	lh_mat4_t m, n;
	lh_mat4_transpose(&m, a);
	lh_mat4_invert(&n, &m);
	lh_mat3_from_mat4(r, &n);
}

static inline void lh_mat3_identity(lh_mat3_t *r) {
	float (*R)[3] = r->a;

	R[0][0] = 1.f;
	R[0][1] = 0.f;
	R[0][2] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 1.f;
	R[1][2] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = 1.f;
}

static inline void lh_mat3_scale(lh_mat3_t *r, lh_vec3_t *scale) {
	float (*R)[3] = r->a;

	R[0][0] = scale->c.x;
	R[0][1] = 0.f;
	R[0][2] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = scale->c.y;
	R[1][2] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = scale->c.z;
}

static inline void lh_mat3_mul(lh_mat3_t *r, const lh_mat3_t *a, const lh_mat3_t *b) {
	float (*R)[3] = r->a;
	const float (*A)[3] = a->a;
	const float (*B)[3] = b->a;

	R[0][0] = B[0][0]*A[0][0] + B[0][1]*A[1][0] + B[0][2]*A[2][0];
	R[0][1] = B[0][0]*A[0][1] + B[0][1]*A[1][1] + B[0][2]*A[2][1];
	R[0][2] = B[0][0]*A[0][2] + B[0][1]*A[1][2] + B[0][2]*A[2][2];

	R[1][0] = B[1][0]*A[0][0] + B[1][1]*A[1][0] + B[1][2]*A[2][0];
	R[1][1] = B[1][0]*A[0][1] + B[1][1]*A[1][1] + B[1][2]*A[2][1];
	R[1][2] = B[1][0]*A[0][2] + B[1][1]*A[1][2] + B[1][2]*A[2][2];

	R[2][0] = B[2][0]*A[0][0] + B[2][1]*A[1][0] + B[2][2]*A[2][0];
	R[2][1] = B[2][0]*A[0][1] + B[2][1]*A[1][1] + B[2][2]*A[2][1];
	R[2][2] = B[2][0]*A[0][2] + B[2][1]*A[1][2] + B[2][2]*A[2][2];
}

/* Rotate around the 3 orthogonal axis. */
static inline void lh_mat3_rotXYZ(lh_mat3_t *r, const lh_mat3_t *a, const lh_vec3_t *angle) {
	const float *T = angle->a;
	lh_mat3_t m;
	float (*M)[3] = m.a;
	const float cx = cos(T[0]);
	const float cy = cos(T[1]);
	const float cz = cos(T[2]);
	const float sx = sin(T[0]);
	const float sy = sin(T[1]);
	const float sz = sin(T[2]);

	M[0][0] = cy*cz;
	M[0][1] = cy*sz;
	M[0][2] = -sy;

	M[1][0] = sx*sy*cz - cx*sz;
	M[1][1] = sx*sy*sz + cx*cz;
	M[1][2] = sx*cy;

	M[2][0] = cx*sy*cz + sx*sz;
	M[2][1] = cx*sy*sz - sx*cz;
	M[2][2] = cx*cy;

	lh_mat3_mul(r, a, &m);
}

/* Rotate around a vector */
static inline void lh_mat3_rotate(lh_mat3_t *r, const lh_mat3_t *a, const lh_vec3_t *v, const float angle) {
	lh_mat3_t m;
	float (*M)[3] = m.a;
	const float c = cos(angle);
	const float ic = 1.f - c;
	const float s = sin(angle);
	lh_vec3_t u;

	lh_vec3_norm(&u, v);

	const float x = u.c.x;
	const float y = u.c.y;
	const float z = u.c.z;

	M[0][0] = x*x*ic + c;
	M[0][1] = x*y*ic + z*s;
	M[0][2] = x*z*ic - y*s;

	M[1][0] = y*x*ic - z*s;
	M[1][1] = y*y*ic + c;
	M[1][2] = y*z*ic + x*s;

	M[2][0] = z*x*ic + y*s;
	M[2][1] = z*y*ic - x*s;
	M[2][2] = z*z*ic + c;

	lh_mat3_mul(r, a, &m);
}

static inline void lh_mat3_rotate_quat(lh_mat3_t *r, const lh_mat3_t *a, const lh_quat_t *angle) {
	lh_mat3_t m;
	float (*M)[3] = m.a;
	const float x = angle->c.x;
	const float y = angle->c.y;
	const float z = angle->c.z;
	const float w = angle->c.w;
	const float n2xx = -2.f * x * x;
	const float n2yy = -2.f * y * y;
	const float n2zz = -2.f * z * z;
	const float p2xy = 2.f * x * y;
	const float p2xz = 2.f * x * z;
	const float p2xw = 2.f * x * w;
	const float p2yz = 2.f * y * z;
	const float p2yw = 2.f * y * w;
	const float p2zw = 2.f * z * w;

	M[0][0] = 1.f + n2yy + n2zz;
	M[0][1] = p2xy + p2zw;
	M[0][2] = p2xz - p2yw;

	M[1][0] = p2xy - p2zw;
	M[1][1] = 1.f + n2xx + n2zz;
	M[1][2] = p2yz + p2xw;

	M[2][0] = p2xz + p2yw;
	M[2][1] = p2yz - p2xw;
	M[2][2] = 1.f + n2xx + n2yy;

	lh_mat3_mul(r, a, &m);
}

static inline void lh_mat3_rot_angle(lh_mat3_t *r, const lh_mat3_t *a, const float angle) {
	lh_mat3_t m;
	float (*M)[3] = m.a;
	const float c = cos(angle);
	const float s = sin(angle);

	M[0][0] = c;
	M[0][1] = s;
	M[0][2] = 0.f;

	M[1][0] = -s;
	M[1][1] = c;
	M[1][2] = 0.f;

	M[2][0] = 0.f;
	M[2][1] = 0.f;
	M[2][2] = 1.f;

	lh_mat3_mul(r, a, &m);
}

static inline void lh_mat3_translate(lh_mat3_t *r, const lh_vec3_t *v) {
	float (*R)[3] = r->a;

	lh_mat3_identity(r);
	lh_vec3_dup((lh_vec3_t *)R[2], v);
}

static inline void lh_mat3_translate_in_place(lh_mat3_t *r, const lh_vec3_t *v) {
	lh_mat3_t a, b;

	lh_mat3_dup(&a, r);
	lh_mat3_translate(&b, v);
	lh_mat3_mul(r, &a, &b);
}

static inline void lh_mat3_translate_2d(lh_mat3_t *r, const lh_vec2_t *v) {
	float (*R)[3] = r->a;

	lh_mat3_identity(r);
	lh_vec2_dup((lh_vec2_t *)R[2], v);
}

static inline void lh_mat3_translate_in_place_2d(lh_mat3_t *r, const lh_vec2_t *v) {
	lh_mat3_t a, b;

	lh_mat3_dup(&a, r);
	lh_mat3_translate_2d(&b, v);
	lh_mat3_mul(r, &a, &b);
}

static inline void lh_mat3_view_2d(lh_mat3_t *r, const lh_vec2_t *pos, const lh_vec2_t *scale, const float angle) {
	lh_mat3_t m;
	lh_vec2_t ipos;
	const float iangle = -angle;

	lh_mat3_scale(&m, &LH_VEC3(scale->c.x, scale->c.y, 1.f));
	lh_mat3_rot_angle(r, &m, iangle);
	lh_vec2_neg(&ipos, pos);
	lh_mat3_translate_in_place_2d(r, &ipos);
}

static inline void lh_mat4_dup(lh_mat4_t *r, const lh_mat4_t *a) {
	float (*R)[4] = r->a;
	const float (*A)[4] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];
	R[0][2] = A[0][2];
	R[0][3] = A[0][3];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
	R[1][2] = A[1][2];
	R[1][3] = A[1][3];

	R[2][0] = A[2][0];
	R[2][1] = A[2][1];
	R[2][2] = A[2][2];
	R[2][3] = A[2][3];

	R[3][0] = A[3][0];
	R[3][1] = A[3][1];
	R[3][2] = A[3][2];
	R[3][3] = A[3][3];
}

static inline void lh_mat4_zero(lh_mat4_t *r) {
	float (*R)[4] = r->a;

	R[0][0] = 0.f;
	R[0][1] = 0.f;
	R[0][2] = 0.f;
	R[0][3] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 0.f;
	R[1][2] = 0.f;
	R[1][3] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = 0.f;
	R[2][3] = 0.f;

	R[3][0] = 0.f;
	R[3][1] = 0.f;
	R[3][2] = 0.f;
	R[3][3] = 0.f;
}

static inline void lh_mat4_dup_from_mat3(lh_mat4_t *r, const lh_mat3_t *a) {
	float (*R)[4] = r->a;
	const float (*A)[3] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];
	R[0][2] = A[0][2];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
	R[1][2] = A[1][2];

	R[2][0] = A[2][0];
	R[2][1] = A[2][1];
	R[2][2] = A[2][2];
}

static inline void lh_mat4_from_mat3(lh_mat4_t *r, const lh_mat3_t *a) {
	float (*R)[4] = r->a;

	lh_mat4_dup_from_mat3(r, a);
	R[0][3] = 0.f;
	R[1][3] = 0.f;
	R[2][3] = 0.f;
	R[3][0] = 0.f;
	R[3][1] = 0.f;
	R[3][2] = 0.f;
	R[3][3] = 0.f;
}

static inline void lh_mat4_identity_from_mat3(lh_mat4_t *r, const lh_mat3_t *a) {
	float (*R)[4] = r->a;

	lh_mat4_from_mat3(r, a);
	R[3][3] = 1.f;
}

static inline void lh_mat4_identity(lh_mat4_t *r) {
	float (*R)[4] = r->a;

	R[0][0] = 1.f;
	R[0][1] = 0.f;
	R[0][2] = 0.f;
	R[0][3] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 1.f;
	R[1][2] = 0.f;
	R[1][3] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = 1.f;
	R[2][3] = 0.f;

	R[3][0] = 0.f;
	R[3][1] = 0.f;
	R[3][2] = 0.f;
	R[3][3] = 1.f;
}

static inline void lh_mat4_mul(lh_mat4_t *r, const lh_mat4_t *a, const lh_mat4_t *b) {
	float (*R)[4] = r->a;
	const float (*A)[4] = a->a;
	const float (*B)[4] = b->a;

	R[0][0] = B[0][0]*A[0][0] + B[0][1]*A[1][0] + B[0][2]*A[2][0] + B[0][3]*A[3][0];
	R[0][1] = B[0][0]*A[0][1] + B[0][1]*A[1][1] + B[0][2]*A[2][1] + B[0][3]*A[3][1];
	R[0][2] = B[0][0]*A[0][2] + B[0][1]*A[1][2] + B[0][2]*A[2][2] + B[0][3]*A[3][2];
	R[0][3] = B[0][0]*A[0][3] + B[0][1]*A[1][3] + B[0][2]*A[2][3] + B[0][3]*A[3][3];

	R[1][0] = B[1][0]*A[0][0] + B[1][1]*A[1][0] + B[1][2]*A[2][0] + B[1][3]*A[3][0];
	R[1][1] = B[1][0]*A[0][1] + B[1][1]*A[1][1] + B[1][2]*A[2][1] + B[1][3]*A[3][1];
	R[1][2] = B[1][0]*A[0][2] + B[1][1]*A[1][2] + B[1][2]*A[2][2] + B[1][3]*A[3][2];
	R[1][3] = B[1][0]*A[0][3] + B[1][1]*A[1][3] + B[1][2]*A[2][3] + B[1][3]*A[3][3];

	R[2][0] = B[2][0]*A[0][0] + B[2][1]*A[1][0] + B[2][2]*A[2][0] + B[2][3]*A[3][0];
	R[2][1] = B[2][0]*A[0][1] + B[2][1]*A[1][1] + B[2][2]*A[2][1] + B[2][3]*A[3][1];
	R[2][2] = B[2][0]*A[0][2] + B[2][1]*A[1][2] + B[2][2]*A[2][2] + B[2][3]*A[3][2];
	R[2][3] = B[2][0]*A[0][3] + B[2][1]*A[1][3] + B[2][2]*A[2][3] + B[2][3]*A[3][3];

	R[3][0] = B[3][0]*A[0][0] + B[3][1]*A[1][0] + B[3][2]*A[2][0] + B[3][3]*A[3][0];
	R[3][1] = B[3][0]*A[0][1] + B[3][1]*A[1][1] + B[3][2]*A[2][1] + B[3][3]*A[3][1];
	R[3][2] = B[3][0]*A[0][2] + B[3][1]*A[1][2] + B[3][2]*A[2][2] + B[3][3]*A[3][2];
	R[3][3] = B[3][0]*A[0][3] + B[3][1]*A[1][3] + B[3][2]*A[2][3] + B[3][3]*A[3][3];
}

static inline void lh_mat4_translate(lh_mat4_t *r, const lh_vec3_t *v) {
	float (*R)[4] = r->a;

	lh_mat4_identity(r);
	lh_vec3_dup((lh_vec3_t *)R[3], v);
}

static inline void lh_mat4_translate_in_place(lh_mat4_t *r, const lh_vec3_t *v) {
	lh_mat4_t a, b;

	lh_mat4_dup(&a, r);
	lh_mat4_translate(&b, v);
	lh_mat4_mul(r, &a, &b);
}

/* Rotate around the 3 orthogonal axes. */
static inline void lh_mat4_rotXYZ(lh_mat4_t *r, const lh_mat4_t *a, const lh_vec3_t *angle) {
	const float *T = angle->a;
	lh_mat4_t m;
	float (*M)[4] = m.a;
	const float cx = cos(T[0]);
	const float cy = cos(T[1]);
	const float cz = cos(T[2]);
	const float sx = sin(T[0]);
	const float sy = sin(T[1]);
	const float sz = sin(T[2]);

	M[0][0] = cy*cz;
	M[0][1] = cy*sz;
	M[0][2] = -sy;
	M[0][3] = 0.f;

	M[1][0] = sx*sy*cz - cx*sz;
	M[1][1] = sx*sy*sz + cx*cz;
	M[1][2] = sx*cy;
	M[1][3] = 0.f;

	M[2][0] = cx*sy*cz + sx*sz;
	M[2][1] = cx*sy*sz - sx*cz;
	M[2][2] = cx*cy;
	M[2][3] = 0.f;

	M[3][0] = 0.f;
	M[3][1] = 0.f;
	M[3][2] = 0.f;
	M[3][3] = 1.f;

	lh_mat4_mul(r, a, &m);
}

/* Rotate around the 3 orthogonal axes. */
static inline void lh_mat4_rotZYX(lh_mat4_t *r, const lh_mat4_t *a, const lh_vec3_t *angle) {
	const float *T = angle->a;
	lh_mat4_t m;
	float (*M)[4] = m.a;
	const float cx = cos(T[0]);
	const float cy = cos(T[1]);
	const float cz = cos(T[2]);
	const float sx = sin(T[0]);
	const float sy = sin(T[1]);
	const float sz = sin(T[2]);

	M[0][0] = cy*cz;
	M[0][1] = (sx*sy*cz + cx*sz);
	M[0][2] = (-cx*sy*cz + sx*sz);
	M[0][3] = 0.f;

	M[1][0] = (-cy*sz);
	M[1][1] = (-sx*sy*sz + cx*cz);
	M[1][2] = (cx*sy*sz + sx*cz);
	M[1][3] = 0.f;

	M[2][0] = sy;
	M[2][1] = (-sx*cy);
	M[2][2] = cx*cy;
	M[2][3] = 0.f;

	M[3][0] = 0.f;
	M[3][1] = 0.f;
	M[3][2] = 0.f;
	M[3][3] = 1.f;

	lh_mat4_mul(r, a, &m);
}

/* Rotate around a vector */
static inline void lh_mat4_rotate(lh_mat4_t *r, const lh_mat4_t *a, const lh_vec3_t *v, const float angle) {
	lh_mat4_t m;
	float (*M)[4] = m.a;
	const float c = cos(angle);
	const float ic = 1.f - c;
	const float s = sin(angle);
	lh_vec3_t u;

	lh_vec3_norm(&u, v);

	const float x = u.c.x;
	const float y = u.c.y;
	const float z = u.c.z;

	M[0][0] = x*x*ic + c;
	M[0][1] = x*y*ic + z*s;
	M[0][2] = x*z*ic - y*s;
	M[0][3] = 0.f;

	M[1][0] = y*x*ic - z*s;
	M[1][1] = y*y*ic + c;
	M[1][2] = y*z*ic + x*s;
	M[1][3] = 0.f;

	M[2][0] = z*x*ic + y*s;
	M[2][1] = z*y*ic - x*s;
	M[2][2] = z*z*ic + c;
	M[2][3] = 0.f;

	M[3][0] = 0.f;
	M[3][1] = 0.f;
	M[3][2] = 0.f;
	M[3][3] = 1.f;

	lh_mat4_mul(r, a, &m);
}

static inline void lh_mat4_rotate_quat(lh_mat4_t *r, const lh_mat4_t *a, const lh_quat_t *angle) {
	lh_mat4_t m;
	float (*M)[4] = m.a;
	const float x = angle->c.x;
	const float y = angle->c.y;
	const float z = angle->c.z;
	const float w = angle->c.w;
	const float n2xx = -2.f * x * x;
	const float n2yy = -2.f * y * y;
	const float n2zz = -2.f * z * z;
	const float p2xy = 2.f * x * y;
	const float p2xz = 2.f * x * z;
	const float p2xw = 2.f * x * w;
	const float p2yz = 2.f * y * z;
	const float p2yw = 2.f * y * w;
	const float p2zw = 2.f * z * w;

	M[0][0] = 1.f + n2yy + n2zz;
	M[0][1] = p2xy + p2zw;
	M[0][2] = p2xz - p2yw;
	M[0][3] = 0.f;

	M[1][0] = p2xy - p2zw;
	M[1][1] = 1.f + n2xx + n2zz;
	M[1][2] = p2yz + p2xw;
	M[1][3] = 0.f;

	M[2][0] = p2xz + p2yw;
	M[2][1] = p2yz - p2xw;
	M[2][2] = 1.f + n2xx + n2yy;
	M[2][3] = 0.f;

	M[3][0] = 0.f;
	M[3][1] = 0.f;
	M[3][2] = 0.f;
	M[3][3] = 1.f;

	lh_mat4_mul(r, a, &m);
}

static inline void lh_mat4_invert(lh_mat4_t *r, const lh_mat4_t *a) {
	float (*R)[4] = r->a;
	const float (*A)[4] = a->a;
	float s[6];
	float c[6];

	s[0] = A[0][0]*A[1][1] - A[1][0]*A[0][1];
	s[1] = A[0][0]*A[1][2] - A[1][0]*A[0][2];
	s[2] = A[0][0]*A[1][3] - A[1][0]*A[0][3];
	s[3] = A[0][1]*A[1][2] - A[1][1]*A[0][2];
	s[4] = A[0][1]*A[1][3] - A[1][1]*A[0][3];
	s[5] = A[0][2]*A[1][3] - A[1][2]*A[0][3];

	c[0] = A[2][0]*A[3][1] - A[3][0]*A[2][1];
	c[1] = A[2][0]*A[3][2] - A[3][0]*A[2][2];
	c[2] = A[2][0]*A[3][3] - A[3][0]*A[2][3];
	c[3] = A[2][1]*A[3][2] - A[3][1]*A[2][2];
	c[4] = A[2][1]*A[3][3] - A[3][1]*A[2][3];
	c[5] = A[2][2]*A[3][3] - A[3][2]*A[2][3];

	const float idet = 1.0f/(s[0]*c[5]-s[1]*c[4]+s[2]*c[3]+s[3]*c[2]-s[4]*c[1]+s[5]*c[0]);

	R[0][0] = ( A[1][1] * c[5] - A[1][2] * c[4] + A[1][3] * c[3]) * idet;
	R[0][1] = (-A[0][1] * c[5] + A[0][2] * c[4] - A[0][3] * c[3]) * idet;
	R[0][2] = ( A[3][1] * s[5] - A[3][2] * s[4] + A[3][3] * s[3]) * idet;
	R[0][3] = (-A[2][1] * s[5] + A[2][2] * s[4] - A[2][3] * s[3]) * idet;

	R[1][0] = (-A[1][0] * c[5] + A[1][2] * c[2] - A[1][3] * c[1]) * idet;
	R[1][1] = ( A[0][0] * c[5] - A[0][2] * c[2] + A[0][3] * c[1]) * idet;
	R[1][2] = (-A[3][0] * s[5] + A[3][2] * s[2] - A[3][3] * s[1]) * idet;
	R[1][3] = ( A[2][0] * s[5] - A[2][2] * s[2] + A[2][3] * s[1]) * idet;

	R[2][0] = ( A[1][0] * c[4] - A[1][1] * c[2] + A[1][3] * c[0]) * idet;
	R[2][1] = (-A[0][0] * c[4] + A[0][1] * c[2] - A[0][3] * c[0]) * idet;
	R[2][2] = ( A[3][0] * s[4] - A[3][1] * s[2] + A[3][3] * s[0]) * idet;
	R[2][3] = (-A[2][0] * s[4] + A[2][1] * s[2] - A[2][3] * s[0]) * idet;

	R[3][0] = (-A[1][0] * c[3] + A[1][1] * c[1] - A[1][2] * c[0]) * idet;
	R[3][1] = ( A[0][0] * c[3] - A[0][1] * c[1] + A[0][2] * c[0]) * idet;
	R[3][2] = (-A[3][0] * s[3] + A[3][1] * s[1] - A[3][2] * s[0]) * idet;
	R[3][3] = ( A[2][0] * s[3] - A[2][1] * s[1] + A[2][2] * s[0]) * idet;
}

static inline void lh_mat4_transpose(lh_mat4_t *r, const lh_mat4_t *a) {
	float (*R)[4] = r->a;
	const float (*A)[4] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[1][0];
	R[0][2] = A[2][0];
	R[0][3] = A[3][0];

	R[1][0] = A[0][1];
	R[1][1] = A[1][1];
	R[1][2] = A[2][1];
	R[1][3] = A[3][1];

	R[2][0] = A[0][2];
	R[2][1] = A[1][2];
	R[2][2] = A[2][2];
	R[2][3] = A[3][2];

	R[3][0] = A[0][3];
	R[3][1] = A[1][3];
	R[3][2] = A[2][3];
	R[3][3] = A[3][3];
}

static inline void lh_mat4_perspective(lh_mat4_t *r, const float fovy, const float aspect, const float near, const float far) {
	float (*R)[4] = r->a;
	const float f = 1.f / tan(fovy / 2.f);

	R[0][0] = f / aspect;
	R[0][1] = 0.f;
	R[0][2] = 0.f;
	R[0][3] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = f;
	R[1][2] = 0.f;
	R[1][3] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = (far + near) / (near - far);
	R[2][3] = -1.f;

	R[3][0] = 0.f;
	R[3][1] = 0.f;
	R[3][2] = 2.f * far * near / (near - far);
	R[3][3] = 0.f;
}

static inline void lh_mat4_model(lh_mat4_t *r, const lh_vec3_t *pos, const lh_quat_t *angle) {
	lh_mat4_t m;

	lh_mat4_identity(&m);
	lh_mat4_translate_in_place(&m, pos);
	lh_mat4_rotate_quat(r, &m, angle);
}

static inline void lh_mat4_view(lh_mat4_t *r, const lh_vec3_t *pos, const lh_quat_t *angle) {
	lh_mat4_t m;
	lh_vec3_t ipos;
	lh_quat_t iangle;

	/* The camera is inverted relative to the scene. */
	lh_vec3_neg(&ipos, pos);
	lh_quat_conjug(&iangle, angle);

	lh_mat4_identity(&m);
	lh_mat4_rotate_quat(r, &m, &iangle);
	lh_mat4_translate_in_place(r, &ipos);
}

static inline void lh_rgb_dup(lh_rgb_t *r, const lh_rgb_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_rgb888_dup(lh_rgb888_t *r, const lh_rgb888_t *a) {
	LHubyte *R = r->a;
	const LHubyte *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_rgba_dup(lh_rgba_t *r, const lh_rgba_t *a) {
	float *R = r->a;
	const float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_rgba8888_dup(lh_rgba8888_t *r, const lh_rgba8888_t *a) {
	LHubyte *R = r->a;
	const LHubyte *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline LHbool lh_point_collides_point(const lh_vec3_t *point_a, const lh_vec3_t *point_b) {
	const float *PA = point_a->a;
	const float *PB = point_b->a;

	return PA[0] == PB[0] && PA[1] == PB[1] && PA[2] == PB[2];
}

static inline LHbool lh_point_collides_line(const lh_vec3_t *point, const lh_line_t *line) {
	lh_vec3_t lp, line_v, cross;

	lh_vec3_ux_rotate_quat(&line_v, &line->q);
	lh_vec3_sub(&lp, point, &line->p);
	lh_vec3_mul_cross(&cross, &lp, &line_v);

	return fabsf(cross.c.x) < FLT_EPSILON && fabsf(cross.c.y) < FLT_EPSILON && fabsf(cross.c.z) < FLT_EPSILON;
}

/*
 * Computes the distance along the line between its origin and its intersection
 * with the plane, in direction vector unit.
 */
static inline float lh_line_collides_plane_dist_orig(const lh_line_t *line, const lh_plane_t *plane) {
	lh_vec3_t line_v, plane_v1, plane_v2, normal;

	lh_vec3_ux_rotate_quat(&line_v, &line->q);
	lh_vec3_ux_rotate_quat(&plane_v1, &plane->q);
	lh_vec3_uy_rotate_quat(&plane_v2, &plane->q);

	lh_vec3_mul_cross(&normal, &plane_v1, &plane_v2);

	const float pa = normal.c.x;
	const float pb = normal.c.y;
	const float pc = normal.c.z;
	const float pd = -lh_vec3_mul_inner(&normal, &plane->p);

	return -(pa * line->p.c.x + pb * line->p.c.y + pc * line->p.c.z + pd) / (pa * line_v.c.x + pb * line_v.c.y + pc * line_v.c.z);
}

static inline void lh_line_collides_plane(const lh_line_t *line, const lh_plane_t *plane, lh_vec3_t *inter_point) {
	const float t = lh_line_collides_plane_dist_orig(line, plane);
	lh_vec3_t line_v;

	lh_vec3_ux_rotate_quat(&line_v, &line->q);
	inter_point->c.x = line->p.c.x + t * line_v.c.x;
	inter_point->c.y = line->p.c.y + t * line_v.c.y;
	inter_point->c.z = line->p.c.z + t * line_v.c.z;
}

static inline LHbool lh_line_collides_cuboid(const lh_line_t *line, const lh_cuboid_t *cuboid) {
	lh_vec3_t axes[1 + 3];
	lh_vec3_t p[2 + 8];
	lh_vec3_t *axis;
	lh_vec3_t center;
	lh_vec3_t tmp;
	const lh_vec3_t coords[8] = {
		LH_VEC3(0.f,           0.f,           0.f),
		LH_VEC3(cuboid->s.c.x, 0.f,           0.f),
		LH_VEC3(0.f,           cuboid->s.c.y, 0.f),
		LH_VEC3(cuboid->s.c.x, cuboid->s.c.y, 0.f),
		LH_VEC3(0.f,           0.f,           cuboid->s.c.z),
		LH_VEC3(cuboid->s.c.x, 0.f,           cuboid->s.c.z),
		LH_VEC3(0.f,           cuboid->s.c.y, cuboid->s.c.z),
		LH_VEC3(cuboid->s.c.x, cuboid->s.c.y, cuboid->s.c.z)
	};
	lh_vec3_t line_a;
	lh_vec3_t line_b;
	float t;
	float line_p_comp;
	float line_a_comp;
	float cuboid_p_comp;
	float cuboid_s_comp;

	axis = &axes[0];
	lh_vec3_ux_rotate_quat(&axis[0], &line->q);

	lh_vec3_dup(&line_a, &axis[0]);
	if(fabsf(line_a.c.x) > fabsf(line_a.c.y) && fabsf(line_a.c.x) > fabsf(line_a.c.z))
	{
		line_p_comp = line->p.c.x;
		line_a_comp = line_a.c.x;
		cuboid_p_comp = cuboid->p.c.x;
		cuboid_s_comp = cuboid->s.c.x;
	}
	else if(fabsf(line_a.c.y) > fabsf(line_a.c.z))
	{
		line_p_comp = line->p.c.y;
		line_a_comp = line_a.c.y;
		cuboid_p_comp = cuboid->p.c.y;
		cuboid_s_comp = cuboid->s.c.y;
	}
	else
	{
		line_p_comp = line->p.c.z;
		line_a_comp = line_a.c.z;
		cuboid_p_comp = cuboid->p.c.z;
		cuboid_s_comp = cuboid->s.c.z;
	}
	lh_vec3_scale(&line_a, &line_a, 1.f / line_a_comp);
	line_a_comp = 1.f;

	t = -line_p_comp / line_a_comp;
	lh_vec3_scale(&tmp, &line_a, t);
	lh_vec3_add(&line_b, &tmp, &line->p);

	lh_vec3_scale(&tmp, &line_a, cuboid_p_comp - cuboid_s_comp);
	lh_vec3_add(&p[0], &tmp, &line_b);
	lh_vec3_scale(&tmp, &line_a, cuboid_p_comp + cuboid_s_comp);
	lh_vec3_add(&p[1], &tmp, &line_b);

	axis = &axes[1];
	lh_vec3_ux_rotate_quat(&axis[0], &cuboid->q);
	lh_vec3_uy_rotate_quat(&axis[1], &cuboid->q);
	lh_vec3_uz_rotate_quat(&axis[2], &cuboid->q);

	lh_vec3_add(&center, &cuboid->p, &LH_VEC3(cuboid->s.c.x / 2, cuboid->s.c.y / 2, cuboid->s.c.z / 2));

	for(int i = 0; i < 8; i++) {
		lh_vec3_t *pn = &p[i + 2];

		lh_vec3_add(&tmp, &cuboid->p, &coords[i]);
		lh_vec3_sub(pn, &tmp, &center);
		lh_vec3_rotate_quat(&tmp, pn, &cuboid->q);
		lh_vec3_add(pn, &center, &tmp);
	}

	for(int i = 0; i < 4; i++) {
		lh_vec3_t *axis = &axes[i];
		const float axis_sq = lh_vec3_mul_inner(axis, axis);
		float min[2], max[2];

		for(int j = 0; j < 2; j++) {
			const int idxk[2] = { 0, 2 };
			const int maxk[2] = { 2, 8 };

			min[j] = FLT_MAX;
			max[j] = -FLT_MAX;

			for(int k = idxk[j]; k < maxk[j]; k++) {
				lh_vec3_t *pn = &p[k];
				lh_vec3_t vec3;
				float scalar;

				lh_vec3_scale(&vec3, axis, lh_vec3_mul_inner(pn, axis) / axis_sq);
				scalar = lh_vec3_mul_inner(&vec3, axis);
				min[j] = minf(min[j], scalar);
				max[j] = maxf(max[j], scalar);
			}
		}

		if(min[0] < max[1] && min[1] < max[0])
			continue;
		return LH_FALSE;
	}
	return LH_TRUE;
}

static inline LHbool lh_rect2d_collides_rect2d(lh_rect2d_t *rect1, const lh_rect2d_t *rect2) {
	const lh_rect2d_t *rects[2] = { rect1, rect2 };
	lh_vec2_t axes[2 + 2];
	lh_vec2_t p[2][4];

	for(int i = 0; i < 2; i++) {
		const lh_rect2d_t *rect = rects[i];
		const lh_vec2_t coords[4] = {
			LH_VEC2(0.f,         0.f),
			LH_VEC2(rect->s.c.x, 0.f),
			LH_VEC2(0.f,         rect->s.c.y),
			LH_VEC2(rect->s.c.x, rect->s.c.y)
		};
		lh_vec2_t *axis = &axes[i * 2];
		lh_vec2_t *pn = p[i];
		lh_vec2_t center;
		lh_vec2_t tmp;

		lh_vec2_rotate(&axis[0], &LH_VEC2(1.f, 0.f), rect->angle);
		lh_vec2_rotate(&axis[1], &LH_VEC2(0.f, 1.f), rect->angle);

		lh_vec2_add(&center, &rect->p, &LH_VEC2(rect->s.c.x * .5f, rect->s.c.y * .5f));

		for(int j = 0; j < 4; j++) {
			lh_vec2_t *pnn = &pn[j];

			lh_vec2_add(&tmp, &rect->p, &coords[j]);
			lh_vec2_sub(pnn, &tmp, &center);
			lh_vec2_rotate(&tmp, pnn, rect->angle);
			lh_vec2_add(pnn, &center, &tmp);
		}
	}

	for(int i = 0; i < 4; i++) {
		lh_vec2_t *axis = &axes[i];
		const float axis_sq = lh_vec2_mul_inner(axis, axis);
		float min[2], max[2];

		for(int j = 0; j < 2; j++) {
			lh_vec2_t *pn = p[j];

			min[j] = FLT_MAX;
			max[j] = -FLT_MAX;

			for(int k = 0; k < 4; k++) {
				lh_vec2_t *pnn = &pn[k];
				lh_vec2_t vec2;
				float scalar;

				lh_vec2_scale(&vec2, axis, lh_vec2_mul_inner(pnn, axis) / axis_sq);
				scalar = lh_vec2_mul_inner(&vec2, axis);
				min[j] = minf(min[j], scalar);
				max[j] = maxf(max[j], scalar);
			}
		}

		if(min[0] < max[1] && min[1] < max[0])
			continue;
		return LH_FALSE;
	}
	return LH_TRUE;
}

static inline LHbool lh_rect_collides_rect(lh_rect_t *rect1, const lh_rect_t *rect2) {
	const lh_rect_t *rects[2] = { rect1, rect2 };
	lh_vec3_t axes[2 + 2];
	lh_vec3_t p[2][4];

	for(int i = 0; i < 2; i++) {
		const lh_rect_t *rect = rects[i];
		const lh_vec3_t coords[4] = {
			LH_VEC3(0.f,         0.f,         0.f),
			LH_VEC3(rect->s.c.x, 0.f,         0.f),
			LH_VEC3(0.f,         rect->s.c.y, 0.f),
			LH_VEC3(rect->s.c.x, rect->s.c.y, 0.f)
		};
		lh_vec3_t *axis = &axes[i * 2];
		lh_vec3_t *pn = p[i];
		lh_vec3_t center;
		lh_vec3_t tmp;

		lh_vec3_ux_rotate_quat(&axis[0], &rect->q);
		lh_vec3_uy_rotate_quat(&axis[1], &rect->q);

		lh_vec3_add(&center, &rect->p, &LH_VEC3(rect->s.c.x / 2, rect->s.c.y / 2, 0.f));

		for(int j = 0; j < 4; j++) {
			lh_vec3_t *pnn = &pn[j];

			lh_vec3_add(&tmp, &rect->p, &coords[j]);
			lh_vec3_sub(pnn, &tmp, &center);
			lh_vec3_rotate_quat(&tmp, pnn, &rect->q);
			lh_vec3_add(pnn, &center, &tmp);
		}
	}

	for(int i = 0; i < 4; i++) {
		lh_vec3_t *axis = &axes[i];
		const float axis_sq = lh_vec3_mul_inner(axis, axis);
		float min[2], max[2];

		for(int j = 0; j < 2; j++) {
			lh_vec3_t *pn = p[j];

			min[j] = FLT_MAX;
			max[j] = -FLT_MAX;

			for(int k = 0; k < 4; k++) {
				lh_vec3_t *pnn = &pn[k];
				lh_vec3_t vec3;
				float scalar;

				lh_vec3_scale(&vec3, axis, lh_vec3_mul_inner(pnn, axis) / axis_sq);
				scalar = lh_vec3_mul_inner(&vec3, axis);
				min[j] = minf(min[j], scalar);
				max[j] = maxf(max[j], scalar);
			}
		}

		if(min[0] < max[1] && min[1] < max[0])
			continue;
		return LH_FALSE;
	}
	return LH_TRUE;
}

static inline LHbool lh_cuboid_collides_cuboid(const lh_cuboid_t *cuboid1, const lh_cuboid_t *cuboid2) {
	const lh_cuboid_t *cuboids[2] = { cuboid1, cuboid2 };
	lh_vec3_t axes[3 + 3];
	lh_vec3_t p[2][8];

	for(int i = 0; i < 2; i++) {
		const lh_cuboid_t *cuboid = cuboids[i];
		const lh_vec3_t coords[8] = {
			LH_VEC3(0.f,         0.f,         0.f),
			LH_VEC3(cuboid->s.c.x, 0.f,         0.f),
			LH_VEC3(0.f,         cuboid->s.c.y, 0.f),
			LH_VEC3(cuboid->s.c.x, cuboid->s.c.y, 0.f),
			LH_VEC3(0.f,         0.f,         cuboid->s.c.z),
			LH_VEC3(cuboid->s.c.x, 0.f,         cuboid->s.c.z),
			LH_VEC3(0.f,         cuboid->s.c.y, cuboid->s.c.z),
			LH_VEC3(cuboid->s.c.x, cuboid->s.c.y, cuboid->s.c.z)
		};
		lh_vec3_t *axis = &axes[i * 3];
		lh_vec3_t *pn = p[i];
		lh_vec3_t center;
		lh_vec3_t tmp;

		lh_vec3_ux_rotate_quat(&axis[0], &cuboid->q);
		lh_vec3_uy_rotate_quat(&axis[1], &cuboid->q);
		lh_vec3_uz_rotate_quat(&axis[2], &cuboid->q);

		lh_vec3_add(&center, &cuboid->p, &LH_VEC3(cuboid->s.c.x / 2, cuboid->s.c.y / 2, cuboid->s.c.z / 2));

		for(int j = 0; j < 8; j++) {
			lh_vec3_t *pnn = &pn[j];

			lh_vec3_add(&tmp, &cuboid->p, &coords[j]);
			lh_vec3_sub(pnn, &tmp, &center);
			lh_vec3_rotate_quat(&tmp, pnn, &cuboid->q);
			lh_vec3_add(pnn, &center, &tmp);
		}
	}

	for(int i = 0; i < 6; i++) {
		lh_vec3_t *axis = &axes[i];
		const float axis_sq = lh_vec3_mul_inner(axis, axis);
		float min[2], max[2];

		for(int j = 0; j < 2; j++) {
			lh_vec3_t *pn = p[j];

			min[j] = FLT_MAX;
			max[j] = -FLT_MAX;

			for(int k = 0; k < 8; k++) {
				lh_vec3_t *pnn = &pn[k];
				lh_vec3_t vec3;
				float scalar;

				lh_vec3_scale(&vec3, axis, lh_vec3_mul_inner(pnn, axis) / axis_sq);
				scalar = lh_vec3_mul_inner(&vec3, axis);
				min[j] = minf(min[j], scalar);
				max[j] = maxf(max[j], scalar);
			}
		}

		if(min[0] < max[1] && min[1] < max[0])
			continue;
		return LH_FALSE;
	}
	return LH_TRUE;
}

#endif /* LIBHANG_LIBSUICIDE_H */
