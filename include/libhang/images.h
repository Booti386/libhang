/*
 * Copyright (C) 2014-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_IMAGES_H
#define LIBHANG_IMAGES_H 1

/**
 * @file
 * @brief Image files loader
 */

#include "types.h"

enum {
	LH_IMG_COLOR_RGB = 0,
	LH_IMG_COLOR_RGBA
};

struct lh_image {
	LHuint width;
	LHuint height;
	LHuint depth;
	LHuint color_type;
	void *data;
};

#define LH_IMG_INVAL ((struct lh_image *)NULL)

#ifdef __cplusplus
extern "C" {
#endif

	extern struct lh_image *lhImageCreate(LHuint w, LHuint h, LHuint color_type, lh_dynamic_type_t *fill_color);
	extern struct lh_image *lhImageDup(struct lh_image *img);
	extern int lhImageFill(struct lh_image *image, lh_dynamic_type_t *fill_color);
	extern int lhImageFillRect(struct lh_image *image, lh_dynamic_type_t *fill_color, lh_ivec4_t *rect);
	extern struct lh_image *lhImageLoadPNG(const char *path);
	extern struct lh_image *lhImageLoad(const char *path);
	extern int lhImageWritePNG(struct lh_image *image, const char *path);
	extern int lhImageDestroy(struct lh_image *image);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_IMAGES_H */
