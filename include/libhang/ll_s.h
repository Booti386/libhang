/*
 * Copyright (C) 2016-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_LL_S_H
#define LIBHANG_LL_S_H 1

/**
 * @file
 * @brief Thread-safe linked list
 */

#include "types.h"
#include "ll.h"
#include "driver_os.h"

struct SyncLinkedList
{
	struct LinkedList   base;
	struct LinkedList  *base_ptr;
	struct lh_spinlock *lock;
};

static inline volatile struct SyncLinkedList *sync_list_init(struct SyncLinkedList *slist)
{
	list_init(&slist->base);
	slist->base_ptr = &slist->base;
	slist->lock = lhSpinCreate();
	if(!slist->lock)
		return NULL;
	return slist;
}

static inline struct SyncLinkedList *sync_list_init_from_list(struct SyncLinkedList *slist, struct LinkedList *list)
{
	/* Leave slist->base unused. */
	slist->base_ptr = list;
	slist->lock = lhSpinCreate();
	if(!slist->lock)
		return NULL;
	return slist;
}

static inline struct LinkedList *sync_list_lock(volatile struct SyncLinkedList *slist)
{
	lhSpinLock(slist->lock);
	return slist->base_ptr;
}

static inline void sync_list_unlock(volatile struct SyncLinkedList *slist)
{
	lhSpinUnlock(slist->lock);
}

static inline struct LinkedList *sync_list_get_unsafe_list(volatile struct SyncLinkedList *slist)
{
	return slist->base_ptr;
}

static inline volatile struct SyncLinkedList *sync_list_deinit(volatile struct SyncLinkedList *slist)
{
	/* If slist was created from sync_list_init(). */
	if(slist->base_ptr == &slist->base)
		list_deinit(slist->base_ptr);
	slist->base_ptr = NULL;
	lhSpinDestroy(slist->lock);
	slist->lock = NULL;

	return slist;
}

static inline void sync_list_seek_last(volatile struct SyncLinkedList *slist)
{
	lhSpinLock(slist->lock);
	list_seek_last(slist->base_ptr);
	lhSpinUnlock(slist->lock);
}

static inline void sync_list_seek_first(volatile struct SyncLinkedList *slist)
{
	lhSpinLock(slist->lock);
	list_seek_first(slist->base_ptr);
	lhSpinUnlock(slist->lock);
}

static inline int sync_list_is_empty(volatile struct SyncLinkedList *slist)
{
	int ret;
	lhSpinLock(slist->lock);
	ret = list_is_empty(slist->base_ptr);
	lhSpinUnlock(slist->lock);
	return ret;
}

static inline void sync_list_append(struct SyncLinkedList *slist, struct LinkedList *other)
{
	lhSpinLock(slist->lock);
	list_append(slist->base_ptr, other);
	lhSpinUnlock(slist->lock);
}

static inline unsigned long sync_list_push_entry(volatile struct SyncLinkedList *slist, struct LinkedListEntry *list_entry)
{
	unsigned long id;

	lhSpinLock(slist->lock);
	list_push_entry(slist->base_ptr, list_entry);
	id = list_entry->h.id;
	lhSpinUnlock(slist->lock);
	return id;
}

static inline unsigned long sync_list_push(volatile struct SyncLinkedList *slist, void *elem)
{
	struct LinkedListEntry *list_entry;
	unsigned long id;

	list_entry = list_entry_alloc(0);
	if (!list_entry)
		return LL_INVAL_ID;
	list_entry_set_data(list_entry, elem);

	lhSpinLock(slist->lock);
	list_push_entry(slist->base_ptr, list_entry);
	id = list_entry->h.id;
	lhSpinUnlock(slist->lock);
	return id;
}

static inline unsigned long sync_list_push_dup(volatile struct SyncLinkedList *slist, void *elem, unsigned long size)
{
	struct LinkedListEntry *list_entry;
	unsigned long id;

	list_entry = list_entry_alloc(size);
	if (!list_entry)
		return LL_INVAL_ID;
	list_entry_copy_data(list_entry, elem, size);

	lhSpinLock(slist->lock);
	list_push_entry(slist->base_ptr, list_entry);
	id = list_entry->h.id;
	lhSpinUnlock(slist->lock);
	return id;
}

static inline struct LinkedListEntry *sync_list_pop_entry(volatile struct SyncLinkedList *slist)
{
	struct LinkedListEntry *last_list_entry;

	lhSpinLock(slist->lock);
	last_list_entry = list_pop_entry(slist->base_ptr);
	lhSpinUnlock(slist->lock);
	return last_list_entry;
}

static inline int sync_list_pop(volatile struct SyncLinkedList *slist)
{
	struct LinkedListEntry *last_list_entry;

	last_list_entry = sync_list_pop_entry(slist);
	if(last_list_entry == LL_EMPTY)
		return -1;
	list_entry_free(last_list_entry);
	return 0;
}

static inline void sync_list_clear(volatile struct SyncLinkedList *slist)
{
	lhSpinLock(slist->lock);
	list_clear(slist->base_ptr);
	lhSpinUnlock(slist->lock);
}

static inline int sync_list_remove(volatile struct SyncLinkedList *slist, unsigned long id)
{
	int ret;

	lhSpinLock(slist->lock);
	ret = list_remove(slist->base_ptr, id);
	lhSpinUnlock(slist->lock);
	return ret;
}

static inline unsigned long sync_list_get_last_elem_id(volatile struct SyncLinkedList *slist)
{
	unsigned long id;

	lhSpinLock(slist->lock);
	id = list_get_last_elem_id(slist->base_ptr);
	lhSpinUnlock(slist->lock);
	return id;
}

static inline void *sync_list_get_last_elem(volatile struct SyncLinkedList *slist)
{
	void *elem;

	lhSpinLock(slist->lock);
	elem = list_get_last_elem(slist->base_ptr);
	lhSpinUnlock(slist->lock);
	return elem;
}

static inline void *sync_list_get_elem_by_id(volatile struct SyncLinkedList *slist, unsigned long id)
{
	void *elem;

	lhSpinLock(slist->lock);
	elem = list_get_elem_by_id(slist->base_ptr, id);
	lhSpinUnlock(slist->lock);
	return elem;
}

#endif /* LIBHANG_LL_S_H */
