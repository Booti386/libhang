/*
 * Copyright (C) 2016-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_LLIT_S_H
#define LIBHANG_LLIT_S_H 1

/**
 * @file
 * @brief Thread-safe linked list iterator
 */

#include "types.h"
#include "llit.h"
#include "ll_s.h"

struct SyncLinkedListIterator
{
	struct LinkedListIterator  base;
	struct LinkedListIterator *base_ptr;
	struct SyncLinkedList     *slist;
};


static inline struct SyncLinkedListIterator *sync_list_it_init(struct SyncLinkedListIterator *sit, struct SyncLinkedList *slist)
{
	sync_list_lock(slist);
	list_it_init(&sit->base, slist->base_ptr);
	sit->base_ptr = &sit->base;
	sit->slist = slist;
	sync_list_unlock(slist);
	return sit;
}

static inline struct SyncLinkedListIterator *sync_list_it_init_from_list_it(struct SyncLinkedListIterator *sit, struct LinkedListIterator *it, struct SyncLinkedList *slist)
{
	/* Leave sit->base unused. */
	sit->base_ptr = it;
	sit->slist = slist;
	return sit;
}

static inline struct SyncLinkedListIterator *sync_list_it_deinit(struct SyncLinkedListIterator *sit)
{
	/* If sit was created from sync_list_it_init(). */
	if(sit->base_ptr == &sit->base)
		list_it_deinit(sit->base_ptr);
	sit->base_ptr = NULL;
	sit->slist = NULL;
	return sit;
}

static inline struct LinkedListIterator *sync_list_it_lock(struct SyncLinkedListIterator *sit, struct LinkedList **dst_list)
{
	struct LinkedList *list;

	list = sync_list_lock(sit->slist);
	if(dst_list)
		*dst_list = list;
	return sit->base_ptr;
}

static inline void sync_list_it_unlock(struct SyncLinkedListIterator *sit)
{
	sync_list_unlock(sit->slist);
}

#endif /* LIBHANG_LLIT_S_H */
