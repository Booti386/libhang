/*
 * Copyright (C) 2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_TEXT_H
#define LIBHANG_TEXT_H 1

/**
 * @file
 * @brief Text decoding functions
 */

#include "types.h"

#define LH_UTF8_BOM_STR    "\xEF\xBB\xBF"
#define LH_UTF16LE_BOM_STR "\xFF\xFE"
#define LH_UTF16BE_BOM_STR "\xFE\xFF"
#define LH_UTF32LE_BOM_STR "\xFF\xFE\x00\x00"
#define LH_UTF32BE_BOM_STR "\x00\x00\xFE\xFF"
#define LH_UTF8_BOM_LEN  (sizeof(LH_UTF8_BOM_STR) - 1)
#define LH_UTF16_BOM_LEN (sizeof(LH_UTF16LE_BOM_STR) - 1)
#define LH_UTF32_BOM_LEN (sizeof(LH_UTF32LE_BOM_STR) - 1)


#ifdef __cplusplus
extern "C" {
#endif

	extern uint32_t lhUTF8CharDecode(const char *cs, int *bytes_read);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_TEXT_H */
