/*
 * Copyright (C) 2015-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_OPENSIMPLEXNOISE_H
#define LIBHANG_OPENSIMPLEXNOISE_H 1

/**
 * @file
 * @brief OpenSimplex noise
 */

#include "types.h"

#define OSN_INVAL ((struct lh_osimplex_noise *)NULL)

#ifdef __cplusplus
extern "C" {
#endif

	extern struct lh_osimplex_noise *lh_osimplex_create(struct lh_osimplex_noise **posn);
	extern void lh_osimplex_destroy(struct lh_osimplex_noise **posn);
	extern LH64u lh_osimplex_get_seed(struct lh_osimplex_noise *osn);
	extern void lh_osimplex_set_seed(struct lh_osimplex_noise *osn, LH64u seed);
	extern float lh_osimplex_noise2D(struct lh_osimplex_noise *osn, float x, float y);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_OPENSIMPLEXNOISE_H */
