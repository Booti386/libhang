/*
 * Copyright (C) 2015-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_TYPES_H
#define LIBHANG_TYPES_H 1

/**
 * @file
 * @brief Basic types declaration
 */

#define INSIDE_LIBHANG_TYPES_H 1

#include <stddef.h> /* offsetof() */
#include <stdint.h>
#include <stdlib.h> /* NULL */

/**
 * @def lh_mark_used(var)
 * Avoids a warning when a variable is declared but not used.
 */
#define lh_mark_used(var) ((void)(var))

#define LH_CONTAINER_OF(ptr, type, member) ((type *)((uintptr_t)(ptr) - offsetof(type, member)))
#define LH_ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))

#define LH_TO_FLAG(x) (1 << (x))

#define TO_STR_(x) #x
#define TO_STR(x) TO_STR_(x)

#define LH_FALSE 0
#define LH_TRUE  1

#define LH_DYNAMIC_PTR(x) ((lh_dynamic_type_t){ .as_ptr = (x) })
#define LH_DYNAMIC_SHORT(x) ((lh_dynamic_type_t){ .as_short = (x) })
#define LH_DYNAMIC_USHORT(x) ((lh_dynamic_type_t){ .as_ushort = (x) })
#define LH_DYNAMIC_INT(x) ((lh_dynamic_type_t){ .as_int = (x) })
#define LH_DYNAMIC_UINT(x) ((lh_dynamic_type_t){ .as_uint = (x) })
#define LH_DYNAMIC_LONG(x) ((lh_dynamic_type_t){ .as_long = (x) })
#define LH_DYNAMIC_ULONG(x) ((lh_dynamic_type_t){ .as_ulong = (x) })
#define LH_DYNAMIC_FLOAT(x) ((lh_dynamic_type_t){ .as_float = (x) })
#define LH_DYNAMIC_DOUBLE(x) ((lh_dynamic_type_t){ .as_double = (x) })

#define LH_DYNAMIC_VEC2(...) ((lh_dynamic_type_t){ .as_vec2 = (LH_VEC2(__VA_ARGS__)) })
#define LH_DYNAMIC_VEC3(...) ((lh_dynamic_type_t){ .as_vec3 = (LH_VEC3(__VA_ARGS__)) })
#define LH_DYNAMIC_VEC4(...) ((lh_dynamic_type_t){ .as_vec4 = (LH_VEC4(__VA_ARGS__)) })

#define LH_DYNAMIC_IVEC2(...) ((lh_dynamic_type_t){ .as_ivec2 = (LH_IVEC2(__VA_ARGS__)) })
#define LH_DYNAMIC_IVEC3(...) ((lh_dynamic_type_t){ .as_ivec3 = (LH_IVEC3(__VA_ARGS__)) })
#define LH_DYNAMIC_IVEC4(...) ((lh_dynamic_type_t){ .as_ivec4 = (LH_IVEC4(__VA_ARGS__)) })

#define LH_DYNAMIC_UVEC2(...) ((lh_dynamic_type_t){ .as_uvec2 = (LH_UVEC2(__VA_ARGS__)) })
#define LH_DYNAMIC_UVEC3(...) ((lh_dynamic_type_t){ .as_uvec3 = (LH_UVEC3(__VA_ARGS__)) })
#define LH_DYNAMIC_UVEC4(...) ((lh_dynamic_type_t){ .as_uvec4 = (LH_UVEC4(__VA_ARGS__)) })

#define LH_DYNAMIC_MAT2(...) ((lh_dynamic_type_t){ .as_mat2 = (LH_MAT2(__VA_ARGS__)) })
#define LH_DYNAMIC_MAT3(...) ((lh_dynamic_type_t){ .as_mat3 = (LH_MAT3(__VA_ARGS__)) })
#define LH_DYNAMIC_MAT4(...) ((lh_dynamic_type_t){ .as_mat4 = (LH_MAT4(__VA_ARGS__)) })

#define LH_DYNAMIC_RGB(...) ((lh_dynamic_type_t){ .as_rgb = (LH_RGB(__VA_ARGS__)) })
#define LH_DYNAMIC_RGBA(...) ((lh_dynamic_type_t){ .as_rgba = (LH_RGBA(__VA_ARGS__)) })
#define LH_DYNAMIC_RGB888(...) ((lh_dynamic_type_t){ .as_rgb888 = (LH_RGB888(__VA_ARGS__)) })
#define LH_DYNAMIC_RGBA8888(...) ((lh_dynamic_type_t){ .as_rgba8888 = (LH_RGBA8888(__VA_ARGS__)) })

typedef unsigned char  LHuchar;
typedef unsigned int   LHbool;
typedef unsigned short LHushort;
typedef unsigned int   LHuint;
typedef unsigned long  LHulong;

typedef int8_t   LHbyte;
typedef uint8_t  LHubyte;
typedef int16_t  LH16s;
typedef uint16_t LH16u;
typedef int32_t  LH32s;
typedef uint32_t LH32u;
typedef int64_t  LH64s;
typedef uint64_t LH64u;

typedef void (*lh_void_proc_t)();

#include "libsuicide.h"

typedef union lh_s_dynamic_type {
	void          *as_ptr;
	short          as_short;
	LHushort       as_ushort;
	int            as_int;
	LHuint         as_uint;
	long           as_long;
	LHulong        as_ulong;
	float          as_float;
	double         as_double;

	LHbyte         as_byte;
	LHubyte        as_ubyte;
	LH16s          as_i16;
	LH16u          as_u16;
	LH32s          as_i32;
	LH32u          as_u32;
	LH64s          as_i64;
	LH64u          as_u64;

	lh_vec2_t      as_vec2;
	lh_vec3_t      as_vec3;
	lh_vec4_t      as_vec4;

	lh_ivec2_t     as_ivec2;
	lh_ivec3_t     as_ivec3;
	lh_ivec4_t     as_ivec4;

	lh_uvec2_t     as_uvec2;
	lh_uvec3_t     as_uvec3;
	lh_uvec4_t     as_uvec4;

	lh_mat2_t      as_mat2;
	lh_mat3_t      as_mat3;
	lh_mat4_t      as_mat4;

	lh_rgb_t       as_rgb;
	lh_rgba_t      as_rgba;
	lh_rgb888_t    as_rgb888;
	lh_rgba8888_t  as_rgba8888;
} lh_dynamic_type_t;

#undef INSIDE_LIBHANG_TYPES_H

#endif /* LIBHANG_TYPES_H */
