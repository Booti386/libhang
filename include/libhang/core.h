/*
 * Copyright (C) 2014-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_CORE_H
#define LIBHANG_CORE_H 1

/**
 * @file
 * @brief 3D core engine
 */

#include "types.h"
#include "ll.h"
#include "ll_s.h"
#include "driver_os.h"
#include "driver_gl.h"
#include "images.h"

/* Declarations of stuctures with cyclic dependencies go here. */
struct lh_window;

/* Lights */
#define LH_LIGHT_DISABLED 0
#define LH_LIGHT_SUN      1
#define LH_LIGHT_POINT    2
#define LH_MAX_LIGHTS 8

/* lh_shader_program::drawing_type */
#define LH_DRAW_TRIANGLES   0
#define LH_DRAW_LINES       1

enum lh_type {
	LH_TYPE_FLOAT = 0,
	LH_TYPE_INT,
	LH_TYPE_UINT,
	LH_TYPE_VEC2,
	LH_TYPE_VEC3,
	LH_TYPE_VEC4,
	LH_TYPE_IVEC2,
	LH_TYPE_IVEC3,
	LH_TYPE_IVEC4,
	LH_TYPE_UVEC2,
	LH_TYPE_UVEC3,
	LH_TYPE_UVEC4,
	LH_TYPE_MAT2,
	LH_TYPE_MAT3,
	LH_TYPE_MAT4
};

#define LH_NB_ATTRS 4
#define LH_ATTR_POS         0
#define LH_ATTR_NORMAL      1
#define LH_ATTR_TEX_COORD   2
#define LH_ATTR_MODELMATRIX 3 /* 3-6 */

#define LH_NB_BUFS 5
#define LH_BUF_POS         0
#define LH_BUF_NORMAL      1
#define LH_BUF_TEX_COORD   2
#define LH_BUF_MODELMATRIX 3
#define LH_BUF_INDEX       4

#define LH_BUF_TO_ATTR(type) \
		((const LHuint[]) { \
			[LH_BUF_POS] = LH_ATTR_POS, \
			[LH_BUF_NORMAL] = LH_ATTR_NORMAL, \
			[LH_BUF_TEX_COORD] = LH_ATTR_TEX_COORD, \
			[LH_BUF_MODELMATRIX] = LH_ATTR_MODELMATRIX \
		}[type])

#define LH_BUF_ITEM_SIZE(type) \
		((const LHuint[]) { \
			[LH_BUF_POS] = 3, \
			[LH_BUF_NORMAL] = 3, \
			[LH_BUF_TEX_COORD] = 2, \
			[LH_BUF_MODELMATRIX] = 4, \
			[LH_BUF_INDEX] = 1 \
		}[type])

#define LH_BUF_POS_FLAG         LH_TO_FLAG(0)
#define LH_BUF_NORMAL_FLAG      LH_TO_FLAG(1)
#define LH_BUF_TEX_COORD_FLAG   LH_TO_FLAG(2)
#define LH_BUF_MODELMATRIX_FLAG LH_TO_FLAG(3)
#define LH_BUF_INDEX_FLAG       LH_TO_FLAG(4)
#define LH_TEX_FLAG             LH_TO_FLAG(5)
#define LH_REV_CULLING_FLAG     LH_TO_FLAG(6)

#define LH_TEX_COLOR_RGB888    0
#define LH_TEX_COLOR_RGBA8888  1
#define LH_TEX_COLOR_RGB       LH_TEX_COLOR_RGB888
#define LH_TEX_COLOR_RGBA      LH_TEX_COLOR_RGBA8888

#define LH_TEX_SMOOTH_NONE               0
#define LH_TEX_SMOOTH_LINEAR_FLAG        LH_TO_FLAG(0)
#define LH_TEX_SMOOTH_MIPMAP_LINEAR_FLAG LH_TO_FLAG(1)
#define LH_TEX_SMOOTH_BOTH_FLAG          (LH_TEX_SMOOTH_LINEAR_FLAG | LH_TEX_SMOOTH_MIPMAP_LINEAR_FLAG)

#define LH_TEX_INVAL ((LHuint)-1)

/* MSAA */

#define LH_MSAA_NONE 0
#define LH_MSAA_2x   2
#define LH_MSAA_4x   4
#define LH_MSAA_8x   8
#define LH_MSAA_16x  16
#define LH_MSAA_32x  32

enum lh_perf_counter {
	LH_PERF_COUNTER_FRAMES = 0,
};

struct lh_light {
	LHuint    type;
	lh_rgb_t  color;
	lh_vec3_t direction;
};

struct lh_camera {
	lh_vec3_t pos;
	lh_vec3_t real_pos;
	lh_quat_t angle;
};

struct lh_shader_src {
	char **src;
	LHuint nb_lines;
};

struct lh_custom_uniform {
	char *name;
	enum lh_type type;
	LHuint is_gbl : 1;
	int id; // private
};

struct lh_shader_program {
	struct lh_shader_src vertex_src;
	struct lh_shader_src fragment_src;
	LHuint program; // private

	LHuint drawing_type;

	int isInstancedUniform; // private
	int modelMatrixUniform; // private
	int viewMatrixUniform; // private
	int pMatrixUniform; // private

	int fogEnabledUniform; // private
	int fogMinDistanceUniform; // private
	int fogMaxDistanceUniform; // private
	int fogColorUniform; // private

	int absPositionUniform; // private
	int cameraPositionUniform; // private

	int ambientLightUniform; // private

	int isIlluminableUniform; // private
	int lightColorUniform; // private
	int lightDirectionUniform; // private

	LHuint nb_custom_uniforms;
	struct lh_custom_uniform *custom_uniforms;
};

struct lh_cmd_buffer {
	struct LinkedList cmds;
};

struct lh_buffer {
	LHuint id; // private
	LHuint nb_items;

	void *data;
};

struct lh_obj_class {
	LHulong id; // private
	struct LinkedList objs; // private
	char *name;

	LHbool is_visible;
	LHuint priv_flags; /* lh_obj_class_priv_flags */
	LHuint uses;
	LHuint overrides;
	struct lh_shader_program shader_program;
	struct lh_buffer buffers[LH_NB_BUFS];
	lh_dynamic_type_t *gbl_uniforms_data;

	LHuint nb_textures;
	LHuint *textures;
};

struct lh_obj {
	LHulong id; /* private */
	struct lh_obj_class *obj_class; /* private (set by object loader) */

	void (*delete_proc)(struct lh_window *, struct lh_obj *);

	void *private_data; /* private (can be used by optional obj_class-specific methods) */

	lh_vec3_t pos;
	lh_quat_t angle;
	LHbool is_visible;
	LHbool culling_enabled;
	LHbool reversed_culling;
	LHbool depth_test_enabled;
	LHbool is_illuminable;

	LHuint *custom_textures; /* Optional array containing each texture id to bind. */
	lh_dynamic_type_t *custom_uniforms_data; /* Optional array containing each value of custom uniform. */
	struct lh_buffer custom_buffers[LH_NB_BUFS]; /* Optional structure containing buffer replacements. */

	LHuint nb_clones;
	struct LinkedList clones;
	LHuint nb_allocated_clones;
	struct LinkedList gc_clones;
	struct lh_spinlock *clones_model_matrix_lock;
};

struct lh_obj_clone {
	LHulong id; /* private */
	struct lh_obj *obj; /* private (set by object loader) */

	lh_vec3_t pos;
	lh_quat_t angle;

	LHuint instanced_idx;
	LHbool is_visible;
};

struct lh_obj_group {
	LHulong id; /* private */
	struct LinkedList obj_groups;
	struct LinkedList objs;

	lh_vec3_t pos;
	lh_quat_t angle;

	LHbool is_visible;
};

struct lh_msaa_ctx {
	LHuint nb_samples;
	LHuint fbo;
	LHuint color_renderbuf;
	LHuint depth_renderbuf;
};

struct lh_core_ctx {
	struct lh_atomic_pointer graphic_cmds;
	struct SyncLinkedList immediate_cmds;
	struct SyncLinkedList deferred_cmds;

	LHbool is_obj_buffer_dirty;
	LHbool is_win_resized;
	struct LinkedList obj_classes;
	struct LinkedList textures;

	lh_distances_t render_distances;
	lh_rgba_t back_color;
	lh_rgb_t ambient_light;
	struct lh_light lights[LH_MAX_LIGHTS];
	struct lh_camera camera;
	LHbool fog_enabled;
	lh_distances_t fog_distances;
	lh_rgba_t fog_color;
	LHuint draw_vao;
	lh_mat4_t pMatrix;

	LHbool depth_test_enabled;
	LHbool culling_enabled;
	LHbool reversed_culling;

	struct lh_msaa_ctx msaa;

	struct lh_atomic_counter counter_frames;

	struct lh_skybox_ctx *skybox;
	struct lh_fb2d_ctx *fb2d;

	int internal_ev_handler_id;
};


#ifdef __cplusplus
extern "C" {
#endif

	extern lh_distances_t *lhGetRenderDistances(struct lh_window *window, lh_distances_t *dst_distances);
	extern void lhSetRenderDistances(struct lh_window *window, lh_distances_t *src_distances);
	extern lh_rgba_t *lhGetBackColor(struct lh_window *window, lh_rgba_t *dst_color);
	extern void lhSetBackColor(struct lh_window *window, lh_rgba_t *src_color);
	extern lh_rgb_t *lhGetAmbientLight(struct lh_window *window, lh_rgb_t *dst_color);
	extern void lhSetAmbientLight(struct lh_window *window, lh_rgb_t *src_color);
	extern void lhEnableLight(struct lh_window *window, LHuint num, LHuint type);
	extern void lhSetLightParameters(struct lh_window *window, LHuint num, lh_rgb_t *color, lh_vec3_t *direction);
	extern lh_vec3_t *lhGetCameraPos(struct lh_window *window, lh_vec3_t *dst_pos);
	extern void lhSetCameraPos(struct lh_window *window, lh_vec3_t *src_pos);
	extern lh_quat_t *lhGetCameraAngle(struct lh_window *window, lh_quat_t *dst_angle);
	extern void lhSetCameraAngle(struct lh_window *window, lh_quat_t *src_angle);
	extern void lhEnableFog(struct lh_window *window, LHbool enabled);
	extern void lhSetFogParameters(struct lh_window *window, lh_distances_t *fog_distances, lh_rgba_t *color);

	extern int lhBeginCmdBuffer(struct lh_window *window);
	extern struct lh_cmd_buffer *lhEndCmdBuffer(struct lh_window *window);
	extern int lhOptimizeCmdBuffer(struct lh_cmd_buffer *buffer);
	extern int lhExecuteCmdBuffer(struct lh_window *window, struct lh_cmd_buffer *buffer);

	extern LHuint lhTexLoadFromImage(struct lh_window *window, struct lh_image *image);
	extern LHuint lhTexLoadRGB(struct lh_window *window, lh_rgb888_t *data, LHuint width, LHuint height);
	extern LHuint lhTexLoadRGBA(struct lh_window *window, lh_rgba8888_t *data, LHuint width, LHuint height);
	extern void lhTexEnableSmoothing(struct lh_window *window, LHuint tex_id, LHuint mask);
	extern void lhTexUnload(struct lh_window *window, LHuint texture_id);

	extern struct lh_obj_class *lhObjClassCreate(struct lh_window *window, void *params);
	extern struct lh_obj *lhObjCreate(struct lh_window *window, void *params);
	extern struct lh_obj_clone *lhObjCloneCreate(struct lh_window *window, struct lh_obj *parent_obj, lh_vec3_t *pos, lh_quat_t *angle);
	extern struct lh_obj_group *lhObjGroupCreate(struct lh_window *window, LHuint nb_objs, struct lh_obj **objs);
	extern int lhObjClassDestroy(struct lh_window *window, struct lh_obj_class *obj_class);
	extern int lhObjDestroy(struct lh_window *window, struct lh_obj *obj);
	extern int lhObjCloneDestroy(struct lh_window *window, struct lh_obj_clone *obj_clone);
	extern int lhObjGroupDestroy(struct lh_window *window, struct lh_obj_group *obj_group);

	extern int lhObjClassReg(struct lh_window *window, struct lh_obj_class *obj_class);
	extern int lhObjReg(struct lh_window *window, struct lh_obj_class *obj_class, struct lh_obj *obj);
	extern int lhObjCloneReg(struct lh_window *window, struct lh_obj_clone *obj_clone, LHbool is_visible);
	extern int lhObjGroupReg(struct lh_window *window, struct lh_obj_group *obj_group);

	extern void lhObjSetPos(struct lh_obj *obj, lh_vec3_t *pos);
	extern void lhObjSetAngle(struct lh_obj *obj, lh_quat_t *angle);
	extern void lhObjSetParams(struct lh_obj *obj, lh_vec3_t *pos, lh_quat_t *angle);
	extern void lhObjSetVisible(struct lh_obj *obj, LHbool is_visible);
	extern void lhObjEnableCulling(struct lh_obj *obj, LHbool is_enabled);
	extern void lhObjSetReversedCulling(struct lh_obj *obj, LHbool is_reversed);
	extern void lhObjEnableDepthTest(struct lh_obj *obj, LHbool is_enabled);
	extern void lhObjSetIlluminable(struct lh_obj *obj, LHbool is_illuminable);
	extern void lhObjCloneSetParams(struct lh_window *window, struct lh_obj_clone *obj_clone, lh_vec3_t *pos, lh_quat_t *angle);
	extern void lhObjCloneSetVisible(struct lh_window *window, struct lh_obj_clone *obj_clone, LHbool is_visible);

	extern int lhObjClassUnreg(struct lh_window *window, struct lh_obj_class *obj_class);
	extern int lhObjUnreg(struct lh_window *window, struct lh_obj *obj);
	extern int lhObjCloneUnreg(struct lh_window *window, struct lh_obj_clone *obj_clone);
	extern int lhObjGroupUnreg(struct lh_window *window, struct lh_obj_group *obj_group);

	extern LHuint lhGetBufferDataLen(int type, LHuint nb_elems);
	extern void *lhAllocBufferData(int type, LHuint nb_elems);
	extern int lhSetupCustomBuffer(struct lh_window *window, struct lh_obj *obj, int type);
	extern int lhUpdateCustomBuffer(struct lh_window *window, struct lh_obj *obj, int type);
	extern int lhDeleteCustomBuffer(struct lh_window *window, struct lh_obj *obj, int type);

	extern int lhEnableMSAA(struct lh_window *window, LHuint nb_samples);

	extern long lhGetPerfCounter(struct lh_window *window, enum lh_perf_counter counter);
	extern void lhResetPerfCounter(struct lh_window *window, enum lh_perf_counter counter);

	/* Should be called each time an object is modified (else nothing will be drawn). */
	extern void lhDirtyObjBuffer(struct lh_window *window);
	extern void lhDirtyObjBufferWaitRender(struct lh_window *window);
	extern LHbool lhIsDirtyObjBuffer(struct lh_window *window);
	extern void lhDraw(struct lh_window *window);
	extern void lhRedraw(struct lh_window *window);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_CORE_H */
