/*
 * Copyright (C) 2016-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_BITWISE_H
#define LIBHANG_BITWISE_H 1

/**
 * @file
 * @brief Bitwise operations
 */

#include <float.h>
#include <math.h>

#include "types.h"

/*
 * Returns v if v is a power of two, else the next power of two.
 * Returns 0 if v > 0x80 (overflow).
 */
static inline uint8_t lh_next_pot_u8(uint8_t v) {
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v++;
	return v;
}

/*
 * Returns v if v is a power of two, else the next power of two.
 * Returns 0 if v > 0x8000 (overflow).
 */
static inline uint16_t lh_next_pot_u16(uint16_t v) {
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v++;
	return v;
}

/*
 * Returns v if v is a power of two, else the next power of two.
 * Returns 0 if v > 0x80000000 (overflow).
 */
static inline uint32_t lh_next_pot_u32(uint32_t v) {
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	return v;
}

/*
 * Returns v if v is a power of two, else the next power of two.
 * Returns 0 if v > 0x8000000000000000 (overflow).
 */
static inline uint64_t lh_next_pot_u64(uint64_t v) {
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v |= v >> 32;
	v++;
	return v;
}

static inline uint8_t *lh_u16_to_binle16(uint16_t src, uint8_t dst[2]) {
	dst[0] = src & 0xFF;
	dst[1] = (src >> 8) & 0xFF;
	return dst;
}

static inline uint16_t lh_binle16_to_u16(const uint8_t src[2]) {
	return (uint32_t)src[0]
		| ((uint32_t)src[1] << 8);
}

static inline uint8_t *lh_u16_to_binbe16(uint16_t src, uint8_t dst[2]) {
	dst[0] = (src >> 8) & 0xFF;
	dst[1] = src & 0xFF;
	return dst;
}

static inline uint16_t lh_binbe16_to_u16(const uint8_t src[4]) {
	return ((uint32_t)src[0] << 8)
		| (uint32_t)src[1];
}

static inline uint8_t *lh_u32_to_binle32(uint32_t src, uint8_t dst[4]) {
	dst[0] = src & 0xFF;
	dst[1] = (src >> 8) & 0xFF;
	dst[2] = (src >> 16) & 0xFF;
	dst[3] = (src >> 24) & 0xFF;
	return dst;
}

static inline uint32_t lh_binle32_to_u32(const uint8_t src[4]) {
	return (uint32_t)src[0]
		| ((uint32_t)src[1] << 8)
		| ((uint32_t)src[2] << 16)
		| ((uint32_t)src[3] << 24);
}

static inline uint8_t *lh_u32_to_binbe32(uint32_t src, uint8_t dst[4]) {
	dst[0] = (src >> 24) & 0xFF;
	dst[1] = (src >> 16) & 0xFF;
	dst[2] = (src >> 8) & 0xFF;
	dst[3] = src & 0xFF;
	return dst;
}

static inline uint32_t lh_binbe32_to_u32(const uint8_t src[4]) {
	return ((uint32_t)src[0] << 24)
		| ((uint32_t)src[1] << 16)
		| ((uint32_t)src[2] << 8)
		| (uint32_t)src[3];
}

static inline uint32_t lh_float_to_ieee754_u32(float v) {
	static const uint32_t val_inf = (0xFF << 23) | 0;
	static const uint32_t val_nan_quiet = (0xFF << 23) | 0x400000;
	static const uint32_t val_zero = (0 << 23) | 0;
	const uint32_t sign = signbit(v) ? (1 << 31) : 0;
	uint32_t mantissa;
	int exponent = 0;
	float f;

	if(isinf(v))
		return sign | val_inf;

	if(isnan(v))
		return sign | val_nan_quiet;

	if(fpclassify(v) == FP_ZERO)
		return sign | val_zero;

	v = fabsf(v);

	f = frexpf(v, &exponent);
	exponent += 127 - 1;

	if(!isnormal(v)) {
		/* Subnormal */
		f = ldexpf(f, exponent);
		exponent = 0;
	} else {
		/* Normal */
		f = f * 2.f - 1.f;
	}

	exponent = (exponent & 0xFF) << 23;
	mantissa = (uint32_t)(f * (float)0x800000) & 0x7FFFFF;

	return sign | exponent | mantissa;
}

static inline float lh_ieee754_u32_to_float(uint32_t v) {
	float ret;
	int exponent = 0;
	const uint32_t v_exp = (v >> 23) & 0xFF;
	const uint32_t v_abs = v & ((0xFF << 23) | 0x7FFFFF);
	const float sign = (v & (1 << 31)) ? -1.f : 1.f;

	if(v_abs == ((0xFF << 23) | 0))
		ret = INFINITY;
	else if(v_exp == 0xFF)
		ret = NAN;
	else if(v_abs == ((0 << 23) | 0))
		ret = 0.f;
	else {
		ret = (float)(v & 0x7FFFFF) / (float)0x800000;

		/* Normal */
		if(v_exp)
			ret += 1.f;

		ret = frexpf(ret, &exponent);

		/* Subnormal */
		if(!v_exp)
			exponent++;

		exponent += -127 + v_exp;
		ret = ldexpf(ret, exponent);
	}
	return copysignf(ret, sign);
}

#endif /* LIBHANG_BITWISE_H */
