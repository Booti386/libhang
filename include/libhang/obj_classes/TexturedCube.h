/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_TEXTUREDCUBE_H
#define LIBHANG_TEXTUREDCUBE_H 1

#include <libhang.h>

#ifdef __cplusplus
extern "C" {
#endif

	extern int lhRegTexturedCube(struct lh_window *window);
	extern void lhUnregTexturedCube(struct lh_window *window);
	extern struct lh_obj *lhNewTexturedCube(struct lh_window *window, float factor, lh_vec3_t *pos, lh_quat_t *angle, LHuint texture, LHbool is_visible);
	extern void lhTexturedCubeSetTexture(struct lh_obj *cube, LHuint texture);
	extern void lhDeleteTexturedCube(struct lh_window *window, struct lh_obj *cube);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_TEXTUREDCUBE_H */
