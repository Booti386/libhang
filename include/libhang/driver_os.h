/*
 * Copyright (C) 2016-2017 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_DRIVER_OS_H
#define LIBHANG_DRIVER_OS_H 1

/**
 * @file
 * @brief OS abstraction layer
 */

#include "types.h"

enum lh_driver_os_caps {
	LH_DRIVER_OS_CAP_ALIGNED_ALLOC = 1,
	LH_DRIVER_OS_CAP_DELAY         = (1 << 1),
	LH_DRIVER_OS_CAP_ATOMIC        = (1 << 2),
	LH_DRIVER_OS_CAP_THREAD        = (1 << 3),
	LH_DRIVER_OS_CAP_SPINLOCK      = (1 << 4),
	LH_DRIVER_OS_CAP_MUTEX         = (1 << 5),
	LH_DRIVER_OS_CAP_COND          = (1 << 6)
};

struct lh_atomic_counter {
	char reserved[16];
};

struct lh_atomic_pointer {
	char reserved[16];
};

struct lh_thread {
	LHuint id;
};

struct lh_spinlock {
	LHuint id;
};

struct lh_mutex {
	LHuint id;
};

struct lh_cond {
	LHuint id;
};

#ifdef __cplusplus
extern "C" {
#endif

	extern int lhInit(void);
	extern void lhDeinit(void);
	extern enum lh_driver_os_caps lhGetOSCaps(void);
	extern void *lhMallocAligned(size_t alignment, size_t size);
	extern void lhFree(void *ptr);
	extern void lhDelay(LHuint msec);
	extern void lhDelayUSec(LHuint usec);
	extern long lhAtomicCounterGet(struct lh_atomic_counter *cnt);
	extern long lhAtomicCounterSet(struct lh_atomic_counter *cnt, long val);
	extern long lhAtomicCounterInc(struct lh_atomic_counter *cnt);
	extern long lhAtomicCounterDec(struct lh_atomic_counter *cnt);
	extern void *lhAtomicPointerGet(struct lh_atomic_pointer *pointer);
	extern void *lhAtomicPointerSet(struct lh_atomic_pointer *pointer, void *val);
	extern struct lh_thread *lhThreadCreate(int (*func)(void *), void *params);
	extern int lhThreadWait(struct lh_thread *thread, int *status);
	extern LHbool lhThreadIsRunning(struct lh_thread *thread);
	extern int lhThreadRelease(struct lh_thread *thread);
	extern int lhThreadDestroy(struct lh_thread *thread);

	extern struct lh_spinlock *lhSpinCreate(void);
	extern int lhSpinLock(struct lh_spinlock *);
	extern int lhSpinUnlock(struct lh_spinlock *);
	extern int lhSpinDestroy(struct lh_spinlock *);

	extern struct lh_mutex *lhMutexCreate(void);
	extern int lhMutexLock(struct lh_mutex *);
	extern int lhMutexUnlock(struct lh_mutex *);
	extern int lhMutexDestroy(struct lh_mutex *);

	extern struct lh_cond *lhCondCreate(void);
	extern int lhCondWait(struct lh_cond *cond);
	extern int lhCondWakeSingle(struct lh_cond *cond);
	extern int lhCondWakeAll(struct lh_cond *cond);
	extern int lhCondDestroy(struct lh_cond *cond);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_DRIVER_OS_H */
